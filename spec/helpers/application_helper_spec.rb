require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "class" do
    it "active_class" do
      expect(helper.active_class(nil, nil)).not_to eq(nil)
    end
  end

  describe "url" do
   

    it "get_url_reload" do
      
      expect(helper.get_url_reload).not_to eq(nil)
    end
  end

  describe "other" do
    

    it "boolean_to_text" do
      expect(helper.boolean_to_text(true)).not_to eq(nil)
    end

    it "get min errors" do
      expect(helper.get_min_errors("position", "appointments")).not_to eq(nil)

      expect(helper.get_min_errors("appointments.position", "corporation")).not_to eq(nil)
    end

    it "undescore" do
      expect(helper.undescore(nil)).to eq("")
      expect(helper.undescore("JobLevel")).to eq(:job_level)
    end
    
  end

end

