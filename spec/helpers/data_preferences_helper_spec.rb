require 'rails_helper'

RSpec.describe DataPreferencesHelper, type: :helper do
  describe "preference" do
    it "get_type_preference nil" do
      expect(helper.get_type_preference).to eq("")
    end

    it "get_type_preference" do
      expect(helper.get_type_preference(0)).not_to eq("")
    end

    it "type_preference" do
      expect(helper.type_preference(0)).not_to eq(nil)
      expect(helper.type_preference(1)).not_to eq(nil)
      expect(helper.type_preference(2)).not_to eq(nil)
      expect(helper.type_preference(5)).not_to eq(nil)
      expect(helper.type_preference).not_to eq(nil)
    end
  end
end
