require 'rails_helper'

RSpec.describe SocialHelper, type: :helper do
  describe "text" do
    it "twitter_url" do
      expect(helper.twitter_url("XXXX")).not_to eq(nil)
    end

    it "facebook_url" do
      expect(helper.facebook_url("XXXX")).not_to eq(nil)
    end
  end

end

