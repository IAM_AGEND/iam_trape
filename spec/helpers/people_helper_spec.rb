require 'rails_helper'

RSpec.describe PeopleHelper, type: :helper do
  describe "profile" do
    it "profile_picture" do
      job=Person.find(1)
      job.profile = FactoryGirl.create(:profile)
      expect(helper.profile_picture(nil)).not_to eq(nil)
      expect(helper.profile_picture(Person.find(1))).not_to eq(nil)
      expect(helper.profile_picture(job.profile)).not_to eq(nil)

      job.profile.portrait=File.new(Rails.root + 'spec/fixtures/images/pp.png')
      expect(helper.profile_picture(job.profile)).not_to eq(nil)
    end
  end

  describe "field" do
    

    it "breadcrumb_person_hash_title_link" do
      expect(helper.breadcrumb_person_hash_title_link(JobLevel.find(1))).not_to eq(nil)
    end

    it "breadcrumb_corporation_hash_title_link" do
      expect(helper.breadcrumb_corporation_hash_title_link(Corporation.find(1))).not_to eq(nil)
    end

    it "breadcrumb_person_hash_pre_title_link" do
      job = JobLevel.find(1)
      
      expect(helper.breadcrumb_person_hash_pre_title_link(job,Corporation.find(1))).not_to eq(nil)
      expect(helper.breadcrumb_person_hash_pre_title_link(job,Corporation.find(2))).not_to eq(nil)
      expect(helper.breadcrumb_person_hash_pre_title_link(job,nil)).not_to eq(nil)
      job.person_type = "director"
      job.appointment = FactoryGirl.create(:appointment)
      expect(helper.breadcrumb_person_hash_pre_title_link(job,nil)).not_to eq(nil)
      job.appointment = FactoryGirl.create(:appointment)
      job.appointment.end_date = Time.zone.now
      expect(helper.breadcrumb_person_hash_pre_title_link(job,nil)).not_to eq(nil)

    end

    it "load_menu" do
      expect(helper.load_menu(JobLevel.find(1),Corporation.find(1))).not_to eq(nil)
    end

    it "get_currency_value(value)" do
      expect(helper.get_currency_value(50)).to eq("50,00 €")
    end
  end
end
