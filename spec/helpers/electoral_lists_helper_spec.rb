require 'rails_helper'

RSpec.describe ElectoralListsHelper, type: :helper do
  describe "logo" do
    it "logo_picture" do
      expect(helper.logo_picture(nil)).not_to eq(nil)
      expect(helper.logo_picture(ElectoralList.find(1))).not_to eq(nil)
    end

    it "logo_picture_public" do
      expect(helper.logo_picture_public(nil)).to eq(nil)
      expect(helper.logo_picture_public(ElectoralList.find(1))).to eq(nil)
    end

    it "logo_picture nil" do
      electoral=ElectoralList.find(1)
      electoral.logo = File.new(Rails.root + 'spec/fixtures/images/pp.png')

      expect(helper.logo_picture(electoral)).not_to eq(nil)
    end

    it "logo_picture_public nil" do
      electoral=ElectoralList.find(1)
      electoral.logo = File.new(Rails.root + 'spec/fixtures/images/pp.png')

      expect(helper.logo_picture_public(electoral)).not_to eq(nil)
    end
  end
end
