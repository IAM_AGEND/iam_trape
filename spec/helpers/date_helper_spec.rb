require 'rails_helper'

RSpec.describe DateHelper, type: :helper do
  describe "date" do
    it "format_date" do
      expect(helper.format_date(Time.zone.now)).not_to eq(nil)
    end

    it "iso_date" do
      expect(helper.iso_date(Time.zone.now)).not_to eq(nil)
    end

    it "parse_date" do
      expect(helper.parse_date("19/09/2015")).not_to eq(nil)
    end
  end

end

