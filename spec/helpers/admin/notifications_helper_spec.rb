require 'rails_helper'

RSpec.describe Admin::NotificationsHelper, type: :helper do
  
  describe "functions" do
    it "getNumberNotifications" do
      expect(helper.getNumberNotifications).not_to eq(nil)
    end

    it "getNumberNotificationsPending" do
        expect(helper.getNumberNotificationsPending('Person')).not_to eq(nil)
      end
  end

end