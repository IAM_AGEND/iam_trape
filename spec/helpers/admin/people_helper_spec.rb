require 'rails_helper'

RSpec.describe Admin::PeopleHelper, type: :helper do
  describe "headers" do
    it "headers_files" do
      expect(helper.headers_files).not_to eq(nil)
    end

    it "headers_files_export" do
      expect(helper.headers_files_export).not_to eq(nil)
    end

    it "headers_files_not" do
      expect(helper.headers_files_not).not_to eq(nil)
    end
  end

  describe "validates" do
    it "validates_filter_councillor" do
      expect(helper.validates_filter_councillor(CouncillorsCorporation.find(1),{"status" => "xx"})).to eq(false)
      expect(helper.validates_filter_councillor(CouncillorsCorporation.find(1),{"corporation" => 0})).to eq(false)
      expect(helper.validates_filter_councillor(CouncillorsCorporation.find(1),{"area" => "xx"})).to eq(false)
      
      councillor= CouncillorsCorporation.find(1)
      councillor.appointment.end_date = Time.zone.now
      expect(helper.validates_filter_councillor(councillor,{"status" => I18n.t("people.status.active")})).to eq(false)

      expect(helper.validates_filter_councillor(CouncillorsCorporation.find(1),{"status" => I18n.t("people.status.active")})).to eq(true)
    end
  end

  describe "streaming" do
    it "stream_query_rows" do
      expect(helper.stream_query_rows(nil)).not_to eq(nil)
    end
  end

  describe "empty" do
    it "activities_declarations_tab_name" do
      declaration = FactoryGirl.create(:activities_declaration)
      expect(helper.activities_declarations_tab_name(declaration)).not_to eq(nil)

      declaration.temporality = nil
      expect(helper.activities_declarations_tab_name(declaration)).not_to eq(nil)
    end

    it "assets_declarations_tab_name" do
      declaration = FactoryGirl.create(:assets_declaration)
      expect(helper.assets_declarations_tab_name(declaration)).not_to eq(nil)
      declaration.temporality = nil
      expect(helper.assets_declarations_tab_name(declaration)).not_to eq(nil)
    end
  end

  describe "get collections" do
    it "get_collection_mounths" do
      expect(helper.get_collection_mounths).not_to eq(nil)
    end

    it "get_real_estate_kind_collect" do
      expect(helper.get_real_estate_kind_collect).not_to eq(nil)
    end

    it "get_real_estate_straight_type_collect" do
      expect(helper.get_real_estate_straight_type_collect).not_to eq(nil)
    end

    it "get_real_estate_adquisition_title_collect" do
      expect(helper.get_real_estate_adquisition_title_collect).not_to eq(nil)
    end

    it "get_account_kind_collect" do
      expect(helper.get_account_kind_collect).not_to eq(nil)
    end

    it "get_other_types_collect" do
      expect(helper.get_other_types_collect).not_to eq(nil)
    end

    it "get_sex_type_collect" do
      expect(helper.get_sex_type_collect).not_to eq(nil)
    end

    it "element_display?" do
      expect(helper.element_display?).to eq("")
      expect(helper.element_display?([])).to eq("")
      expect(helper.element_display?(["xx","xx"])).not_to eq("")
    end

  end
end

