require 'rails_helper'

RSpec.describe TextHelper, type: :helper do
  describe "text" do
    it "format_free_text" do
      expect(helper.format_free_text("XXXX")).not_to eq(nil)
    end

    it "upper_case" do
      expect(helper.upper_case("áxxx")).to eq("ÁXXX")
    end
  end

end

