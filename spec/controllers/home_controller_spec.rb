require 'rails_helper'

RSpec.describe HomeController do

  describe 'Home' do
    it 'should include a welcome' do
      controller = HomeController.new 
      expect(controller.send(:full_feature?)).to eq(true)
      get :welcome
      expect(response).to be_ok
    end

    it 'should include a index' do
      get :index
      expect(response).to be_ok
    end
  end
end
