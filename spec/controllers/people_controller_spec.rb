require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(302)
    end
  end

  describe "GET #councillors" do
    it "returns http success" do
      get :councillors
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #directors" do
    it "returns http success" do
      get :directors
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #temporary_workers" do
    it "returns http success" do
      get :temporary_workers
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #public_workers" do
    it "returns http success" do
      get :public_workers
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #spokespeople" do
    it "returns http success" do
      get :spokespeople
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "assigns the requested job_level as @job_level" do
      job_level = FactoryGirl.create(:job_level)
      activity=FactoryGirl.create(:activities_declaration)
      activity.person_level = job_level
      job_level.activities_declarations << activity
      get :show, {:id => job_level.to_param}
      expect(response).to be_ok
    end
  end

  it 'should private methods' do
    controller = PeopleController.new 
    expect(controller.send(:full_feature?)).to eq(true)
  end 

end
