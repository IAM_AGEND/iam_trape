require 'rails_helper'

RSpec.describe Admin::ProfileUploadsController do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "assigns the requested profile_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      get :show, {:id => profile_upload.to_param}
      expect(response).to be_ok
    end
  end

  describe "POST #create" do
    it "assigns the requested profile_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      post :create, {:id => profile_upload.to_param, :profile_upload => profile_upload.attributes}
      expect(assigns(:profile_upload)).not_to eq(profile_upload)
    end
    it "assigns the requested profile_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      post :create, {:id => profile_upload.to_param, :profile_upload => profile_upload.attributes}
      expect(assigns(:profile_upload)).not_to eq(profile_upload)
    end

    it "assigns the requested profile_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      post :create, {:profile_upload => {file: fixture_file_upload('spec/fixtures/files/profiles.xls', 'text/xls'), corporation: 1}}
      expect(assigns(:profile_upload)).not_to eq(profile_upload)
    end
  end

  describe "GET #show_mailer" do
    it "assigns the requested profiles_email_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      profiles_email_upload = ProfilesEmailLog.new
      profiles_email_upload.administrator = profile_upload.administrator
      profiles_email_upload.log_id = profile_upload.id
      profiles_email_upload.save
      get :show_mailer, {:id => profiles_email_upload.to_param}
      expect(response).to be_ok
    end
  end

  describe "GET #send_mailer" do
    it "assigns the requested profiles_email_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      get :send_mailer, {:id => profile_upload.to_param, :emails => "[\"xx@xx.es\"]"}
      expect(response).not_to eq(nil)
    end

    it "assigns the requested profiles_email_upload as @profile_upload" do
      profile_upload = FactoryGirl.create(:profile_upload)
      get :send_mailer, {:id => profile_upload.to_param, :emails => nil}
      expect(response).not_to eq(nil)
    end
  end
end