require 'rails_helper'

RSpec.describe Admin::PreferencesController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end