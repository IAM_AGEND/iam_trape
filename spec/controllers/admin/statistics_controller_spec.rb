require 'rails_helper'

RSpec.describe Admin::StatisticsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success profiles" do
      get :update_data
      expect(response).to have_http_status(302)
      get :profiles, {section: "profiles"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success directors" do
      get :update_data
      expect(response).to have_http_status(302)
      get :directors, {section: "directors"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params corporation" do
      get :update_data
      expect(response).to have_http_status(302)
      get :councillors, {section: "corporation"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params profile CSV" do
      get :update_data
      expect(response).to have_http_status(302)
      get :profiles, {section: "profiles", person: 'councillor', part: :all, format: "csv"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params profile XLS" do
      get :update_data
      expect(response).to have_http_status(302)
      get :profiles, {section: "profiles", person: 'councillor', part: :all, format: "xls"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params corporation CSV" do
      get :update_data
      expect(response).to have_http_status(302)
      get :councillors, {section: "corporation", active: true, declaration: "AssetsDeclaration", temporality: 'Inicial', corporation: 1, part: :all, format: "csv"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params corporation XLS" do
      get :update_data
      expect(response).to have_http_status(302)
      get :councillors, {section: "corporation", active: false, declaration: "AssetsDeclaration", temporality: 'Inicial', corporation: 1, part: :all, format: "xls"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params directors CSV" do
      get :update_data
      expect(response).to have_http_status(302)
      get :directors, {section: "directors", active: true, declaration: "AssetsDeclaration", temporality: 'Inicial', part: :all, format: "csv"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params directors XLS" do
      get :update_data
      expect(response).to have_http_status(302)
      get :directors, {section: "directors", active: false, declaration: "AssetsDeclaration", temporality: 'Inicial', part: :all, format: "xls"}
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #update_data" do
    it "returns http success" do
      get :update_data
      expect(response).to have_http_status(302)
    end
  end
end
