require 'rails_helper'

RSpec.describe Admin::DashboardController do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe 'Dasboard' do
    it 'should include a index' do
      get :index
      expect(response).to be_ok
    end
  end
end
