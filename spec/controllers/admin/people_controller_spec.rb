require 'rails_helper'

RSpec.describe Admin::PeopleController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index, {name: "xxxx"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {name: "xxxx", format: :csv}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with session params" do
      get :index,nil, search_params: {name:"xxx"}
      expect(response).to have_http_status(:success)
    end

    it "returns http success with search params" do
      get :index, {name: "xxxx", start_date: Time.zone.now, end_date: Time.zone.now + 3.days, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", start_date: Time.zone.now, status: I18n.t("people.status.inactive"), area: "ca"}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", end_date: Time.zone.now, status: I18n.t("people.status.inactive"), area: "ca"}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", start_incorporate_date: Time.zone.now, end_incorporate_date: Time.zone.now + 3.days, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", end_incorporate_date: Time.zone.now + 3.days, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", start_incorporate_date: Time.zone.now, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", start_cessation_date: Time.zone.now, end_cessation_date: Time.zone.now + 3.days, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", end_cessation_date: Time.zone.now + 3.days, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)

      get :index, {name: "xxxx", start_cessation_date: Time.zone.now, status: I18n.t("people.status.active")}
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end


  describe "GET #edit" do
    it "assigns the requested person as @person" do
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "GET #show_hide" do
    it "assigns the requested person as @person" do
      get :show_hide, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "GET #audits" do
    it "assigns the requested person as @person" do
      get :audits, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "GET #hide" do
    it "assigns the requested person as @person" do
      post :hide, {:person_id => 1,hidden_at: Time.zone.now}
      expect(response).not_to eq(nil)
    end

    it "assigns the requested person as @person with public data" do
      post :hide, {:public_data => true,:person_id => 1,hidden_at: Time.zone.now}
      expect(response).not_to eq(nil)
    end
  end

  describe "GET #unhide" do
    it "assigns the requested person as @person" do
      post :unhide, {:person_id => 1, unhidden_at: Time.zone.now}
      expect(response).not_to eq(nil)
    end

    it "assigns the requested person as @person with public data" do
      post :unhide, {:public_data => true, :person_id => 1, unhidden_at: Time.zone.now}
      expect(response).not_to eq(nil)
    end
  end

  describe "GET #show_unhide" do
    it "assigns the requested person as @person" do
      get :show_unhide, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "GET #portrait_delete" do
    it "assigns the requested person as @person" do

      job = JobLevel.find(1)
      person = job.person
      person.profile = FactoryGirl.create(:profile)
      job.save
      get :portrait_delete, {:id => person.profile, :job_id => job.slug}
      expect(response).not_to eq(nil)
    end
  end

  describe "PUT #update" do
    it "assigns the requested person as @person" do
      person = Person.find(1)
      put :update, {:id => person.to_param, :person => person.attributes}
      expect(assigns(:person)).to eq(person)
    end

    it "assigns the requested person as @person with public data" do
      person = Person.find(1)
      put :update, {public_data: true, :id => person.to_param, :person => person.attributes}
      expect(assigns(:person)).to eq(person)
    end

    it "assigns the requested person as @person" do
      person = Person.find(1)
      person.name = nil
      put :update, {:id => person.to_param, :person => person.attributes}
      expect(assigns(:person)).to eq(person)
    end
  end

  describe "POST #create" do
    it "assigns the requested person as @person" do
      person = FactoryGirl.create(:person)
      person.id= 20000000
      post :create, {:id => person.to_param, :person => person.attributes}
      expect(assigns(:person)).to eq(person)
    end
    it "assigns the requested person as @person" do
      person = FactoryGirl.create(:person)
      person.name = nil
      post :create, {:id => person.to_param, :person => person.attributes}
      expect(assigns(:person)).to eq(person)
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested person" do
      person = Person.find(1)
      expect {
        delete :destroy, {:id => person.to_param}
      }.to change(Person, :count).by(-1)
    end

    it "destroys the requested person" do
      person = Person.find(1)
      
      expect {
        delete :destroy, {:id => person.to_param}
      }.to change(Person, :count).by(-1)
    end
  end

 

end
