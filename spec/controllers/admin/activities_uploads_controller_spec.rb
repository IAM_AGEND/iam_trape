require 'rails_helper'

RSpec.describe Admin::ActivitiesUploadsController do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "assigns the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      get :show, {:id => activities_upload.to_param}
      expect(response).to be_ok
    end
  end

  describe "POST #create" do
    it "assigns the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      post :create, {:id => activities_upload.to_param, :activities_upload => activities_upload.attributes}
      expect(assigns(:activities_upload)).not_to eq(activities_upload)
    end
    it "assigns the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      post :create, {:id => activities_upload.to_param, :activities_upload => activities_upload.attributes}
      expect(assigns(:activities_upload)).not_to eq(activities_upload)
    end

    it "assigns the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      post :create, {:activities_upload => {file: fixture_file_upload('spec/fixtures/files/profiles.xls', 'text/xls'), corporation: 1, period: "2015", job: "councillor"}}
      expect(assigns(:activities_upload)).not_to eq(activities_upload)
    end
  end

  describe "GET #show_mailer" do
    it "assigns the requested activities_email_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      activities_email_upload = ActivitiesEmailLog.new
      activities_email_upload.administrator = activities_upload.administrator
      activities_email_upload.log_id = activities_upload.id
      activities_email_upload.save
      get :show_mailer, {:id => activities_email_upload.to_param}
      expect(response).to be_ok
    end
  end

  describe "GET #export" do
    it "assigns the requested export_data as @export_data" do
      get :export, {export_data: {export_type: "simple",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).not_to eq(nil)
    end

    it "assigns the requested export_data as @export_data" do
      get :export, {export_data: {export_type: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).not_to eq(nil)
    end

    it "with activities declarations" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      activity = FactoryGirl.create(:activities_declaration)
      activity_anual = ActivitiesDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      activity.person_level = job
      job.activities_declarations << activity
      job.activities_declarations << activity_anual
      job.save!
      get :export, {export_data: {export_type: "complete",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :export, {export_data: {export_type: "complete",period: "2015", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)

    end

    it "with assets declarations" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      asset = FactoryGirl.create(:assets_declaration)
      asset_anual = AssetsDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      asset.person_level = job
      job.assets_declarations << asset
      job.assets_declarations << asset_anual
      job.save!
      get :export, {export_data: {export_type: "complete",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :export, {export_data: {export_type: "complete",period: "2015", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "with activities declarations single" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      activity = FactoryGirl.create(:activities_declaration)
      activity_anual = ActivitiesDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      activity.person_level = job
      job.activities_declarations << activity
      job.activities_declarations << activity_anual
      job.save!
      get :export, {export_data: {export_type: "single",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :export, {export_data: {export_type: "single",period: "2015", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "with assets declarations single" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      asset = FactoryGirl.create(:assets_declaration)
      asset_anual = AssetsDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      asset.person_level = job
      job.assets_declarations << asset
      job.assets_declarations << asset_anual
      job.save!
      get :export, {export_data: {export_type: "single",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :export, {export_data: {export_type: "single",period: "2015", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "with profile" do
      job = JobLevel.find(1)
      job.person_type ="director"
      job.person.profile = FactoryGirl.create(:profile)

      job.save!
      get :export, {export_data: {export_type: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "with profile single" do
      job = JobLevel.find(1)
      job.person_type ="director"
      job.person.profile = FactoryGirl.create(:profile)

      job.save!
      get :export, {export_data: {export_type: "single",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "without data params" do
      get :export, {export_data: {}}
      expect(response).not_to eq(nil)

      get :export, {export_data: {export_type: "complete", type_people: "councillor"}}
      expect(response).not_to eq(nil)

      get :export, {export_data: {export_type: "single"}}
      expect(response).not_to eq(nil)
    end
  end

  describe "GET #send_mailer" do
    it "assigns the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      get :send_mailer, {:id => activities_upload.to_param, :emails => "[\"xx@xx.es\"]"}
      expect(response).not_to eq(nil)
    end

    it "assigns nil emails the requested activities_upload as @activities_upload" do
      activities_upload = FactoryGirl.create(:activities_upload)
      get :send_mailer, {:id => activities_upload.to_param, :emails => nil}
      expect(response).not_to eq(nil)
    end
  end
end