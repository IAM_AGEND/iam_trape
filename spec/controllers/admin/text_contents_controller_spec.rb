require 'rails_helper'

RSpec.describe Admin::TextContentsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end

  describe "GET #edit" do
    it "assigns the requested text_content as @text_content" do
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "PUT #update" do
    it "assigns the requested text_content as @text_content" do
      text_content = TextContent.find(1)
      put :update, {:id => text_content.to_param, :text_content => text_content.attributes}
      expect(assigns(:text_content)).to eq(text_content)
    end
  end
end
