require 'rails_helper'

RSpec.describe Admin::DataExportsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #export" do
    it "returns http success" do
      get :index, {format: :xls, url: "assets/BienesAnual_Directivos_2016.xls", type: "directors"}
      expect(response).to have_http_status(302)
     
    end

    it "returns http success" do
      get :index, {format: :xls, url: "assets/BienesAnual_Directivos_2016.xls", type: "councillors", corporation: 1}
      expect(response).to have_http_status(302)
    end
  end
end