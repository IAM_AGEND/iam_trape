require 'rails_helper'

RSpec.describe Admin::ManageEmailsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end

  describe "GET #edit" do
    it "assigns the requested manage email as @manage email" do
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "PUT #update" do
    it "assigns the requested manage email as @manage_email" do
      manage_email = ManageEmail.find(1)
      put :update, {:id => manage_email.to_param, :manage_email => manage_email.attributes}
      expect(assigns(:manage_email)).to eq(manage_email)
    end
  end
end
