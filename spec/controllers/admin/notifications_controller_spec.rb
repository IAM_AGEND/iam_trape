require 'rails_helper'

RSpec.describe Admin::NotificationsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success profiles" do
      get :profiles
      expect(response).to have_http_status(:success)
    end

    it "returns http success assets" do
      get :assets
      expect(response).to have_http_status(:success)
    end

    it "returns http success activities" do
      get :activities
      expect(response).to have_http_status(:success)
    end

    it "returns http success profiles_historic" do
      get :profiles_historic
      expect(response).to have_http_status(:success)
    end

    it "returns http success assets_historic" do
      get :assets_historic
      expect(response).to have_http_status(:success)
    end

    it "returns http success activities_historic" do
      get :activities_historic
      expect(response).to have_http_status(:success)
    end

    it "returns http success person" do
      get :person
      expect(response).to have_http_status(:success)
    end

    it "returns http success person_historic" do
      get :person_historic
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #update_data" do
    it "returns http success" do
      get :update_data
      expect(response).to have_http_status(302)
    end
  end

  describe "GET #reject" do
    it "returns http success" do
      get :reject, {id: Notification.where(type_data: 'Person').first.id}
      expect(response).to have_http_status(302)
    end
  end

  describe "GET #permit" do
   
    it "returns http success 4" do
      get :permit, {id: 4}
      expect(response).to have_http_status(302)
    end
    it "returns http success 5" do
      get :permit, {id: 5}
      expect(response).to have_http_status(302)
    end
    it "returns http success 6" do
      get :permit, {id: 6}
      expect(response).to have_http_status(302)
    end
    it "returns http success 7" do
      get :permit, {id: 7}
      expect(response).to have_http_status(302)
    end
    it "returns http success 8" do
      get :permit, {id: 8}
      expect(response).to have_http_status(302)
    end
    it "returns http success 9" do
      get :permit, {id: 9}
      expect(response).to have_http_status(302)
    end
    it "returns http success 10" do
      get :permit, {id: 10}
      expect(response).to have_http_status(302)
    end
    it "returns http success 11" do
      get :permit, {id: 11}
      expect(response).to have_http_status(302)
    end
    it "returns http success 12" do
      get :permit, {id: 12}
      expect(response).to have_http_status(302)
    end
    it "returns http success 13" do
      get :permit, {id: 13}
      expect(response).to have_http_status(302)
    end
    it "returns http success 14" do
      get :permit, {id: 14}
      expect(response).to have_http_status(302)
    end
    it "returns http success 15" do
      get :permit, {id: 15}
      expect(response).to have_http_status(302)
    end
    it "returns http success 16" do
      get :permit, {id: 16}
      expect(response).to have_http_status(302)
    end
    it "returns http success 17" do
      get :permit, {id: 17}
      expect(response).to have_http_status(302)
    end
    it "returns http success 18" do
      get :permit, {id: 18}
      expect(response).to have_http_status(302)
    end
    it "returns http success 19" do
      get :permit, {id: 19}
      expect(response).to have_http_status(302)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      Notification.all.each do |n|
        get :show, {id: n.id}
        expect(response).to have_http_status(:success)
      end
    end
  end
end