require 'rails_helper'

RSpec.describe Admin::DeleteDataController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end
  describe "GET #index" do
    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "councillor"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "councillor"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).to have_http_status(:success)
    end

    it "returns http success" do
      get :index, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "assigns the requested delete_data as @delete_data" do
      delete_data = FactoryGirl.create(:delete_data)
      get :show, {:id => delete_data.to_param}
      expect(response).to be_ok
    end
  end

  describe "GET #delete_information" do
    it "assigns the requested delete_data as @delete_data" do
      get :delete_information, {delete_data: {delete: "simple",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).not_to eq(nil)
    end

    it "assigns the requested delete_data as @delete_data" do
      get :delete_information, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "councillor"}}
      expect(response).not_to eq(nil)
    end

    it "with activities declarations" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      activity = FactoryGirl.create(:activities_declaration)
      activity_anual = ActivitiesDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      activity.person_level = job
      job.activities_declarations << activity
      job.activities_declarations << activity_anual
      job.save
      get :delete_information, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :delete_information, {delete_data: {delete: "complete",period: "2015", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)

    end

    it "with assets declarations" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      asset = FactoryGirl.create(:assets_declaration)
      asset_anual = AssetsDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      asset.person_level = job
      job.assets_declarations << asset
      job.assets_declarations << asset_anual
      job.save
      get :delete_information, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :delete_information, {delete_data: {delete: "complete",period: "2015", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)

    end
    it "with activities declarations single" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      activity = FactoryGirl.create(:activities_declaration)
      activity_anual = ActivitiesDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      activity.person_level = job
      job.activities_declarations << activity
      job.activities_declarations << activity_anual
      job.save
      get :delete_information, {delete_data: {delete: "single",period: "Inicial", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :delete_information, {delete_data: {delete: "single",period: "2015", corporation: 1,type_information: "activities_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)

    end

    it "with assets declarations single" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      asset = FactoryGirl.create(:assets_declaration)
      asset_anual = AssetsDeclaration.create!(
        temporality:  Temporality.find_by(name: "Anual"),
        declaration_date: Time.zone.now,
        year_period: 2015, person_level: job )
      asset.person_level = job
      job.assets_declarations << asset
      job.assets_declarations << asset_anual
      job.save
      get :delete_information, {delete_data: {delete: "single",period: "Inicial", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
      get :delete_information, {delete_data: {delete: "single",period: "2015", corporation: 1,type_information: "assets_declaration", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)

    end

    it "with profile" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      job.person.profile = FactoryGirl.create(:profile)

      job.save
      get :delete_information, {delete_data: {delete: "complete",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "with profile single" do
      job = JobLevel.find(1)
    
      job.person_type ="director"
      job.person.profile = FactoryGirl.create(:profile)

      job.save
      get :delete_information, {delete_data: {delete: "single",period: "Inicial", corporation: 1,type_information: "profile", people: 1, type_people: "director"}}
      expect(response).not_to eq(nil)
    end

    it "without data params" do
      get :delete_information, {delete_data: {}}
      expect(response).not_to eq(nil)

      get :delete_information, {delete_data: {delete: "complete", type_people: "councillor"}}
      expect(response).not_to eq(nil)

      get :delete_information, {delete_data: {delete: "single"}}
      expect(response).not_to eq(nil)
    end
  end

  it 'should private methods' do
    controller = Admin::DeleteDataController.new
    @periods = {}  
    job = JobLevel.find(1)
    expect(controller.send(:generate_period,job, :activities_declarations)).to eq([])
    expect(controller.send(:generate_period,job, :assets_declarations)).to eq([])
    job.person_type ="director"
    activity = FactoryGirl.create(:activities_declaration)
    asset = FactoryGirl.create(:assets_declaration)
    asset_anual = AssetsDeclaration.create!(
      temporality:  Temporality.find_by(name: "Anual"),
      declaration_date: Time.zone.now,
      year_period: 2015, person_level: job )
    activity_anual = ActivitiesDeclaration.create!(
      temporality:  Temporality.find_by(name: "Anual"),
      declaration_date: Time.zone.now,
      year_period: 2015, person_level: job )
    activity.person_level = job
    job.activities_declarations << activity
    job.activities_declarations << activity_anual
    job.assets_declarations << asset
    job.assets_declarations << asset_anual
    job.save
    expect(controller.send(:generate_period,job, :activities_declarations)).not_to eq([])

    expect(controller.send(:generate_period,job, :assets_declarations)).not_to eq([])
  end 
end
