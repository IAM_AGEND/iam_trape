require 'rails_helper'

RSpec.describe Admin::CorporationsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end

  describe "GET #index" do
    it "assigns all corporations as @corporations" do
      get :index
      expect(response).to be_ok
    end
  end

  describe "GET #edit" do
    it "assigns the requested corporation as @corporation" do
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "PUT #update" do
    it "assigns the requested corporation as @corporation" do
      corporation = Corporation.find(1)
      put :update, {:id => corporation.to_param, :corporation => corporation.attributes}
      expect(assigns(:corporation)).to eq(corporation)
    end

    it "assigns the requested corporation as @corporation" do
      corporation = Corporation.find(1)
      corporation.name = nil
      put :update, {:id => corporation.to_param, :corporation => corporation.attributes}
      expect(assigns(:corporation)).to eq(corporation)
    end
  end

  describe "POST #create" do
    it "assigns the requested corporation as @corporation" do
      corporation = Corporation.find(1)
      post :create, {:id => corporation.to_param, :corporation => corporation.attributes}
      expect(assigns(:corporation)).to eq(corporation)
    end
    it "assigns the requested corporation as @corporation" do
      corporation = Corporation.find(1)
      corporation.name = nil
      post :create, {:id => corporation.to_param, :corporation => corporation.attributes}
      expect(assigns(:corporation)).to eq(corporation)
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested corporation" do
      corporation = FactoryGirl.create(:corporation)
      expect {
        delete :destroy, {:id => corporation.to_param}
      }.to change(Corporation, :count).by(-1)
    end

    it "destroys the requested corporation" do
      corporation = FactoryGirl.create(:corporation)
      
      expect {
        delete :destroy, {:id => corporation.to_param}
      }.to change(Corporation, :count).by(-1)
    end
  end
end
