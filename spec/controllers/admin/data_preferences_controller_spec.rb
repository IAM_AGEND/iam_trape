require 'rails_helper'

RSpec.describe Admin::DataPreferencesController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end

  describe "GET #update_all" do
    it "update all data preferences" do
      datas = {"info"=>{"1"=>{"id"=>"1", "content_data"=>"2"}, "2"=>{"id"=>"2", "content_data"=>"5"}}}
      get :update_all,{datas: datas}
      expect(response).not_to eq(nil)
    end
  end
end
