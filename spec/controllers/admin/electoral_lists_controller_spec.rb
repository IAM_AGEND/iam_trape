require 'rails_helper'

RSpec.describe Admin::ElectoralListsController, type: :controller do
  before(:each) do
    sign_in Administrator.find(1)
  end

  describe "GET #index" do
    it "assigns all electoral_lists as @electoral_lists" do
      get :index
      expect(response).to be_ok
    end
  end

  describe "GET #edit" do
    it "assigns the requested electoral_list as @electoral_list" do
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "PUT #update" do
    it "assigns the requested electoral_list as @electoral_list" do
      electoral_list = ElectoralList.find(1)
      put :update, {:id => electoral_list.to_param, :electoral_list => electoral_list.attributes}
      expect(assigns(:electoral_list)).to eq(electoral_list)
    end

    it "assigns the requested electoral_list as @electoral_list" do
      electoral_list = ElectoralList.find(1)
      electoral_list.name = nil
      put :update, {:id => electoral_list.to_param, :electoral_list => electoral_list.attributes}
      expect(assigns(:electoral_list)).to eq(electoral_list)
    end
  end

  describe "POST #create" do
    it "assigns the requested electoral_list as @electoral_list" do
      electoral_list = ElectoralList.find(1)
      post :create, {:id => electoral_list.to_param, :electoral_list => electoral_list.attributes}
      expect(assigns(:electoral_list)).to eq(electoral_list)
    end
    it "assigns the requested electoral_list as @electoral_list" do
      electoral_list = ElectoralList.find(1)
      electoral_list.name = nil
      post :create, {:id => electoral_list.to_param, :electoral_list => electoral_list.attributes}
      expect(assigns(:electoral_list)).to eq(electoral_list)
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested electoral_list" do
      electoral_list = ElectoralList.new(id: 50, name: "xxxxx", long_name: "xxxx xxxx")
      electoral_list.save!
      expect {
        delete :destroy, {:id => electoral_list.to_param}
      }.to change(ElectoralList, :count).by(-1)
    end

    it "destroys the requested electoral_list" do
      electoral_list = ElectoralList.new(id: 50, name: "xxxxx", long_name: "xxxx xxxx")
      electoral_list.save!
      electoral_list.name=nil
      expect {
        delete :destroy, {:id => electoral_list.to_param}
      }.to change(ElectoralList, :count).by(-1)
    end
  end
end
