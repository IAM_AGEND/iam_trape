require 'rails_helper'

RSpec.describe StaticPagesController do

  describe 'Static pages' do
    it 'should include a privacy page' do
      controller = StaticPagesController.new 
      expect(controller.send(:full_feature?)).to eq(true)
      get :privacy
      expect(response).to be_ok
    end

    it 'should include a conditions page' do
      get :conditions
      expect(response).to be_ok
    end

    it 'should include a accessibility page' do
      get :accessibility
      expect(response).to be_ok
    end
  end

end
