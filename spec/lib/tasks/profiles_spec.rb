require "rails_helper"
require "rake"

describe "Profiles tasks" do
  before do
    Rake.application.rake_require "tasks/profiles"
    Rake::Task.define_task(:environment)
  end

  describe "#insert" do
    let :run_rake_task do
      Rake::Task["profiles:insert"].reenable
      Rake.application.invoke_task "profiles:insert"
    end

    context "Insert success" do
      it "insert" do
        run_rake_task

        expect(Administrator.find_by(email: 'ajmorenoh@indra.es')).not_to eq(nil)
        expect(Administrator.find_by(email: 'galvezip@madrid.es')).not_to eq(nil)
      end
    end
  end

  describe "#cumplimented" do
    let :run_rake_task do
      Rake::Task["profiles:cumplimented"].reenable
      Rake.application.invoke_task "profiles:cumplimented"
    end

    context "Insert success" do
      it "insert" do
        expect(run_rake_task).not_to eq(nil)    end
    end
  end
end