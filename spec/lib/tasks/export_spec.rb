require "rails_helper"
require "rake"

describe "Export tasks" do
  before do
    Rake.application.rake_require "tasks/export"
    Rake::Task.define_task(:environment)
  end

  describe "#profiles" do
    let :run_rake_task do
      Rake::Task["export:profiles"].reenable
      Rake.application.invoke_task "export:profiles"
    end

    context "export success" do
      it "export" do
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#activities" do
    let :run_rake_task do
      Rake::Task["export:activities"].reenable
      Rake.application.invoke_task "export:activities"
    end

    context "export success" do
      it "export" do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)
        declaration = FactoryGirl.create(:activities_declaration)
        declaration_2 = ActivitiesDeclaration.create(person_level: councillors_corporation, temporality: Temporality.find_by(name: "Final"), year_period: 2015, declaration_date: Time.zone.now)
        declaration_3 = ActivitiesDeclaration.create(person_level: councillors_corporation, temporality: Temporality.find_by(name: "Anual"), year_period: 2015, declaration_date: Time.zone.now)
        declaration.public_activities << PublicActivity.create(activities_declaration: declaration,entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2016'))
        declaration.private_activities << PrivateActivity.create(activities_declaration: declaration,private_activity_type: "xxxx", description: 'Very Private', entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2016'), end_date: Time.zone.parse('1/1/2017'))
        declaration.other_activities << OtherActivity.create(activities_declaration: declaration,description: 'Other things', start_date: Time.zone.parse('2/2/2016'), end_date: Time.zone.parse('2/2/2017'))

        councillors_corporation.activities_declarations << declaration
        councillors_corporation.activities_declarations << declaration_2
        councillors_corporation.activities_declarations << declaration_3
        councillors_corporation.save
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#assets" do
    let :run_rake_task do
      Rake::Task["export:assets"].reenable
      Rake.application.invoke_task "export:assets"
    end

    context "export success" do
      it "export" do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)
        councillors_corporation.job_level = FactoryGirl.create(:job_level)
        declaration = FactoryGirl.create(:assets_declaration)
        declaration.debts << Debt.new(assets_declaration: declaration, kind: "xxx",observations: 'Development', import: 5000)
        declaration.other_deposits << OtherDeposit.new(assets_declaration: declaration, types: "xxx",description: 'Development',mounth: 1, year: 2015, value: 5000)
        declaration.account_deposits << AccountDeposit.new(assets_declaration: declaration, kind: "xxx",entity: 'Development', balance: 5000)
        declaration.other_personal_properties << OtherPersonalProperty.new(assets_declaration: declaration, types: "XXX", mounth: 5, year: 2015)
        declaration.real_estate_properties << RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20,  mounth: 1, year: 2015, cadastral_value: 5000, observations: 'Development')
        declaration.tax_data << TaxDatum.new(assets_declaration: declaration, irpf_big_import: 5000, irpf_bi_import: 5000)
        declaration.vehicles << Vehicle.new(assets_declaration: declaration, kind: "xxx", model: "xxxx", year: 2015, mounth: 1)



        declaration_2 = AssetsDeclaration.create(person_level: councillors_corporation, temporality: Temporality.find_by(name: "Anual"), year_period: 2015, declaration_date: Time.zone.now)
        councillors_corporation.assets_declarations << declaration
        councillors_corporation.assets_declarations << declaration_2
        councillors_corporation.save
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end
end