require "rails_helper"
require "rake"

describe "Notifications tasks" do
  before do
    Rake.application.rake_require "tasks/notifications"
    Rake::Task.define_task(:environment)
  end

  describe "#import" do
    let :run_rake_task do
      Rake::Task["notifications:date"].reenable
      Rake.application.invoke_task "notifications:date"
      Rake::Task["notifications:import"].reenable
      Rake.application.invoke_task "notifications:import"
    end

    context "Insert success" do
      it "import" do
        run_rake_task

        expect(Notification.all.count).not_to eq(0)
      end
    end
  end

end