require "rails_helper"
require "rake"

describe "Appointmnents tasks" do
  before do
    Rake.application.rake_require "tasks/appointment"
    Rake::Task.define_task(:environment)
  end

  describe "#change" do
    let :run_rake_task do
      Rake::Task["appointment:change"].reenable
      Rake.application.invoke_task "appointment:change"
    end

    context "Quitar texto" do
      it "change" do
        run_rake_task

        expect(Appointment.where(area: "Sin Área de Gobierno").count).to eq(0)
      end
    end
  end

end