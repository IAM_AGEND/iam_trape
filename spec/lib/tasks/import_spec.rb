require "rails_helper"
require "rake"

describe "Import tasks" do
  before do
    Rake.application.rake_require "tasks/import"
    Rake::Task.define_task(:environment)
  end

  describe "#all" do
    let :run_rake_task do
      Rake::Task["import:all"].reenable
      Rake.application.invoke_task "import:all"
    end

    context "import success" do
      it "import" do
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#administrator" do
    let :run_rake_task do
      Rake::Task["import:administrator"].reenable
      Rake.application.invoke_task "import:administrator"
    end

    context "import success" do
      it "import" do
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#preferences" do
    let :run_rake_task do
      Rake::Task["import:preferences"].reenable
      Rake.application.invoke_task "import:preferences"
    end

    context "import success" do
      it "import" do
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#emails" do
    let :run_rake_task do
      Rake::Task["import:emails"].reenable
      Rake.application.invoke_task "import:emails"
    end

    context "import success" do
      it "import" do
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#admin" do
    let :run_rake_task do
      Rake::Task["import:admin"].reenable
      Rake.application.invoke_task "import:admin"
    end

    context "import success" do
      it "import" do
        expect(run_rake_task).not_to eq(nil)
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end
end