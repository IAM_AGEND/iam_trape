require "rails_helper"
require "rake"

describe "Duties tasks" do
  before do
    Rake.application.rake_require "tasks/duties"
    Rake::Task.define_task(:environment)
  end

  describe "#import" do
    let :run_rake_task do
      Rake::Task["duties:duties_date"].reenable
      Rake.application.invoke_task "duties:duties_date"
      Rake::Task["duties:import"].reenable
      Rake.application.invoke_task "duties:import"
    end

    context "Insert success" do
      it "import" do
        run_rake_task

        expect(Duty.all.count).not_to eq(0)
      end
    end
  end

end