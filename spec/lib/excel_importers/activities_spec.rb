require 'rails_helper'
require 'excel_importers/activities'

describe ExcelImporters::Activities do
  describe '#import_inicial' do
    let(:declaration) { JobLevel.joins(:person).find_by("people.personal_code='1'").activities_declarations.first}

    before do
      ExcelImporters::Activities.new('./spec/fixtures/files/activities.xls', 'Inicial','','director').import
    end

    it 'Parses declarations' do
      expect(declaration.temporality.name).to eq('Inicial')
      expect(declaration.declaration_date).to eq(Date.new(2017, 5, 30))
    end
  end

  describe '#import_anual' do
    let(:declaration) { JobLevel.joins(:person).find_by("people.personal_code='1'").activities_declarations.first}

    before do
      ExcelImporters::Activities.new('./spec/fixtures/files/activities.xls', '2015','','director').import
    end

    it 'Parses declarations' do
      expect(declaration.temporality.name).to eq('Anual')
      expect(declaration.year_period).to eq(2015)
      expect(declaration.declaration_date).to eq(Date.new(2017, 5, 30))
    end

  end
end
