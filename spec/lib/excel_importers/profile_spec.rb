require 'rails_helper'
require 'excel_importers/profile'

describe ExcelImporters::ProfileImport do
  describe '#import!' do
    before do
      ExcelImporters::ProfileImport.new('./spec/fixtures/files/single_profile.xls', header_field: 'Fecha',author: Administrator.find(1)).import
    end 

    it 'Parses job levels correctly' do
      expect(Person.find_by(personal_code: 11440)).not_to be_nil
      job = Person.find_by(personal_code: 11440)
      expect(job.profile).to be_nil
      ExcelImporters::ProfileImport.new('./spec/fixtures/files/single_profile.xls', header_field: 'Fecha',corporation: '2015-2019',author: Administrator.find(1)).import
      expect(Person.find_by(personal_code: 178273)).not_to be_nil
      job = Person.find_by(personal_code: 178273)
      expect(job.profile).to be_nil
    end

    it 'Uses the last profile date for updating' do
      j = JobLevel.joins(:person).find_by("people.personal_code=?", 11440)
      expect(j.get_appointment.position).to eq('ASESOR/A N28')
    end
  end
end
