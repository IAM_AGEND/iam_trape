require 'rails_helper'

RSpec.describe Notification, type: :model do
  it 'is valid' do
    notification = Notification.all.first
    expect(notification).to be_valid
  end

  it 'type_action' do
    notification = Notification.all.first
    expect(notification.type_action).to eq(nil)
  end

  it 'content_data_formated' do
    notification = Notification.all.first
    expect(notification.content_data_formated('')).to eq('')
  end
end
