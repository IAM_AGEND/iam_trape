require 'rails_helper'

describe RealEstateProperty, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
        real_estate_property = RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20,  mounth: 1, year: 2015, cadastral_value: 5000, observations: 'Development')

        expect(real_estate_property).to be_valid
    end

    it "get_cadastral_value" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        real_estate_property = RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20, mounth: 1, year: 2015, cadastral_value: 5000, observations: 'Development')

        expect(real_estate_property.get_cadastral_value).to eq("5.000,00 €")
    end

    it "person_identificator" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        real_estate_property = RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20, mounth: 1, year: 2015, cadastral_value: 5000, observations: 'Development')

        expect(real_estate_property.person_identificator).to eq(nil)
    end

    it "purchase_date" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        real_estate_property = RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20, mounth: 1, year: 2015, cadastral_value: 5000, observations: 'Development', purchase_date: "Enero/2015")

        expect(real_estate_property.purchase_date).to eq("Enero/2015")
    end

    it "purchase_date without month" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        real_estate_property = RealEstateProperty.new(assets_declaration: declaration, kind: "xxx",kind: 'Development',
            straight_type: 'Development', adquisition_title: 'Development', municipality: 'Development',
            participation_percentage: 20, mounth: nil, year: 2015, cadastral_value: 5000, observations: 'Development', purchase_date: "2015")

        expect(real_estate_property.purchase_date).to eq("2015")
    end
end
