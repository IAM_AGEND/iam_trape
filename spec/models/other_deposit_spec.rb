require 'rails_helper'

RSpec.describe OtherDeposit, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
        other_deposit = OtherDeposit.new(assets_declaration: declaration, types: "xxx",description: 'Development',
            mounth: 1, year: 2015, value: 5000)

        expect(other_deposit).to be_valid
    end

    it "get_value" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        other_deposit = OtherDeposit.new(assets_declaration: declaration, types: "xxx",description: 'Development',
            mounth: 1, year: 2015, value: 5000)

        expect(other_deposit.get_value).to eq("5000")
    end
end
