require 'rails_helper'

describe PublicActivity do
  it 'not personal code' do
    declaration = FactoryGirl.create(:activities_declaration)
    job =JobLevel.find(1)
    declaration.person_level = JobLevel.find(1)
    public_activity = PublicActivity.create(activities_declaration: declaration,entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2016'))  
    expect(public_activity.person_identificator).to eq job.person.personal_code
  end
end
