require 'rails_helper'

RSpec.describe ElectoralList, type: :model do
    it 'is valid' do
        electoral_list = ElectoralList.find(1)
        expect(electoral_list).to be_valid
    end

    it 'main columns' do
        expect(ElectoralList.main_columns).not_to eq(nil)
    end
end
