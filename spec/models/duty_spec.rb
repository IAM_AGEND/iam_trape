require 'rails_helper'

RSpec.describe Duty, type: :model do
  it 'is valid' do
    duty = Duty.create!(declaration: nil, person_level: JobLevel.all.first, type_duty: 'AssetsDeclaration', temporality: Temporality.all.first)
    expect(duty).to be_valid
  end
end