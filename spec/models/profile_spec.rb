require 'rails_helper'

RSpec.describe Profile, type: :model do
  
  it 'is valid' do
    profile = FactoryGirl.create(:profile)
    expect(profile).to be_valid
  end
 
  it 'publication blank' do
    profile = FactoryGirl.create(:profile)
    publication = FactoryGirl.create(:information)

    expect(profile.publication_blank?(publication.attributes)).to eq(false)
  end

  it 'has_profile?' do
    profile = FactoryGirl.create(:profile)
    expect(profile.has_profile?).to eq(false)
  end

  it 'especific_columns' do
    profile = FactoryGirl.create(:profile)
    expect(Profile.especific_columns).not_to eq(nil)
  end

  it 'person_type' do
    profile = FactoryGirl.create(:profile)
    person = FactoryGirl.create(:person)
    profile.people << person
    person.profile = profile
    expect(profile.person_type).not_to eq(nil)
  end

  it 'personal_code' do
    profile = FactoryGirl.create(:profile)
    person = FactoryGirl.create(:person)
    profile.people << person
    person.profile = profile
    expect(profile.personal_code).not_to eq(nil)
  end

  it 'name' do
    profile = FactoryGirl.create(:profile)
    person = FactoryGirl.create(:person)
    profile.people << person
    person.profile = profile
    expect(profile.name).not_to eq(nil)
  end

  it 'last_name' do
    profile = FactoryGirl.create(:profile)
    person = FactoryGirl.create(:person)
    profile.people << person
    person.profile = profile
    expect(profile.last_name).not_to eq(nil)
  end

  it "generate notification" do
    notificacion = Notification.where(type_data: 'Profile').first
    vacio = Profile.getNotificationData(notificacion).blank?

    expect(vacio).not_to be_nil
  end

  it "transformDataHash" do
    notificacion = Notification.where(type_data: 'Profile').first
    vacio = Profile.transformDataHash(notificacion).blank?

    expect(vacio).not_to be_nil
  end
end
