require 'rails_helper'

describe AccountDeposit, type: :model do
  it "be valid" do
    declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
    account_deposit = AccountDeposit.new(assets_declaration: declaration, kind: "xxx",entity: 'Development', balance: 5000)

    expect(account_deposit).to be_valid
  end

  it "get_balance" do
      declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
      account_deposit = AccountDeposit.new(assets_declaration: declaration, kind: "xxx",entity: 'Development', balance: 5000)

      expect(account_deposit.get_balance).to eq("5.000,00 €")
  end
end
