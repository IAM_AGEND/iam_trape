require 'rails_helper'

describe ActivitiesDeclaration do

  describe 'Adding hstore data' do
    it 'adds public activities to data' do
      declaration = FactoryGirl.create(:activities_declaration)
      declaration.public_activities << PublicActivity.create(activities_declaration: declaration,entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2016'))

      public_activity = declaration.public_activities.last
      expect(public_activity.entity).to eq 'Entity'
      expect(public_activity.position).to eq 'Position'
    end

    it 'adds private activities to data' do
      declaration = FactoryGirl.create(:activities_declaration)
      declaration.private_activities << PrivateActivity.create(activities_declaration: declaration,private_activity_type: "xxxx", description: 'Very Private', entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2016'), end_date: Time.zone.parse('1/1/2017'))

      private_activity = declaration.private_activities.last
      expect(private_activity.private_activity_type).to eq "xxxx"
      expect(private_activity.description).to eq 'Very Private'
      expect(private_activity.entity).to eq 'Entity'
      expect(private_activity.position).to eq 'Position'
    end

    it 'adds other activities to data' do
      declaration = FactoryGirl.create(:activities_declaration)
      declaration.other_activities << OtherActivity.create(activities_declaration: declaration,description: 'Other things', start_date: Time.zone.parse('2/2/2016'), end_date: Time.zone.parse('2/2/2017'))

      other_activity = declaration.other_activities.last
      expect(other_activity.description).to eq 'Other things'
    end
  end

  it 'period_name inicial' do
    declaration = FactoryGirl.create(:activities_declaration)
    expect(declaration.period_name).to eq 'Inicial'
  end

  it 'period_name anual' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.temporality = Temporality.find_by(name: "Anual")
    declaration.year_period = 2015
    expect(declaration.period_name).to eq 'Anual 2015'
  end

  it 'period_name anual' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.temporality = nil
    expect(declaration.period_name).to eq ''
  end

  it 'person_fist_name' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_fist_name).not_to eq ''
  end

  it 'person_first_last_name' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_first_last_name).not_to eq ''
  end

  it 'person_second_last_name' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_second_last_name).not_to eq ''
  end

  it 'person_type' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_type).not_to eq ''
  end

  it 'person_last_name' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_last_name).not_to eq ''
  end

  it 'person_appointment' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_appointment).not_to eq ''
  end

  it 'person_name' do
    declaration = FactoryGirl.create(:activities_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_name).not_to eq ''
  end

  it "generate notification" do
    notificacion = Notification.where(type_data: 'ActivitiesDeclaration').first
    vacio = ActivitiesDeclaration.getNotificationData(notificacion).blank?

    expect(vacio).not_to be_nil
  end

  it "transformDataHash" do
    notificacion = Notification.where(type_data: 'ActivitiesDeclaration').first
    vacio = ActivitiesDeclaration.transformDataHash(notificacion).blank?

    expect(vacio).not_to be_nil
  end

  it "especific_columns" do
    declaration = FactoryGirl.create(:activities_declaration)

    expect(ActivitiesDeclaration.especific_columns).not_to eq(nil)
  end

end
