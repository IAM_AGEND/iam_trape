require 'rails_helper'

RSpec.describe Log, type: :model do
  it 'is valid' do
    log = FactoryGirl.create(:log)
    expect(log).to be_valid
  end

  it 'author email' do
    log = FactoryGirl.create(:log)
    expect(log.author_email).not_to eq("")
  end

  it 'translated successful' do
    log = FactoryGirl.create(:log)
    expect(log.translated_successful).to eq("Sí")
  end

  it 'log_parent' do
    log = FactoryGirl.create(:log)
    expect(log.log_parent).to eq(nil)
  end

  it 'logs_children' do
    log = FactoryGirl.create(:log)
    expect(log.logs_children).to eq([])
  end
end
