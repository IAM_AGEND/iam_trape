require 'rails_helper'

RSpec.describe Corporation, type: :model do
    it 'is valid' do
        corporation = Corporation.find(1)
        expect(corporation).to be_valid
    end

    it 'main columns' do
        corporation = Corporation.find(1)
        expect(Corporation.main_columns).not_to eq(nil)
    end

    it 'especific_columns' do
        corporation = Corporation.find(1)
        expect(Corporation.especific_columns).not_to eq(nil)
    end

    it 'active_formated' do
        corporation = Corporation.find(1)
        expect(corporation.active_formated).not_to eq(nil)
    end

    it 'electoral_asociated' do
        corporation = Corporation.find(1)
        expect(corporation.electoral_asociated).not_to eq(nil)
    end

    it "generate notification" do
        notificacion = Notification.where(type_data: 'Corporation').first
        vacio = Corporation.getNotificationData(notificacion)
    
        expect(vacio).not_to be_nil
      end

end
