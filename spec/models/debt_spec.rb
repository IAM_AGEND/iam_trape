require 'rails_helper'

describe Debt, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
        debt = Debt.new(assets_declaration: declaration, kind: "xxx",observations: 'Development', import: 5000)
    
        expect(debt).to be_valid
    end

    it "get_import" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        debt = Debt.new(assets_declaration: declaration, kind: "xxx",observations: 'Development', import: 5000)

        expect(debt.get_import).to eq("5.000,00 €")
    end
end
