require 'rails_helper'

RSpec.describe Person do

  it 'is valid' do
    person = FactoryGirl.create(:person)
    expect(person).to be_valid
  end
  
  it 'full name' do
    person = FactoryGirl.create(:person)
    expect(person.full_name).not_to eq(nil)
  end

  it 'especific_columns' do
    person = FactoryGirl.create(:person)
    expect(Person.especific_columns).not_to eq(nil)
  end

  it 'type_job' do
    person = FactoryGirl.create(:person)
    expect(person.type_job).not_to eq(nil)
  end

  it 'backwards name' do
    person = FactoryGirl.create(:person)
    expect(person.backwards_name).not_to eq(nil)
  end

  it 'sorting name' do
    person = FactoryGirl.create(:person)
    expect(person.sorting_name).not_to eq(nil)
  end

  it "generate notification" do
    notificacion = Notification.where(type_data: 'Person').first
    vacio = Person.getNotificationData(notificacion)

    expect(vacio).not_to be_nil
  end

  it "transformDataHash" do
    notificacion = Notification.where(type_data: 'Person').first
    vacio = Person.transformDataHash(notificacion)

    expect(vacio).not_to be_nil
  end
end
