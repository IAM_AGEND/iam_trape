require 'rails_helper'

RSpec.describe OtherActivity, type: :model do
  it 'not personal code' do
    declaration = FactoryGirl.create(:activities_declaration)
    job =JobLevel.find(1)
    declaration.person_level = JobLevel.find(1)
    other_activity = OtherActivity.create(activities_declaration: declaration,description: "xxxx", start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2016'))
    expect(other_activity.person_identificator).to eq job.person.personal_code
  end

  it 'validator dates' do
    declaration = FactoryGirl.create(:activities_declaration)
    job =JobLevel.find(1)
    declaration.person_level = JobLevel.find(1)
    other_activity = OtherActivity.new(activities_declaration: declaration,description: "xxxx", start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2014'))
    other_activity.save
    expect(other_activity.errors).not_to eq 0
  end
end
