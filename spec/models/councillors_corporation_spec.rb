require 'rails_helper'

RSpec.describe CouncillorsCorporation, type: :model do
    it 'is valid' do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)
        expect(councillors_corporation).to be_valid
    end

    it 'backwards_name' do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)
        expect(councillors_corporation.backwards_name).not_to eq(nil)
    end

    it 'person' do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)

        expect(councillors_corporation.person).not_to eq(nil)
    end

    it 'person' do
        councillors_corporation = FactoryGirl.create(:councillors_corporation)

        expect(councillors_corporation.type_person).not_to eq(nil)
    end
end
