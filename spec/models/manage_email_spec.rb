require 'rails_helper'

RSpec.describe ManageEmail, type: :model do
  it 'is valid' do
    manage_email = ManageEmail.find(1)
    expect(manage_email).to be_valid
  end

  it 'is not valid' do
    manage_email = ManageEmail.find(1)
    manage_email.sender ="xx"
    expect(manage_email).not_to be_valid
  end

  it 'cc' do
    manage_email = ManageEmail.find(1)
    expect(manage_email.cc).not_to eq("")
  end

  it 'cco' do
    manage_email = ManageEmail.find(1)
    expect(manage_email.cco).not_to eq("")
  end
end
