require 'rails_helper'

RSpec.describe Audit, type: :model do
    it 'is valid' do
        audit = FactoryGirl.create(:audit)
        expect(audit).to be_valid
    end

    it 'author' do
        audit = FactoryGirl.create(:audit)
        expect(audit.author).not_to eq("")
    end
end
