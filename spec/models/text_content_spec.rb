require 'rails_helper'

RSpec.describe TextContent, type: :model do
  it 'is valid' do
    text_content = TextContent.find(1)
    expect(text_content).to be_valid
  end

  it 'get text section' do
    text_content = TextContent.find(1)
    expect(TextContent.get_text_section(text_content.section)).not_to eq("")
  end
end
