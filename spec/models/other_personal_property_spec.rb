require 'rails_helper'

RSpec.describe OtherPersonalProperty, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
        other_personal_property = OtherPersonalProperty.new(assets_declaration: declaration, types: "XXX", mounth: 5, year: 2015)
    
        expect(other_personal_property).to be_valid
    end
end
