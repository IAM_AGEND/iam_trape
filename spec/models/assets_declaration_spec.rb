require "rails_helper"

describe AssetsDeclaration do
  it "especific_columns" do
    declaration = FactoryGirl.create(:assets_declaration)

    expect(AssetsDeclaration.especific_columns).not_to eq(nil)
  end

  t 'period_name inicial' do
    declaration = FactoryGirl.create(:assets_declaration)
    expect(declaration.period_name).to eq 'Inicial'
  end

  it 'period_name anual' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.temporality = Temporality.find_by(name: "Anual")
    declaration.year_period = 2015
    expect(declaration.period_name).to eq 'Anual 2015'
  end

  it 'period_name anual' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.temporality = nil
    expect(declaration.period_name).to eq ''
  end

  it 'person_fist_name' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_fist_name).not_to eq ''
  end

  it 'person_first_last_name' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_first_last_name).not_to eq ''
  end

  it 'person_second_last_name' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_second_last_name).not_to eq ''
  end

  it 'person_type' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_type).not_to eq ''
  end

  it 'person_last_name' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_last_name).not_to eq ''
  end

  it 'person_appointment' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_appointment).not_to eq ''
  end

  it 'person_name' do
    declaration = FactoryGirl.create(:assets_declaration)
    declaration.person_level = JobLevel.find(1)
    expect(declaration.person_name).not_to eq ''
  end

  it "generate notification" do
    notificacion = Notification.where(type_data: 'AssetsDeclaration').first
    vacio = AssetsDeclaration.getNotificationData(notificacion).blank?

    expect(vacio).not_to be_nil
  end

  it "transformDataHash" do
    notificacion = Notification.where(type_data: 'AssetsDeclaration').first
    vacio = AssetsDeclaration.transformDataHash(notificacion).blank?

    expect(vacio).not_to be_nil
  end
end
