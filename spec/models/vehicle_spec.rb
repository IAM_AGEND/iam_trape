require 'rails_helper'

describe Vehicle, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        vehicle = Vehicle.new(assets_declaration: declaration, kind: "xxx", model: "xxxx", year: 2015, mounth: 1)

        expect(vehicle).to be_valid
    end
end
