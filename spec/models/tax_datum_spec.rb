require 'rails_helper'

describe TaxDatum, type: :model do
    it "be valid" do
        declaration = AssetsDeclaration.new(temporality_id: 2, year_period: 2015, declaration_date: Time.zone.now)
        tax_datum = TaxDatum.new(assets_declaration: declaration, irpf_big_import: 5000, irpf_bi_import: 5000, import: 5000)
    
        expect(tax_datum).to be_valid
    end

    it "get_ imports" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        tax_datum = TaxDatum.new(assets_declaration: declaration, irpf_big_import: 5000, irpf_bi_import: 5000,
            irpf_deduc_import_a: 5000, irpf_deduc_import_b: 5000, isp_bi_import: 5000, is_bi_import: 5000, import: 5000 )

        expect(tax_datum.get_amount).to eq("5.000,00 €")
        
    end

    it "get_ imports" do
        declaration = AssetsDeclaration.new(temporality_id: 1, year_period: 2015, declaration_date: Time.zone.now)
        tax_datum = TaxDatum.new(assets_declaration: declaration, irpf_big_import: 5000, irpf_bi_import: 5000, import: 5000,
            irpf_deduc_import_a: 5000, irpf_deduc_import_b: 5000, isp_bi_import: 5000, is_bi_import: 5000, observation: "xxx" )

        expect(tax_datum.observation).not_to eq(nil)
    end
end
