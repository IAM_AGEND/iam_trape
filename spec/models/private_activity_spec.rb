require 'rails_helper'

RSpec.describe PrivateActivity, type: :model do
  it 'not personal code' do
    declaration = FactoryGirl.create(:activities_declaration)
    job =JobLevel.find(1)
    declaration.person_level = JobLevel.find(1)
    private_activity = PrivateActivity.create(activities_declaration: declaration,entity: 'Entity', position: 'Position', start_date: Time.zone.parse('1/1/2015'), end_date: Time.zone.parse('1/1/2016'))

    expect(private_activity.person_identificator).to eq job.person.personal_code
  end
end
