require 'rails_helper'

describe JobLevel, type: :model do
    it 'is valid' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level).to be_valid
    end

    it 'grouped_by_electoral_list' do
        expect(JobLevel.grouped_by_electoral_list(Corporation.find(1))).not_to eq(nil)
    end

    it 'grouped_by_initial' do
        expect(JobLevel.joins(:person).grouped_by_initial).not_to eq(nil)
    end

    it 'validates_directos' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(JobLevel.validates_directos([job_level])).to eq(false)
    end

    it 'councillor?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.councillor?).to eq(true)
    end

    it 'director?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.director?).to eq(false)
    end

    it 'temporary_worker?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.temporary_worker?).to eq(false)
    end

    it 'public_worker?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.public_worker?).to eq(false)
    end

    it 'spokesperson?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.spokesperson?).to eq(false)
    end

    it 'working?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.working?(Corporation.find(1))).to eq(true)
    end

    it 'not_working?' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.not_working?(Corporation.find(1))).to eq(false)
    end

    it 'name' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.name).not_to eq("")
    end

    it 'backwards_name' do
        job_level = FactoryGirl.create(:job_level)
        
        expect(job_level.backwards_name).not_to eq("")
    end

    it 'has_profile' do
        job = JobLevel.find(1)
        person = job.person
        person.profile = FactoryGirl.create(:profile)
        information = FactoryGirl.create(:information)
        information.information_type = InformationType.find_by(name: "other")
        information.information ="xxxxx"
        information.profile=person.profile
        information.save
        person.profile.other << information
        person.save
        job.save
        expect(job.has_profile?).to eq("Sí")

        information.information ="El contenido del perfil y trayectoria profesional se publicará una vez que sea facilitado por el empleado público"
        information.save
        expect(job.has_profile?).to eq("No")
    end

    it 'get_initial_assets_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_initial_assets_declarations(Corporation.find(1))).to eq("-")
    end

    it 'get_initial_activities_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_initial_activities_declarations(Corporation.find(1))).to eq("-")
    end

    it 'get_annual_assets_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_annual_assets_declarations(Corporation.find(1),2016)).to eq("-")
    end

    it 'get_annual_activities_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_annual_activities_declarations(Corporation.find(1),2016)).to eq("-")
    end

    it 'get_final_assets_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_final_assets_declarations(Corporation.find(1))).to eq("-")
    end

    it 'get_final_activities_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_final_activities_declarations(Corporation.find(1))).to eq("-")
    end

    it 'get_activities_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_activities_declarations(Corporation.find(1))).not_to eq([])
    end

    it 'get_activities_declarations director' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.get_activities_declarations).to eq([])
    end

    it 'get_assets_declarations' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_assets_declarations(Corporation.find(1))).not_to eq([])
    end

    it 'get_assets_declarations director' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.get_assets_declarations).to eq([])
    end


    it 'get_appointment' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.get_appointment).not_to eq(nil)
    end

    it 'get_profile director' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.get_profile).not_to eq(nil)
    end

    it 'get_profile' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_profile).not_to eq(nil)
    end

    it 'get_active director' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.get_active).not_to eq("")
    end

    it 'get_active' do
        job_level = FactoryGirl.create(:job_level)
        expect(job_level.get_active).not_to eq("")
    end

    it 'get_active director inactive' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        job_level.get_appointment.end_date = Time.zone.now
        expect(job_level.get_active).not_to eq("")
    end

    it 'not_leaving_date?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.not_leaving_date?).to eq(true)
    end

    it 'should_display_personal_data?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        job_level.get_appointment.end_date = Time.zone.now

        expect(job_level.should_display_personal_data?).to eq(true)
    end

    it 'should_display_profile?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.should_display_profile?).to eq(true)
    end

    it 'should_display_image?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.should_display_image?).to eq(false)
    end

    it 'should_display_declarations?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.should_display_declarations?).to eq(true)
    end

    it 'should_display_calendar?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.should_display_calendar?).to eq(true)
    end

    it 'has_career?' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.has_career?).to eq(false)
    end

    it 'change_working hide' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.change_working("hide", Administrator.find(1), "")).not_to eq(nil)
    end

    it 'change_working unhide' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.change_working("unhide", Administrator.find(1), "")).not_to eq(nil)
    end

    it 'twitter' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.twitter).not_to eq(nil)
    end

    it 'position' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.position).not_to eq(nil)
    end

    it 'first_study_center' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_study_center).not_to eq(nil)
    end

    it 'first_course_center' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_course_center).not_to eq(nil)
    end

    it 'first_public_job_position' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_public_job_position).not_to eq(nil)
    end

    it 'first_public_job_public_administration' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_public_job_public_administration).not_to eq(nil)
    end

    it 'first_private_job_position' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_private_job_position).not_to eq(nil)
    end

    it 'first_political_post_position' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.first_political_post_position).not_to eq(nil)
    end

    it 'language_english_level' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.language_english_level).not_to eq(nil)
    end

    it 'language_other_name' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.language_other_name).not_to eq(nil)
    end

    it 'language_other_level' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.language_other_level).not_to eq(nil)
    end

    it 'job_level_code' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.job_level_code).not_to eq(nil)
    end

    it 'public_jobs_body' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.public_jobs_body).not_to eq(nil)
    end

    it 'public_jobs_start_year' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.public_jobs_start_year).not_to eq(nil)
    end

    it 'public_jobs_level' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.public_jobs_level).not_to eq(nil)
    end

    it 'publications' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.publications).not_to eq(nil)
    end

    it 'studies_comments' do
        job_level = FactoryGirl.create(:job_level)
        job_level.person_type = "director"
        expect(job_level.studies_comments).not_to eq(nil)
    end
end
