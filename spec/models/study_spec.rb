require 'rails_helper'

RSpec.describe Study, type: :model do
    it 'is valid' do
        study = FactoryGirl.create(:study)
        expect(study).to be_valid
    end

    it 'not valid' do
        profile = FactoryGirl.create(:profile)
        study = Study.new(profile: profile, official_degree: "xx", center: "xx", start_year: 2015, end_year: 2014)
        study.save
        expect(study.errors.count).to eq 0
    end
end
