require 'rails_helper'

RSpec.describe CorporationsElectoralList, type: :model do
    it 'is valid' do
        corporations_electoral_list = CorporationsElectoralList.find(1)
        expect(corporations_electoral_list).to be_valid
    end
  
    it 'electoral list name' do
        corporations_electoral_list = CorporationsElectoralList.find(1)
        expect(corporations_electoral_list.electoral_list_name).not_to eq("")
    end
end
