require 'rails_helper'

RSpec.describe LanguageLevel, type: :model do
    it 'is valid' do
        language_level = LanguageLevel.find(1)
        expect(language_level).to be_valid
    end

    it 'name' do
        language_level = LanguageLevel.find(1)
        expect(language_level.name).not_to eq("") 
    end
end
