require "rails_helper"

RSpec.describe Mailer, :type => :mailer do
  
    it "activities_upload" do
        expect(Mailer.activities_upload("councillor","to@example.org").deliver_now).not_to eq(nil)
    end

    it "assets_upload" do
        expect(Mailer.assets_upload("councillor","to@example.org").deliver_now).not_to eq(nil)
    end

    it "profile_upload" do
        expect(Mailer.profile_upload("councillor","to@example.org").deliver_now).not_to eq(nil)
    end
end