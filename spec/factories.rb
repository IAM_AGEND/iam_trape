require 'securerandom'

FactoryGirl.define do
  factory :notification do
    
  end
  factory :manage_email do
    
  end
  factory :data_preference do
    
  end
  factory :information do
    information_type_id 1
    information "xxxx"
    information_at Time.zone.now
  end
  factory :electoral_list do
    name "XXX"
    logo_name "XXXX"
  end
  factory :corporations_electoral_list do
    corporation_id 1
    electoral_list_id 1
    order 5
  end
  
  factory :corporation do
    name "2050-2054"
    election_date Time.zone.now
    start_year 2050
    end_year  2054
    active true
  end
  factory :councillors_corporation do
    corporation_id 1
    job_level
    electoral_list_id 1
    appointment
    order_num 5
  end
  
  factory :appointment do
    position "XXXXX"
    unit "XXXXX"
    area "XXXXXX"
    position_alt "XXXXX"
    start_date Time.zone.today
  end
  factory :job_level do
    updated_at Time.current.to_date
    person
    appointment
    person_type "councillor"
  end
  factory :language_level do
    level "Alto"
  end
  factory :language_name do
    name "Inglés"
  end
  factory :public_administration do
    administration_code "AD"
    name "Local"
  end
  factory :information_type do
    name "xxxxx"
  end
  factory :document_type do
    name "DNI"
  end
  factory :log do
    administrator_id 1
  end
  factory :text_content do
    section "XXXXXX"
    content "XXXXX"
  end
  factory :audit do
    person
    administrator_id nil
    action 'alguna'
    description 'descripción de alguna'
  end
  
  factory :other_personal_property do
    assets_declaration
    kind 'clase de otra'
    purchase_date 2016
  end
  factory :political_post do
    profile
    position  'Gorn Tamer'
    entity  'Metron Arena'
    start_year 2266
    end_year 2266
  end
  factory :private_job do
    profile
    position  'Gorn Tamer'
    entity  'Metron Arena'
    start_year 2266
    end_year 2266
  end
  factory :public_job do
    profile
    position 'Starfleet captain'
    public_administration 'xxxx'
    body_scale "xxxx"
    init_year 2015
    consolidation_degree 30
    start_year 2265
    end_year 2270
  end
  factory :course do
    profile
    title 'Ancient Literature'
    center  "John Gill's class"
    start_year 2253
    end_year 2253
  end
  factory :study do
    profile
    education_level "xxx"
    official_degree 'Officer Training Program'
    center 'Starfleet Academy'
    start_year 2252
    end_year 2257
  end
  factory :tax_datum do
    assets_declaration
    tax 5000
    fiscal_data 'nada que alegar'
    amount 50000
    comments 'algún comentario'
  end
  factory :debt do
    assets_declaration_id
    kind 'clase z'
    amount 50000
    comments 'Nada que comentar'
  end
  factory :vehicle do
    assets_declaration_id 1
    kind 'clase z'
    model 'modelo x'
    mounth 1
    year 2015
  end
  factory :other_deposit do
    assets_declaration
    kind 'clase de otro deposito'
    description 'descripción de otro deposito'
    amount 2500
    purchase_date 2018
  end
  factory :account_deposit do
    kind      'Development'
    banking_entity 'Bankia'
    balance   500   
  end
  factory :real_estate_property do
    assets_declaration_id 1
    kind 'Development'
    straight_type 'Development'
    adquisition_title 'Development'
    municipality 'Development'
    participation_percentage 20
    mounth 1
    year 2015
    cadastral_value 5000
    observations 'Development'
  end
  factory :other_activity do
    activities_declaration
    description 'Development'
    start_date 2252
    end_date 2253
  end
  factory :private_activity do
    private_activity_type_id 1
    activities_declaration
    entity "xxx"
    position "xxxx"
  end

  factory :private_activity_type do
    name 'xxxxx'
  end
  factory :public_activity do
    activities_declaration
    entity 'New entity'
    position 'New position'
    start_date 2253
    end_date 2254
  end
  factory :language do
    profile
    name 'Inglés'
    level 'alto'
  end
  factory :profile do
    updated_at Time.current.to_date
    email "xxx@xxx.es"
  end

  factory :person do
    sequence(:name) { |n| "person#{n}" }
    sequence(:last_name) { |n| "surname#{n}" }
    personal_code 50
    updated_at Time.current.to_date
    profile
  end

  factory :director, parent: :person do
    job_level { 'director' }
  end

  factory :temporary_worker, parent: :person do
    job_level { 'temporary_worker' }
  end

  factory :party do
    sequence(:name) { |n| "party#{n}" }
    logo { 'logo.png' }
  end

  factory :activities_declaration do
    temporality_id 1
    declaration_date Time.current
  end

  factory :temporality do
    name "Inicial"
    description "Inicial"
  end

  factory :assets_declaration do
    temporality_id 1
    declaration_date Time.current
  end

  factory :administrator do
    sequence(:email) { |n| "admin#{n}@madrid.es" }
    password         '12345678'
  end

  factory :area do
    sequence(:name) { |n| "Area#{n}" }
  end

  factory :profile_upload do
    administrator_id 1
  end

  factory :activities_upload do
    administrator_id 1
  end

  factory :assets_upload do
    administrator_id 1
  end

  factory :delete_data do
    administrator_id 1
  end

  factory :profiles_email_log do
    administrator_id 1
  end

  factory :activities_email_log do
    administrator_id 1
  end

  factory :assets_email_log do
    administrator_id 1
  end

end
