module TextHelper
  def format_free_text(text)
    text = "" if text.blank?
    simple_format Rinku.auto_link(text, :all, 'target="_blank" rel="nofollow"')
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def upper_case(text)
    text.try { |tx| tx.mb_chars.upcase}
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
