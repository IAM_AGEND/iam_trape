module SocialHelper
  def twitter_url(handle)
    "https://twitter.com/#{handle.gsub('@', '')}"
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def facebook_url(handle)
    handle = handle.gsub('https://', '')
                   .gsub('http://', '')
                   .gsub('www.facebook.com/', '')
                   .gsub('facebook.com/', '')
    "https://www.facebook.com/#{handle}"
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
