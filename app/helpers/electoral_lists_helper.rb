module ElectoralListsHelper

    def logo_picture(electoral_list, size = nil)        
        if electoral_list.logo.blank? || !electoral_list.logo.exists?
            I18n.t('admin.electoral_lists.no_logo')
        else
            image_tag electoral_list.logo.url(size || :medium), alt: ''
        end
    rescue => e
        begin
            Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        I18n.t('admin.electoral_lists.no_logo')        
    end

    def logo_picture_public(electoral_list, size = nil)       
        if !electoral_list.logo.blank? && electoral_list.logo.exists?
            image_tag electoral_list.logo.url(size || :medium), alt: ''
        end
    rescue => e
        begin
            Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end        
    end
end
