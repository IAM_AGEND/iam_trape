module ApplicationHelper

  def active_class(current_object, object)
    object == current_object ? "active" : ""
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def current_url(new_params)
    url_for params.merge(new_params)
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def get_url_reload
    "#{request.base_url}/" if ["https://transparenciapersonasdesa.madrid.es/", "https://transparenciapersonaspre.madrid.es/","https://transparenciapersonas.madrid.es/" ].map {|url| url.to_s == "#{request.base_url}/"}
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column.to_s == sort_column.to_s ? "current #{sort_direction}" : nil
    direction =  column.to_s == sort_column.to_s && sort_direction.to_s == "asc" ? "desc" : "asc"    
    link_to title, params.except(:sort,:direction).merge({:sort => column, :direction => direction}), {:class => css_class}
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def boolean_to_text(bool)
    bool ? t("booleans.t") : t("booleans.f")
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  # if current path is /people current_path_with_query_params(foo: 'bar') returns /people?foo=bar
  # notice: if query_params have a param which also exist in current path, it "overrides" (query_params is merged last)
  def current_path_with_query_params(query_parameters)
    url_for(request.query_parameters.merge(query_parameters))
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def get_min_errors(error_name,model)
    return "" if error_name.blank?
    aux= error_name.to_s.split('.')

    return I18n.t("#{model}.#{error_name}")  if aux.count <= 1
   
    I18n.t("#{aux[aux.count-2]}.#{aux[aux.count-1]}")
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def undescore(model_name)
    return "" if model_name.blank?
    model_name.underscore.to_sym
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def is_number? string
    true if Float(string) rescue false
  end

  def transform_link(link)
    link.sub("://","").gsub(/[https|http]/,"")
  end

end
