include ActionView::Helpers::TranslationHelper

module DeclarationsHelper

  PERIODS_START_YEAR = 2016

  def periods_list
    initial = { "Inicial" => "Inicial" }
    years = Hash[(PERIODS_START_YEAR..Date.current.year + 1).map { |year| [year, year] }]
    final = { "Final" => "Final" }
    initial.merge(years).merge(final)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
