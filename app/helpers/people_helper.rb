
module PeopleHelper

  def profile_picture(profile, size = nil)    
    if profile.portrait.blank? || !profile.portrait.exists?
      image_tag "#{asset_path "missing.png"}", alt: "Sin imagen asociada al concejal #{profile.try(:name)} #{profile.try(:last_name)}"
    else
      image_tag profile.portrait.url(size || :medium),alt: "Imagen del concejal #{profile.try(:name)} #{profile.try(:last_name)}"
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    image_tag "#{asset_path "missing.png"}",alt: "Sin imagen asociada al concejal #{profile.try(:name)} #{profile.try(:last_name)}"
  end

  def breadcrumb_person_hash_title_link(job_level)
    return if job_level.blank?
    {title: t("people.titles.#{job_level.try(:person_type).try{|pt| pt.pluralize}}"), link_path: public_send("#{job_level.try(:person_type).try {|pt| pt.pluralize}}_people_path")}
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def breadcrumb_corporation_hash_title_link(corporation)
    return if corporation.blank?
    {title: t("people.corporation_old", corporation: corporation.try(:name)), link_path: (corporation.active ? councillors_people_path(path_corporation: corporation.try(:name)) : councillors_other_people_path(path_corporation: corporation.try(:name)))}
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def breadcrumb_person_hash_pre_title_link(job_level, corporation)
    return if job_level.blank? || !job_level.councillor? && !job_level.director?
    if job_level.councillor?
      if !corporation.blank? && !corporation.active
        {title: t("people.corporation_oldest"), link_path: councillors_other_people_path(path_corporation: corporation.try(:name))}
      elsif !corporation.blank? && job_level.get_appointment(corporation).try(:end_date).blank?
        {title: t("people.working"), link_path: councillors_people_path(path_working: true)}
      else
        {title: t("people.not_working"), link_path: councillors_not_work_people_path(path_not_working: true)}
      end
    else
      if job_level.get_appointment.try(:end_date).blank?
        {title: t("people.director_working"), link_path: directors_people_path(path_working: true)}
      else
        {title: t("people.director_not_working"), link_path: directors_not_work_people_path(path_not_working: true)}
      end
    end
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def load_menu(job_level, corporation, appointment)
    options = []
    options.push({accesskey: 6, path: "appointment"}) unless @job_level.public_worker? || @job_level.spokesperson?
    options.push({accesskey: 7, path: "economic"}) unless @job_level.public_worker?
    #options.push({accesskey: 8, path: "contact"}) if (job_level.councillor? || job_level.director?) && (!appointment.try(:email).blank? || !appointment.try(:url_form).blank?)
    options.push({accesskey: 8, path: "profile"}) if job_level.should_display_profile?(corporation)   
    
    if job_level.should_display_declarations?(corporation)
      options.push({accesskey: 9, path: "assets_declarations"})
      options.push({accesskey: 's', path: "activities_declarations"}) 
    end

    # options.push({accesskey: 6, path: "profile"}) if job_level.should_display_profile?(corporation)   
    
    # if job_level.should_display_declarations?(corporation)
    #   options.push({accesskey: 7, path: "assets_declarations"})
    #   options.push({accesskey: 8, path: "activities_declarations"}) 
    # end
    

   

    options
    
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end


  def get_currency_value(value)
    return "" if value.blank?
    number_to_currency(value, precision: 2)
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
