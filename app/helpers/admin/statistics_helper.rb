module Admin
  module StatisticsHelper
    extend ActiveSupport::Concern

    def self.file_name(year=nil, corporation=nil)
      {
        'profiles' => {
          'councillor' => {'all' => 'concejales_activos', 'profile' => 'concejales_con_perfil', 'unprofile' => 'concejales_sin_perfil'},
          'director' => {'all' => 'directivos_activos', 'profile' => 'directivos_con_perfil', 'unprofile' => 'directivos_sin_perfil'},
          'public_worker' => {'all' => 'funcionarios_activos', 'profile' => 'funcionarios_con_perfil', 'unprofile' => 'funcionarios_sin_perfil'},
          'temporary_worker' => {'all' => 'eventuales_activos', 'profile' => 'eventuales_con_perfil', 'unprofile' => 'eventuales_sin_perfil'},
          'spokesperson' => {'all' => 'vocales_vecinos_activos', 'profile' => 'vocales_vecinos_con_perfil', 'unprofile' => 'vocales_vecinos_sin_perfil'},
          'total' => {'all' => 'total_activos', 'profile' => 'total_con_perfil', 'unprofile' => 'total_sin_perfil'}
        },
        'directors' => {
          'active' => {
            'inicial' => {
              'asset' => { 'all' => 'directivos_act_bienes_inicial', 'with' => 'directivos_act_bienes_inicial_obligado', 'without' => 'directivos_act_bienes_inicial_no_obligado'},
              'activity' => { 'all' => 'directivos_act_activi_inicial', 'with' => 'directivos_act_activi_inicial_obligado', 'without' => 'directivos_act_activid_inicial_no_obligado'}
            },
            'final' => {
              'asset' => { 'all' => 'directivos_act_bienes_final', 'with' => 'directivos_act_bienes_final_obligado', 'without' => 'directivos_act_bienes_final_no_obligado'},
              'activity' => { 'all' => 'directivos_act_activi_final', 'with' => 'directivos_act_activi_final_obligado', 'without' => 'directivos_act_activid_final_no_obligado'}
            },
            'anual' => {
              'asset' => { 'all' => "directivos_act_bienes_#{year.to_s}", 'with' => "directivos_act_bienes_obligado_#{year.to_s}", 'without' => "directivos_act_bienes_no_obligado_#{year.to_s}"},
              'activity' => { 'all' => "directivos_act_activi_#{year.to_s}", 'with' => "directivos_act_activi_obligado_#{year.to_s}", 'without' => "directivos_act_activid_no_obligado_#{year.to_s}"}
            }
          },
          'inactive' => {
            'inicial' => {
              'asset' => {'all' => 'directivos_ina_bienes_inicial', 'with' => 'directivos_ina_bienes_inicial_obligado', 'without' => 'directivos_ina_bienes_inicial_no_obligado'},
              'activity' => {'all' => 'directivos_ina_activi_inicial', 'with' => 'directivos_ina_activi_inicial_obligado', 'without' => 'directivos_ina_activid_inicial_no_obligado'}
            },
            'final' => {
              'asset' => {'all' => 'directivos_ina_bienes_final', 'with' => 'directivos_ina_bienes_final_obligado', 'without' => 'directivos_ina_bienes_final_no_obligado'},
              'activity' => { 'all' => 'directivos_ina_activi_final', 'with' => 'directivos_ina_activi_final_obligado', 'without' => 'directivos_ina_activid_final_no_obligado'}
            },
            'anual' => {
              'asset' => {'all' => "directivos_ina_bienes_#{year.to_s}", 'with' => "directivos_ina_bienes_obligado_#{year.to_s}", 'without' => "directivos_ina_bienes_no_obligado_#{year.to_s}"},
              'activity' => {'all' => "directivos_ina_activi_#{year.to_s}", 'with' => "directivos_ina_activi_obligado_#{year.to_s}", 'without' => "directivos_ina_activid_no_obligado_#{year.to_s}"}
            }
          }
        },
        'corporation' => {
          'active' => {
            'inicial' => {
              'asset' => { 'all' => "#{corporation}_concejales_act_bienes_inicial", 'with' => "#{corporation}_concejales_act_bienes_inicial_obligado", 'without' => "#{corporation}_concejales_act_bienes_inicial_no_obligado"},
              'activity' => { 'all' => "#{corporation}_concejales_act_activi_inicial", 'with' => "#{corporation}_concejales_act_activi_inicial_obligado", 'without' => "#{corporation}_concejales_act_activid_inicial_no_obligado"}
            },
            'final' => {
              'asset' => { 'all' => "#{corporation}_concejales_act_bienes_final", 'with' => "#{corporation}_concejales_act_bienes_final_obligado", 'without' => "#{corporation}_concejales_act_bienes_final_no_obligado"},
              'activity' => { 'all' => "#{corporation}_concejales_act_activi_final", 'with' => "#{corporation}_concejales_act_activi_final_obligado", 'without' => "#{corporation}_concejales_act_activid_final_no_obligado"}
            },
            'anual' => {
              'asset' => { 'all' => "#{corporation}_concejales_act_bienes_#{year.to_s}", 'with' => "#{corporation}_concejales_act_bienes_obligado_#{year.to_s}", 'without' => "#{corporation}_concejales_act_bienes_no_obligado_#{year.to_s}" },
              'activity' => { 'all' => "#{corporation}_concejales_act_activi_#{year.to_s}", 'with' => "#{corporation}_concejales_act_activi_obligado_#{year.to_s}", 'without' => "#{corporation}_concejales_act_activid_no_obligado_#{year.to_s}"}
            }
          },
          'inactive' => {
            'inicial' => {
              'asset' => { 'all' => "#{corporation}_concejales_ina_bienes_inicial", 'with' => "#{corporation}_concejales_ina_bienes_inicial_obligado", 'without' => "#{corporation}_concejales_ina_bienes_inicial_no_obligado"},
              'activity' => {'all' => "#{corporation}_concejales_ina_activi_inicial", 'with' => "#{corporation}_concejales_ina_activi_inicial_obligado", 'without' => "#{corporation}_concejales_ina_activid_inicial_no_obligado"}
            },
            'final' => {
              'asset' => { 'all' => "#{corporation}_concejales_ina_bienes_final", 'with' => "#{corporation}_concejales_ina_bienes_final_obligado", 'without' => "#{corporation}_concejales_ina_bienes_final_no_obligado" },
              'activity' => { 'all' => "#{corporation}_concejales_ina_activi_final", 'with' => "#{corporation}_concejales_ina_activi_final_obligado", 'without' => "#{corporation}_concejales_ina_activid_final_no_obligado" }
            },
            'anual' => {
              'asset' => { 'all' => "#{corporation}_concejales_ina_bienes_#{year.to_s}", 'with' => "#{corporation}_concejales_ina_bienes_obligado_#{year.to_s}", 'without' => "#{corporation}_concejales_ina_bienes_no_obligado_#{year.to_s}"},
              'activity' => { 'all' => "#{corporation}_concejales_ina_activi_#{year.to_s}", 'with' => "#{corporation}_concejales_ina_activi_obligado_#{year.to_s}", 'without' => "#{corporation}_concejales_ina_activid_no_obligado_#{year.to_s}"}
            }
          }
        }
      }
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    #####################################################################################################################
    ## COUNCILLORS
    #####################################################################################################################
    def stream_query_rows_councillors(active, declaration, temporality, part, corporation, editable)
      preference_interval = DataPreference.find_by(:code => "public_year")
      year_interval= preference_interval.blank? ? 2 : preference_interval.type_data.to_i==0 ? preference_interval.content_data.to_i : 2
      data_where = ''

      if !editable.blank? && editable.to_s == "true"
        data_where = data_where + " AND (((d.declaration_type ='AssetsDeclaration' AND d.declaration_id in (select ad.id from assets_declarations ad where ad.editable=true)) OR (d.declaration_type ='ActivitiesDeclaration' AND d.declaration_id in (select ad.id from activities_declarations ad where ad.editable=true))))"
      else
        data_where = data_where + " AND (d.declaration_id is null or ((d.declaration_type ='AssetsDeclaration' AND d.declaration_id not in (select ad.id from assets_declarations ad where ad.editable=true)) OR (d.declaration_type ='ActivitiesDeclaration' AND d.declaration_id not in (select ad.id from activities_declarations ad where ad.editable=true))))"
      end

      if editable.blank? || editable.to_s != "true"
        if !active.blank? && active.to_s=="true"
          data_where = data_where + " AND (cc.appointment_id in (select  a.id from appointments a where a.end_date is null or cast(a.end_date as date) > cast(NOW() as date)) or cc.appointment_id is null)"
        else
          data_where = data_where + " AND (cc.appointment_id not in (select  a.id from appointments a where a.end_date is null or cast(a.end_date as date) > cast(NOW() as date)) and cc.appointment_id is not null)"
        end
      end

      if !temporality.blank?
        data_where = data_where + " AND (d.temporality_id in (select t.id from temporalities t where t.name = '#{temporality.include?("Anual") ? 'Anual'.strip : temporality.strip}') #{"AND d.year = '#{temporality.gsub(" ","").split('-')[1]}'" if temporality.include?("Anual") })"
      end
      data_where = data_where + " AND d.declaration_id is not null" if ['with', 'editable'].include?(part)
      data_where = data_where + " AND d.declaration_id is null" if ['without'].include?(part)

      sqldata= "select
      COALESCE(CAST(p.personal_code as varchar), '-') as \"Identificador\",
      COALESCE(CAST(to_char(jl.updated_at,'dd/MM/yyyy HH24:MI') as varchar),'-') as \"Fecha de actualización\",
      COALESCE(p.last_name,'-')  as \"Apellidos\",
      COALESCE(p.name,'-')  as \"Nombre\",
      CASE jl.person_type WHEN 'director' THEN 'Personal directivo'
              WHEN 'public_worker' THEN 'Personal funcionario'
              WHEN 'temporary_worker' THEN 'Personal eventual'
              WHEN 'spokesperson' THEN 'Vocal vecino' ELSE 'otro' end as \"Tipo persona\",
      COALESCE((select CASE WHEN a.end_date IS NULL THEN 'Activo'
              WHEN  a.end_date IS NOT NULL AND a.end_date > current_date - interval '#{year_interval} year'
              THEN 'Cesado visible'
              ELSE 'Cesado no visible' end from appointments a where a.id = jl.appointment_id),'Cesado no visible')  as \"Estado\",
      (select COALESCE(UPPER(TRIM(a.position)),'-')  from appointments a where a.id = cc.appointment_id) as \"Cargo\",
      (select COALESCE(UPPER(TRIM(a.position_alt)),'-')  from appointments a where a.id = cc.appointment_id) as \"Otro Cargo\",
      (select COALESCE(UPPER(TRIM(a.unit)),'-')  from appointments a where a.id = cc.appointment_id) as \"Unidad\",
      (select COALESCE(UPPER(TRIM(a.area)),'-')  from appointments a where a.id = cc.appointment_id) as \"Área de Gobierno\",
      (select COALESCE(CAST(to_char(a.start_date,'dd/MM/yyyy') as varchar),'-')  from appointments a where a.id = cc.appointment_id) as \"Fecha de alta\",
      (select COALESCE(CAST(to_char(a.end_date,'dd/MM/yyyy') as varchar),'-')  from appointments a where a.id = cc.appointment_id) as \"Fecha de cese\",
      COALESCE(c.name,'-') as \"#{Person.human_attribute_name(:corporation)}\",
      COALESCE((Select el.name from electoral_lists el where el.id=cc.electoral_list_id),'-') as \"#{Person.human_attribute_name(:party)}\",
      CASE WHEN d.declaration_id is NULL THEN 'No se ha realizado' ELSE 'Realizada' END as \"Estado obligación\",
      (select COALESCE(p2.email,'-')  from profiles p2  where jl.person_id=p.id and p.profile_id=p2.id) as \"Email\",
      case when d.declaration_type ='AssetsDeclaration'
      then (select COALESCE(CAST(to_char(ad.declaration_date ,'dd/MM/yyyy HH24:MI') as varchar),'-') from assets_declarations ad where ad.id =d.declaration_id)
      when d.declaration_type ='ActivitiesDeclaration'
      then (select COALESCE(CAST(to_char(ad.declaration_date ,'dd/MM/yyyy HH24:MI') as varchar),'-') from activities_declarations ad where ad.id =d.declaration_id)
      else '-' end as \"Fecha de la declaración#{" modificación" if editable.to_s == "true"}\",
      COALESCE(p.document_nif,'-')  as \"Número de documento\"
      from duties d,councillors_corporations cc, corporations c, job_levels jl, people p  where
      d.person_level_type = 'CouncillorsCorporation'
      and d.person_level_id =cc.id
      and c.id=cc.corporation_id
      and c.id = #{corporation}
      and cc.job_level_id=jl.id
      and jl.person_id = p.id
      #{"and d.type_duty = '#{declaration}'" if !declaration.blank?}
      #{"#{data_where}" if !data_where.blank?}
      ORDER BY p.last_name,p.name"


      conn = ActiveRecord::Base.connection.raw_connection
      conn.copy_data "COPY (#{sqldata}) TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
        while row = conn.get_copy_data
          yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        end
      end

    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    #####################################################################################################################
    ## DIRECTORS
    #####################################################################################################################
    def stream_query_rows_directors(active, declaration, temporality, part, editable = nil)
      preference_interval = DataPreference.find_by(:code => "public_year")
      year_interval= preference_interval.blank? ? 2 : preference_interval.type_data.to_i==0 ? preference_interval.content_data.to_i : 2
      data_where = ''

      if !editable.blank? && editable.to_s == "true"
        data_where = data_where + " AND (((d.declaration_type ='AssetsDeclaration' AND d.declaration_id in (select ad.id from assets_declarations ad where ad.editable=true)) OR (d.declaration_type ='ActivitiesDeclaration' AND d.declaration_id in (select ad.id from activities_declarations ad where ad.editable=true))))"
      else
        data_where = data_where + " AND (d.declaration_id is null or ((d.declaration_type ='AssetsDeclaration' AND d.declaration_id not in (select ad.id from assets_declarations ad where ad.editable=true)) OR (d.declaration_type ='ActivitiesDeclaration' AND d.declaration_id not in (select ad.id from activities_declarations ad where ad.editable=true))))"
      end

      if editable.blank? || editable.to_s != "true"
        if !active.blank? && active.to_s=="true"
          data_where = data_where + " AND (jl.appointment_id in (select  a.id from appointments a where a.end_date is null or cast(a.end_date as date) > cast(NOW() as date)) or jl.appointment_id is null)"
        else
          data_where = data_where + " AND (jl.appointment_id not in (select  a.id from appointments a where a.end_date is null or cast(a.end_date as date) > cast(NOW() as date)) and jl.appointment_id is not null)"
        end
      end

      if !temporality.blank?
        data_where = data_where + " AND (d.temporality_id in (select t.id from temporalities t where t.name = '#{temporality.include?("Anual") ? 'Anual'.strip : temporality.strip}') #{"AND d.year = '#{temporality.gsub(" ","").split('-')[1]}'" if temporality.include?("Anual") })"
      end
      data_where = data_where + " AND d.declaration_id is not null" if ['with', 'editable'].include?(part)
      data_where = data_where + " AND d.declaration_id is null" if ['without'].include?(part)

      sqldata= "select
      COALESCE(CAST(p.personal_code as varchar), '-') as \"Identificador\",
      COALESCE(CAST(to_char(jl.updated_at,'dd/MM/yyyy HH24:MI') as varchar),'-') as \"Fecha de actualización\",
      COALESCE(p.last_name,'-')  as \"Apellidos\",
      COALESCE(p.name,'-')  as \"Nombre\",
      CASE jl.person_type WHEN 'director' THEN 'Personal directivo'
              WHEN 'public_worker' THEN 'Personal funcionario'
              WHEN 'temporary_worker' THEN 'Personal eventual'
              WHEN 'spokesperson' THEN 'Vocal vecino' ELSE 'otro' end as \"Tipo persona\",
      COALESCE((select CASE WHEN a.end_date IS NULL THEN 'Activo'
              WHEN  a.end_date IS NOT NULL AND jl.person_type='director' AND a.end_date > current_date - interval '#{year_interval} year'
              THEN 'Cesado visible'
              ELSE 'Cesado no visible' end from appointments a where a.id = jl.appointment_id),'Cesado no visible')  as \"Estado\",
      (select COALESCE(UPPER(TRIM(a.position)),'-')  from appointments a where a.id = jl.appointment_id) as \"Cargo\",
      (select COALESCE(UPPER(TRIM(a.position_alt)),'-')  from appointments a where a.id = jl.appointment_id) as \"Otro Cargo\",
      (select COALESCE(UPPER(TRIM(a.unit)),'-')  from appointments a where a.id = jl.appointment_id) as \"Unidad\",
      (select COALESCE(UPPER(TRIM(a.area)),'-')  from appointments a where a.id = jl.appointment_id) as \"Área de Gobierno\",
      (select COALESCE(CAST(to_char(a.start_date,'dd/MM/yyyy') as varchar),'-')  from appointments a where a.id = jl.appointment_id) as \"Fecha de alta\",
      (select COALESCE(CAST(to_char(a.end_date,'dd/MM/yyyy') as varchar),'-')  from appointments a where a.id = jl.appointment_id) as \"Fecha de cese\",
      CASE WHEN d.declaration_id is NULL THEN 'No se ha realizado' ELSE 'Realizada' END as \"Estado obligación\",
      (select COALESCE(p2.email,'-')  from profiles p2  where jl.person_id=p.id and p.profile_id=p2.id) as \"Email\",
      case when d.declaration_type ='AssetsDeclaration'
      then (select COALESCE(CAST(to_char(ad.declaration_date ,'dd/MM/yyyy HH24:MI') as varchar),'-') from assets_declarations ad where ad.id =d.declaration_id)
      when d.declaration_type ='ActivitiesDeclaration'
      then (select COALESCE(CAST(to_char(ad.declaration_date ,'dd/MM/yyyy HH24:MI') as varchar),'-') from activities_declarations ad where ad.id =d.declaration_id)
      else '-' end as \"Fecha de la declaración#{" modificación" if editable.to_s == "true"}\",
      COALESCE(p.document_nif,'-')  as \"Número de documento\"
      from duties d, job_levels jl, people p  where d.person_level_type = 'JobLevel'
      and d.person_level_id =jl.id
      and jl.person_id = p.id
      #{"and d.type_duty = '#{declaration}'" if !declaration.blank?}
      #{"#{data_where}" if !data_where.blank?}
      ORDER BY p.last_name,p.name"

      conn = ActiveRecord::Base.connection.raw_connection
      conn.copy_data "COPY (#{sqldata}) TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
        while row = conn.get_copy_data
          yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end


    #####################################################################################################################
    ## PERFILES
    #####################################################################################################################
    def stream_query_rows_statistics(section, person, part)
      data_where = ''
      header = ''

      data_where = data_where + " AND job_levels.person_type = '#{person}'"  if person != 'total' && person != 'councillor'
      data_where = data_where + " AND profiles.cumplimented = true" if part == 'profile'
      data_where = data_where + " AND (profiles.cumplimented = false OR  profiles.cumplimented IS NULL)" if part == 'unprofile'

      preference_interval = DataPreference.find_by(:code => "public_year")
      year_interval= preference_interval.blank? ? 2 : preference_interval.type_data.to_i==0 ? preference_interval.content_data.to_i : 2


      common_query = "COALESCE(CAST(people.personal_code as varchar), '-') as \"#{Person.human_attribute_name(:personal_code)}\",
        COALESCE(CAST(to_char(job_levels.updated_at,'yyyy-MM-dd HH24:MI') as varchar),'-') as \"#{Person.human_attribute_name(:updated_at)}\",
        TRIM(from UPPER(people.last_name)) as \"#{Person.human_attribute_name(:last_name)}\",
        TRIM(from UPPER(people.name)) as \"#{Person.human_attribute_name(:name)}\",
        COALESCE(CAST((select to_char(profiles.updated_at,'yyyy-MM-dd') from profiles where profiles.id=people.profile_id) as varchar),'-')  as \"#{Person.human_attribute_name(:updated_profile_date)}\",
        COALESCE(CAST((select to_char(profiles.updated_at,'HH24:MI') from profiles where profiles.id=people.profile_id) as varchar),'-')  as \"#{Person.human_attribute_name(:updated_profile_time)}\","

      common_query_appointment_generic= "TRIM(from UPPER(COALESCE(appointments.position,'-'))) as \"#{Person.human_attribute_name(:role)}\",
        TRIM(from UPPER(COALESCE(appointments.position_alt,'-'))) as \"#{Person.human_attribute_name(:position_alt)}\",
        TRIM(from UPPER(COALESCE(appointments.unit,'-'))) as \"#{Person.human_attribute_name(:unit)}\",
        TRIM(from UPPER(COALESCE(appointments.area,'-'))) as \"#{Person.human_attribute_name(:area)}\","

      common_query_end_generic = "COALESCE((select profiles.email from profiles where profiles.id=people.profile_id),'-')  as \"#{Person.human_attribute_name(:email)}\",
        COALESCE(people.document_nif,'-')  as \"#{Person.human_attribute_name(:document_nif)}\""


      conn = ActiveRecord::Base.connection.raw_connection
      if person != 'councillor'
        conn.copy_data "COPY (Select distinct
          #{common_query}
          CASE job_levels.person_type WHEN 'director' THEN 'Personal directivo'
            WHEN 'public_worker' THEN 'Personal funcionario'
            WHEN 'temporary_worker' THEN 'Personal eventual'
            WHEN 'spokesperson' THEN 'Vocal vecino' ELSE 'otro' END as \"#{Person.human_attribute_name(:job_level)}\",
          CASE WHEN appointments.end_date IS NULL THEN 'Activo'
            WHEN  appointments.end_date IS NOT NULL AND job_levels.person_type='director' AND appointments.end_date > current_date - interval '#{year_interval} year' THEN 'Cesado visible'
            ELSE 'Cesado no visible' END  as \"#{I18n.t("admin.people.headers.status")}\",
          #{common_query_appointment_generic}
          COALESCE('-') as \"#{Person.human_attribute_name(:corporation)}\",
          COALESCE('-') as \"#{Person.human_attribute_name(:party)}\",
          COALESCE(CAST(appointments.start_date as varchar),'-') as \"#{Person.human_attribute_name(:starting_date)}\",
          COALESCE(CAST(appointments.end_date as varchar),'-')  as \"#{Person.human_attribute_name(:leaving_date)}\",
          #{common_query_end_generic}
          from people, job_levels, appointments, profiles
          where people.id = job_levels.person_id
          AND job_levels.person_type!='councillor'
          AND (appointments.id=job_levels.appointment_id AND appointments.end_date is null OR job_levels.appointment_id is null)
          AND (people.profile_id=profiles.id)
          #{"#{data_where}" unless data_where.blank?})
          TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
          while row = conn.get_copy_data
            yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
          end
        end
      end

      if person == 'councillor' || person == 'total'
        conn.copy_data "COPY (Select distinct
          #{common_query}
          'Concejal' as \"#{Person.human_attribute_name(:job_level)}\",
          CASE WHEN appointments.end_date IS NULL THEN 'Activo'
          WHEN appointments.end_date > current_date - interval '#{year_interval} year' THEN 'Cesado visible'
          ELSE 'Cesado no visible' END  as \"#{I18n.t("admin.people.headers.status")}\",
          #{common_query_appointment_generic}
          COALESCE(CASE job_levels.person_type WHEN 'councillor' THEN corporations.name ELSE '-' END,'-') as \"#{Person.human_attribute_name(:corporation)}\",
          COALESCE((Select electoral_lists.name from electoral_lists where electoral_lists.id=councillors_corporations.electoral_list_id),'-') as \"#{Person.human_attribute_name(:party)}\",
          COALESCE(CAST(appointments.start_date as varchar),'-') as \"#{Person.human_attribute_name(:starting_date)}\",
          COALESCE(CAST(appointments.end_date as varchar),'-')  as \"#{Person.human_attribute_name(:leaving_date)}\",
          #{common_query_end_generic}
          from people, job_levels, councillors_corporations, corporations, appointments, profiles
          where  people.id = job_levels.person_id
          AND job_levels.person_type='councillor'
          AND job_levels.id=councillors_corporations.job_level_id
          AND councillors_corporations.corporation_id=corporations.id
          AND (people.profile_id=profiles.id)
          AND (appointments.id=councillors_corporations.appointment_id AND appointments.end_date is null OR councillors_corporations.appointment_id is null)
          #{"#{data_where}" unless data_where.blank?} )
          TO STDOUT (delimiter ';', FORMAT CSV#{ ",HEADER" if person != 'total'});" do
          while row = conn.get_copy_data
            yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
          end
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  end
end



