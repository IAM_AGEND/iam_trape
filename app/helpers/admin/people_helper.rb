module Admin
  module PeopleHelper

    extend ActiveSupport::Concern
    def headers_files
      [
        Person.human_attribute_name(:updated_at),
        Person.human_attribute_name(:backwards_name),
        Person.human_attribute_name(:updated_profile),
        Person.human_attribute_name(:job_level),
        I18n.t("admin.people.headers.status"),
        Person.human_attribute_name(:role),
        Person.human_attribute_name(:starting_date),
        Person.human_attribute_name(:leaving_date),
        Person.human_attribute_name(:unit),
        # Person.human_attribute_name(:area),
        Person.human_attribute_name(:corporation),
        # Person.human_attribute_name(:party),
        # Person.human_attribute_name(:initial_declaration_date),
        # Person.human_attribute_name(:annual_declaration_date),
        # Person.human_attribute_name(:final_declaration_date),
        # Person.human_attribute_name(:email),
        # Person.human_attribute_name(:calendar_url),
        # Person.human_attribute_name(:person_url),
        # Person.human_attribute_name(:personal_code),
        # Person.human_attribute_name(:document_nif),
        # Person.human_attribute_name(:sex),
      ]
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def headers_files_export
      year_end = Time.zone.now.year + 1
      preference = DataPreference.find_by(:code => "num_year_declaration")
      rest_aux= preference.blank? ? 5 : preference.type_data.to_i==0 ? preference.content_data.to_i : 5
      year_start = year_end - rest_aux

      assets =  (year_start..year_end).map {|x|   I18n.t('person.years_assets_declaration_date',anno: x)}
      activities =  (year_start..year_end).map {|x|    I18n.t('person.years_activities_declaration_date',anno: x)}
      [
        Person.human_attribute_name(:updated_at),
        Person.human_attribute_name(:backwards_name),
        Person.human_attribute_name(:updated_profile),
        Person.human_attribute_name(:job_level),
        I18n.t("admin.people.headers.status"),
        Person.human_attribute_name(:role),
        Person.human_attribute_name(:starting_date),
        Person.human_attribute_name(:leaving_date),
        Person.human_attribute_name(:unit),
        Person.human_attribute_name(:area),
        Person.human_attribute_name(:corporation),
        Person.human_attribute_name(:party),
        Person.human_attribute_name(:init_assets_declaration_date)]+assets+
        [Person.human_attribute_name(:final_assets_declaration_date),
        Person.human_attribute_name(:init_activities_declaration_date)]+activities+
        [Person.human_attribute_name(:final_activities_declaration_date),
        Person.human_attribute_name(:email),
        Person.human_attribute_name(:calendar_url),
        Person.human_attribute_name(:person_url),
        Person.human_attribute_name(:personal_code),
        Person.human_attribute_name(:document_nif),
        Person.human_attribute_name(:sex),
      ]
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def headers_files_not
      [ Person.human_attribute_name(:job_level) ]
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def stream_query_rows(sql_query=nil)
      data_where_councillors = sql_query.blank? ? nil :  sql_query.select{|o| !o[:councillor_id].blank? }.map {|o| o[:councillor_id].blank? ? "" : o[:councillor_id]}
      data_where_jobs = sql_query.blank? ? nil :  sql_query.select{|o| o[:councillor_id].blank? }.map {|o| o[:councillor_id].blank? ? o[:job_id] : ""}
      assets = ""
      activities = ""
      assets_councillor = ""
      activities_councillor = ""

      year_end = Time.zone.now.year + 1
      preference = DataPreference.find_by(:code => "num_year_declaration")
      rest_aux= preference.blank? ? 5 : preference.type_data.to_i==0 ? preference.content_data.to_i : 5

      preference_interval = DataPreference.find_by(:code => "public_year")
      year_interval= preference_interval.blank? ? 2 : preference_interval.type_data.to_i==0 ? preference_interval.content_data.to_i : 2

      year_start = year_end - rest_aux
      (year_start..year_end).each do |x|
        assets = assets + "COALESCE(CAST(
          (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=job_levels.id
           AND assets_declarations.person_level_type='JobLevel' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Anual'
           AND assets_declarations.year_period = #{x})
           as varchar),'-') as \"#{I18n.t('person.years_assets_declaration_date',anno: x)}\","
        activities = activities + "COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=job_levels.id
             AND activities_declarations.person_level_type='JobLevel' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Anual'
             AND activities_declarations.year_period = #{x})
             as varchar),'-') as \"#{I18n.t('person.years_activities_declaration_date',anno: x)}\","
        assets_councillor = assets_councillor + "COALESCE(CAST(
          (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=councillors_corporations.id
           AND assets_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Anual'
           AND assets_declarations.year_period = #{x})
           as varchar),'-') as \"#{I18n.t('person.years_assets_declaration_date',anno: x)}\","
        activities_councillor = activities_councillor + "COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=councillors_corporations.id
             AND activities_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Anual'
             AND activities_declarations.year_period = #{x})
             as varchar),'-') as \"#{I18n.t('person.years_activities_declaration_date',anno: x)}\","
      end

      common_query = "COALESCE(CAST(people.personal_code as varchar), '-') as \"#{Person.human_attribute_name(:personal_code)}\",
        COALESCE(CAST(to_char(job_levels.updated_at,'yyyy-MM-dd HH24:MI') as varchar),'-') as \"#{Person.human_attribute_name(:updated_at)}\",
        TRIM(from UPPER(people.last_name)) as \"#{Person.human_attribute_name(:last_name)}\",
        TRIM(from UPPER(people.name)) as \"#{Person.human_attribute_name(:name)}\",
        COALESCE(CAST((select to_char(profiles.updated_at,'yyyy-MM-dd') from profiles where profiles.id=people.profile_id) as varchar),'-')  as \"#{Person.human_attribute_name(:updated_profile_date)}\",
        COALESCE(CAST((select to_char(profiles.updated_at,'HH24:MI') from profiles where profiles.id=people.profile_id) as varchar),'-')  as \"#{Person.human_attribute_name(:updated_profile_time)}\","

      common_query_appointment = "TRIM(from UPPER(COALESCE(appointments.position,'-'))) as \"#{Person.human_attribute_name(:role)}\",
        TRIM(from UPPER(COALESCE(appointments.position_alt,'-'))) as \"#{Person.human_attribute_name(:position_alt)}\",
        TRIM(from UPPER(COALESCE(appointments.unit,'-'))) as \"#{Person.human_attribute_name(:unit)}\",
        TRIM(from UPPER(COALESCE(appointments.area,'-'))) as \"#{Person.human_attribute_name(:area)}\",
        COALESCE(cast(appointments.retribution as varchar),'-') as \"#{Person.human_attribute_name(:retribution)}\",
        COALESCE(cast(appointments.retribution_year as varchar),'-') as \"#{Person.human_attribute_name(:retribution_year)}\",
        COALESCE(cast(appointments.observations as varchar),'-') as \"#{Person.human_attribute_name(:observations)}\","

      common_query_end = "COALESCE((select profiles.email from profiles where profiles.id=people.profile_id),'-')  as \"#{Person.human_attribute_name(:email)}\",
        COALESCE((select profiles.calendar_url from profiles where profiles.id=people.profile_id),'-')  as \"#{Person.human_attribute_name(:calendar_url)}\",
        COALESCE(job_levels.slug,'-')  as \"#{Person.human_attribute_name(:person_url)}\",
        COALESCE(people.document_nif,'-')  as \"#{Person.human_attribute_name(:document_nif)}\",
        CASE people.sex WHEN 'H' THEN 'Hombre' WHEN 'M' THEN 'Mujer' ELSE '-' END as \"#{Person.human_attribute_name(:sex)}\""

        conn = ActiveRecord::Base.connection.raw_connection
      unless data_where_jobs.blank?
        conn.copy_data "COPY (Select distinct
          #{common_query}
          CASE job_levels.person_type WHEN 'director' THEN 'Personal directivo'
            WHEN 'public_worker' THEN 'Personal funcionario'
            WHEN 'temporary_worker' THEN 'Personal eventual'
            WHEN 'spokesperson' THEN 'Vocal vecino' ELSE 'otro' END as \"#{Person.human_attribute_name(:job_level)}\",
          CASE WHEN appointments.end_date IS NULL THEN 'Activo'
            WHEN  appointments.end_date IS NOT NULL AND job_levels.person_type='director' AND appointments.end_date > current_date - interval '#{year_interval} year' THEN 'Cesado visible'
            ELSE 'Cesado no visible' END  as \"#{I18n.t("admin.people.headers.status")}\",
          #{common_query_appointment}
          COALESCE((select STRING_AGG(charges.charge, ' / ') from charges where charges.appointment_id = job_levels.appointment_id),'-') as \"#{Charge.human_attribute_name(:charge)}\",
          COALESCE((select STRING_AGG(commisions.commision, ' / ') from commisions where commisions.appointment_id = job_levels.appointment_id),'-') as \"#{Commision.human_attribute_name(:commision)}\",
          COALESCE('-') as \"#{Person.human_attribute_name(:corporation)}\",
          COALESCE('-') as \"#{Person.human_attribute_name(:party)}\",
          COALESCE(CAST(appointments.start_date as varchar),'-') as \"#{Person.human_attribute_name(:starting_date)}\",
          COALESCE(CAST(appointments.end_date as varchar),'-')  as \"#{Person.human_attribute_name(:leaving_date)}\",
          COALESCE(CAST(
            (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=job_levels.id
            AND assets_declarations.person_level_type='JobLevel' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Inicial')
            as varchar),'-') as \"#{Person.human_attribute_name(:init_assets_declaration_date)}\",
          #{assets}
          COALESCE(CAST(
              (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=job_levels.id
              AND assets_declarations.person_level_type='JobLevel' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Final')
              as varchar),'-') as \"#{Person.human_attribute_name(:final_assets_declaration_date)}\",
          COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=job_levels.id
            AND activities_declarations.person_level_type='JobLevel' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Inicial')
            as varchar),'-') as \"#{Person.human_attribute_name(:init_activities_declaration_date)}\",
          #{activities}
          COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=job_levels.id
            AND activities_declarations.person_level_type='JobLevel' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Final')
            as varchar),'-') as \"#{Person.human_attribute_name(:final_activities_declaration_date)}\",
          #{common_query_end},
          CASE WHEN (Select count(ca.id) from compatibility_activities ca where ca.person_level_type='JobLevel' and ca.person_level_id = job_levels.id and (ca.resolution_date is not null OR (ca.compatibility_description  is not null and TRIM(ca.compatibility_description) != '')
          or (ca.public_private is not null and TRIM(ca.public_private) != ''))) > 0 THEN 'SÍ' ELSE 'NO' END as \"Actividades compatibilidad\",
          CASE WHEN (Select count(ca.id) from termination_authorizations ca where ca.person_level_type='JobLevel' and ca.person_level_id = job_levels.id and (ca.authorization_date is not null OR (ca.authorization_description is not null and TRIM(ca.authorization_description) != ''))) > 0 THEN 'SÍ' ELSE 'NO' END as \"Autorizaciones compatibilidad\"
          from people, job_levels, appointments
          where people.id = job_levels.person_id
          AND job_levels.person_type!='councillor'
          AND (appointments.id=job_levels.appointment_id OR job_levels.appointment_id is null)
          AND job_levels.id in (#{data_where_jobs.to_s.gsub(/\[|\]|\"/, '').gsub('nil','null')}))
          TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
          while row = conn.get_copy_data
            yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
          end
        end
      end

      unless data_where_councillors.blank?
        conn.copy_data "COPY (Select distinct
          #{common_query}
          'Concejal' as \"#{Person.human_attribute_name(:job_level)}\",
          CASE WHEN appointments.end_date IS NULL THEN 'Activo'
          WHEN appointments.end_date > current_date - interval '#{year_interval} year' THEN 'Cesado visible'
          ELSE 'Cesado no visible' END  as \"#{I18n.t("admin.people.headers.status")}\",
          #{common_query_appointment}
          COALESCE((select STRING_AGG(charges.charge, ' / ') from charges where charges.appointment_id = councillors_corporations.appointment_id),'-') as \"#{Charge.human_attribute_name(:charge)}\",
          COALESCE((select STRING_AGG(commisions.commision, ' / ') from commisions where commisions.appointment_id = councillors_corporations.appointment_id),'-') as \"#{Commision.human_attribute_name(:commision)}\",
          COALESCE(CASE job_levels.person_type WHEN 'councillor' THEN corporations.name ELSE '-' END,'-') as \"#{Person.human_attribute_name(:corporation)}\",
          COALESCE((Select electoral_lists.name from electoral_lists where electoral_lists.id=councillors_corporations.electoral_list_id),'-') as \"#{Person.human_attribute_name(:party)}\",
          COALESCE(CAST(appointments.start_date as varchar),'-') as \"#{Person.human_attribute_name(:starting_date)}\",
          COALESCE(CAST(appointments.end_date as varchar),'-')  as \"#{Person.human_attribute_name(:leaving_date)}\",
          COALESCE(CAST(
            (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=councillors_corporations.id
            AND assets_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Inicial')
            as varchar),'-') as \"#{Person.human_attribute_name(:init_assets_declaration_date)}\",
          #{assets_councillor}
          COALESCE(CAST(
              (SELECT MAX(assets_declarations.declaration_date) from assets_declarations, temporalities where person_level_id=councillors_corporations.id
              AND assets_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=assets_declarations.temporality_id AND temporalities.name='Final')
              as varchar),'-') as \"#{Person.human_attribute_name(:final_assets_declaration_date)}\",
          COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=councillors_corporations.id
            AND activities_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Inicial')
            as varchar),'-') as \"#{Person.human_attribute_name(:init_activities_declaration_date)}\",
          #{activities_councillor}
          COALESCE(CAST(
            (SELECT MAX(activities_declarations.declaration_date) from activities_declarations, temporalities where person_level_id=councillors_corporations.id
            AND activities_declarations.person_level_type='CouncillorsCorporation' AND temporalities.id=activities_declarations.temporality_id AND temporalities.name='Final')
            as varchar),'-') as \"#{Person.human_attribute_name(:final_activities_declaration_date)}\",
          #{common_query_end},
          CASE WHEN (Select count(ca.id) from compatibility_activities ca where ca.person_level_type='CouncillorsCorporation' and ca.person_level_id = councillors_corporations.id and (ca.resolution_date is not null OR (ca.compatibility_description  is not null and TRIM(ca.compatibility_description) != '')
          or (ca.public_private is not null and TRIM(ca.public_private) != ''))) > 0 THEN 'SÍ' ELSE 'NO' END as \"Actividades compatibilidad\",
          CASE WHEN (Select count(ca.id) from termination_authorizations ca where ca.person_level_type='CouncillorsCorporation' and ca.person_level_id = councillors_corporations.id and (ca.authorization_date is not null OR (ca.authorization_description is not null and TRIM(ca.authorization_description) != ''))) > 0 THEN 'SÍ' ELSE 'NO' END as \"Autorizaciones compatibilidad\"
          from people, job_levels, councillors_corporations, corporations, appointments
          where  people.id = job_levels.person_id AND job_levels.person_type='councillor' AND
          job_levels.id=councillors_corporations.job_level_id AND councillors_corporations.corporation_id=corporations.id
          AND (appointments.id=councillors_corporations.appointment_id OR councillors_corporations.appointment_id is null)
          AND councillors_corporations.id in (#{data_where_councillors.to_s.gsub(/\[|\]|\"/, '')})  )
          TO STDOUT (delimiter ';', FORMAT CSV #{", HEADER" if data_where_jobs.blank?});" do
          while row = conn.get_copy_data
            yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
          end
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def validates_filter_councillor(councillor, params_aux)
      return true if councillor.blank?
      value_aux = true

      if !params_aux["status"].blank? && params_aux["status"].to_s !=  I18n.t("people.status.active") && councillor.try(:appointment).try(:end_date).blank?
        return false
      elsif !params_aux["status"].blank? && params_aux["status"].to_s ==  I18n.t("people.status.active") && !councillor.try(:appointment).try(:end_date).blank?
        return false
      end

      return false if !params_aux["corporation"].blank? && councillor.corporation_id != params_aux["corporation"].to_i
      return false if !params_aux["area"].blank? && !councillor.try(:appointment).try(:position).try{|p| p.mb_chars.upcase.include? params_aux["area"].mb_chars.upcase}

      value_aux
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def activities_declarations_tab_name(declaration)
      if declaration.try(:temporality)
        declaration.try(:editable) ? "#{ActivitiesDeclaration.human_attribute_name(:editable)} - #{declaration.try(:declaration_date).try {|x| x.strftime('%d/%m/%Y')}} - #{declaration.period_name}" : declaration.period_name
      else
        t('activities_declarations.add')
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def assets_declarations_tab_name(declaration)
      if declaration.try(:temporality)
        declaration.try(:editable) ? "#{AssetsDeclaration.human_attribute_name(:editable)} - #{declaration.try(:declaration_date).try {|x| x.strftime('%d/%m/%Y')}} - #{declaration.period_name}" : declaration.period_name
      else
        t('assets_declarations.add')
      end
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_collection_mounths
      (1..12).map {|m| [I18n.l(DateTime.parse(Date::MONTHNAMES[m]), format: "%B").capitalize,m]}
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_real_estate_kind_collect
      [:urban, :rustic].map {|c| [I18n.t("real_estate_properties.kind_collect.#{c.to_s}"), I18n.t("real_estate_properties.kind_collect.#{c.to_s}")]}
        .sort_by {|x| x[0]}.to_h
    rescue => e
      begin
        Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_real_estate_straight_type_collect
      [:administrative_concession, :real_right,:timeshare, :nuda_property, :full_control]
        .map {|c| [I18n.t("real_estate_properties.straight_type_collect.#{c.to_s}"), I18n.t("real_estate_properties.straight_type_collect.#{c.to_s}")]}
        .sort_by {|x| x[0]}.to_h
    rescue => e
      begin
        Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_real_estate_adquisition_title_collect
      [:buy_and_sell, :donation,:heritage, :others]
        .map {|c| [I18n.t("real_estate_properties.adquisition_title_collect.#{c.to_s}"), I18n.t("real_estate_properties.adquisition_title_collect.#{c.to_s}")]}
        .sort_by {|x| x[0]}.to_h
    rescue => e
      begin
        Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_account_kind_collect
      [:deposit_checking, :deposit_savings,:other_deposit]
        .map {|c| [I18n.t("account_deposits.kind_collect.#{c.to_s}"), I18n.t("account_deposits.kind_collect.#{c.to_s}")]}
        .sort_by {|x| x[0]}.to_h
    rescue => e
      begin
        Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_other_types_collect
      [:actions, :bonus,:public_debt, :obligations, :other_assets, :pension_plan, :temporary_income, :annuities, :life_insurances]
        .map {|c| [I18n.t("other_deposits.types_collect.#{c.to_s}"), I18n.t("other_deposits.types_collect.#{c.to_s}")]}
        .sort_by {|x| x[0]}.to_h
    rescue => e
      begin
        Rails.logger.error("COD-00013: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_sex_type_collect
      [:man, :woman].map {|s| [I18n.t("#{s}.name"),I18n.t("#{s}.value")]}
    rescue => e
      begin
        Rails.logger.error("COD-00014: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def element_display?(element = nil)
      return "" if element.blank?
      element.try(:count) == 0 ? "" : "display:none"
    rescue => e
      begin
        Rails.logger.error("COD-00015: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  end
end
