module Admin
    module ActivitiesHelper
        extend ActiveSupport::Concern
        def stream_query_rows(params, idsheet)

            if params[:period] == "Inicial"
                year = Corporation.find(params[:corporation]).start_year
            elsif params[:period] == "Final"
                year = Corporation.find(params[:corporation]).end_year
            else
                year = params[:period]
            end 
            activities = []
            ActivitiesDeclaration.joins(:temporality)
                .where("temporalities.name = ? AND (activities_declarations.year_period= ? OR activities_declarations.year_period is null) ", params[:period], year)
                .find_each do |activity|
                if activity.should_display_declarations?
                activities.push(activity.id.to_i)
                end
            end
            
            conn = ActiveRecord::Base.connection.raw_connection
            conn.copy_data "COPY (#{get_sql_sheet(params,idsheet, activities) })
            TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
                while row = conn.get_copy_data
                    yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
                end
            end
        rescue => e
            begin
              Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end


        def get_sql_sheet(params,sheet=nil, ids) 
            return '' if sheet.blank? || params.blank?

            case sheet.to_i
            when 1
                personal_data(params, ids)   
            when 2
                public_activities(params, ids)
            when 3
                private_activities(params, ids)   
            when 4
                other_activities(params, ids)
            end
        rescue => e
            begin
              Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def personal_data(params, ids)
            
            export_type=params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"
            sql_data_person = "FROM people, job_levels #{", councillors_corporations" if params[:type_people].to_s=="councillor"}
                WHERE people.id = job_levels.person_id  AND job_levels.person_type='#{params[:type_people]}'
                #{"AND councillors_corporations.job_level_id=job_levels.id" if params[:type_people].to_s=="councillor" }
                AND activities_declarations.person_level_id = #{ params[:type_people].to_s=="director" ? "job_levels.id" : "councillors_corporations.id"}
                AND activities_declarations.person_level_type=#{export_type}"

            sql_data_appointment = "FROM appointments, #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}
                WHERE activities_declarations.person_level_id = #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}.id 
                AND activities_declarations.person_level_type = #{params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"}
                AND appointments.id = #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}.appointment_id"

            "Select distinct 'Declaración de actividades' as \"#{I18n.t("activities_exporter.sheet1_attr.type")}\",
                CASE temporalities.name WHEN 'Anual' THEN CAST(activities_declarations.year_period as varchar)
                    ELSE temporalities.name END as \"#{I18n.t("activities_exporter.sheet1_attr.period_name")}\",
                COALESCE(to_char(activities_declarations.declaration_date,'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("activities_exporter.sheet1_attr.declaration_date")}\",
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("activities_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(CAST((Select distinct UPPER(SPLIT_PART(people.last_name,' ',1)) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("activities_exporter.sheet1_attr.person_first_last_name")}\",
                COALESCE(CAST((Select distinct UPPER(SPLIT_PART(people.last_name,' ',2)) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("activities_exporter.sheet1_attr.person_second_last_name")}\",
                COALESCE(CAST((Select distinct UPPER(people.name) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("activities_exporter.sheet1_attr.person_fist_name")}\",
                COALESCE(to_char((Select distinct appointments.start_date #{sql_data_appointment}),'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("activities_exporter.sheet1_attr.appointment_start_date")}\",
                COALESCE(to_char((Select distinct appointments.end_date #{sql_data_appointment}),'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("activities_exporter.sheet1_attr.appointment_end_date")}\"
            FROM activities_declarations, temporalities
            WHERE #{"activities_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = activities_declarations.temporality_id 
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND activities_declarations.year_period=#{params[:period]}"}
            #{ "AND activities_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND activities_declarations.person_level_type=#{export_type}" 
        rescue => e
            begin
              Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
        
        def public_activities(params, ids)

            export_type=params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"
            sql_data_person = "FROM people, job_levels #{", councillors_corporations" if params[:type_people].to_s=="councillor"}
                WHERE people.id = job_levels.person_id  AND job_levels.person_type='#{params[:type_people]}'
                #{"AND councillors_corporations.job_level_id=job_levels.id" if params[:type_people].to_s=="councillor" }
                AND activities_declarations.person_level_id = #{ params[:type_people].to_s=="director" ? "job_levels.id" : "councillors_corporations.id"}
                AND activities_declarations.person_level_type=#{export_type}"

            "Select distinct 
                CASE temporalities.name WHEN 'Anual' THEN CAST(activities_declarations.year_period as varchar)
                    ELSE temporalities.name END as \"#{I18n.t("activities_exporter.sheet1_attr.period_name")}\",
                COALESCE(public_activities.entity,'-') 
                    as \"#{I18n.t("activities_exporter.sheet2_attr.entity")}\",
                COALESCE(public_activities.position,'-') 
                    as \"#{I18n.t("activities_exporter.sheet2_attr.position")}\",
                COALESCE(public_activities.start_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet2_attr.start_date")}\",
                COALESCE(public_activities.end_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet2_attr.end_date")}\"
            FROM public_activities, activities_declarations, temporalities
            WHERE #{"activities_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = activities_declarations.temporality_id 
            AND public_activities.activities_declaration_id = activities_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND activities_declarations.year_period=#{params[:period]}"}
            #{ "AND activities_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND activities_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def private_activities(params, ids)
        
            export_type=params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"
            sql_data_person = "FROM people, job_levels #{", councillors_corporations" if params[:type_people].to_s=="councillor"}
                WHERE people.id = job_levels.person_id  AND job_levels.person_type='#{params[:type_people]}'
                #{"AND councillors_corporations.job_level_id=job_levels.id" if params[:type_people].to_s=="councillor" }
                AND activities_declarations.person_level_id = #{ params[:type_people].to_s=="director" ? "job_levels.id" : "councillors_corporations.id"}
                AND activities_declarations.person_level_type=#{export_type}"

            "Select distinct 
                CASE temporalities.name WHEN 'Anual' THEN CAST(activities_declarations.year_period as varchar)
                    ELSE temporalities.name END as \"#{I18n.t("activities_exporter.sheet1_attr.period_name")}\",
                COALESCE(private_activities.private_activity_type,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.private_activity_type")}\",
                COALESCE(private_activities.description,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.description")}\",
                COALESCE(private_activities.entity,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.entity")}\",
                COALESCE(private_activities.position,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.position")}\",
                COALESCE(private_activities.start_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.start_date")}\",
                COALESCE(private_activities.end_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.end_date")}\"
            FROM private_activities, activities_declarations, temporalities
            WHERE #{"activities_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = activities_declarations.temporality_id 
            AND private_activities.activities_declaration_id = activities_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND activities_declarations.year_period=#{params[:period]}"}
            #{ "AND activities_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND activities_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def other_activities(params, ids)

            export_type=params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"
            sql_data_person = "FROM people, job_levels #{", councillors_corporations" if params[:type_people].to_s=="councillor"}
                WHERE people.id = job_levels.person_id  AND job_levels.person_type='#{params[:type_people]}'
                #{"AND councillors_corporations.job_level_id=job_levels.id" if params[:type_people].to_s=="councillor" }
                AND activities_declarations.person_level_id = #{ params[:type_people].to_s=="director" ? "job_levels.id" : "councillors_corporations.id"}
                AND activities_declarations.person_level_type=#{export_type}"

            "Select distinct 
                CASE temporalities.name WHEN 'Anual' THEN CAST(activities_declarations.year_period as varchar)
                    ELSE temporalities.name END as \"#{I18n.t("activities_exporter.sheet1_attr.period_name")}\",
                COALESCE(other_activities.description,'-') 
                    as \"#{I18n.t("activities_exporter.sheet4_attr.description")}\",
                COALESCE(other_activities.start_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.start_date")}\",
                COALESCE(other_activities.end_date,'-') 
                    as \"#{I18n.t("activities_exporter.sheet3_attr.end_date")}\"
            FROM other_activities, activities_declarations, temporalities
            WHERE #{"activities_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = activities_declarations.temporality_id 
            AND other_activities.activities_declaration_id = activities_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND activities_declarations.year_period=#{params[:period]}"}
            #{ "AND activities_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND activities_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
    end
end