module Admin
    module AssetsHelper
        
        extend ActiveSupport::Concern
        def stream_query_rows(params, idsheet)
            export_type=params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"
            sql_data_person = "FROM people, job_levels #{", councillors_corporations" if params[:type_people].to_s=="councillor"}
                WHERE people.id = job_levels.person_id  AND job_levels.person_type='#{params[:type_people]}'
                #{"AND councillors_corporations.job_level_id=job_levels.id" if params[:type_people].to_s=="councillor" }
                AND assets_declarations.person_level_id = #{ params[:type_people].to_s=="director" ? "job_levels.id" : "councillors_corporations.id"}
                AND assets_declarations.person_level_type=#{export_type}"

            if params[:period] == "Inicial"
                year = Corporation.find(params[:corporation]).start_year
            elsif params[:period] == "Final"
                year = Corporation.find(params[:corporation]).end_year
            else
                year = params[:period]
            end 
            
            assets = []
            AssetsDeclaration.joins(:temporality)
                .where("temporalities.name = ? AND (assets_declarations.year_period= ? OR assets_declarations.year_period is null) ", params[:period], year)
                .find_each do |asset|
                    if asset.should_display_declarations?
                        assets.push(asset.id.to_i)
                    end
            end

            conn = ActiveRecord::Base.connection.raw_connection
            conn.copy_data "COPY (#{get_sql_sheet(params,idsheet, export_type, sql_data_person, assets) })
            TO STDOUT (delimiter ';', FORMAT CSV,HEADER);" do
                while row = conn.get_copy_data
                    yield row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
                end
            end
        rescue => e
            begin
              Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end


        def get_sql_sheet(params,sheet=nil, export_type, sql_data_person, ids) 
            return '' if sheet.blank? || params.blank?

            case sheet.to_i
            when 1
                personal_data(params, export_type, sql_data_person, ids)   
            when 2
                real_state_properties(params, export_type, sql_data_person, ids)
            when 3
                account_deposits(params, export_type, sql_data_person, ids)
            when 4
                other_deposits(params, export_type, sql_data_person, ids)
            when 5
                vehicles(params, export_type, sql_data_person, ids) 
            when 6
                other_personal_properties(params, export_type, sql_data_person, ids)
            when 7
                debts(params, export_type, sql_data_person, ids)
            when 8
                tax_datas(params, export_type, sql_data_person, ids)
            end
        rescue => e
            begin
              Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def personal_data(params, export_type, sql_data_person, ids)
            sql_data_appointment = "FROM appointments, #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}
                WHERE assets_declarations.person_level_id = #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}.id 
                AND assets_declarations.person_level_type = #{params[:type_people].to_s=="director" ? "'JobLevel'" : "'CouncillorsCorporation'"}
                AND appointments.id = #{params[:type_people].to_s=="director" ? "job_levels" : "councillors_corporations"}.appointment_id"

            "Select distinct 
                'Declaración de bienes' as \"#{I18n.t("activities_exporter.sheet1_attr.type")}\",
                CASE temporalities.name WHEN 'Anual' THEN CAST(assets_declarations.year_period as varchar)
                    ELSE temporalities.name END as \"#{I18n.t("assets_exporter.sheet1_attr.period_name")}\",
                COALESCE(to_char(assets_declarations.declaration_date,'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("assets_exporter.sheet1_attr.declaration_date")}\",
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(CAST((Select distinct UPPER(SPLIT_PART(people.last_name,' ',1)) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_first_last_name")}\",
                COALESCE(CAST((Select distinct UPPER(SPLIT_PART(people.last_name,' ',2)) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_second_last_name")}\",
                COALESCE(CAST((Select distinct UPPER(people.name) #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_fist_name")}\",
                COALESCE(to_char((Select distinct appointments.start_date #{sql_data_appointment}),'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("activities_exporter.sheet1_attr.appointment_start_date")}\",
                COALESCE(to_char((Select distinct appointments.end_date #{sql_data_appointment}),'dd/MM/yyyy'),'-') 
                    as \"#{I18n.t("activities_exporter.sheet1_attr.appointment_end_date")}\"
            FROM assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}" 
        rescue => e
            begin
              Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def real_state_properties(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(real_estate_properties.kind,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.kind")}\",
                COALESCE(real_estate_properties.straight_type,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.straight_type")}\",
                COALESCE(real_estate_properties.adquisition_title,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.adquisition_title")}\",
                COALESCE(real_estate_properties.municipality,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.municipality")}\",
                COALESCE(real_estate_properties.participation_percentage,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.participation_percentage")}\",
                COALESCE(real_estate_properties.purchase_date,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.purchase_date")}\",
                COALESCE(real_estate_properties.cadastral_value) 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.get_cadastral_value")}\",
                COALESCE(real_estate_properties.observations,'-') 
                    as \"#{I18n.t("assets_exporter.sheet2_attr.observations")}\"
            FROM real_estate_properties, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND real_estate_properties.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def account_deposits(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(account_deposits.kind,'-') 
                    as \"#{I18n.t("assets_exporter.sheet3_attr.kind")}\",
                COALESCE(account_deposits.entity,'-') 
                    as \"#{I18n.t("assets_exporter.sheet3_attr.entity")}\",
                COALESCE(account_deposits.balance) 
                    as \"#{I18n.t("assets_exporter.sheet3_attr.get_balance")}\"
            FROM account_deposits, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND account_deposits.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
        
        def other_deposits(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(other_deposits.types,'-') 
                    as \"#{I18n.t("assets_exporter.sheet4_attr.types")}\",
                COALESCE(other_deposits.description,'-') 
                    as \"#{I18n.t("assets_exporter.sheet4_attr.description")}\",
                COALESCE(other_deposits.purchase_date,'-') 
                    as \"#{I18n.t("assets_exporter.sheet4_attr.purchase_date")}\",
                COALESCE(other_deposits.value) 
                    as \"#{I18n.t("assets_exporter.sheet4_attr.get_value")}\"
            FROM other_deposits, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND other_deposits.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def vehicles(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(vehicles.kind,'-') 
                    as \"#{I18n.t("assets_exporter.sheet5_attr.kind")}\",
                COALESCE(vehicles.model,'-') 
                    as \"#{I18n.t("assets_exporter.sheet5_attr.model")}\",
                COALESCE(vehicles.purchase_date,'-') 
                    as \"#{I18n.t("assets_exporter.sheet5_attr.purchase_date")}\"
            FROM vehicles, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND vehicles.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def other_personal_properties(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(other_personal_properties.types,'-') 
                    as \"#{I18n.t("assets_exporter.sheet6_attr.types")}\",
                COALESCE(other_personal_properties.purchase_date,'-') 
                    as \"#{I18n.t("assets_exporter.sheet6_attr.purchase_date")}\"            
            FROM other_personal_properties, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND other_personal_properties.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def debts(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(debts.kind,'-') 
                    as \"#{I18n.t("assets_exporter.sheet7_attr.kind")}\",
                COALESCE(debts.import) 
                    as \"#{I18n.t("assets_exporter.sheet7_attr.get_import")}\",
                COALESCE(debts.observations,'-') 
                    as \"#{I18n.t("assets_exporter.sheet7_attr.observations")}\"
            FROM debts, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND debts.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def tax_datas(params, export_type, sql_data_person, ids)
            "Select distinct 
                COALESCE(CAST((Select distinct people.personal_code #{sql_data_person}) 
                    as varchar),'-') as \"#{I18n.t("assets_exporter.sheet1_attr.person_identificator")}\",
                COALESCE(tax_data.tax,'-') 
                    as \"#{I18n.t("assets_exporter.sheet8_attr.tax")}\",
                COALESCE(tax_data.fiscal_data,'-') 
                    as \"#{I18n.t("assets_exporter.sheet8_attr.fiscal_data")}\",
                COALESCE(tax_data.import) 
                    as \"#{I18n.t("assets_exporter.sheet8_attr.get_amount")}\",
                COALESCE(tax_data.observation,'-') 
                    as \"#{I18n.t("assets_exporter.sheet8_attr.comments")}\"
            FROM tax_data, assets_declarations, temporalities
            WHERE #{"assets_declarations.id IN ('#{ids.join("', '")}') AND" if ids.count != 0} temporalities.id = assets_declarations.temporality_id 
            AND tax_data.assets_declaration_id = assets_declarations.id
            #{params[:period].to_s == "Inicial" || params[:period].to_s=="Final" ? "AND temporalities.name='#{params[:period]}'" : "AND temporalities.name='Anual' AND assets_declarations.year_period=#{params[:period]}"}
            #{ "AND assets_declarations.person_level_id= #{params[:people]}" unless params[:people].blank? }
            AND assets_declarations.person_level_type=#{export_type}"
        rescue => e
            begin
              Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

    end
end