module Admin::NotificationsHelper
    def getNumberNotifications
        Notification.pending.count
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def getNumberNotificationsPending(type=nil)
        return 0 if type.blank?
        Notification.pending.with_type_data(type).count
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def stream_query_rows(notifications, params, type)
        header = ""
        params_header = ""
        params_text = ""

        # if params[:export_all].to_s == "false"
        #     [
        #         :name, :last_name, :type_person, :not_start_date, :not_end_date,
        #         :update_start_date, :update_end_date, :personal_code,:process_start_date,
        #         :process_end_date
        #     ].each do |p|
        #         if !params[p].blank?
        #             params_text = params_text + ";" if !params_text.blank?
        #             params_header = params_header + ";" if !params_header.blank?
        #             params_text = params_text + "#{params[p]}"
        #             params_header = params_header + "#{I18n.t("filter_notifications.#{p.to_s}")}"
        #         end
        #     end
        # end

        notifications.first.type_data.constantize.especific_columns.each do |c|
            header = header + ";" if !header.blank?
            header = header + "#{notifications.first.type_data.constantize.human_attribute_name(c)}"
        end
        header = header + ";#{Notification.human_attribute_name(:created_at)}"
        if type.to_s == 'historic'
            header = header + ";#{Notification.human_attribute_name(:type_action)};#{Notification.human_attribute_name(:change_state_at)};#{Notification.human_attribute_name(:administrator)}"
        else
            header = header + ";#{Notification.human_attribute_name(:no_publish_reason) }"
        end


        # if params[:export_all].to_s == "false"
        #     yield "Filtros:".force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        #     yield params_header.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        #     yield params_text.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        #     yield "".force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        # end
        yield header.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?


        notifications.each do |notification|
          content = ""
          notification.type_data.constantize.especific_columns.each do |c|
              content = content + ";" if !content.blank?
              content = content + "#{notification.content_data_hash_formated(c).blank? ? '-' : notification.content_data_hash_formated(c)}"
          end
          content = content + ";#{notification.try(:created_at)}"
          if type.to_s == 'historic'
              content = content + ";#{notification.try(:no_publish_reason).blank? ? notification.try(:type_action) : notification.try(:no_publish_reason)};#{notification.try(:change_state_at)};#{notification.try(:author)}"
          else
          content = content + ";#{notification.try(:no_publish_reason)}"
          end


          yield content.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) if block_given?
        end
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def archived_select_options
        [[t('filter_notifications.archived_options.manually_reviewed'), "manually_reviewed"], [t('filter_notifications.archived_options.auto_reviewed'), "auto_reviewed"], [t('filter_notifications.archived_options.all'), "all"]]
    end
end
