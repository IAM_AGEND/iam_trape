module DataPreferencesHelper
    def get_type_preference(type=nil)
        return "" if type.blank?
        I18n.t("data_preference.types.#{type}")
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def type_preference(type=nil)
        return :string if type.blank?
        case type.to_s
        when "0"
            :integer
        when "1"
            :string
        when "2"
            :boolean
        when "3"
            :date
        else
            :string
        end
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end
