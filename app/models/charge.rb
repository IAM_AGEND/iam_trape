class Charge < ActiveRecord::Base   

    belongs_to :appointment
    validates :charge, presence: :true, length: { maximum: 300 }

end