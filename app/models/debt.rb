class Debt < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validate :not_blank

    def not_blank
        if self.kind.blank? && self.import.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end

    def get_import
        get_currency_value(self.try(:import))
    end
end
