class Temporality < ActiveRecord::Base
    has_many :assets_declarations
    has_many :activities_declarations
    
    validates :name, presence: true
    validates :name, uniqueness: true
   
end