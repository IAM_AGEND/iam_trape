class ActivitiesDeclaration < ActiveRecord::Base
    include Declarations
    include OrdersCommon

    belongs_to :person_level, :polymorphic => true, touch: true
    belongs_to :temporality
    has_many :public_activities, dependent: :destroy
    has_many :private_activities, dependent: :destroy
    has_many :other_activities, dependent: :destroy
    has_many :duties, as: :declaration, dependent: :destroy

    accepts_nested_attributes_for :public_activities, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :private_activities, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :other_activities, reject_if: :all_blank, allow_destroy: true

    #validates_uniqueness_of :temporality_id, scope: [:year_period, :person_level_id,:person_level_type], if: '!editable'
    validates :declaration_date, :temporality, presence: true
    validates_associated :public_activities
    validates_associated :private_activities
    validates_associated :other_activities
    validate :unique_declaration
    validate :editable_declaration
    validate :order_tables

    scope :sort_for_list, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 2 ELSE 1 END) ASC, activities_declarations.year_period ASC") }
    scope :sort_for_temporality, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 1 ELSE 2 END) ASC, activities_declarations.year_period DESC") }
    scope :original, -> { where(editable: false) }
    scope :sort_for_list, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 2 ELSE 1 END) ASC, activities_declarations.year_period ASC") }
    scope :in_temporality, -> (temporality, year_period){ where(temporality: temporality, year_period: year_period) }


    def order_tables
        valid_order(self.public_activities) && valid_order(self.private_activities) &&  valid_order(self.other_activities)
    end

    def self.especific_columns
        %w{
            person_type
            period_name_notification
            declaration_date
            person_name
            person_last_name
            person_identificator
        }
    end

    def self.getNotificationData(notification)
        aux = ''
        activity = self.transformData(notification)

        aux = aux + "<h1>Vista de la declaración de actividad - #{notification.historic? ? "Histórico" : "Pendiente"}</h1><br>"
        aux = aux + "<fieldset><legend>Datos de la notificación</legend>"
        aux = aux + "<b>#{Person.human_attribute_name(:type_job)}:</b> #{notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : I18n.t("person_abrev.#{notification.try(:content_data).try{|x| x['gperAbrev']}}")}<br>"
        aux = aux + "<b>#{Notification.human_attribute_name(:created_at)}:</b> #{notification.created_at}<br>"
        if notification.historic?
            aux = aux + "<b>#{Notification.human_attribute_name(:type_action)}:</b> #{notification.type_action}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:change_state_at)}:</b> #{notification.change_state_at}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:administrator)}:</b> #{notification.try(:author).blank? ? "Administrador del sistema" : notification.try(:author) }<br>"
        else
            aux = aux + "<b>#{Notification.human_attribute_name(:no_publish_reason)}:</b> #{notification.no_publish_reason}<br>"
        end
        aux = aux + "</fieldset>"

        aux = aux + "<fieldset><legend>Datos de la declaración de actividad</legend><table><thead><tr>"
        [ :declaration_date, :year_period ].each {|x| aux =  aux + "<th>#{ActivitiesDeclaration.human_attribute_name(x)}</th>"}
        aux =  aux + "<th>Temporalidad</th>"
        aux = aux + "</tr></thead><tbody><tr>"
        [ :declaration_date, :year_period ].each {|x| aux =  aux + "<td>#{activity.try(x)}</td>"}
        aux =  aux + "<td>#{activity.try(:temporality).try(:name)}</th>"
        aux = aux + "</tr></tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Datos de la persona</legend><table><thead><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<th>#{Person.human_attribute_name(x)}</th>"}
        aux =  aux + "<th>Tipo persona</th>"
        aux = aux + "</tr></thead><tbody><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<td>#{activity.person_level.try(:person).try(x)}</td>"}
        aux =  aux + "<td>#{activity.person_level.try(:type_person)}</td>"
        aux = aux + "</tr></tbody></table></fieldset>"


        aux = aux + "<fieldset><legend>Actividades públicas</legend><table><thead><tr>"
        [ :entity, :position, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<th>#{PublicActivity.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        activity.public_activities.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :entity, :position, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Actividades privadas</legend><table><thead><tr>"
        [ :description, :entity,:position, :private_activity_type, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<th>#{PrivateActivity.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        activity.private_activities.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :description, :entity,:position, :private_activity_type, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Otras actividades</legend><table><thead><tr>"
        [ :description, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<th>#{OtherActivity.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        activity.other_activities.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :description, :start_date, :end_date, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux.html_safe
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
        ""
    end

    def self.transformDataHash(notification)
        fecAlta = notification.try(:content_data).try{|x| x['decgFecAlta']}
        fecRegistro = notification.try(:content_data).try{|x| x['decgFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta
        person = Person.find_by(personal_code: notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']} : notification.try(:content_data).try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{notification.try(:content_data).try{|x| x['dperNombre']}}%", "%#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}%")
        end
        personal_code = person.blank? ? notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : notification.try(:content_data).try{|x| x['dperNumper']}.to_s : person.personal_code

        {
            :person_type => notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : I18n.t("person_abrev.#{notification.try(:content_data).try{|x| x['gperAbrev']}}"),
            :period_name_notification => "#{notification.try(:content_data).try{|x| x['tdNombre']}} #{fecFinal.blank? || notification.try(:content_data).try{|x| x['tdNombre']} !="Anual" ? '' : notification.try(:content_data).try{|x| x['decgYear']}}",
            :declaration_date => fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y"),
            :person_name => "#{notification.try(:content_data).try{|x| x['dperNombre']}}".upcase,
            :person_last_name => "#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}".upcase,
            :person_identificator => personal_code.to_s
        }
    rescue
        {}
    end

    def self.transformData(notification, actualiza = false)
        contenido = notification.try(:content_data)
        return {} if contenido.blank?

        fecAlta = notification.try(:content_data).try{|x| x['decgFecAlta']}
        fecRegistro = notification.try(:content_data).try{|x| x['decgFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta

        temporality = Temporality.find_by(name: contenido.try{|x| x['tdNombre']})

        activity = ActivitiesDeclaration.new(
            temporality: temporality, declaration_date: fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y"),
            year_period: fecFinal.blank? || temporality.name !="Anual" ? '' : contenido.try{|x| x['decgYear']}
        )
        person_type = contenido.try{|x| x['gperAbrev']}
        person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
                "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
        end

        if person.blank?
            person = Person.new(name: contenido.try{|x| x['dperNombre']},
                sex: contenido.try{|x| x['dperSexo']},
                document_nif: contenido.try{|x| x['dperDocumento']},
                document_type: DocumentType.find_by(name: 'DNI'),
                last_name: "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}",
                personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
            job = JobLevel.new(person: person, person_type: person_type == 'C' ? 'councillor' : person_type == 'R' ? 'directors' : person_type == 'F' ? 'public_worker' : person_type == 'E' ? 'temporary_worker' : 'spokesperson')
            if job.councillor?
                corporation = Corporation.new(name: contenido.try{|x| x['cpNombre']}.gsub('/','-'))
                councillor = CouncillorsCorporation.new(job_level: job, corporation: corporation)
                job.councillors_corporations << councillor
                activity.person_level = councillor
            else
                activity.person_level = job
            end
        else
            case person_type
            when "C"
                corporation = Corporation.find_by(name: contenido.try{|x| x['cpNombre']}.gsub('/','-'))
                activity.person_level = person.councillors[0].councillors_corporations.corporation(corporation)[0]
            when "R"
                activity.person_level = person.directors[0]
            when "F"
                activity.person_level = person.public_workers[0]
            when "E"
                activity.person_level = person.temporary_workers[0]
            when "V"
                activity.person_level = person.spokespeople[0]
            end
        end

        if activity.person_level.blank?
            job = JobLevel.new(person: person, person_type: person_type == 'C' ? 'councillor' : person_type == 'R' ? 'directors' : person_type == 'F' ? 'public_worker' : person_type == 'E' ? 'temporary_worker' : 'spokesperson')
            if job.councillor?
                corporation = Corporation.new(name: contenido.try{|x| x['cpNombre']})
                councillor = CouncillorsCorporation.new(job_level: job, corporation: corporation)
                job.councillors_corporations << councillor
                activity.person_level = councillor
            else
                activity.person_level = job
            end
        end

        if !contenido.try{|x| x['actividadesPrivadas']}.blank?
            contenido.try{|x| x['actividadesPrivadas']}.each do |l|
                aux = PrivateActivity.new(
                    activities_declaration: activity,
                    description: l['descripcion'],
                    entity: l['entidad'], position: l['cargo'],
                    start_date: l['fechaInicio'].blank? ? '' : Time.at(l['fechaInicio']/1000).strftime("%d/%m/%Y"),
                    end_date: l['fechaCese'].blank? ? '' : Time.at(l['fechaCese']/1000).strftime("%d/%m/%Y"),
                    private_activity_type: l['actividad'],
                    table_order: l['orden'])
                activity.private_activities << aux
            end
        end

        if !contenido.try{|x| x['actividadesPublicas']}.blank?
            contenido.try{|x| x['actividadesPublicas']}.each do |l|
                aux = PublicActivity.new(
                    activities_declaration: activity,
                    entity: l['acpuEntidad'], position: l['acpuCargo'],
                    start_date: l['acpuFecInicio'].blank? ? '' : Time.at(l['acpuFecInicio']/1000).strftime("%d/%m/%Y"),
                    end_date: l['acpuFecCese'].blank? ? '' : Time.at(l['acpuFecCese']/1000).strftime("%d/%m/%Y"),
                    table_order: l['orden'])
                activity.public_activities << aux
            end
        end

        if !contenido.try{|x| x['actividadesOtras']}.blank?
            contenido.try{|x| x['actividadesOtras']}.each do |l|
                aux = OtherActivity.new(
                    activities_declaration: activity,
                    description: l['descripcion'],
                    start_date: l['fechaInicio'].blank? ? '' : Time.at(l['fechaInicio']/1000).strftime("%d/%m/%Y"),
                    end_date: l['fechaCese'].blank? ? '' : Time.at(l['fechaCese']/1000).strftime("%d/%m/%Y"),
                    table_order: l['orden'])
                activity.other_activities << aux
            end
        end

        activity
    # rescue
    #     {}
    end

    private

    def unique_declaration
        activity = ActivitiesDeclaration.where(temporality_id: self.temporality_id, year_period: self.year_period, editable: false, person_level_id: self.person_level_id, person_level_type: self.person_level_type)

        if !self.id.blank?
            activity = activity.where("id not in (?)", self.id)
        end

        if self.editable == false && !activity.blank?
            self.errors.add(:temporality, I18n.t('activities_declaration.error_temporalidad'))
        end
    end

    def editable_declaration
        activity = ActivitiesDeclaration.where(temporality_id: self.temporality_id, year_period: self.year_period, editable: false, person_level_id: self.person_level_id, person_level_type: self.person_level_type).order(declaration_date: :desc).first

        if activity.blank? && self.editable == true && self.id.blank?
            self.errors.add(:editable, I18n.t('activities_declaration.error_editable'))
        elsif self.declaration_date.blank? || !activity.blank? && activity.declaration_date > self.declaration_date && self.id.blank?
            self.errors.add(:declaration_date, I18n.t('activities_declaration.error_declaration_date', date: activity.declaration_date))
        end
    end
end
