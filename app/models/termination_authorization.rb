require 'active_record/diff'
class TerminationAuthorization < ActiveRecord::Base
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]
    
    belongs_to :person_level, :polymorphic => true, touch: true

    def self.valid_column_type
        [:authorization_date, :authorization_description]
    end

end