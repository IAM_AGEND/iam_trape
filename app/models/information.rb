class Information < ActiveRecord::Base
    belongs_to :profile
    belongs_to :information_type

    validates :information_type, presence: true
end
