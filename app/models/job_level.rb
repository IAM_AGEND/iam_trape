require 'active_record/diff'
class JobLevel < ActiveRecord::Base
    extend FriendlyId
    extend Admin::PeopleHelper
    extend Admin::StatisticsHelper
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]
    friendly_id :slug_name, use: :slugged

    belongs_to :person
    belongs_to :appointment, dependent: :destroy, touch: true
    has_many :councillors_corporations,  -> { order(corporation_id: :asc) }, dependent: :destroy
    has_many :activities_declarations,  -> { sort_for_list },as: :person_level, dependent: :destroy
    has_many :assets_declarations,  -> { sort_for_list },as: :person_level, dependent: :destroy
    has_many :duties,  -> { sort_for_list },as: :person_level, dependent: :destroy
    has_many :termination_authorizations, as: :person_level, dependent: :destroy
    has_many :compatibility_activities, as: :person_level, dependent: :destroy

    accepts_nested_attributes_for :appointment,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :councillors_corporations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :activities_declarations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :assets_declarations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :termination_authorizations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :compatibility_activities,reject_if: :all_blank, allow_destroy: true

    validates_associated :activities_declarations
    validates_associated :assets_declarations
    validates_associated :appointment
    validates_associated :councillors_corporations
    validates :person_type, :slug, presence: true
    validates :slug, uniqueness: true
    validates :updated_at, presence: true
    validate :duplicated_job

    scope :councillors,       -> { where(person_type: 'councillor') }
    scope :councillors_working_without, -> { joins(:councillors_corporations => [:appointment]).where("appointments.end_date IS NULL")}
    scope :councillors_working_without_profile, -> { joins(:person => [:profile],:councillors_corporations => [:appointment]).where("appointments.end_date IS NULL AND profiles.cumplimented = true")}
    scope :councillors_not_working_without, -> { joins(:councillors_corporations => [:appointment]).where("appointments.end_date IS NOT NULL")}
    scope :councillors_working, -> (corporation){ councillors.joins(:councillors_corporations => [:appointment]).where("councillors_corporations.corporation_id = ? AND appointments.end_date IS NULL",corporation)}
    scope :councillors_not_working, -> (corporation){ councillors.joins(:councillors_corporations => [:appointment]).where("councillors_corporations.corporation_id = ? AND appointments.end_date IS NOT NULL",corporation).order('appointments.end_date ASC')}
    scope :directors,         -> { where(person_type: 'director') }
    scope :not_directors,     -> { where("job_levels.person_type!= 'director'") }
    scope :temporary_workers, -> { where(person_type: 'temporary_worker') }
    scope :public_workers,    -> { where(person_type: 'public_worker') }
    scope :spokespeople,      -> { where(person_type: 'spokesperson') }
    scope :working, -> { joins(:appointment).where("appointments.end_date IS NULL")}
    scope :not_working, -> { joins(:appointment).where("appointments.end_date IS NOT NULL")}
    scope :no_visible_director, -> {not_working.where("appointments.end_date > current_date - interval '#{DataPreference.find_by(:code => "public_year").blank? ? 2 : DataPreference.find_by(:title => "Años publicados").type_data.to_i==0 ? DataPreference.find_by(:code => "public_year").content_data.to_i : 2} year'")}
    scope :invisible_director, -> {not_working.where("appointments.end_date <= current_date - interval '#{DataPreference.find_by(:code => "public_year").blank? ? 2 : DataPreference.find_by(:title => "Años publicados").type_data.to_i==0 ? DataPreference.find_by(:code => "public_year").content_data.to_i : 2} year'")}
    scope :without_profile, -> { joins("INNER JOIN people ON people.id = job_levels.person_id LEFT JOIN profiles ON profiles.id = people.profile_id")
        .where("profiles.id = people.profile_id OR people.profile_id IS NULL")
        .where("profiles.cumplimented = false OR profiles.cumplimented IS NULL")}
    scope :with_profile, -> { joins(:person => [:profile]).where("profiles.cumplimented = true")}
    scope :working_profile_others, -> {joins(:appointment,:person => [:profile]).where("profiles.cumplimented = true AND appointments.end_date IS NULL")}
    scope :sort_list,  -> {order("(CASE WHEN job_levels.person_type = 'councillor' THEN 0 WHEN job_levels.person_type = 'director' THEN 1 WHEN job_levels.person_type = 'public_worker' THEN 3 WHEN job_levels.person_type = 'temporary_worker' THEN 4 ELSE 5 END) ASC")}

    JOB_LEVELS_CHAR = { 'councillor' => 'C', 'director' => 'D', 'temporary_worker' => 'E', 'public_worker' => 'F',
        'spokesperson' => 'V', 'labour' => 'L'}.freeze

    def self.grouped_by_electoral_list(corporation)
        Hash[
            self.includes(:councillors_corporations => [:appointment,:corporation, :electoral_list])
            .where("councillors_corporations.corporation_id = ?", corporation)
            .order("councillors_corporations.order_num asc")
            .group_by { |t| t.councillors_corporations.corporation(corporation).first.electoral_list.blank? ? nil : t.councillors_corporations.corporation(corporation).first.electoral_list  }
            .sort_by { |key, value| CorporationsElectoralList.find_by(corporation_id: corporation, electoral_list_id: key.try(:id)).blank? ? 20 : CorporationsElectoralList.find_by(corporation_id: corporation, electoral_list_id: key.try(:id)).try(:order) }
        ]
    end

    def self.grouped_by_initial
        Hash[
            self.order("people.last_name asc, people.name asc")
            .group_by { |t| (t.person.last_name).strip[0].upcase }
        ]
    end

    def self.validates_directos(directors)
        validated = false
        directors.each do |d|
            return true if d.director_active?
        end
        validated
    end

    def type_person
        I18n.t("person.titles.#{person_type}")
    end

    def councillor?
        person_type == 'councillor'
    end

    def director?
        person_type == 'director'
    end

    def temporary_worker?
        person_type == 'temporary_worker'
    end

    def public_worker?
        person_type == 'public_worker'
    end

    def spokesperson?
        person_type == 'spokesperson'
    end

    def working?(corporation=nil)
        return false if corporation.blank? && self.councillor?
        self.get_appointment(corporation).try(:end_date).blank?
    end

    def not_working?(corporation=nil)
        !working?(corporation)
    end

    def backwards_name
        person.try(:backwards_name).try { |tx| tx.mb_chars.upcase}
    end

    def slug_name
        person.blank? ? nil : "#{JOB_LEVELS_CHAR[self.person_type]} #{person.try(:full_name)}"
    end

    def get_appointment(corporation=nil)
        if self.councillor?
            corporation.blank? ? nil : self.try(:councillors_corporations).try {|cc| cc.corporation(corporation).first}.try(:appointment)
        else
            self.try(:appointment)
        end
    end

    def get_profile
        Person.find(self.try(:person).try(:id)).try(:profile)
    rescue
        nil
    end

    def has_profile?
        return t("booleans.false") if self.get_profile.blank?
        t("booleans.#{self.get_profile.has_profile?.to_s}")
    end

    def get_initial_assets_declarations(corporation=nil)
        aux_assets = self.get_assets_declarations(corporation).try{ |ad| ad.where(temporality: Temporality.find_by(name: "Inicial")).order(:declaration_date).first}
        aux_assets.blank? ? "-" : t("person.date_assets_declarations", date: aux_assets.declaration_date.to_date)
    end

    def get_initial_activities_declarations(corporation=nil)
        aux_activities = self.get_activities_declarations(corporation).try{ |ad| ad.where(temporality: Temporality.find_by(name: "Inicial")).order(:declaration_date).first}
        aux_activities.blank? ? "-" : t("person.date_activities_declarations", date: aux_activities.declaration_date.to_date)
    end

    def get_annual_assets_declarations(corporation=nil, year=nil)
        aux_assets = self.get_assets_declarations(corporation).try{|ad| ad.where(year_period: year)}.try{ |ad| ad.where(temporality: Temporality.find_by(name: "Anual")).order(:declaration_date).first}
        aux_assets.blank? ? "-" : t('person.date_assets_declarations_anual', year: aux_assets.year_period.to_s, date: aux_assets.declaration_date.to_date)
    end

    def get_annual_activities_declarations(corporation=nil, year=nil)
        aux_activities = self.get_activities_declarations(corporation).try{|ad| ad.where(year_period: year)}.try{ |ad| ad.where(temporality: Temporality.find_by(name: "Anual")).order(:declaration_date).first}
        aux_activities.blank? ? "-" : t('person.date_activities_declarations_anual', year: aux_activities.year_period.to_s, date: aux_activities.declaration_date.to_date)
    end

    def get_final_assets_declarations(corporation=nil)
        aux_assets = self.get_assets_declarations(corporation).try{ |ad| ad.where(temporality: Temporality.find_by(name: "Final")).order(:declaration_date).first}
        aux_assets.blank? ? "-" : t("person.date_assets_declarations", date: aux_assets.declaration_date.strftime.to_date)
    end

    def get_final_activities_declarations(corporation=nil)
        aux_activities = self.get_activities_declarations(corporation).try{ |ad| ad.where(temporality: Temporality.find_by(name: "Final")).order(:declaration_date).first}
        aux_activities.blank? ? "-" : t("person.date_activities_declarations", date: aux_activities.declaration_date.to_date)
    end

    def get_activities_declarations(corporation=nil)
        if self.councillor?
            corporation.blank? ? nil : self.try(:councillors_corporations).try {|cc| cc.corporation(corporation).first}.try(:activities_declarations)
        else
            self.try(:activities_declarations)
        end
    end

    def get_assets_declarations(corporation=nil)
        if self.councillor?
            corporation.blank? ? nil : self.try(:councillors_corporations).try {|cc| cc.corporation(corporation).first}.try(:assets_declarations)
        else
            self.try(:assets_declarations)
        end
    end

    def get_active(corporation=nil)
        if self.working?(corporation)
            I18n.t("people.status.active")
        else
            if self.try(:person_type) == "councillor" || (self.try(:person_type) == "director" && self.director_active?)
                I18n.t("people.status.inactive_show")
            else
                I18n.t("people.status.inactive_hidden")
            end
        end
    end

    def self.get_active(job_level = nil)
        return "" if job_level.blank?
        if job_level['end_date'].blank?
            I18n.t("people.status.active")
        else
            if job_level['person_type'] == "councillor" || (job_level['person_type'] == "director" && JobLevel.director_active?(job_level))
                I18n.t("people.status.inactive_show")
            else
                I18n.t("people.status.inactive_hidden")
            end
        end
    end

    def self.director_active?(job_level = nil)
        return false if job_level.blank?
        return true if job_level['end_date'].blank? && job_level['person_type'] == "director"
        (job_level['person_type'] == "director" && !job_level['end_date'].blank? && (Time.zone.now - JobLevel.get_publish_year.years) < job_level['end_date'].to_date)
    end

    def self.get_publish_year(job_level=nil)
        if !job_level.blank? && job_level['person_type'] == "councillor"
            preference = DataPreference.find_by(:code => "councillors_publication_year")
            default_year = 4
        else
            preference = DataPreference.find_by(:code => "public_year")
            default_year = 2
        end
        preference.blank? ? default_year : preference.type_data.to_i==0 ? preference.content_data.to_i : default_year
    end


    def not_leaving_date?(corporation=nil)
        return false if corporation.blank? && self.councillor?
        if self.councillor?
            end_date = corporation.try(:end_corporation_date)
        else
            end_date= self.get_appointment(corporation).try(:end_date)
        end
        end_date = Time.zone.now if end_date.blank?
        (working?(corporation) || (not_working?(corporation) && ((Time.zone.now - get_publish_year.years) >= end_date || (Time.zone.now + get_publish_year.years) <= end_date)))
    end

    def should_display_profile?(corporation=nil)
        !councillor? && working?(corporation) || councillor? && working?(corporation) && corporation.try(:name) == Corporation.active.first.try(:name)
    end

    def should_display_image?(corporation=nil)
        councillor? && working?(corporation) && corporation.try(:name) == Corporation.active.first.try(:name)
    end

    def should_display_declarations?(corporation=nil)
        return false if corporation.blank? && self.councillor?
        if self.councillor?
            end_date = corporation.try(:end_corporation_date)
        else
            end_date= self.get_appointment(corporation).try(:end_date)
        end
        end_date = Time.zone.now if end_date.blank?
        (councillor? || director?) && (working?(corporation) || (not_working?(corporation) &&  (Time.zone.now - get_publish_year.years) < end_date ))
    end

    def should_display_calendar?(corporation=nil)
        return false if corporation.blank? && self.councillor?
        if self.councillor?
            end_date = corporation.try(:end_corporation_date)
        else
            end_date= self.get_appointment(corporation).try(:end_date)
        end
        end_date = Time.zone.now if end_date.blank?
        (temporary_worker? || councillor? || director?) && (working?(corporation) || (!temporary_worker? && not_working?(corporation) &&  (Time.zone.now - get_publish_year.years) < end_date ))
    end

    def should_display_personal_data?(corporation=nil)
        return false if corporation.blank? && self.councillor?
        if self.councillor?
            end_date = corporation.try(:end_corporation_date)
        else
            end_date= self.get_appointment(corporation).try(:end_date)
        end
        end_date = Time.zone.now if end_date.blank?
        !director? || director? && (working?(corporation) || (not_working?(corporation) &&  (Time.zone.now - get_publish_year.years) < end_date ))
    end

    def has_career?
        (!self.get_profile.try(:public_jobs).blank? ||
        !self.get_profile.try(:private_jobs).blank? ||
        !self.get_profile.try(:informations)
            .try {|i| i.find_by(information_type_id: InformationType.find_by(name: "career_comments").try(:id))}
            .try(:information).blank?)
    end

    def director_active?
        return true if self.get_appointment.try(:end_date).blank? && director?
        (director? && self.not_working? && (Time.zone.now - get_publish_year.years) < self.get_appointment.try(:end_date))
    end

    # Regenerate slug if name changes
    def should_generate_new_friendly_id?
       slug.blank? || slug_name_changed?
    end

    def slug_name_changed?
        return true if person.blank?
        person.name_changed? || person.last_name_changed? || person_type_changed?
    end

    def change_working(type,author, reason, date_at = Time.zone.now, corporation=nil)
        corporation = Corporation.find_by(name: corporation)
        if type.to_s == "hide"
            appointment = self.get_appointment(corporation)
            unless appointment.blank?
                appointment.end_date = date_at
                if appointment.save
                    self.updated_at = Time.zone.now
                    if self.save
                        Audit.create!(administrator: author,
                            person: self.try(:person),
                            action: I18n.t('audits.hide'),
                            description: reason)
                    end
                end
            end
        elsif type.to_s == "unhide"
            appointment = self.get_appointment(corporation)
            unless appointment.blank?
                appointment.end_date = nil
                if appointment.save
                    self.updated_at = Time.zone.now
                    if self.save
                        Audit.create!(administrator: author,
                            person: self.try(:person),
                            action: I18n.t('audits.unhide'),
                            description: reason)
                    end
                end
            end
        end
    end

    ###############################################################################
    #   FIELDS FOR CSV OR XLS
    ###############################################################################
    %i(personal_code name last_name).each do |ordinal|
      define_method "#{ordinal}" do
        self.try(:person).try(ordinal).blank? ? "" : self.try(:person).try(ordinal)
      end
    end

    %i(profiled_at twitter facebook).each do |ordinal|
        define_method "#{ordinal}" do
            ordinal= "updated_at" if ordinal.to_s == "profiled_at"
            self.get_profile.try(ordinal).blank? ? "" : self.get_profile.try(ordinal)
        end
    end

    %i(position unit).each do |ordinal|
        define_method "#{ordinal}" do
            self.get_appointment.try(ordinal).blank? ? "" : self.get_appointment.try(ordinal)
        end
    end

    %i(first second third fourth).each_with_index do |ordinal, index|
        %i(official_degree center start_year end_year).each do |field|
            define_method "#{ordinal}_study_#{field}" do
                self.get_profile.try{ |j| j.studies[index]}.try(field).blank? ?
                    "" : self.get_profile.try{ |j| j.studies[index]}.try(field)
            end
        end
    end

    %i(first second third fourth).each_with_index do |ordinal, index|
        %i(title center start_year end_year).each do |field|
            define_method "#{ordinal}_course_#{field}" do
                self.get_profile.try{ |j| j.courses[index]}.try(field).blank? ?
                    "" : self.get_profile.try{ |j| j.courses[index]}.try(field)
            end
        end
    end

    %i(first second third fourth).each_with_index do |ordinal, index|
        %i(position public_administration start_year end_year).each do |field|
            define_method "#{ordinal}_public_job_#{field}" do
                # if field.to_s == "public_administration"
                #     self.get_profile.try{|j| j.public_jobs[index]}.try(field).try(:name).blank? ?
                #     "" : self.get_profile.try{|j| j.public_jobs[index]}.try(field).try(:name)
                # else
                    self.get_profile.try{|j| j.public_jobs[index]}.try(field).blank? ?
                        "" : self.get_profile.try{|j| j.public_jobs[index]}.try(field)
                # end
            end
        end
    end

    %i(first second third fourth).each_with_index do |ordinal, index|
        %i(position entity start_year end_year).each do |field|
            define_method "#{ordinal}_private_job_#{field}" do
                self.get_profile.try{ |j| j.private_jobs[index]}.try(field).blank? ?
                    "" : self.get_profile.try{|j| j.private_jobs[index]}.try(field)
            end

            define_method "#{ordinal}_political_post_#{field}" do
                self.get_profile.try{|j| j.political_posts[index]}.try(field).blank? ?
                    "" : self.get_profile.try{|j| j.political_posts[index]}.try(field)
            end
        end
    end

    %i(studies_comments courses_comments career_comments political_posts_comments theacher_activity special_mentions other).each do |ordinal|
        define_method "#{ordinal}" do
            self.get_profile.try{|j| j.informations.find_by(information_type: InformationType.where(name: "#{ordinal}"))}.try(:information).blank? ?
                "" : self.get_profile.try{|j| j.informations.find_by(information_type: InformationType.where(name: "#{ordinal}"))}.try(:information)
        end
    end

    { english: 'Inglés', french: 'Francés',
        german: 'Alemán', italian: 'Italiano' }.each do |symbol, name|
        define_method "language_#{symbol}_level" do
            self.get_profile.try{|p| p.languages.find { |l| l.try(:name).to_s == name.to_s}}.try {|l| l.level }.blank? ?
                "" : self.get_profile.try{|p| p.languages.find { |l| l.try(:name).to_s == name.to_s}}.try {|l| l.level }
        end
    end

    def language_other_name
        self.get_profile.try{|p| p.languages.find { |l| !%w(Inglés Francés Alemán Italiano).include?(l.try(:name).to_s)}}.try {|l| l.name }.blank? ?
            "" : self.get_profile.try{|p| p.languages.find { |l| !%w(Inglés Francés Alemán Italiano).include?(l.try(:name).to_s)}}.try {|l| l.name }
    end

    def language_other_level
        self.get_profile.try{|p| p.languages.find { |l| !%w(Inglés Francés Alemán Italiano).include?(l.try(:name).to_s)}}.try {|l| l.level }.blank? ?
            "" : self.get_profile.try{|p| p.languages.find { |l| !%w(Inglés Francés Alemán Italiano).include?(l.try(:name).to_s)}}.try {|l| l.level }
    end

    def job_level_code
        JOB_LEVELS_CHAR[self.person_type]
    end

    def public_jobs_body
        self.get_profile.try{|p| p.public_jobs[0]}.try(:body_scale).blank? ?
            "" : self.get_profile.try{|p| p.public_jobs[0]}.try(:body_scale)
    end

    def public_jobs_start_year
        self.get_profile.try{|p| p.public_jobs[0]}.try(:init_year).blank? ?
            "" : self.get_profile.try{|p| p.public_jobs[0]}.try(:init_year)
    end

    def public_jobs_level
        self.get_profile.try{|p| p.public_jobs[0]}.try(:consolidation_degree).blank? ?
            "" : self.get_profile.try{|p| p.public_jobs[0]}.try(:consolidation_degree)
    end

    def publications
        out_text = ""
        return out_text if self.get_profile.try(:publications).blank?
        self.get_profile.try(:publications).each do |p|
            if !p.try(:informtaion).blank?
                if out_text.blank?
                    if !p.try(:information_at).blank?
                        out_text= "#{p.try(:informtaion)}(#{p.try(:information_at)})"
                    else
                        out_text= "#{p.try(:informtaion)}"
                    end
                else
                    if !p.try(:information_at).blank?
                        out_text= out_text + "; #{p.try(:informtaion)}(#{p.try(:information_at)})"
                    else
                        out_text= out_text + "; #{p.try(:informtaion)}"
                    end
                end
            end
        end
        out_text
    end


    def self.valid_column_type(type)
        case type.to_s
        when "councillors"
            []
        when "directors"
            []
        when "public_workers"
            []
        when "temporary_workers"
            []
        when "spokespeople"
            []
        else
            []
        end
    end


    private

    def get_publish_year
        if self.councillor?
            preference = DataPreference.find_by(:title => "Años de publicación de concejales")
            default_year = 4
        else
            preference = DataPreference.find_by(:title => "Años publicados")
            default_year = 2
        end
        preference.blank? ? default_year : preference.type_data.to_i==0 ? preference.content_data.to_i : default_year
    end

    def duplicated_job
        exist = JobLevel.where(person_type: self.person_type, person_id: self.person_id)
        if !exist.blank? &&  exist.count > 1
            errors.add(:person_type, "Ya existe este tipo de persona")
        end
    end
end
