class Vehicle < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validate :not_blank

    def not_blank
        if self.kind.blank? && self.model.blank? && self.year.blank?
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end

end
