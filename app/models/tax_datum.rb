class TaxDatum < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    scope :ordering_data, -> { order("case when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre la renta de las personas físicas'), 'AEIOU', 'ÁÉÍÓÚ') AND  TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')=TRANSLATE(UPPER('Base imponible general'), 'AEIOU', 'ÁÉÍÓÚ') then 0
    when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre la renta de las personas físicas'), 'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Base imponible de ahorro'), 'AEIOU', 'ÁÉÍÓÚ') then 1
    when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre la renta de las personas físicas'), 'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')=TRANSLATE(UPPER('Deducciones por donativos y otras aportaciones (casillas G), H), J) y M) del Impuesto o las que las sustituyan)'), 'AEIOU', 'ÁÉÍÓÚ') then 3
    when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre la renta de las personas físicas'), 'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')=TRANSLATE(UPPER('Deducciones por donativos y otras aportaciones (casillas 649, 650, 651 y 652 del Impuesto o las que las sustituyan)'), 'AEIOU', 'ÁÉÍÓÚ') then 4
    when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre el patrimonio'), 'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')=TRANSLATE(UPPER('Base imponible'), 'AEIOU', 'ÁÉÍÓÚ') then 5
    when TRANSLATE(UPPER(tax), 'AEIOU', 'ÁÉÍÓÚ')= TRANSLATE(UPPER('Impuesto sobre sociedades'), 'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(fiscal_data), 'AEIOU', 'ÁÉÍÓÚ')=TRANSLATE(UPPER('Base imponible'), 'AEIOU', 'ÁÉÍÓÚ') then 6 
    ELSE 7 END")}

    validate :not_blank

    def not_blank
        if self.import.blank? && self.tax.blank? && self.fiscal_data.blank? && self.observation.blank?
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
    
    def get_amount
        get_currency_value(self.try(:import))
    end
end
