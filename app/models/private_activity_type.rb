class PrivateActivityType < ActiveRecord::Base
    has_many :private_activities

    validates :name, presence: true
    validates :name, uniqueness: true
end