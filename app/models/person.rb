require 'sorting_name_calculator'
require 'active_record/diff'

class Person < ActiveRecord::Base
    include Admin::PeopleHelper
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]

    belongs_to :document_type
    belongs_to :profile, dependent: :destroy
    has_many :job_levels, dependent: :destroy
    has_many :audits, dependent: :destroy
    has_many :councillors, -> {councillors},:class_name => 'JobLevel', dependent: :destroy
    has_many :directors, -> {directors},:class_name => 'JobLevel', dependent: :destroy
    has_many :temporary_workers, -> {temporary_workers},:class_name => 'JobLevel', dependent: :destroy
    has_many :public_workers, -> {public_workers},:class_name => 'JobLevel', dependent: :destroy
    has_many :spokespeople, -> {spokespeople},:class_name => 'JobLevel', dependent: :destroy

    accepts_nested_attributes_for :job_levels, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :councillors, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :directors, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :temporary_workers, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :public_workers, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :spokespeople, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :audits, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :profile, :allow_destroy => true, reject_if: :all_blank

    validates :name, :last_name, :personal_code, presence: true
    validate :validate_job

    validates_associated :councillors
    validates_associated :directors
    validates_associated :temporary_workers
    validates_associated :public_workers
    validates_associated :spokespeople
    validates_associated :profile


    def full_name
        "#{name} #{last_name}"
    end

    def sorting_name
        SortingNameCalculator.calculate(name, last_name)
    end

    def backwards_name
        "#{last_name}, #{name}"
    end

    def self.especific_columns
        %w{
            notification_type
            notification_type_date
            type_job
            name
            last_name
            email
            personal_code
            position
            unit
            area
        }
    end

    def type_job
        aux = ""
        self.councillors.each do |c|
            aux = aux + "Concejal/a ("
            i = 0
            c.councillors_corporations.each do |cc|
                aux = aux + (i == 0 ? "" : ", ") + "#{cc.corporation.try(:name)} - #{cc.appointment.try(:position)}"
                i = i + 1
            end
            aux = aux + ")"
        end

        [["Personal Directivo", :directors], ["Personal eventual", :temporary_workers],["Personal funcionario",:public_workers],["Vocal vecino",:spokespeople]].each do |job|
            self.try(job[1]).each do |d|
                aux = aux + (aux == "" ? "" : ", ") + "#{job[0]} (#{d.appointment.try(:position)})"
            end
        end
        aux
    end

    def self.getNotificationData(notification)
        aux = ''
        person = self.transformData(notification)
        old_person = nil
        alta = false

        # BUSQUEDA DE LA PERSONA
        contenido = notification.try(:content_data)
        if !notification.historic? || notification.origin_data.blank? && notification.changes_data.blank?
          old_person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']}.to_i)
          if old_person.blank?
            old_person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
                "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
            alta = true if old_person.blank?
          end

          notification.update(changes_data: Person.get_data_json(person), origin_data: Person.get_data_json(old_person))
        end

        # ============================================================================================================
        # DATOS DE LA NOTIFICACION
        # ============================================================================================================
        aux = aux + "<h1>Vista de la persona - #{notification.historic? ? "Histórico" : "Pendiente"}</h1><br>"
        aux = aux + "<fieldset><legend>Datos de la notificación</legend>"
        aux = aux + "<b>#{Person.human_attribute_name(:type_job)}:</b> #{notification.try(:action_abrev).blank? ? '' : I18n.t("person_abrev.#{notification.try(:action_abrev)}")}<br>"
        aux = aux + "<b>#{Person.human_attribute_name(:notification_type_date)}:</b> #{notification.date_action.try {|x| x.strftime("%d/%m/%Y")}}<br>"
        aux = aux + "<b>#{Person.human_attribute_name(:notification_type)}:</b> #{notification.action}<br>"
        aux = aux + "<b>#{Notification.human_attribute_name(:created_at)}:</b> #{notification.created_at}<br>"
        if notification.historic?
            aux = aux + "<b>#{Notification.human_attribute_name(:type_action)}:</b> #{notification.type_action}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:change_state_at)}:</b> #{notification.change_state_at}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:administrator)}:</b> #{notification.try(:author).blank? ? "Administrador del sistema" : notification.try(:author) }<br>"
        else
            aux = aux + "<b>#{Notification.human_attribute_name(:no_publish_reason)}:</b> #{notification.no_publish_reason}<br>"
        end
        aux = aux + "</fieldset>"


        # ============================================================================================================
        # DATOS DE LA PERSONA
        # ============================================================================================================
        aux = aux + "<fieldset><legend>Datos de la persona</legend>"
        if !alta
            aux= aux +"<div class='small-12 large-6 columns'><fieldset><legend>Datos antiguos</legend>"
            [:name, :last_name,  :sex, :personal_code, :document_nif].each do |x|
              aux = aux + "<b>#{Person.human_attribute_name(x)}:</b>"
              aux = aux + " <span #{"style='color: orange'" if notification.changes_data.try{|y| y[x.to_s]} != notification.origin_data.try{|y| y[x.to_s]}}>#{notification.origin_data.try{|y| y[x.to_s]}}</span><br>"
            end
            aux = aux + "<b>#{Person.human_attribute_name(:document_type)}:</b>"
            aux = aux + " <span #{"style='color: orange'" if notification.changes_data.try{|x| x['document_type']}.try{|x| x['name']} != notification.origin_data.try{|x| x['document_type']}.try{|x| x['name']}}>#{notification.origin_data.try{|x| x['document_type']}.try{|x| x['name']}}</span><br>"
            aux = aux + "<b>#{Person.human_attribute_name(:email)}:</b>"
            aux = aux + " <span #{"style='color: orange'" if notification.changes_data.try{|x| x['profile']}.try{|x| x['email']} != notification.origin_data.try{|x| x['profile']}.try{|x| x['email']}}>#{notification.origin_data.try{|x| x['profile']}.try{|x| x['email']}}</span><br>"
            aux = aux + "</fieldset></div>"
        end

        aux = aux + "<div class='small-12 large-6 columns end'><fieldset><legend>Datos nuevos</legend>"
        [:name, :last_name,  :sex, :personal_code, :document_nif].each do |x|
            aux = aux + "<b>#{Person.human_attribute_name(x)}:</b>"
            aux = aux + " <span #{"style='color: orange'" if alta || notification.changes_data.try{|y| y[x.to_s]} != notification.origin_data.try{|y| y[x.to_s]}}>#{notification.changes_data.try{|y| y[x.to_s]}}</span><br>"
        end
        aux = aux + "<b>#{Person.human_attribute_name(:document_type)}:</b>"
        aux = aux + " <span #{"style='color: orange'" if alta || notification.changes_data.try{|x| x['document_type']}.try{|x| x['name']} != notification.origin_data.try{|x| x['document_type']}.try{|x| x['name']}}>#{notification.changes_data.try{|x| x['document_type']}.try{|x| x['name']}}</span><br>"
        aux = aux + "<b>#{Person.human_attribute_name(:email)}:</b>"
        aux = aux + " <span #{"style='color: orange'" if alta || notification.changes_data.try{|x| x['profile']}.try{|x| x['email']} != notification.origin_data.try{|x| x['profile']}.try{|x| x['email']}}>#{notification.changes_data.try{|x| x['profile']}.try{|x| x['email']}}</span><br>"
        aux = aux + "</fieldset></div></fieldset>"

        # ============================================================================================================
        # DATOS DEL CONCEJAL
        # ============================================================================================================
        aux = aux + "<fieldset><legend>Concejal</legend>"

        if !alta && !notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.blank?
          aux = aux + "<div class='small-12 large-6 columns' style='word-break: break-all;'><fieldset><legend>Datos antiguos</legend>"

          last_notification = notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.last 

          aux =  aux + "<pre><b>Corporación:</b>"
          aux =  aux + " <span #{"style='color: orange'" if alta || last_notification.try{|x| x["corporation"]}.try{|x| x['name']} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["corporation"]}.try{|x| x['name']}}>#{last_notification.try{|x| x["corporation"]}.try{|x| x['name']}}</span></pre>"

          aux =  aux + "<pre><b>Afiliación:</b>"
          aux =  aux + " <span #{"style='color: orange'" if alta || last_notification.try{|x| x["electoral_list"]}.try{|x| x['name']} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["electoral_list"]}.try{|x| x['name']}}>#{last_notification.try{|x| x["electoral_list"]}.try{|x| x['name']}}</span></pre>"

          aux =  aux + "<pre><b>#{CouncillorsCorporation.human_attribute_name(:order_num)}:</b>"
          aux =  aux + " <span #{"style='color: orange'" if alta || last_notification.try{|x| x["order_num"]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["order_num"]}}>#{last_notification.try{|x| x["order_num"]}}</span></pre>"


          Appointment.valid_column_type("councillors").each do |app|
            aux =  aux + "<pre><b>#{Appointment.human_attribute_name(app)}:</b>"
            aux =  aux + " <span #{"style='color: orange'" if alta || last_notification.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}}}>#{last_notification.try{|x| x["appointment"]}.try{|x| app.to_s.include?("_format") && !x[app.to_s.gsub("_custom","").to_s.gsub("_format","")].blank? ? Time.parse(x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]).strftime("%d/%m/%Y") : x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}}</span></pre>"
          end

          CouncillorsCorporation.valid_column_type("councillors").each do |app|
              aux =  aux + "<pre><b>#{CouncillorsCorporation.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if alta || last_notification.try{|x| x[app.to_s]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x[app.to_s]}}>#{cc.try{|x| x[app.to_s]}}</span></pre>"
          end

          if !last_notification.try{|x| x["termination_authorizations"]}.blank?
            aux = aux + "<fieldset><legend>Autorizaciones tras el cese</legend>"
            last_notification.try{|x| x["termination_authorizations"]}.each.with_index do |ta,index|
              aux = aux + "<hr>" if index>0
              TerminationAuthorization.valid_column_type.each do |app|
                  aux =  aux + "<pre><b>#{TerminationAuthorization.human_attribute_name(app)}:</b>"
                  aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.try{|x| x[0]}.try{|x| x[app.to_s]}}>#{app.to_s == "authorization_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
              end
            end
            aux = aux + "</fieldset>"
          end

          if !last_notification.try{|x| x["compatibility_activities"]}.blank?
            aux = aux + "<fieldset><legend>Actividades para las que tienen concedida la compatibilidad</legend>"
            last_notification.try{|x| x["compatibility_activities"]}.each.with_index do |ta,index|
              aux = aux + "<hr>" if index>0
              CompatibilityActivity.valid_column_type.each do |app|
                  aux =  aux + "<pre><b>#{CompatibilityActivity.human_attribute_name(app)}:</b>"
                  aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "resolution_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
              end
            end
            aux = aux + "</fieldset>"
          end

          if !last_notification.try{|x| x["appointment"]}.try{|x| x["charges"]}.blank?
            aux = aux + "<fieldset><legend>Cargos</legend>"
            last_notification.try{|x| x["appointment"]}.try{|x| x["charges"]}.each.with_index do |ch,index|
              aux = aux + "<hr>" if index > 0
              aux =  aux + " <span #{"style='color: orange'" if alta || ch.try{|x| x[:charge.to_s]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x["charges"]}.try{|x| x[index]}.try{|x| x[:charge.to_s]}}>#{ch.try{|x| x[:charge.to_s]}}</span>"
            end
            aux = aux + "</fieldset>"
          end

          if !last_notification.try{|x| x["appointment"]}.try{|x| x["commisions"]}.blank?
            aux = aux + "<fieldset><legend>Comisiones del pleno</legend>"
            last_notification.try{|x| x["appointment"]}.try{|x| x["commisions"]}.each.with_index do |ch,index|
              aux = aux + "<hr>" if index > 0
              aux =  aux + " <span #{"style='color: orange'" if alta || ch.try{|x| x[:commision.to_s]} != notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x["commisions"]}.try{|x| x[index]}.try{|x| x[:commision.to_s]}}>#{ch.try{|x| x[:commision.to_s]}}</span>"
            end
            aux = aux + "</fieldset>"
          end

          aux = aux + "</fieldset></div>"
        end

        if !notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.blank?
          aux = aux + "<div class='small-12 large-6 columns end' style='word-break: break-all;'><fieldset><legend>Datos nuevos</legend>"

          notification.changes_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.each.with_index do |cc, index|
            aux =  aux + "<hr>" if index > 1
            aux =  aux + "<pre><b>Corporación:</b>"
            aux =  aux + " <span #{"style='color: orange'" if alta || cc.try{|x| x["corporation"]}.try{|x| x['name']} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[index]}.try{|x| x["corporation"]}.try{|x| x['name']}}>#{cc.try{|x| x["corporation"]}.try{|x| x['name']}}</span></pre>"

            aux =  aux + "<pre><b>Afiliación:</b>"
            aux =  aux + " <span #{"style='color: orange'" if alta || cc.try{|x| x["electoral_list"]}.try{|x| x['name']} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[index]}.try{|x| x["electoral_list"]}.try{|x| x['name']}}>#{cc.try{|x| x["electoral_list"]}.try{|x| x['name']}}</span></pre>"

            aux =  aux + "<pre><b>#{CouncillorsCorporation.human_attribute_name(:order_num)}:</b>"
            aux =  aux + " <span #{"style='color: orange'" if alta || cc.try{|x| x["order_num"]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[index]}.try{|x| x["order_num"]}}>#{cc.try{|x| x["order_num"]}}</span></pre>"


            Appointment.valid_column_type("councillors").each do |app|
              aux =  aux + "<pre><b>#{Appointment.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if alta || cc.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[index]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}}}>#{cc.try{|x| x["appointment"]}.try{|x| app.to_s.include?("_format") && !x[app.to_s.gsub("_custom","").to_s.gsub("_format","")].blank? ? Time.parse(x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]).strftime("%d/%m/%Y") : x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}}</span></pre>"
            end

            CouncillorsCorporation.valid_column_type("councillors").each do |app|
                aux =  aux + "<pre><b>#{CouncillorsCorporation.human_attribute_name(app)}:</b>"
                aux =  aux + " <span #{"style='color: orange'" if alta || cc.try{|x| x[app.to_s]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{cc.try{|x| x[app.to_s]}}</span></pre>"
            end

            if !cc.try{|x| x["termination_authorizations"]}.blank?
              aux = aux + "<fieldset><legend>Autorizaciones tras el cese</legend>"
              cc.try{|x| x["termination_authorizations"]}.each.with_index do |ta,index|
                aux = aux + "<hr>" if index>0
                TerminationAuthorization.valid_column_type.each do |app|
                    aux =  aux + "<pre><b>#{TerminationAuthorization.human_attribute_name(app)}:</b>"
                    aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "authorization_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                end
              end
              aux = aux + "</fieldset>"
            end

            if !cc.try{|x| x["compatibility_activities"]}.blank?
              aux = aux + "<fieldset><legend>Actividades para las que tienen concedida la compatibilidad</legend>"
              cc.try{|x| x["compatibility_activities"]}.each.with_index do |ta,index|
                aux = aux + "<hr>" if index>0
                CompatibilityActivity.valid_column_type.each do |app|
                    aux =  aux + "<pre><b>#{CompatibilityActivity.human_attribute_name(app)}:</b>"
                    aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "resolution_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                end
              end
              aux = aux + "</fieldset>"
            end

            if !cc.try{|x| x["appointment"]}.try{|x| x["charges"]}.blank?
              aux = aux + "<fieldset><legend>Cargos</legend>"
              cc.try{|x| x["appointment"]}.try{|x| x["charges"]}.each.with_index do |ch,index|
                aux = aux + "<hr>" if index > 0
                aux =  aux + " <span #{"style='color: orange'" if alta || ch.try{|x| x[:charge.to_s]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x["charges"]}.try{|x| x[index]}.try{|x| x[:charge.to_s]}}>#{ch.try{|x| x[:charge.to_s]}}</span>"
              end
              aux = aux + "</fieldset>"
            end

            if !cc.try{|x| x["appointment"]}.try{|x| x["commisions"]}.blank?
              aux = aux + "<fieldset><legend>Comisiones del pleno</legend>"
              cc.try{|x| x["appointment"]}.try{|x| x["commisions"]}.each.with_index do |ch,index|
                aux = aux + "<hr>" if index > 0
                aux =  aux + " <span #{"style='color: orange'" if alta || ch.try{|x| x[:commision.to_s]} != notification.origin_data.try{|x| x["councillors"]}.try{|x| x[0]}.try{|x| x["councillors_corporations"]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x["commisions"]}.try{|x| x[index]}.try{|x| x[:commision.to_s]}}>#{ch.try{|x| x[:commision.to_s]}}</span>"
              end
              aux = aux + "</fieldset>"
            end

          end
          aux = aux + "</fieldset></div>"
        end
        aux = aux + "</fieldset>"


        # ============================================================================================================
        # DATOS DEL RESTO
        # ============================================================================================================
        [["Directivo", :directors], ["Eventual", :temporary_workers], ["Funcionario", :public_workers], ["Vocal vecino", :spokespeople]].each do |job|
          aux = aux + "<fieldset><legend>#{job[0]}</legend>"
          if !alta && !notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.blank?
            aux = aux + "<div class='small-12 large-6 columns'><fieldset><legend>Datos antiguos</legend>"

            Appointment.valid_column_type(job[1].to_s).each do |app|
              aux =  aux + "<pre><b>#{Appointment.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]} != notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}}}>#{notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| (app.to_s.include?("_format") && !x[app.to_s.gsub("_custom","").to_s.gsub("_format","")].blank? ? Time.parse(x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]).strftime("%d/%m/%Y") : x[app.to_s.gsub("_custom","").to_s.gsub("_format","")])}}</span></pre>"
            end
            JobLevel.valid_column_type(job[1].to_s).each do |app|
              aux =  aux + "<pre><b>#{JobLevel.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]} != notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]}}>#{ notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]}}</span></pre>"
            end


            if job[0].to_s == "Directivo"
              if !notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.blank?
                aux = aux + "<fieldset><legend>Autorizaciones tras el cese</legend>"
                notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.each.with_index do |ta,index|
                  aux = aux + "<hr>" if index>0
                  TerminationAuthorization.valid_column_type.each do |app|
                      aux =  aux + "<pre><b>#{TerminationAuthorization.human_attribute_name(app)}:</b>"
                      aux =  aux + " <span #{"style='color: orange'" if ta.try{|x| x[app.to_s]} !=  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "authorization_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                  end
                end
                aux = aux + "</fieldset>"
              end
              if !notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.blank?
                aux = aux + "<fieldset><legend>Actividades para las que tienen concedida la compatibilidad</legend>"
                notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.each.with_index do |ta,index|
                  aux = aux + "<hr>" if index>0
                  CompatibilityActivity.valid_column_type.each do |app|
                      aux =  aux + "<pre><b>#{CompatibilityActivity.human_attribute_name(app)}:</b>"
                      aux =  aux + " <span #{"style='color: orange'" if ta.try{|x| x[app.to_s]} !=  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "resolution_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                  end
                end
                aux = aux + "</fieldset>"
              end
              
              if !notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.blank?
                aux = aux + "<fieldset><legend>Cargos</legend>"
                notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.each.with_index do |ch,index|
                  aux = aux + "<hr>" if index > 0
                  aux =  aux + " <span #{"style='color: orange'" if ch.try{|x| x[:charge.to_s]} !=  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.try{|x| x[index]}.try{|x| x[:charge.to_s]}}>#{ch.try{|x| x[:charge.to_s]}}</span>"
                end
                aux = aux + "</fieldset>"
              end
    
              if !notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.blank?
                aux = aux + "<fieldset><legend>Comisiones del pleno</legend>"
                notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.each.with_index do |ch,index|
                  aux = aux + "<hr>" if index > 0
                  aux =  aux + " <span #{"style='color: orange'" if ch.try{|x| x[:commision.to_s]} !=  notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.try{|x| x[index]}.try{|x| x[:commision.to_s]}}>#{ch.try{|x| x[:commision.to_s]}}</span>"
                end
                aux = aux + "</fieldset>"
              end

            end


            aux = aux + "</fieldset></div>"
          end
          if !notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.blank?
            aux = aux + "<div class='small-12 large-6 columns end'><fieldset><legend>Datos nuevos</legend>"

            Appointment.valid_column_type(job[1].to_s).each do |app|
              aux =  aux + "<pre><b>#{Appointment.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if alta || notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.to_s.gsub("_custom","").to_s.gsub("_format","")]}.to_s != notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[app.to_s.gsub("_custom","").to_s.gsub("_format","")]}.to_s}}>#{notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| app.to_s.include?("_format") && !x[app.to_s.to_s.gsub("_custom","").to_s.gsub("_format","")].blank? ? Time.parse(x[app.to_s.to_s.gsub("_custom","").to_s.gsub("_format","")]).strftime("%d/%m/%Y") : x[app.to_s.to_s.gsub("_custom","").to_s.gsub("_format","")] }.to_s}</span></pre>"
            end
            JobLevel.valid_column_type(job[1].to_s).each do |app|
              aux =  aux + "<pre><b>#{JobLevel.human_attribute_name(app)}:</b>"
              aux =  aux + " <span #{"style='color: orange'" if alta || notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]} != notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]}}>#{ notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x[app.to_s]}}</span></pre>"
            end
            if job[0].to_s == "Directivo"
              if !notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.blank?
                aux = aux + "<fieldset><legend>Autorizaciones tras el cese</legend>"
                notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.each.with_index do |ta,index|
                  aux = aux + "<hr>" if index>0
                  TerminationAuthorization.valid_column_type.each do |app|
                      aux =  aux + "<pre><b>#{TerminationAuthorization.human_attribute_name(app)}:</b>"
                      aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} !=  notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["termination_authorizations"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "authorization_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                  end
                end
                aux = aux + "</fieldset>"
              end

              if !notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.blank?
                aux = aux + "<fieldset><legend>Actividades para las que tienen concedida la compatibilidad</legend>"
                notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.each.with_index do |ta,index|
                  aux = aux + "<hr>" if index>0
                  CompatibilityActivity.valid_column_type.each do |app|
                      aux =  aux + "<pre><b>#{CompatibilityActivity.human_attribute_name(app)}:</b>"
                      aux =  aux + " <span #{"style='color: orange'" if alta || ta.try{|x| x[app.to_s]} !=  notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["compatibility_activities"]}.try{|x| x[index]}.try{|x| x[app.to_s]}}>#{app.to_s == "resolution_date" && !ta.try{|x| x[app.to_s]}.blank? ? ta.try{|x| x[app.to_s]}.try{|x| x[0..9]}.to_date.strftime('%d/%m/%Y') : ta.try{|x| x[app.to_s]}}</span></pre>"
                  end
                end
                aux = aux + "</fieldset>"
              end

              if !notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.blank?
                aux = aux + "<fieldset><legend>Cargos</legend>"
                notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.each.with_index do |ch,index|
                  aux = aux + "<hr>" if index > 0
                  aux =  aux + " <span #{"style='color: orange'" if ch.try{|x| x[:charge.to_s]} !=  notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["charges"]}.try{|x| x[index]}.try{|x| x[:charge.to_s]}}>#{ch.try{|x| x[:charge.to_s]}}</span>"
                end
                aux = aux + "</fieldset>"
              end
    
              if !notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.blank?
                aux = aux + "<fieldset><legend>Comisiones del pleno</legend>"
                notification.changes_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.each.with_index do |ch,index|
                  aux = aux + "<hr>" if index > 0
                  aux =  aux + " <span #{"style='color: orange'" if ch.try{|x| x[:commision.to_s]} !=  notification.origin_data.try{|x| x[job[1].to_s]}.try{|x| x[0]}.try{|x| x["appointment"]}.try{|x| x[0]}.try{|x| x["commisions"]}.try{|x| x[index]}.try{|x| x[:commision.to_s]}}>#{ch.try{|x| x[:commision.to_s]}}</span>"
                end
                aux = aux + "</fieldset>"
              end
            end
            aux = aux + "</fieldset></div>"
          end
          aux=aux+"</fieldset>"
        end


        aux.html_safe
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      ""
    end

    def self.transformDataHash(notification)
        contenido = notification.try(:content_data)
        personal_code = contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']}.to_s
        data_aux = {unit: "", area: "", position: ""}
        gper_aux = notification.action_abrev

        data_aux = { unit: '', area: contenido.try {|x| x['concejal']}.try{|x| x['cjnArea']}, position: contenido.try {|x| x['concejal']}.try{|x| x['cjnAlcalde'].to_i} == 1 ? 'Alcalde' : 'Concejal'} if !contenido.try {|x| x['concejal']}.blank? && gper_aux == 'C'

        [["directivo","drn", "R"],["eventual", "evtn", "E"], ["funcionario", "fldn", "F"]].each do |job|
            if gper_aux == job[2]
                type_job = contenido.try {|x| x[job[0]]}
                data_aux = { unit: type_job.try{|x| x["#{job[1]}Unidad"]}, area: type_job.try{|x| x["#{job[1]}Area"]}, position: type_job.try{|x| x["#{job[1]}Cargo"]}} if !type_job.blank?
            end
        end

        [["vocal", "vvn", "V"]].each do |job|
            if gper_aux == job[2]
                type_job = contenido.try {|x| x[job[0]]}
                data_aux = { unit: type_job.try{|x| x["gpNombre"]}, area: type_job.try{|x| x["#{job[1]}Unidad"]}, position: type_job.try{|x| x["#{job[1]}Cargo"]}} if !type_job.blank?
            end
        end

        {
            :type_job => gper_aux.blank? ? '' : I18n.t("person_abrev.#{gper_aux}"),
            :name => "#{contenido.try{|x| x['dperNombre']}}".mb_chars.upcase,
            :last_name => "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}".mb_chars.upcase,
            :personal_code => personal_code.to_s,
            :notification_type => notification.action.blank? ? '' : notification.action,
            :email => contenido.try{|x| x['dperCorreoCorp']}.blank? ? '' : contenido.try{|x| x['dperCorreoCorp']},
            :position => data_aux[:position].to_s,
            :unit => data_aux[:unit].to_s,
            :area => data_aux[:area].to_s,
            :notification_type_date => notification.date_action.blank? ? '' : notification.date_action.to_date.strftime("%d/%m/%Y")
        }
    end

    def self.transformData(notification, actualiza = false)
        contenido = notification.try(:content_data)
        return {} if contenido.blank?

        # BUSQUEDA DE LA PERSONA
        person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
                "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
        end

        # CARGA DE DATOS DE LA PERSONA
        if !actualiza || person.blank?
            profile = Profile.new(updated_at: Time.zone.now, email: contenido.try{|x| x['dperCorreoCorp']}.blank? ? '' : contenido.try{|x| x['dperCorreoCorp']} )
            person = Person.new(
                name: contenido.try{|x| x['dperNombre']},
                last_name: "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}",
                document_nif: contenido.try{|x| x['dperDocumento']},
                document_type: DocumentType.find_by(name: contenido.try{|x| x['tipoDocumento']['tdocNombre']}),
                sex: contenido.try{|x| x['dperSexo']},
                personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']},
                profile: profile
            )
        else
            person.name = contenido.try{|x| x['dperNombre']}
            person.last_name = "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}"
            person.document_nif = contenido.try{|x| x['dperDocumento']}
            person.document_type = DocumentType.find_by(name: contenido.try{|x| x['tipoDocumento']['tdocNombre']})
            person.sex = contenido.try{|x| x['dperSexo']}
            person.personal_code = contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']}
            person.profile.email = contenido.try{|x| x['dperCorreoCorp']}.blank? ? '' : contenido.try{|x| x['dperCorreoCorp']}
        end

        # CARGA DE DATOS DE LOS PUESTOS
        if !contenido.try{|x| x['concejal']}.blank?
            aux = contenido.try{|x| x['concejal']}
            corporation = Corporation.find_by(name: aux['corporacion'].try{|x| x['cpNombre'].gsub('/','-')})
            if person.councillors.count > 0 && !corporation.blank?
                appointment = person.councillors[0].get_appointment(corporation)
                person.councillors[0].updated_at = Time.zone.now
                if appointment.blank?
                    appointment = Appointment.new(position: aux['cjnAlcalde'].to_i==1 ? 'Alcalde' : 'Concejal',
                        start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom'].to_i/1000).strftime("%d/%m/%Y"),
                        end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese'].to_i/1000).strftime("%d/%m/%Y"),
                        area: aux['cjnArea'],
                        possession_date: aux["cjnFechaPosesion"].blank? || aux["cjnFechaPosesion"].to_s=="0" ? '' : Time.at(aux["cjnFechaPosesion"].to_i/1000).strftime("%d/%m/%Y"),
                        url_possession: aux["cjnUrlPosesion"].blank? ? '' : aux["cjnUrlPosesion"],
                        url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                        functions: aux["cjnFunciones"].blank? ? '' : aux["cjnFunciones"],
                        retribution: aux["cjnRetribucion"].blank? ? '' : aux["cjnRetribucion"],
                        retribution_year: aux["cjnAnnoRetribucion"].blank? ? '' : aux["cjnAnnoRetribucion"],
                        observations: aux["cjnObservaciones"].blank? ? '' : aux["cjnObservaciones"])
                    councillor = CouncillorsCorporation.new(job_level: person.councillors[0], appointment: appointment,
                        corporation: corporation, order_num: aux['numLista'])

                    councillor.termination_authorizations = []
                    if !aux["autorizacionesCese"].blank?
                      aux["autorizacionesCese"].each do |act|
                          councillor.termination_authorizations << TerminationAuthorization.new(authorization_date: act["atcFecha"].blank? ? '' : Time.at(act["atcFecha"].to_i/1000).strftime("%d/%m/%Y"),
                            authorization_description: act["atcDescripcion"].blank? ? '' : act["atcDescripcion"])
                      end
                    end
                    councillor.compatibility_activities = []
                    if !aux["actividadesCompatibilidad"].blank?
                      aux["actividadesCompatibilidad"].each do |act|
                          councillor.compatibility_activities << CompatibilityActivity.new(resolution_date: act["accFechaResolucion"].blank? ? '' : Time.at(act["accFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                            compatibility_description: act["accDescripcion"].blank? ? '' : act["accDescripcion"],
                            public_private: act["accPrivadaPublica"].blank? ? '' : act["accPrivadaPublica"].to_s == "1" ? "Pública" :  act["accPrivadaPublica"].to_s == "2" ? "Privada" : act["accPrivadaPublica"])
                      end
                    end
                    person.councillors[0].councillors_corporations << councillor
                else
                    person.councillors[0].councillors_corporations.each do |cc|
                        if cc.corporation_id == corporation.id
                            councillor = cc
                            break
                        end
                    end
                    councillor.order_num = aux['numLista']
                    list = ElectoralList.find_by("long_name LIKE '%#{aux['afiliacion']}%' OR name Like '%#{aux['afiliacion']}%'")
                    if list.blank?
                        list = ElectoralList.new(long_name: aux['afiliacion'], name: aux['afiliacion'])
                        if list.save
                            aux_list = CorporationsElectoralList.new(corporation: corporation, electoral_list: list, order: 0)
                            aux_list.save
                        end
                    end
                    councillor.electoral_list = list
                    appointment.position = aux['cjnAlcalde'].to_i == 1 ? 'Alcalde' : 'Concejal'
                    appointment.start_date = aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom'].to_i/1000).strftime("%d/%m/%Y")
                    appointment.end_date = aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese'].to_i/1000).strftime("%d/%m/%Y")
                    appointment.area = aux['cjnArea']
                    appointment.possession_date = aux["cjnFechaPosesion"].blank? || aux["cjnFechaPosesion"].to_s=="0" ? '' : Time.at(aux["cjnFechaPosesion"].to_i/1000).strftime("%d/%m/%Y")
                    appointment.url_possession = aux["cjnUrlPosesion"].blank? ? '' : aux["cjnUrlPosesion"]
                    appointment.url_form= aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"]
                    appointment.functions = aux["cjnFunciones"].blank? ? '' : aux["cjnFunciones"]
                    appointment.retribution = aux["cjnRetribucion"].blank? ? '' : aux["cjnRetribucion"]
                    appointment.retribution_year = aux["cjnAnnoRetribucion"].blank? ? '' : aux["cjnAnnoRetribucion"]
                    appointment.observations = aux["cjnObservaciones"].blank? ? '' : aux["cjnObservaciones"]
                    councillor.appointment=appointment

                    councillor.termination_authorizations = []
                    if !aux["autorizacionesCese"].blank?
                      aux["autorizacionesCese"].each do |act|
                          councillor.termination_authorizations << TerminationAuthorization.new(authorization_date: act["atcFecha"].blank? ? '' : Time.at(act["atcFecha"].to_i/1000).strftime("%d/%m/%Y"),
                            authorization_description: act["atcDescripcion"].blank? ? '' : act["atcDescripcion"])
                      end
                    end
                    councillor.compatibility_activities = []
                    if !aux["actividadesCompatibilidad"].blank?
                      aux["actividadesCompatibilidad"].each do |act|
                          councillor.compatibility_activities << CompatibilityActivity.new(resolution_date: act["accFechaResolucion"].blank? ? '' : Time.at(act["accFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                            compatibility_description: act["accDescripcion"].blank? ? '' : act["accDescripcion"],
                            public_private: act["accPrivadaPublica"].blank? ? '' : act["accPrivadaPublica"].to_s == "1" ? "Pública" :  act["accPrivadaPublica"].to_s == "2" ? "Privada" : act["accPrivadaPublica"])
                      end
                    end
                end
            elsif person.councillors.count > 0 && corporation.blank?
                person.councillors[0].updated_at = Time.zone.now
                corporation = Corporation.new(
                    name: aux['corporacion'].try{|x| x['cpNombre'].gsub('/','-')},
                    description: aux['corporacion'].try{|x| x['cpObserv']},
                    active: aux['corporacion'].try{|x| x['cpActiva']},
                    end_year: aux['corporacion'].try{|x| x['cpAnioFin']},
                    start_year: aux['corporacion'].try{|x| x['cpAnioInicio']},
                    councillors_num: aux['corporacion'].try{|x| x['cpNumConcej']},
                    end_corporation_date: aux['corporacion'].try{|x| x['cpFechaFinal']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaFinal']}.to_i/1000).strftime("%d/%m/%Y"),
                    election_date: aux['corporacion'].try{|x| x['cpFechaElecciones']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaElecciones']}.to_i/1000).strftime("%d/%m/%Y"),
                    full_constitutional_date:aux['corporacion'].try{|x| x['cpFechaPlenoConst']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaPlenoConst']}.to_i/1000).strftime("%d/%m/%Y"),
                )
                corporation.save

                appointment = Appointment.new(position: aux['cjnAlcalde'].to_i == 1 ? 'Alcalde' : 'Concejal',
                    start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom'].to_i/1000).strftime("%d/%m/%Y"),
                    end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese'].to_i/1000).strftime("%d/%m/%Y"),
                    area: aux['cjnArea'],
                    possession_date: aux["cjnFechaPosesion"].blank? || aux["cjnFechaPosesion"].to_s=="0" ? '' : Time.at(aux["cjnFechaPosesion"].to_i/1000).strftime("%d/%m/%Y"),
                    url_possession: aux["cjnUrlPosesion"].blank? ? '' : aux["cjnUrlPosesion"],
                    url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                    functions: aux["cjnFunciones"].blank? ? '' : aux["cjnFunciones"],
                    retribution: aux["cjnRetribucion"].blank? ? '' : aux["cjnRetribucion"],
                    retribution_year: aux["cjnAnnoRetribucion"].blank? ? '' : aux["cjnAnnoRetribucion"],
                    observations: aux["cjnObservaciones"].blank? ? '' : aux["cjnObservaciones"])
                councillor = CouncillorsCorporation.new(job_level: person.councillors[0], appointment: appointment,
                    corporation: corporation, order_num: aux['numLista'])


                councillor.termination_authorizations = []
                if !aux["autorizacionesCese"].blank?
                  aux["autorizacionesCese"].each do |act|
                      councillor.termination_authorizations << TerminationAuthorization.new(authorization_date: act["atcFecha"].blank? ? '' : Time.at(act["atcFecha"].to_i/1000).strftime("%d/%m/%Y"),
                        authorization_description: act["atcDescripcion"].blank? ? '' : act["atcDescripcion"])
                  end
                end
                councillor.compatibility_activities = []
                if !aux["actividadesCompatibilidad"].blank?
                  aux["actividadesCompatibilidad"].each do |act|
                      councillor.compatibility_activities << CompatibilityActivity.new(resolution_date: act["accFechaResolucion"].blank? ? '' : Time.at(act["accFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                        compatibility_description: act["accDescripcion"].blank? ? '' : act["accDescripcion"],
                        public_private: act["accPrivadaPublica"].blank? ? '' : act["accPrivadaPublica"].to_s == "1" ? "Pública" :  act["accPrivadaPublica"].to_s == "2" ? "Privada" : act["accPrivadaPublica"])
                  end
                end
                person.councillors[0].councillors_corporations << councillor

                aux['corporacion']['listasElectorales'].each do |l|
                    list = ElectoralList.find_by("long_name LIKE '%#{l['leNombreLista']}%' OR name Like '%#{l['leNombreLista']}%'")
                    if list.blank?
                        list = ElectoralList.new(long_name: l['leNombreLista'],name: l['leNombreLista'])
                        if list.save
                            aux_list = CorporationsElectoralList.new(corporation: corporation, electoral_list: list, order: 0)
                            aux_list.save
                        end
                    end

                end

                list = ElectoralList.find_by("long_name LIKE '%#{aux['afiliacion']}%' OR name Like '%#{ aux['afiliacion']}%'")
                if list.blank?
                    list = ElectoralList.new(long_name: aux['afiliacion'], name:  aux['afiliacion'])
                    if list.save
                        aux_list = CorporationsElectoralList.new(corporation: corporation, electoral_list: list, order: 0)
                        aux_list.save
                    end
                end
                councillor.electoral_list = list

                person.councillors << job
            else
                job = JobLevel.new(person: person,person_type: 'councillor')
                job.updated_at = Time.zone.now
                corporation = Corporation.find_by(name: aux['corporacion']['cpNombre'].gsub('/','-'))
                if corporation.blank?
                    corporation = Corporation.new(
                        name: aux['corporacion'].try{|x| x['cpNombre'].gsub('/','-')},
                        description: aux['corporacion'].try{|x| x['cpObserv']},
                        active: aux['corporacion'].try{|x| x['cpActiva']},
                        end_year: aux['corporacion'].try{|x| x['cpAnioFin']},
                        start_year: aux['corporacion'].try{|x| x['cpAnioInicio']},
                        councillors_num: aux['corporacion'].try{|x| x['cpNumConcej']},
                        end_corporation_date: aux['corporacion'].try{|x| x['cpFechaFinal']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaFinal']}.to_i/1000).strftime("%d/%m/%Y"),
                        election_date: aux['corporacion'].try{|x| x['cpFechaElecciones']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaElecciones']}.to_i/1000).strftime("%d/%m/%Y"),
                        full_constitutional_date:aux['corporacion'].try{|x| x['cpFechaPlenoConst']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaPlenoConst']}.to_i/1000).strftime("%d/%m/%Y"),
                    )
                    corporation.save
                end
                appointment = Appointment.new(position: aux['cjnAlcalde'].to_i == 1 ? 'Alcalde' : 'Concejal',
                    start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom'].to_i/1000).strftime("%d/%m/%Y"),
                    end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese'].to_i/1000).strftime("%d/%m/%Y"),
                    area: aux['cjnArea'],
                    possession_date: aux["cjnFechaPosesion"].blank? || aux["cjnFechaPosesion"].to_s == "0" ? '' : Time.at(aux["cjnFechaPosesion"].to_i/1000).strftime("%d/%m/%Y"),
                    url_possession: aux["cjnUrlPosesion"].blank? ? '' : aux["cjnUrlPosesion"],
                    url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                    functions: aux["cjnFunciones"].blank? ? '' : aux["cjnFunciones"],
                    retribution: aux["cjnRetribucion"].blank? ? '' : aux["cjnRetribucion"],
                    retribution_year: aux["cjnAnnoRetribucion"].blank? ? '' : aux["cjnAnnoRetribucion"],
                    observations: aux["cjnObservaciones"].blank? ? '' : aux["cjnObservaciones"])
                councillor = CouncillorsCorporation.new(job_level: job, appointment: appointment,
                    corporation: corporation, order_num: aux['numLista'])

                    councillor.termination_authorizations = []
                    if !aux["autorizacionesCese"].blank?
                      aux["autorizacionesCese"].each do |act|
                        councillor.termination_authorizations << TerminationAuthorization.new(authorization_date: act["atcFecha"].blank? ? '' : Time.at(act["atcFecha"].to_i/1000).strftime("%d/%m/%Y"),
                          authorization_description: act["atcDescripcion"].blank? ? '' : act["atcDescripcion"])
                      end
                    end
                    councillor.compatibility_activities = []
                    if !aux["actividadesCompatibilidad"].blank?
                      aux["actividadesCompatibilidad"].each do |act|
                        councillor.compatibility_activities << CompatibilityActivity.new(resolution_date: act["accFechaResolucion"].blank? ? '' : Time.at(act["accFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                          compatibility_description: act["accDescripcion"].blank? ? '' : act["accDescripcion"],
                          public_private: act["accPrivadaPublica"].blank? ? '' : act["accPrivadaPublica"].to_s == "1" ? "Pública" :  act["accPrivadaPublica"].to_s == "2" ? "Privada" : act["accPrivadaPublica"])
                      end
                    end
                job.councillors_corporations << councillor

                aux['corporacion']['listasElectorales'].each do |l|
                    list = ElectoralList.find_by("long_name LIKE '%#{l['leNombreLista']}%' OR name Like '%#{l['leNombreLista']}%'")
                    if list.blank?
                        list = ElectoralList.new(long_name: l['leNombreLista'], name: l['leNombreLista'])
                        if list.save
                            aux_list = CorporationsElectoralList.new(corporation: corporation, electoral_list: list, order: 0)
                            aux_list.save
                        end
                    end
                end

                list = ElectoralList.find_by("long_name LIKE '%#{aux['afiliacion']}%' OR name Like '%#{aux['afiliacion']}%'")
                if list.blank?
                    list = ElectoralList.new(long_name: aux['afiliacion'],name: aux['afiliacion'])
                    if list.save
                        aux_list = CorporationsElectoralList.new(corporation: corporation, electoral_list: list, order: 0)
                        aux_list.save
                    end
                end
                councillor.electoral_list = list
                person.councillors << job
            end
        end


        [["directivo","drn", :directors, "director"],["eventual", "evtn", :temporary_workers, "temporary_worker"], ["funcionario", "fldn", :public_workers, "public_worker"]].each do |ent|
            if !contenido.try{|x| x[ent[0]]}.blank?
                aux = contenido.try{|x| x[ent[0]]}
                if person.send(ent[2]).count > 0
                    person.send(ent[2])[0].updated_at = Time.zone.now
                    person.send(ent[2])[0].appointment.position = aux["#{ent[1]}Cargo"]
                    person.send(ent[2])[0].appointment.unit = aux["#{ent[1]}Unidad"]
                    person.send(ent[2])[0].appointment.start_date = aux["#{ent[1]}FechaNomb"].blank? ? '' : Time.at(aux["#{ent[1]}FechaNomb"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.end_date = aux["#{ent[1]}FechaCese"].blank? ? '' : Time.at(aux["#{ent[1]}FechaCese"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.area = aux["#{ent[1]}Area"]
                    person.send(ent[2])[0].appointment.possession_date = aux["#{ent[1]}FechaPosesion"].blank? || aux["#{ent[1]}FechaPosesion"].to_s=="0" ? '' : Time.at(aux["#{ent[1]}FechaPosesion"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.description_possession = aux["#{ent[1]}DescripcionPosesion"].blank? ? '' : aux["#{ent[1]}DescripcionPosesion"]
                    person.send(ent[2])[0].appointment.email = aux["#{ent[1]}Email"].blank? ? '' : aux["#{ent[1]}Email"]
                    person.send(ent[2])[0].appointment.functions = aux["#{ent[1]}Funciones"].blank? ? '' : aux["#{ent[1]}Funciones"]
                    person.send(ent[2])[0].appointment.retribution = aux["#{ent[1]}Retribucion"].blank? ? '' : aux["#{ent[1]}Retribucion"]
                    person.send(ent[2])[0].appointment.retribution_year = aux["#{ent[1]}AnnoRetribucion"].blank? ? '' : aux["#{ent[1]}AnnoRetribucion"]
                    person.send(ent[2])[0].appointment.observations = aux["#{ent[1]}Observaciones"].blank? ? '' : aux["#{ent[1]}Observaciones"]
                    person.send(ent[2])[0].appointment.description = aux["#{ent[1]}Descripcion"].blank? ? '' : aux["#{ent[1]}Descripcion"]
                    if ent[2].to_s=="directors"
                        person.send(ent[2])[0].termination_authorizations = []
                        if !aux["autorizacionesCese"].blank?
                          aux["autorizacionesCese"].each do |act|
                            person.send(ent[2])[0].termination_authorizations << TerminationAuthorization.new(authorization_date: act["atrFecha"].blank? ? '' : Time.at(act["atrFecha"].to_i/1000).strftime("%d/%m/%Y"),
                            authorization_description: act["atrDescripcion"].blank? ? '' : act["atrDescripcion"])
                          end
                        end
                        person.send(ent[2])[0].compatibility_activities = []
                        if !aux["actividadesCompatibilidad"].blank?
                          aux["actividadesCompatibilidad"].each do |act|
                            person.send(ent[2])[0].compatibility_activities << CompatibilityActivity.new(resolution_date: act["acrFechaResolucion"].blank? ? '' : Time.at(act["acrFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                              compatibility_description: act["acrDescripcion"].blank? ? '' : act["acrDescripcion"],
                              public_private: act["acrPrivadaPublica"].blank? ? '' : act["acrPrivadaPublica"].to_s == "1" ? "Pública" :  act["acrPrivadaPublica"].to_s == "2" ? "Privada" : act["acrPrivadaPublica"])
                          end
                        end
                    end
                else
                    appointment = Appointment.new(position: aux["#{ent[1]}Cargo"], unit: aux["#{ent[1]}Unidad"],
                        start_date: aux["#{ent[1]}FechaNomb"].blank? ? '' : Time.at(aux["#{ent[1]}FechaNomb"].to_i/1000).strftime("%d/%m/%Y"),
                        end_date: aux["#{ent[1]}FechaCese"].blank? ? '' : Time.at(aux["#{ent[1]}FechaCese"].to_i/1000).strftime("%d/%m/%Y"),
                        area: aux["#{ent[1]}Area"],
                        possession_date: aux["#{ent[1]}FechaPosesion"].blank? || aux["#{ent[1]}FechaPosesion"].to_s=="0" ? '' : Time.at(aux["#{ent[1]}FechaPosesion"].to_i/1000).strftime("%d/%m/%Y"),
                        description_possession: aux["#{ent[1]}DescripcionPosesion"].blank? ? '' : aux["#{ent[1]}DescripcionPosesion"],
                        email: aux["#{ent[1]}Email"].blank? ? '' : aux["#{ent[1]}Email"],
                        functions: aux["#{ent[1]}Funciones"].blank? ? '' : aux["#{ent[1]}Funciones"],
                        retribution: aux["#{ent[1]}Retribucion"].blank? ? '' : aux["#{ent[1]}Retribucion"],
                        retribution_year: aux["#{ent[1]}AnnoRetribucion"].blank? ? '' : aux["#{ent[1]}AnnoRetribucion"],
                        observations: aux["#{ent[1]}Observaciones"].blank? ? '' : aux["#{ent[1]}Observaciones"],
                        description: aux["#{ent[1]}Descripcion"].blank? ? '' : aux["#{ent[1]}Descripcion"])
                    job = JobLevel.new(person: person, person_type: ent[3], appointment: appointment)
                    if ent[2].to_s == "directors"
                        job.termination_authorizations = []
                        if !aux["autorizacionesCese"].blank?
                          aux["autorizacionesCese"].each do |act|
                            job.termination_authorizations << TerminationAuthorization.new(authorization_date: act["atrFecha"].blank? ? '' : Time.at(act["atrFecha"].to_i/1000).strftime("%d/%m/%Y"),
                                authorization_description: act["atrDescripcion"].blank? ? '' : act["atrDescripcion"])
                          end
                        end
                        job.compatibility_activities = []
                        if !aux["actividadesCompatibilidad"].blank?
                          aux["actividadesCompatibilidad"].each do |act|
                            job.compatibility_activities << CompatibilityActivity.new(resolution_date: act["acrFechaResolucion"].blank? ? '' : Time.at(act["acrFechaResolucion"].to_i/1000).strftime("%d/%m/%Y"),
                                compatibility_description: act["acrDescripcion"].blank? ? '' : act["acrDescripcion"],
                                public_private: act["acrPrivadaPublica"].blank? ? '' : act["acrPrivadaPublica"].to_s == "1" ? "Pública" :  act["acrPrivadaPublica"].to_s == "2" ? "Privada" : act["acrPrivadaPublica"])
                          end
                        end
                    end
                    job.updated_at = Time.zone.now
                    person.send(ent[2]) << job
                end
            end
        end

        [["vocal", "vvn", :spokespeople, "spokesperson"]].each do |ent|
            if !contenido.try{|x| x[ent[0]]}.blank?
                aux = contenido.try{|x| x[ent[0]]}
                if person.send(ent[2]).count > 0
                    person.send(ent[2])[0].updated_at = Time.zone.now
                    person.send(ent[2])[0].appointment.position = aux["#{ent[1]}Cargo"]
                    person.send(ent[2])[0].appointment.unit = aux["gpNombre"]
                    person.send(ent[2])[0].appointment.start_date = aux["#{ent[1]}FechaNomb"].blank? ? '' : Time.at(aux["#{ent[1]}FechaNomb"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.end_date = aux["#{ent[1]}FechaCese"].blank? ? '' : Time.at(aux["#{ent[1]}FechaCese"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.area = aux["#{ent[1]}Unidad"]
                    person.send(ent[2])[0].appointment.district = aux["disNombre"]
                    person.send(ent[2])[0].appointment.politic_group = aux["gpNombre"]
                    person.send(ent[2])[0].appointment.corporation = aux["cpNombre"]
                    person.send(ent[2])[0].appointment.juntamd = aux["#{ent[1]}JuntaMd"]
                    person.send(ent[2])[0].appointment.retribution = aux["#{ent[1]}Retribucion"].blank? ? '' : aux["#{ent[1]}Retribucion"]
                    person.send(ent[2])[0].appointment.retribution_year = aux["#{ent[1]}AnnoRetribucion"].blank? ? '' : aux["#{ent[1]}AnnoRetribucion"]
                    person.send(ent[2])[0].appointment.observations = aux["#{ent[1]}Observaciones"].blank? ? '' : aux["#{ent[1]}Observaciones"]
                    person.send(ent[2])[0].appointment.possession_date = aux["#{ent[1]}FechaPosesion"].blank? || aux["#{ent[1]}FechaPosesion"].to_s == "0" ? '' : Time.at(aux["#{ent[1]}FechaPosesion"].to_i/1000).strftime("%d/%m/%Y")
                    person.send(ent[2])[0].appointment.description_possession = aux["#{ent[1]}DescripcionPosesion"].blank? ? '' : aux["#{ent[1]}DescripcionPosesion"]
                else
                    appointment = Appointment.new(position: aux["#{ent[1]}Cargo"], unit: aux["gpNombre"],
                        start_date: aux["#{ent[1]}FechaNomb"].blank? ? '' : Time.at(aux["#{ent[1]}FechaNomb"].to_i/1000).strftime("%d/%m/%Y"),
                        end_date: aux["#{ent[1]}FechaCese"].blank? ? '' : Time.at(aux["#{ent[1]}FechaCese"].to_i/1000).strftime("%d/%m/%Y"),
                        area: aux["#{ent[1]}Unidad"], district: aux["disNombre"], politic_group: aux["gpNombre"], corporation: aux["cpNombre"],juntamd: aux["#{ent[1]}JuntaMd"],
                        retribution: aux["#{ent[1]}Retribucion"].blank? ? '' : aux["#{ent[1]}Retribucion"],
                        retribution_year: aux["#{ent[1]}AnnoRetribucion"].blank? ? '' : aux["#{ent[1]}AnnoRetribucion"],
                        observations: aux["#{ent[1]}Observaciones"].blank? ? '' : aux["#{ent[1]}Observaciones"],
                        possession_date: aux["#{ent[1]}FechaPosesion"].blank? || aux["#{ent[1]}FechaPosesion"].to_s == "0" ? '' : Time.at(aux["#{ent[1]}FechaPosesion"].to_i/1000).strftime("%d/%m/%Y"),
                        description_possession: aux["#{ent[1]}DescripcionPosesion"].blank? ? '' : aux["#{ent[1]}DescripcionPosesion"]
                    )
                    job = JobLevel.new(person: person,person_type: ent[3], appointment: appointment)
                    job.updated_at = Time.zone.now
                    person.send(ent[2]) << job
                end
            end
        end

        person
    rescue
        {}
    end

    def self.get_data_json(person)
      person.to_json(include: [:profile,:document_type,
        {:councillors =>
          {include: [:councillors_corporations=>
            {include: [:appointment,:corporation,:electoral_list,:termination_authorizations,:compatibility_activities]}
          ]}
        },
        {:directors => {include: [:appointment,:termination_authorizations,:compatibility_activities]}},
        {:spokespeople=> {include: [:appointment]}},
        {:temporary_workers=> {include: [:appointment]}},
        {:public_workers=> {include: [:appointment]}}])
    end

    def validate_job
      if (self.job_levels.blank?) && (self.councillors.blank?) && (self.directors.blank?) && (self.temporary_workers.blank?) && (self.public_workers.blank?) && (self.spokespeople.blank?)
        self.errors.add(:job_levels, "Es necesario asociar un perfil para crear una persona en TRAPE por administración")

      end
    end
end
