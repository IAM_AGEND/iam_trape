require 'active_record/diff'
class Appointment < ActiveRecord::Base
    include Validator
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]

    has_many :job_levels
    has_many :councillors_corporations
    has_many :charges
    has_many :commisions

    accepts_nested_attributes_for :charges, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :commisions, reject_if: :all_blank, allow_destroy: true

    validates :position, presence: true
    validate :date_greather
    validates :retribution_year, length: { maximum: 4}


    after_save :touch_job_levels
    after_save :touch_councillors_corporations


    def self.valid_column_type(type)
        case type.to_s
        when "councillors"
            [:position,:position_alt,:unit,:start_date_format,:end_date_format,:possession_date_format,:url_possession, :url_form,:functions,:retribution_custom,:retribution_year,:observations]
        when "directors"
            [ :position,:unit,:area,:start_date_format,:end_date_format,:possession_date_format,:description_possession,:email,:functions,:retribution_custom,:retribution_year,:observations,:description]
        when "public_workers"
            [ :position,:unit,:area,:start_date_format,:end_date_format,:possession_date_format]
        when "temporary_workers"
            [ :position,:unit,:area,:start_date_format,:end_date_format,:possession_date_format,:description_possession,:retribution_custom,:retribution_year,:observations]
        when "spokespeople"
            [ :position,:unit,:area,:start_date_format,:end_date_format, :possession_date_format,:description_possession, :retribution_custom,:retribution_year,:observations]
        else
            []
        end
    end

    def retribution_custom
        return "" if retribution.blank?
        ActionController::Base.helpers.number_to_currency(retribution, precision: 2)
    rescue
        retribution
    end

    def start_date_format
        start_date.strftime('%d/%m/%Y')
    rescue
        start_date
    end

    def end_date_format
        end_date.strftime('%d/%m/%Y')
    rescue
        end_date
    end

    def possession_date_format
        possession_date.strftime('%d/%m/%Y')
    rescue
        possession_date
    end

    def url_possession_format
        if !self.url_possession.blank?
            if self.url_possession.include?("http")
                self.url_possession
            else
                "http://#{self.url_possession}"
            end
        end
    end

  private

    def touch_job_levels
        job_levels.find_each(&:touch)
    end

    def touch_councillors_corporations
        councillors_corporations.find_each(&:touch)
    end
end
