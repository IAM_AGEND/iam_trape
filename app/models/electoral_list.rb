require 'active_record/diff'
class ElectoralList < ActiveRecord::Base
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]

    has_many :corporations_electoral_lists
    
    validates :name, :long_name, uniqueness: true
    validates :long_name, presence: true

    has_attached_file :logo,
        styles: { medium: "245x245>", thumb: "100x100>" },
        url: "/system/people/:class/:id/:style/logo.:extension",
        default_url: "/missing.png",
        use_timestamp: false
    validates_attachment_content_type :logo, content_type: %r{\Aimage\/.*\z}

    def self.main_columns
        [:long_name]
    end
end
