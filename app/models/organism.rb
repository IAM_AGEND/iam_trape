class Organism < ActiveRecord::Base   

    belongs_to :councillor_corporation
    validates :title, presence: :true
end