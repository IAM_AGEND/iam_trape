require 'active_record/diff'
class Profile < ActiveRecord::Base
    include OrdersCommon
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]

    has_many :people
    has_many :studies, dependent: :destroy
    has_many :courses, dependent: :destroy
    has_many :public_jobs, dependent: :destroy
    has_many :private_jobs, dependent: :destroy
    has_many :political_posts, dependent: :destroy
    has_many :languages, dependent: :destroy
    has_many :informations, dependent: :destroy
    has_many :publications,-> {where(information_type: InformationType.find_by(name: "publications"))}, :class_name => 'Information', dependent: :destroy
    has_many :studies_comments, ->{where(information_type: InformationType.find_by(name: "studies_comments")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :courses_comments, ->{where(information_type: InformationType.find_by(name: "courses_comments")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :career_comments, ->{where(information_type: InformationType.find_by(name: "career_comments")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :political_posts_comments, ->{where(information_type: InformationType.find_by(name: "political_posts_comments")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :theacher_activity, ->{where(information_type: InformationType.find_by(name: "theacher_activity")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :special_mentions, ->{where(information_type: InformationType.find_by(name: "special_mentions")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy
    has_many :other, ->{where(information_type: InformationType.find_by(name: "other")).order(created_at: :asc).limit(1)}, :class_name => 'Information', dependent: :destroy

    accepts_nested_attributes_for :studies,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :courses,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :public_jobs, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :private_jobs, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :political_posts, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :languages, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :informations, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :publications, reject_if: :publication_blank?, allow_destroy: true
    accepts_nested_attributes_for :studies_comments, allow_destroy: true
    accepts_nested_attributes_for :courses_comments, allow_destroy: true
    accepts_nested_attributes_for :career_comments, allow_destroy: true
    accepts_nested_attributes_for :political_posts_comments, allow_destroy: true
    accepts_nested_attributes_for :theacher_activity, allow_destroy: true
    accepts_nested_attributes_for :special_mentions, allow_destroy: true
    accepts_nested_attributes_for :other, allow_destroy: true

    validates_associated :studies
    validates_associated :courses
    validates_associated :public_jobs
    validates_associated :political_posts
    validates_associated :languages
    validates_associated :informations
    validates_associated :publications
    validates_associated :studies_comments
    validates_associated :courses_comments
    validates_associated :career_comments
    validates_associated :political_posts_comments
    validates_associated :theacher_activity
    validates_associated :special_mentions
    validates_associated :other

    #validate :duplicated_email
    validates :updated_at, presence: true
    validate :order_tables

    has_attached_file :portrait,
        styles: { medium: "245x245>", thumb: "100x100>" },
        url: "/system/people/:class/:id/:style/portrait.:extension",
        default_url: "/missing.png",
        use_timestamp: false
    validates_attachment_content_type :portrait, content_type: %r{\Aimage\/.*\z}


    def order_tables
        valid_order(self.studies) && valid_order(self.courses) && valid_order(self.languages) && valid_order(self.public_jobs) && valid_order(self.private_jobs) && valid_order(self.political_posts) && valid_order(self.publications)
    end

    self.record_timestamps = false

    def publication_blank?(att)
        att.reject { |_,v| v.blank?}
        att['information'].blank?
    end

    def has_profile?
        return false if self.blank?
        aux_1 = !self.try(:studies).blank? || !self.try(:courses).blank? || !self.try(:languages).blank? || !self.try(:studies_comments).try{|i| i.first}.try(:information).blank?
        aux_2 = !self.try(:public_jobs).blank? || !self.try(:private_jobs).blank? || !self.try(:career_comments).try{|i| i.first}.try(:information).blank? || !self.try(:political_posts_comments).try{|i| i.first}.try(:information).blank?
        aux_3 = !self.try(:political_posts).blank? || !self.try(:publications).try{|i| i.first}.try(:information).blank? || !self.try(:theacher_activity).try{|i| i.first}.try(:information).blank?
        aux_4 = !self.try(:special_mentions).try{|i| i.first}.try(:information).blank? || (!self.try(:other).try{|i| i.first}.try(:information).blank? && !self.try(:other).try{|i| i.first}.try(:information).to_s.include?("El contenido del perfil y trayectoria profesional")) || !self.try(:courses_comments).try{|i| i.first}.try(:information).blank?
        if aux_1 || aux_2 || aux_3 || aux_4
            aux= true
        else
            aux=false
        end
        aux
    end

    def self.especific_columns
        [
            :person_type,
            :updated_at,
            :name,
            :last_name,
            :personal_code
        ]
    end

    def person_type
        self.people.first.try(:type_job)
    end

    def personal_code
        self.people.first.try(:personal_code)
    end

    def name
        self.people.first.try(:name)
    end

    def last_name
        self.people.first.try(:last_name)
    end

    def self.getNotificationData(notification)
        aux = ''
        profile = self.transformData(notification)

        gper = notification.try(:content_data).try{|x| x['gperAbrev']}
        if gper.blank?
          gper = 'C' if !notification.try(:content_data).try{|x| x['ultimoConcejal']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoConcejal']}.try{|x| x[0]}.try{|x| x["cjnFechaCese"]}.blank?
          gper = 'R' if !notification.try(:content_data).try{|x| x['ultimoDirectivo']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoDirectivo']}.try{|x| x[0]}.try{|x| x["drnFechaCese"]}.blank?
          gper = 'E' if !notification.try(:content_data).try{|x| x['ultimoEventual']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoEventual']}.try{|x| x[0]}.try{|x| x["evtnFechaCese"]}.blank?
          gper = 'F' if !notification.try(:content_data).try{|x| x['ultimoFuncionario']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoFuncionario']}.try{|x| x[0]}.try{|x| x["fldnFechaCese"]}.blank?
          gper = 'V' if !notification.try(:content_data).try{|x| x['ultimoVocal']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoVocal']}.try{|x| x[0]}.try{|x| x["vvnFechaCese"]}.blank?
        end

        aux = aux + "<h1>Vista del perfil - #{notification.historic? ? "Histórico" : "Pendiente"}</h1><br>"
        aux = aux + "<fieldset><legend>Datos de la notificación</legend>"
        aux = aux + "<b>#{Person.human_attribute_name(:type_job)}:</b> #{gper.blank? ? '' : I18n.t("person_abrev.#{gper}")}<br>"
        aux = aux + "<b>#{Notification.human_attribute_name(:created_at)}:</b> #{notification.created_at}<br>"
        if notification.historic?
            aux = aux + "<b>#{Notification.human_attribute_name(:type_action)}:</b> #{notification.type_action}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:change_state_at)}:</b> #{notification.change_state_at}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:administrator)}:</b> #{notification.try(:author).blank? ? "Administrador del sistema" : notification.try(:author) }<br>"
        else
            aux = aux + "<b>#{Notification.human_attribute_name(:no_publish_reason)}:</b> #{notification.no_publish_reason}<br>"
        end
        aux = aux + "</fieldset>"


        aux = aux + "<fieldset><legend>Datos del perfil</legend><table><thead><tr>"
        [ :email, :personal_email,:twitter, :facebook, :updated_at ].each {|x| aux =  aux + "<th>#{Profile.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody><tr>"
        [ :email,:personal_email, :twitter, :facebook, :updated_at ].each {|x| aux =  aux + "<td>#{profile.try(x)}</td>" }
        aux = aux + "</tr></tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Datos de la persona</legend><table><thead><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<th>#{Person.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<td>#{profile.try(:people).try {|x| x[0]}.try(x).to_s.upcase}</td>"}
        aux = aux + "</tr></tbody></table>"
        aux = aux + "<fieldset><legend>Concejal</legend><table><thead><tr>"
        aux = aux + "<th>Corporación</th><th>Afiliación</th><th>#{CouncillorsCorporation.human_attribute_name(:order_num)}</th>"
        Appointment.valid_column_type("councillors").each {|x| aux =  aux + "<th>#{Appointment.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:people).try {|x| x[0]}.try(:councillors).blank?
            profile.try(:people).try {|x| x[0]}.try(:councillors).each do |l|
                l.councillors_corporations.each do |c|
                    aux = aux + "<tr>"
                    aux = aux + "<td>#{c.try(:corporation).try(:name)}</td>"
                    aux = aux + "<td>#{c.try(:electoral_list).try(:name)}</td>"
                    aux = aux + "<td>#{c.try(:order_num)}</td>"
                    Appointment.valid_column_type("councillors").each {|x| aux =  aux + "<td>#{c.try(:appointment).try(x)}</td>" }
                    aux = aux + "</tr>"
                end

            end
        end
        aux = aux + "</tbody></table><table><thead><tr>"
        CouncillorsCorporation.valid_column_type("councillors").each {|x| aux =  aux + "<th>#{CouncillorsCorporation.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:people).try {|x| x[0]}.try(:councillors).blank?
            profile.try(:people).try {|x| x[0]}.try(:councillors).each do |l|
                l.councillors_corporations.each do |c|
                    aux = aux + "<tr>"
                    CouncillorsCorporation.valid_column_type("concillors").each {|x| ux =  aux + "<td>#{c.try(x)}</td>" }
                    aux = aux + "</tr>"
                end

            end
        end
        aux= aux +"</tbody></table></fieldset>"

        [['Directivo', :directors], ['Eventual', :temporary_workers], ['Funcionario', :public_workers], ['Vocal vecino', :spokespeople]].each do |type_profile|
             aux = aux + "<fieldset><legend>#{type_profile[0]}</legend><table><thead><tr>"
            Appointment.valid_column_type(type_profile[1].to_s).each {|x| aux =  aux + "<th>#{Appointment.human_attribute_name(x)}</th>" }
            aux = aux + "</tr></thead><tbody>"

            if !profile.try(:people).try {|x| x[0]}.try(type_profile[1]).blank?
              profile.try(:people).try {|x| x[0]}.try(type_profile[1]).each do |l|
                aux = aux + "<tr>"
                Appointment.valid_column_type(type_profile[1].to_s).each {|x| aux =  aux + "<td>#{l.try(:appointment).try(x)}</td>" }
                aux =  aux + "</tr>"
              end
            end
            if type_profile[1].to_s=="directors"
                aux = aux + "</tbody></table><table><thead><tr>"
                JobLevel.valid_column_type(type_profile[1].to_s).each {|x| aux =  aux + "<th>#{JobLevel.human_attribute_name(x)}</th>" }
                aux = aux + "</tr></thead><tbody>"
                if !profile.try(:people).try {|x| x[0]}.try(type_profile[1]).blank?
                    profile.try(:people).try {|x| x[0]}.try(type_profile[1]).each do |l|
                      aux = aux + "<tr>"
                      JobLevel.valid_column_type(type_profile[1].to_s).each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
                      aux =  aux + "</tr>"
                    end
                end
            end
            aux= aux + "</tbody></table></fieldset>"
        end

        aux = aux + "</fieldset>"

        aux = aux + "<fieldset><legend>Formación académica</legend><table><thead><tr>"
        [ :official_degree, :center, :education_level, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<th>#{Study.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:studies).blank?
            profile.studies.sort_by{|t| t.table_order.to_i}.each do |l|
                aux = aux + "<tr>"
                [ :official_degree, :center, :education_level, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
                aux =  aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Otros estudios</legend><table><thead><tr>"
        [ :title, :center, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<th>#{Course.human_attribute_name(x)}</th>"}
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:courses).blank?
            profile.courses.sort_by{|t| t.table_order.to_i}.each do |l|
                aux = aux + "<tr>"
                [ :title, :center, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
                aux = aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Idiomas</legend><table><thead><tr>"
        [ :name, :level, :table_order ].each {|x| aux =  aux + "<th>#{Language.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:languages).blank?
            profile.languages.sort_by{|t| t.table_order.to_i }.each do |l|
                aux = aux + "<tr>"
                [ :name, :level, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
                aux = aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Cargos politicos</legend><table><thead><tr>"
        [ :position, :entity, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<th>#{PoliticalPost.human_attribute_name(x)}</th>"}
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:political_posts).blank?
            profile.political_posts.sort_by{|t| t.table_order.to_i}.each do |l|
                aux = aux + "<tr>"
                [ :position, :entity, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>"}
                aux = aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Carrera profesional ámbito privado</legend><table><thead><tr>"
        [ :position, :entity, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<th>#{PrivateJob.human_attribute_name(x)}</th>"}
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:private_jobs).blank?
            profile.private_jobs.sort_by{|t| t.table_order.to_i}.each do |l|
                aux = aux + "<tr>"
                [ :position, :entity, :start_year, :end_year, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>"}
                aux = aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Carrera profesional ámbito público</legend><table><thead><tr>"
        [ :position, :public_administration, :start_year, :end_year, :body_scale, :init_year, :consolidation_degree, :table_order ].each do |x|
            aux =  aux + "<th>#{PublicJob.human_attribute_name(x)}</th>"
        end
        aux = aux + "</tr></thead><tbody>"
        if !profile.try(:public_jobs).blank?
            profile.public_jobs.sort_by{|t| t.table_order.to_i}.each do |l|
                aux = aux + "<tr>"
                [ :position, :public_administration, :start_year, :end_year, :body_scale, :init_year, :consolidation_degree, :table_order ].each do |x|
                    aux =  aux + "<td>#{l.try(x)}</td>"
                end
                aux = aux + "</tr>"
            end
        end
        aux = aux + "</tbody></table></fieldset>"

        [['Actividades docentes', :theacher_activity], ['Distinciones', :special_mentions], ['Publicaciones', :publications], ['Otra información', :other]].each do |j|
            aux = aux + "<fieldset><legend>#{j[0]}</legend><table><thead><tr>"
            aux = aux + "<th>#{Information.human_attribute_name(:information)}</th>"
            aux = aux + "</tr></thead><tbody>"
            if !profile.try(j[1]).blank?
                profile.try(j[1]).each {|l| aux = aux + "<tr><td>#{l.try(:information)}</td></tr>" }
            end
            aux = aux + "</tbody></table></fieldset>"
        end

        aux.html_safe
    end

    def self.transformDataHash(notification)
        fecAlta = notification.try(:content_data).try{|x| x['dpfFecAlta']}
        fecRegistro = notification.try(:content_data).try{|x| x['dpfFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta

        person = Person.find_by(personal_code: notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']} : notification.try(:content_data).try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{notification.try(:content_data).try{|x| x['dperNombre']}}%", "%#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}%")
        end
        personal_code = person.blank? ? notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : notification.try(:content_data).try{|x| x['dperNumper']}.to_s : person.personal_code

        gper = notification.try(:content_data).try{|x| x['gperAbrev']}
        if gper.blank?
          gper = 'C' if !notification.try(:content_data).try{|x| x['ultimoConcejal']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoConcejal']}.try{|x| x[0]}.try{|x| x["cjnFechaCese"]}.blank?
          gper = 'R' if !notification.try(:content_data).try{|x| x['ultimoDirectivo']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoDirectivo']}.try{|x| x[0]}.try{|x| x["drnFechaCese"]}.blank?
          gper = 'E' if !notification.try(:content_data).try{|x| x['ultimoEventual']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoEventual']}.try{|x| x[0]}.try{|x| x["evtnFechaCese"]}.blank?
          gper = 'F' if !notification.try(:content_data).try{|x| x['ultimoFuncionario']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoFuncionario']}.try{|x| x[0]}.try{|x| x["fldnFechaCese"]}.blank?
          gper = 'V' if !notification.try(:content_data).try{|x| x['ultimoVocal']}.blank?# && !notification.try(:content_data).try{|x| x['ultimoVocal']}.try{|x| x[0]}.try{|x| x["vvnFechaCese"]}.blank?
        end


        {
            :person_type => gper.blank? ? '' : I18n.t("person_abrev.#{gper}"),
            :updated_at => fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y %H:%M"),
            :name => "#{notification.try(:content_data).try{|x| x['dperNombre']}}".mb_chars.upcase,
            :last_name => "#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}".mb_chars.upcase,
            :personal_code => personal_code.to_s
        }
    end

    def self.transformData(notification, actualiza= false)
        contenido = notification.try(:content_data)
        return {} if contenido.blank?

        ##################################################
        ## Carga de los datos de personas
        ##################################################
        person_type = contenido.try{|x| x['gperAbrev']}
        person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
        end


        fecAlta = contenido.try{|x| x['dpfFecAlta']}
        fecRegistro = contenido.try{|x| x['dpfFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta

        ##################################################
        ## Carga de perfil profesional
        ##################################################
        if !actualiza || person.blank?
            profile = Profile.new(
                email: contenido.try{|x| x['dperCorreoCorp']},
                personal_email: contenido.try{|x| x['dpfMailPersonal']},
                twitter: contenido.try{|x| x['dpfTwitter']},
                facebook: contenido.try{|x| x['dpfFacebook']},
                updated_at: fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y %H:%M")
            )

            person = Person.new(profile: profile,
                name: contenido.try{|x| x['dperNombre']},
                sex: contenido.try{|x| x['dperSexo']},
                document_nif: contenido.try{|x| x['dperDocumento']},
                document_type: DocumentType.find_by(name: 'DNI'),
                last_name: "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}",
                personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})

            profile.people << person
        else
            profile = person.profile
            profile.email = contenido.try{|x| x['dperCorreoCorp']}.blank? ? profile.email : contenido.try{|x| x['dperCorreoCorp']}
            profile.personal_email = contenido.try{|x| x['dpfMailPersonal']}.blank? ? profile.personal_email : contenido.try{|x| x['dpfMailPersonal']}
            profile.twitter = contenido.try{|x| x['dpfTwitter']}.blank? ? profile.twitter : contenido.try{|x| x['dpfTwitter']}
            profile.facebook = contenido.try{|x| x['dpfFacebook']}.blank? ? profile.facebook : contenido.try{|x| x['dpfFacebook']}
            profile.updated_at = fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y %H:%M")
            person.name = contenido.try{|x| x['dperNombre']}
            person.sex = contenido.try{|x| x['dperSexo']}
            person.document_nif = contenido.try{|x| x['dperDocumento']}
            person.document_type = DocumentType.find_by(name: 'DNI')
            person.last_name = "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}"
            person.personal_code = contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']}
            profile.people << person
        end


        ##################################################
        ## Carga de cargos
        ##################################################
        [

            ['ultimoEventual', :temporary_workers, 'evtn', 'temporary_worker'],
            ['ultimoDirectivo', :directors, 'drn', 'director'],
            ['ultimoFuncionario', :public_workers, 'fldn', 'public_worker']
        ].each do |j|
            if !contenido.try{|x| x[j[0]]}.blank?
                aux = contenido.try{|x| x[j[0]]}[0]
                if person.try(j[1]).blank? || person.try(j[1]).count == 0
                    appointment = Appointment.new(position: aux["#{j[2]}Cargo"].blank? ? "Sin cargo" : aux["#{j[2]}Cargo"], unit: aux["#{j[2]}Unidad"],
                        area: aux["#{j[2]}Area"],
                        start_date: aux["#{j[2]}FechaNomb"].blank? ? '' : Time.at(aux["#{j[2]}FechaNomb"]/1000).strftime("%d/%m/%Y"),
                        end_date: aux["#{j[2]}FechaCese"].blank? ? '' : Time.at(aux["#{j[2]}FechaCese"]/1000).strftime("%d/%m/%Y"),
                        possession_date: aux["#{j[2]}FechaPosesion"].blank? ? '' : Time.at(aux["#{j[2]}FechaPosesion"]/1000).strftime("%d/%m/%Y"),
                        description_possession: aux["#{j[2]}DescripcionPosesion"].blank? ? '' : aux["#{j[2]}DescripcionPosesion"],
                        email: aux["#{j[2]}Email"].blank? ? '' : aux["#{j[2]}Email"],
                        functions: aux["#{j[2]}Funciones"].blank? ? '' : aux["#{j[2]}Funciones"],
                        retribution: aux["#{j[2]}Retribucion"].blank? ? '' : aux["#{j[2]}Retribucion"],
                        retribution_year: aux["#{j[2]}AnnoRetribucion"].blank? ? '' : aux["#{j[2]}AnnoRetribucion"],
                        observations: aux["#{j[2]}Observaciones"].blank? ? '' : aux["#{j[2]}Observaciones"],
                        description: aux["#{j[2]}Descripcion"].blank? ? '' : aux["#{j[2]}Descripcion"])
                    job = JobLevel.new(person: person, person_type: j[3], appointment: appointment)
                    if j[1].to_s == "directors"
                        termination_authorization = TerminationAuthorization.new
                        compatibility_activity = CompatibilityActivity.new
                        compatibility_activity.resolution_date = aux["drFechaResolucion"].blank? ? '' : Time.at(aux["drFechaResolucion"]/1000).strftime("%d/%m/%Y")
                        compatibility_activity.compatibility_description = aux["drDescripcionCompatibilidad"].blank? ? '' : aux["drDescripcionCompatibilidad"]
                        compatibility_activity.public_private = aux["drPrivadaPublica"].blank? ? '' : aux["drPrivadaPublica"]
                        termination_authorization.authorization_date = aux["drFechaAutorizacion"].blank? ? '' : Time.at(aux["drFechaAutorizacion"]/1000).strftime("%d/%m/%Y")
                        termination_authorization.authorization_description = aux["drDescripcionAutorizada"].blank? ? '' : aux["drDescripcionAutorizada"]
                        job.termination_authorizations << termination_authorization if !compatibility_activity.resolution_date.blank? || !compatibility_activity.compatibility_description.blank? || !compatibility_activity.public_private.blank?
                        job.compatibility_activities << compatibility_activity if !termination_authorization.authorization_date.blank? || !termination_authorization.authorization_description.blank?
                    end
                    job.updated_at = Time.zone.now
                    person.send(j[1]) << job
                else
                    person.send(j[1])[0].updated_at = Time.zone.now
                    person.send(j[1])[0].appointment.area = aux["#{j[2]}Area"]
                    person.send(j[1])[0].appointment.position = aux["#{j[2]}Cargo"].blank? ? "Sin cargo" : aux["#{j[2]}Cargo"]
                    person.send(j[1])[0].appointment.unit = aux["#{j[2]}Unidad"]
                    person.send(j[1])[0].appointment.start_date = aux["#{j[2]}FechaNomb"].blank? ? '' : Time.at(aux["#{j[2]}FechaNomb"]/1000).strftime("%d/%m/%Y")
                    person.send(j[1])[0].appointment.end_date = aux["#{j[2]}FechaCese"].blank? ? '' : Time.at(aux["#{j[2]}FechaCese"]/1000).strftime("%d/%m/%Y")
                    person.send(j[1])[0].appointment.possession_date = aux["#{j[2]}FechaPosesion"].blank? ? '' : Time.at(aux["#{j[2]}FechaPosesion"]/1000).strftime("%d/%m/%Y")
                    person.send(j[1])[0].appointment.description_possession = aux["#{j[2]}DescripcionPosesion"].blank? ? '' : aux["#{j[2]}DescripcionPosesion"]
                    person.send(j[1])[0].appointment.possession_date = aux["#{j[2]}Email"].blank? ? '' : aux["#{j[2]}Email"]
                    person.send(j[1])[0].appointment.functions = aux["#{j[2]}Funciones"].blank? ? '' : aux["#{j[2]}Funciones"]
                    person.send(j[1])[0].appointment.retribution = aux["#{j[2]}Retribucion"].blank? ? '' : aux["#{j[2]}Retribucion"]
                    person.send(j[1])[0].appointment.retribution_year = aux["#{j[2]}AnnoRetribucion"].blank? ? '' : aux["#{j[2]}AnnoRetribucion"]
                    person.send(j[1])[0].appointment.observations = aux["#{j[2]}Observaciones"].blank? ? '' : aux["#{j[2]}Observaciones"]
                    person.send(j[1])[0].appointment.description = aux["#{j[2]}Descripcion"].blank? ? '' : aux["#{j[2]}Descripcion"]
                    if j[1].to_s == "directors"
                        termination_authorization = TerminationAuthorization.new
                        compatibility_activity = CompatibilityActivity.new
                        compatibility_activity.resolution_date = aux["drFechaResolucion"].blank? ? '' : Time.at(aux["drFechaResolucion"]/1000).strftime("%d/%m/%Y")
                        compatibility_activity.compatibility_description = aux["drDescripcionCompatibilidad"].blank? ? '' : aux["drDescripcionCompatibilidad"]
                        compatibility_activity.public_private = aux["drPrivadaPublica"].blank? ? '' : aux["drPrivadaPublica"]
                        termination_authorization.authorization_date = aux["drFechaAutorizacion"].blank? ? '' : Time.at(aux["drFechaAutorizacion"]/1000).strftime("%d/%m/%Y")
                        termination_authorization.authorization_description = aux["drDescripcionAutorizada"].blank? ? '' : aux["drDescripcionAutorizada"]
                        person.send(j[1])[0].termination_authorizations << termination_authorization if !compatibility_activity.resolution_date.blank? || !compatibility_activity.compatibility_description.blank? || !compatibility_activity.public_private.blank?
                        person.send(j[1])[0].compatibility_activities << compatibility_activity if !termination_authorization.authorization_date.blank? || !termination_authorization.authorization_description.blank?
                    end
                end
            end
        end

        [['ultimoVocal', :spokespeople, 'vvn', 'spokesperson']].each do |j|
            if !contenido.try{|x| x[j[0]]}.blank?
                aux = contenido.try{|x| x[j[0]]}[0]
                if person.try(j[1]).blank? || person.try(j[1]).count == 0
                    appointment = Appointment.new(position: aux["#{j[2]}Cargo"].blank? ? "Sin cargo" : aux["#{j[2]}Cargo"], unit: aux["gpNombre"],
                        area: aux["#{j[2]}Unidad"],
                        start_date: aux["#{j[2]}FechaNomb"].blank? ? '' : Time.at(aux["#{j[2]}FechaNomb"]/1000).strftime("%d/%m/%Y"),
                        end_date: aux["#{j[2]}FechaCese"].blank? ? '' : Time.at(aux["#{j[2]}FechaCese"]/1000).strftime("%d/%m/%Y"),
                        district: aux["disNombre"],
                        politic_group:  aux["gpNombre"],
                        corporation: aux["cpNombre"],
                        juntamd: aux["#{j[2]}JuntaMd"],
                        retribution: aux["#{j[2]}Retribucion"].blank? ? '' : aux["#{j[2]}Retribucion"],
                        retribution_year: aux["#{j[2]}AnnoRetribucion"].blank? ? '' : aux["#{j[2]}AnnoRetribucion"],
                        observations: aux["#{j[2]}Observaciones"].blank? ? '' : aux["#{j[2]}Observaciones"],
                        possession_date: aux["#{j[2]}FechaPosesion"].blank? ? '' : Time.at(aux["#{j[2]}FechaPosesion"]/1000).strftime("%d/%m/%Y"),
                        description_possession: aux["#{j[2]}DescripcionPosesion"].blank? ? '' : aux["#{j[2]}DescripcionPosesion"])
                    job = JobLevel.new(person: person, person_type: j[3], appointment: appointment)

                    job.updated_at = Time.zone.now
                    person.send(j[1]) << job
                else
                    person.send(j[1])[0].updated_at = Time.zone.now
                    person.send(j[1])[0].appointment.area = aux["#{j[2]}Unidad"]
                    person.send(j[1])[0].appointment.position = aux["#{j[2]}Cargo"].blank? ? "Sin cargo" : aux["#{j[2]}Cargo"]
                    person.send(j[1])[0].appointment.unit = aux["gpNombre"]
                    person.send(j[1])[0].appointment.start_date = aux["#{j[2]}FechaNomb"].blank? ? '' : Time.at(aux["#{j[2]}FechaNomb"]/1000).strftime("%d/%m/%Y")
                    person.send(j[1])[0].appointment.end_date = aux["#{j[2]}FechaCese"].blank? ? '' : Time.at(aux["#{j[2]}FechaCese"]/1000).strftime("%d/%m/%Y")
                    person.send(j[1])[0].appointment.district = aux["disNombre"]
                    person.send(j[1])[0].appointment.politic_group = aux["gpNombre"]
                    person.send(j[1])[0].appointment.corporation = aux["cpNombre"]
                    person.send(j[1])[0].appointment.juntamd = aux["#{j[2]}JuntaMd"]
                    person.send(j[1])[0].appointment.retribution = aux["#{j[2]}Retribucion"].blank? ? '' : aux["#{j[2]}Retribucion"]
                    person.send(j[1])[0].appointment.retribution_year = aux["#{j[2]}AnnoRetribucion"].blank? ? '' : aux["#{j[2]}AnnoRetribucion"]
                    person.send(j[1])[0].appointment.observations = aux["#{j[2]}Observaciones"].blank? ? '' : aux["#{j[2]}Observaciones"]
                    person.send(j[1])[0].appointment.possession_date = aux["#{j[2]}FechaPosesion"].blank? ? '' : aux["#{j[2]}FechaPosesion"]
                    person.send(j[1])[0].appointment.description_possession = aux["#{j[2]}DescripcionPosesion"].blank? ? '' : aux["#{j[2]}FechaPosesion"]
                end
            end
        end

        if !contenido.try{|x| x['ultimoConcejal']}.blank?
            aux = contenido.try{|x| x['ultimoConcejal']}[0]

            corporation = Corporation.find_by(name: aux['corporacion']['cpNombre'].gsub('/','-').to_s)
            if corporation.blank?
                corporation = Corporation.new(
                    name: aux['corporacion']['cpNombre'].gsub('/','-').to_s,
                    description: aux['corporacion'].try{|x| x['cpObserv']},
                    active: aux['corporacion'].try{|x| x['cpActiva']},
                    end_year: aux['corporacion'].try{|x| x['cpAnioFin']},
                    start_year: aux['corporacion'].try{|x| x['cpAnioInicio']},
                    councillors_num: aux['corporacion'].try{|x| x['cpNumConcej']},
                    end_corporation_date: aux['corporacion'].try{|x| x['cpFechaFinal']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaFinal']}/1000).strftime("%d/%m/%Y"),
                    election_date: aux['corporacion'].try{|x| x['cpFechaElecciones']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaElecciones']}/1000).strftime("%d/%m/%Y"),
                    full_constitutional_date:aux['corporacion'].try{|x| x['cpFechaPlenoConst']}.blank? ? '' : Time.at(aux['corporacion'].try{|x| x['cpFechaPlenoConst']}/1000).strftime("%d/%m/%Y"),
                )
                if !contenido.try{|x| x['corporacion']}.try{|x| x['listasElectorales']}.blank?
                    contenido.try{|x| x['corporacion']}.try{|x| x['listasElectorales']}.each do |l|
                        aux_l = ElectoralList.find_by("long_name LIKE '%#{ l['leNombreLista']}%' OR name Like '%#{ l['leNombreLista']}%'")
                        if aux_l.blank?
                            aux_l = ElectoralList.create!(name: l['leNombreLista'], long_name: l['leNombreLista'])
                        end
                        aux_union = CorporationsElectoralList.create(corporation: corporation, order: 0, electoral_list: aux_l)
                        corporation.corporations_electoral_lists << aux_union
                    end
                end
                corporation.save
            end

            list = ElectoralList.find_by("long_name LIKE '%#{aux['afiliacion']}%' OR name Like '%#{aux['afiliacion']}%'")
            if list.blank?
                list = ElectoralList.new(long_name: aux['afiliacion'], name: aux['afiliacion'])
                list.save
            end


            if CorporationsElectoralList.find_by(corporation: corporation, electoral_list: list).blank?
                aux_union = CorporationsElectoralList.create!(corporation_id: corporation.id, order: 0, electoral_list_id: list.id)
                corporation.corporations_electoral_lists << aux_union
                corporation.save
            end

            if person.councillors.blank?
                job = JobLevel.new(person: person,person_type: 'councillor')
                job.updated_at = Time.zone.now

                appointment = Appointment.new(position: aux['cjnAlcalde'].to_i ==1 ? 'Alcalde' : 'Concejal',
                    start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom']/1000).strftime("%d/%m/%Y"),
                    end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese']/1000).strftime("%d/%m/%Y"),
                    possession_date: aux['cjnFechaPosesion'].blank? ? '' : Time.at(aux['cjnFechaPosesion']/1000).strftime("%d/%m/%Y"),
                    url_possession: aux['cjnUrlPosesion'].blank? ? '' : aux['cjnUrlPosesion'],
                    url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                    functions: aux['cjnFunciones'].blank? ? '' : aux['cjnFunciones'],
                    retribution_year: aux['cjnAnnoRetribucion'].blank? ? '' : aux['cjnAnnoRetribucion'],
                    retribution: aux['cjnRetribucion'].blank? ? '' : aux['cjnRetribucion'],
                    observations: aux['cjnObservaciones'].blank? ? '' : aux['cjnObservaciones'])
                councillor = CouncillorsCorporation.new(job_level: job, appointment: appointment,
                    corporation: corporation, order_num: aux['numOrden'])
                termination_authorization = TerminationAuthorization.new
                compatibility_activity = CompatibilityActivity.new
                compatibility_activity.resolution_date = aux["cjcFechaResolucion"].blank? ? '' : Time.at(aux["cjcFechaResolucion"]/1000).strftime("%d/%m/%Y")
                compatibility_activity.compatibility_description = aux["cjcDescripcionCompatibilidad"].blank? ? '' : aux["cjcDescripcionCompatibilidad"]
                compatibility_activity.public_private = aux["cjcPrivadaPublica"].blank? ? '' : aux["cjcPrivadaPublica"]
                termination_authorization.authorization_date = aux["cjcFechaAutorizacion"].blank? ? '' : Time.at(aux["cjcFechaAutorizacion"]/1000).strftime("%d/%m/%Y")
                termination_authorization.authorization_description = aux["cjcDescripcionAutorizada"].blank? ? '' : aux["cjcDescripcionAutorizada"]
                councillor.termination_authorizations << termination_authorization if !compatibility_activity.resolution_date.blank? || !compatibility_activity.compatibility_description.blank? || !compatibility_activity.public_private.blank?
                councillor.compatibility_activities << compatibility_activity if !termination_authorization.authorization_date.blank? || !termination_authorization.authorization_description.blank?

                councillor.electoral_list = list

                job.councillors_corporations << councillor
                person.councillors << job
            else
                job = person.councillors[0]
                job.updated_at = Time.zone.now

                if job.councillors_corporations.corporation(corporation).blank?
                    appointment = Appointment.new(position: aux['cjnAlcalde'].to_i ==1 ? 'Alcalde' : 'Concejal',
                        start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom']/1000).strftime("%d/%m/%Y"),
                        end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese']/1000).strftime("%d/%m/%Y"),
                        possession_date: aux['cjnFechaPosesion'].blank? ? '' : Time.at(aux['cjnFechaPosesion']/1000).strftime("%d/%m/%Y"),
                        url_possession: aux['cjnUrlPosesion'].blank? ? '' : aux['cjnUrlPosesion'],
                        url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                        functions: aux['cjnFunciones'].blank? ? '' : aux['cjnFunciones'],
                        retribution_year: aux['cjnAnnoRetribucion'].blank? ? '' : aux['cjnAnnoRetribucion'],
                        retribution: aux['cjnRetribucion'].blank? ? '' : aux['cjnRetribucion'],
                        observations: aux['cjnObservaciones'].blank? ? '' : aux['cjnObservaciones'])
                    councillor = CouncillorsCorporation.new(job_level: job, appointment: appointment,
                        corporation: corporation, order_num: aux['numOrden'])

                    termination_authorization = TerminationAuthorization.new
                    compatibility_activity = CompatibilityActivity.new
                    compatibility_activity.resolution_date = aux["cjcFechaResolucion"].blank? ? '' : Time.at(aux["cjcFechaResolucion"]/1000).strftime("%d/%m/%Y")
                    compatibility_activity.compatibility_description = aux["cjcDescripcionCompatibilidad"].blank? ? '' : aux["cjcDescripcionCompatibilidad"]
                    compatibility_activity.public_private = aux["cjcPrivadaPublica"].blank? ? '' : aux["cjcPrivadaPublica"]
                    termination_authorization.authorization_date = aux["cjcFechaAutorizacion"].blank? ? '' : Time.at(aux["cjcFechaAutorizacion"]/1000).strftime("%d/%m/%Y")
                    termination_authorization.authorization_description = aux["cjcDescripcionAutorizada"].blank? ? '' : aux["cjcDescripcionAutorizada"]
                    councillor.termination_authorizations << termination_authorization if !compatibility_activity.resolution_date.blank? || !compatibility_activity.compatibility_description.blank? || !compatibility_activity.public_private.blank?
                    councillor.compatibility_activities << compatibility_activity if !termination_authorization.authorization_date.blank? || !termination_authorization.authorization_description.blank?

                    councillor.electoral_list = list

                    job.councillors_corporations << councillor
                    person.councillors << job
                else
                    councillor = job.councillors_corporations.corporation(corporation)[0]
                    councillor.order_num = aux['numOrden']
                    if councillor.appointment.blank?
                        appointment = Appointment.new(position: aux['cjnAlcalde'].to_i ==1 ? 'Alcalde' : 'Concejal',
                            start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom']/1000).strftime("%d/%m/%Y"),
                            end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese']/1000).strftime("%d/%m/%Y"),
                            possession_date: aux['cjnFechaPosesion'].blank? ? '' : Time.at(aux['cjnFechaPosesion']/1000).strftime("%d/%m/%Y"),
                            url_possession: aux['cjnUrlPosesion'].blank? ? '' : aux['cjnUrlPosesion'],
                            url_form: aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"],
                            functions: aux['cjnFunciones'].blank? ? '' : aux['cjnFunciones'],
                            retribution_year: aux['cjnAnnoRetribucion'].blank? ? '' : aux['cjnAnnoRetribucion'],
                            retribution: aux['cjnRetribucion'].blank? ? '' : aux['cjnRetribucion'],
                            observations: aux['cjnObservaciones'].blank? ? '' : aux['cjnObservaciones'])
                            councillor.appointment = appointment
                    else
                        councillor.appointment.position = aux['cjnAlcalde'].to_i ==1 ? 'Alcalde' : 'Concejal'
                        councillor.appointment.start_date = aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom']/1000).strftime("%d/%m/%Y")
                        councillor.appointment.end_date = aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese']/1000).strftime("%d/%m/%Y")
                        councillor.appointment.possession_date = aux['cjnFechaPosesion'].blank? ? '' : Time.at(aux['cjnFechaPosesion']/1000).strftime("%d/%m/%Y")
                        councillor.appointment.url_possession = aux['cjnUrlPosesion'].blank? ? '' : aux['cjnUrlPosesion']
                        councillor.appointment.url_form= aux["cjnUrlFormulario"].blank? ? '' : aux["cjnUrlFormulario"]
                        councillor.appointment.functions = aux['cjnFunciones'].blank? ? '' : aux['cjnFunciones']
                        councillor.appointment.retribution_year = aux['cjnAnnoRetribucion'].blank? ? '' : aux['cjnAnnoRetribucion']
                        councillor.appointment.retribution = aux['cjnRetribucion'].blank? ? '' : aux['cjnRetribucion']
                        councillor.appointment.observations = aux['cjnObservaciones'].blank? ? '' : aux['cjnObservaciones']

                        termination_authorization = TerminationAuthorization.new
                        compatibility_activity = CompatibilityActivity.new
                        compatibility_activity.resolution_date = aux["cjcFechaResolucion"].blank? ? '' : Time.at(aux["cjcFechaResolucion"]/1000).strftime("%d/%m/%Y")
                        compatibility_activity.compatibility_description = aux["cjcDescripcionCompatibilidad"].blank? ? '' : aux["cjcDescripcionCompatibilidad"]
                        compatibility_activity.public_private = aux["cjcPrivadaPublica"].blank? ? '' : aux["cjcPrivadaPublica"]
                        termination_authorization.authorization_date = aux["cjcFechaAutorizacion"].blank? ? '' : Time.at(aux["cjcFechaAutorizacion"]/1000).strftime("%d/%m/%Y")
                        termination_authorization.authorization_description = aux["cjcDescripcionAutorizada"].blank? ? '' : aux["cjcDescripcionAutorizada"]
                        councillor.termination_authorizations << termination_authorization if !compatibility_activity.resolution_date.blank? || !compatibility_activity.compatibility_description.blank? || !compatibility_activity.public_private.blank?
                        councillor.compatibility_activities << compatibility_activity if !termination_authorization.authorization_date.blank? || !termination_authorization.authorization_description.blank?
                    end

                    councillor.electoral_list = list
                end
            end
        end

        profile.languages = []
        if !contenido.try{|x| x['idiomas']}.blank?

            contenido.try{|x| x['idiomas']}.each do |l|
                aux = Language.new(name: l['idiomaConf']['idcIdioma'], level: l['idiomaNivel']['nicNivel'], table_order: l['idmOrden'])
                profile.languages << aux
            end
        end

        profile.courses = []
        if !contenido.try{|x| x['otrosEstudios']}.blank?

            contenido.try{|x| x['otrosEstudios']}.each do |l|
                aux = Course.new(title: l['oeCurso'], center: l['oeCentro'], start_year: l['oeAnyoIni'], end_year: l['oeAnyoFin'],table_order: l['oeOrden'] )
                profile.courses << aux
            end
        end

        profile.special_mentions = []
        if !contenido.try{|x| x['distinciones']}.blank?

            contenido.try{|x| x['distinciones']}.each do |l|
                aux = Information.new(information_type: InformationType.find_by(name: "special_mentions"),information: l['dhmDistinciones'], table_order: l['dhmOrden'])
                profile.special_mentions << aux
            end
        end

        profile.publications = []
        if !contenido.try{|x| x['publicaciones']}.blank?

            contenido.try{|x| x['publicaciones']}.each do |l|
                aux = Information.new(information_type: InformationType.find_by(name: "publications"), information: "#{l['pubAnioInicio']} - #{l['pubPublicacion']}", table_order: l['pubOrden'] )
                profile.publications << aux
            end
        end

        profile.political_posts = []
        if !contenido.try{|x| x['cargosPoliticos']}.blank?

            contenido.try{|x| x['cargosPoliticos']}.each do |l|
                aux = PoliticalPost.new(position: l['cpolCargo'], entity: l['cpolInstitucion'], start_year: l['cpolAnyoIni'], end_year: l['cpolAnyoFin'], table_order: l['cpolOrden'])
                profile.political_posts << aux
            end
        end

        profile.other = []
        if !contenido.try{|x| x['otraInformacion']}.blank?

            contenido.try{|x| x['otraInformacion']}.each do |l|
                aux = Information.new(information_type: InformationType.find_by(name: "other"), information: l['oipInformacion'])
                profile.other << aux
            end
        end

        profile.private_jobs = []
        if !contenido.try{|x| x['carreraProfPrivado']}.blank?

            contenido.try{|x| x['carreraProfPrivado']}.each do |l|
                aux = PrivateJob.new(position: l['cpriCargo'], entity: l['cpriInstitucion'], start_year: l['cpriAnyoIni'],end_year: l['cpriAnyoFin'], table_order: l['cpriOrden'])
                profile.private_jobs << aux
            end
        end

        profile.public_jobs = []
        if !contenido.try{|x| x['carreraProfPublico']}.blank?

            contenido.try{|x| x['carreraProfPublico']}.each do |l|
                aux = PublicJob.new(body_scale: l['cpuEscalaCuerpo'], init_year: l['cpuAnyoIngreso'],
                    start_year: l['cpuAnyoIni'] ,end_year: l['cpuAnyoFin'], position: l['cpuPuesto'],
                    consolidation_degree: l['cpuGradoConso'], public_administration: l['administracionesPublicas']['aappAdministracion'],
                    table_order: l['cpuOrden'])
                profile.public_jobs << aux
            end
        end

        profile.studies = []
        if !contenido.try{|x| x['formacionAcademica']}.blank?

            contenido.try{|x| x['formacionAcademica']}.each do |l|
                aux = Study.new(official_degree: l['facTitulacionOficial'], center: l['facCentroExped'],
                    education_level: l['facNivelEducativo'], start_year: l['facAnyoIni'], end_year: l['facAnyoFin'],
                    table_order: l['facOrden'])
                profile.studies << aux
            end
        end

        profile.theacher_activity = []
        if !contenido.try{|x| x['actividadesDocente']}.blank?

            contenido.try{|x| x['actividadesDocente']}.each do |l|
                aux = Information.new(information_type: InformationType.find_by(name: "theacher_activity"), information: l['actdActividad'], table_order: l['actdOrden'])
                profile.theacher_activity << aux
            end
        end

        # Preparado para carga de otros textos no incluidos en inper actualmente

        profile.career_comments.each do |cc|
            cc.information = ""
            profile.association(:informations).add_to_target(cc)
        end

        profile.studies_comments.each do |sc|
            sc.information = ""
            profile.association(:informations).add_to_target(sc)
        end

        profile.courses_comments.each do |cc|
            cc.information = ""
            profile.association(:informations).add_to_target(cc)
        end

        profile.political_posts_comments.each do |ppc|
            ppc.information = ""
            profile.association(:informations).add_to_target(ppc)
        end

        profile
    rescue
        {}
    end

    private

    def duplicated_email
        exist = Profile.where(email: self.email)
        if !exist.blank? && !self.email.blank? && exist.count > 1
            errors.add(:email, "Ya existe este email")
        end
    end
end
