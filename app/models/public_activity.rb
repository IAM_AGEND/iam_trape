class PublicActivity < ActiveRecord::Base
    include ActivitiesCommon

    belongs_to :activities_declaration, touch: true

    #validate :not_blank

    def not_blank
        if self.entity.blank? && self.position.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
end
