class OtherPersonalProperty < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validates :types, presence: true

end
