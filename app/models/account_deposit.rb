class AccountDeposit < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validate :not_blank

    def not_blank
        if self.kind.blank? && self.entity.blank? && self.balance.blank?
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end

    def get_balance
        get_currency_value(self.try(:balance))
    end
end
