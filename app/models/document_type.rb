require 'active_record/diff'
class DocumentType < ActiveRecord::Base
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]

    validates :name, uniqueness: true
end
