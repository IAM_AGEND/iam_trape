class OtherDeposit < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validate :not_blank

    def not_blank
        if self.types.blank? && self.value.blank? && self.description.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end

    def get_value
        self.try(:value)
    end
end
