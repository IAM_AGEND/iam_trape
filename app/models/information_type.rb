class InformationType < ActiveRecord::Base
    validates :name, uniqueness: true
end
