class Audit < ActiveRecord::Base
    belongs_to :person
    belongs_to :administrator

    def author
        self.administrator.blank? ? I18n.t('administrator_system') : self.administrator.email
    end
end
