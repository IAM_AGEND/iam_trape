class Duty < ActiveRecord::Base
  belongs_to :declaration, :polymorphic => true, touch: true
  belongs_to :person_level,:polymorphic => true, touch: true
  belongs_to :temporality

  validate :unique_declaration
  validate :unique_duty
  validate :not_create_year


  scope :with_declaration,        -> { where.not(declaration: nil) }
  scope :without_declaration,     -> { where(declaration: nil) }
  scope :directors,               -> { joins("INNER JOIN job_levels ON duties.person_level_id = job_levels.id AND person_level_type = 'JobLevel' INNER JOIN appointments ON job_levels.appointment_id = appointments.id OR job_levels.appointment_id is null ")}
  scope :directors_working,       -> { directors.where("appointments.end_date IS NULL") }
  scope :directors_not_working,   -> { directors.where("appointments.end_date IS NOT NULL") }
  scope :councillors_specific,    -> (corporation){ joins("INNER JOIN councillors_corporations  ON duties.person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' INNER JOIN appointments ON councillors_corporations.appointment_id = appointments.id OR councillors_corporations.appointment_id  is null").where("councillors_corporations.corporation_id = #{corporation}")}
  scope :councillors_working,     -> (corporation){ councillors_specific(corporation).where("appointments.end_date IS NULL") }
  scope :councillors_not_working, -> (corporation){ councillors_specific(corporation).where("appointments.end_date IS NOT NULL") }

  scope :councillors,           -> { where(person_level_type: 'CouncillorsCorporation') }
  scope :other_job,             -> { where(person_level_type: 'JobLevel') }
  scope :sort_for_list,         -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 2 ELSE 1 END) ASC, duties.year ASC") }
  scope :sort_for_temporality,  -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 1 ELSE 2 END) ASC, duties.year DESC, duties.type_duty") }


  private

  def unique_declaration
    duty_exist = Duty.where(declaration_id: self.declaration_id, declaration_type: self.declaration_type).where("declaration_id is not null")
    if !duty_exist.blank?
      self.errors.add(:declaration, "Ya existe la obligación")
      return false
    end
    true
  end

  def unique_duty
    duty_exist = Duty.where(declaration: self.declaration, person_level: self.person_level, temporality: self.temporality, year: self.year,type_duty: self.type_duty).where.not(id: self.try(:id))
    if !duty_exist.blank?
      self.errors.add(:declaration, "Ya existe la obligación")
      return false
    end

    duty_exist = Duty.where(person_level: self.person_level, temporality: self.temporality, year: self.year,type_duty: self.type_duty).where.not(id: self.try(:id), declaration: nil)
    if !duty_exist.blank?
      self.errors.add(:declaration, "Ya existe la obligación")
      return false
    end
    true
  end

  def not_create_year
    duty_exist = Duty.where(person_level: self.person_level, temporality: Temporality.find_by(name: 'Inicial'),type_duty: self.type_duty).where("cast(start_date as varchar) like (?)","%#{self.year}%").where.not(id: self.try(:id))
    if !duty_exist.blank? && self.declaration.blank?
      self.errors.add(:declaration, "Ya existe la obligación como inicial")
      return false
    end
    duty_exist = Duty.where(person_level: self.person_level, temporality: Temporality.find_by(name: 'Final'),type_duty: self.type_duty).where("cast(start_date as varchar) like (?)","%#{self.year}%").where.not(id: self.try(:id))
    if !duty_exist.blank? && self.declaration.blank?
      self.errors.add(:declaration, "Ya existe la obligación como final")
      return false
    end
    true
  end
end
