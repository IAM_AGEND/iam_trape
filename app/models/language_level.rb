class LanguageLevel < ActiveRecord::Base
    validates :level, uniqueness: true

    default_scope { order(level: :asc) }

    def name
        self.level
    end
end
