class Study < ActiveRecord::Base
    include Validator

    belongs_to :profile

    validate :not_blank


    def not_blank
        if self.official_degree.blank? && self.center.blank? && self.start_year.blank? && self.end_year.blank?
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
end
