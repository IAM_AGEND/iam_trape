class CorporationsElectoralList < ActiveRecord::Base
    belongs_to :corporation
    belongs_to :electoral_list

    validates :electoral_list, presence: true
    validates_uniqueness_of :electoral_list, scope: [:corporation_id, :electoral_list_id]

    def electoral_list_name
        self.try(:electoral_list).try(:name)
    end
end
