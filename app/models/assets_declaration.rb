class AssetsDeclaration < ActiveRecord::Base
    include Declarations
    include OrdersCommon

    belongs_to :person_level, :polymorphic => true, touch: true
    belongs_to :temporality
    has_many :account_deposits, dependent: :destroy
    has_many :real_estate_properties, dependent: :destroy
    has_many :other_deposits, dependent: :destroy
    has_many :vehicles, dependent: :destroy
    has_many :tax_data, dependent: :destroy
    has_many :debts, dependent: :destroy
    has_many :other_personal_properties, dependent: :destroy
    has_many :duties, as: :declaration, dependent: :destroy

    accepts_nested_attributes_for :account_deposits, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :real_estate_properties, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :other_deposits, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :vehicles, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :tax_data,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :debts, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :other_personal_properties, reject_if: :all_blank, allow_destroy: true

    #validates_uniqueness_of :temporality_id, scope: [:year_period, :person_level_id,:person_level_type], if: '!editable'
    validates :declaration_date, :temporality, presence: true
    validates_associated :account_deposits
    validates_associated :real_estate_properties
    validates_associated :other_deposits
    validates_associated :vehicles
    validates_associated :tax_data
    validates_associated :debts
    validates_associated :other_personal_properties
    validate :unique_declaration
    validate :editable_declaration
    validate :order_tables

    scope :sort_for_list, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 2 ELSE 1 END) ASC, assets_declarations.year_period ASC") }
    scope :original, -> { where(editable: false) }
    scope :sort_for_list, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 2 ELSE 1 END) ASC, assets_declarations.year_period ASC") }
    scope :sort_for_temporality, -> { joins(:temporality).order("(CASE WHEN temporalities.name = 'Inicial' THEN 0 WHEN temporalities.name = 'Final' THEN 1 ELSE 2 END) ASC, assets_declarations.year_period DESC") }
    scope :in_temporality, -> (temporality, year_period){ where(temporality: temporality, year_period: year_period) }


    def order_tables
        valid_order(self.account_deposits) && valid_order(self.real_estate_properties) && valid_order(self.other_deposits) && valid_order(self.vehicles) && valid_order(self.tax_data) && valid_order(self.debts) && valid_order(self.other_personal_properties)
    end

    def self.especific_columns
        %w{
            person_type
            period_name_notification
            declaration_date
            person_name
            person_last_name
            person_identificator
        }
    end

    def self.getNotificationData(notification)
        aux = ''
        asset = self.transformData(notification)

        aux = aux + "<h1>Vista de la declaración de bienes - #{notification.historic? ? "Histórico" : "Pendiente"}</h1><br>"
        aux = aux + "<fieldset><legend>Datos de la notificación</legend>"
        aux = aux + "<b>#{Person.human_attribute_name(:type_job)}:</b> #{notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : I18n.t("person_abrev.#{notification.try(:content_data).try{|x| x['gperAbrev']}}")}<br>"
        aux = aux + "<b>#{Notification.human_attribute_name(:created_at)}:</b> #{notification.created_at}<br>"
        if notification.historic?
            aux = aux + "<b>#{Notification.human_attribute_name(:type_action)}:</b> #{notification.type_action}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:change_state_at)}:</b> #{notification.change_state_at}<br>"
            aux = aux + "<b>#{Notification.human_attribute_name(:administrator)}:</b> #{notification.try(:author).blank? ? "Administrador del sistema" : notification.try(:author) }<br>"
        else
            aux = aux + "<b>#{Notification.human_attribute_name(:no_publish_reason)}:</b> #{notification.no_publish_reason}<br>"
        end
        aux = aux + "</fieldset>"

        aux = aux + "<fieldset><legend>Datos de la declaración de bienes</legend><table><thead><tr>"
        [ :declaration_date, :year_period ].each {|x| aux =  aux + "<th>#{AssetsDeclaration.human_attribute_name(x)}</th>"}
        aux =  aux + "<th>Temporalidad</th>"
        aux = aux + "</tr></thead><tbody><tr>"
        [ :declaration_date, :year_period ].each {|x| aux =  aux + "<td>#{asset.try(x)}</td>"}
        aux =  aux + "<td>#{asset.try(:temporality).try(:name)}</th>"
        aux = aux + "</tr></tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Datos de la persona</legend><table><thead><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<th>#{Person.human_attribute_name(x)}</th>"}
        aux =  aux + "<th>Tipo persona</th>"
        aux = aux + "</tr></thead><tbody><tr>"
        [ :backwards_name, :sex, :personal_code, :document_nif ].each {|x| aux =  aux + "<td>#{asset.person_level.try(:person).try(x)}</td>"}
        aux =  aux + "<td>#{asset.person_level.try(:type_person)}</td>"
        aux = aux + "</tr></tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Patrimonio inmobiliario</legend><table><thead><tr>"
        [
            :straight_type, :adquisition_title, :municipality, :participation_percentage, :kind,
            :purchase_date, :cadastral_value, :table_order
        ].each do |x|
            aux =  aux + "<th>#{RealEstateProperty.human_attribute_name(x)}</th>"
        end
        aux = aux + "</tr></thead><tbody>"
        asset.real_estate_properties.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [
                :straight_type, :adquisition_title, :municipality, :participation_percentage, :kind,
                :purchase_date, :cadastral_value, :table_order
            ].each do |x|
                aux =  aux + "<td>#{l.try(x)}</td>"
            end
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Depositos en cuenta</legend><table><thead><tr>"
        [ :kind, :entity, :balance, :table_order].each {|x| aux =  aux + "<th>#{AccountDeposit.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.account_deposits.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :kind, :entity, :balance, :table_order].each {|x| aux =  aux + "<td>#{l.try(x)}</td>"}
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Otros depósitos en cuenta</legend><table><thead><tr>"
        [ :types, :description, :purchase_date, :value, :table_order ].each {|x| aux =  aux + "<th>#{OtherDeposit.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.other_deposits.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :types, :description, :purchase_date, :value, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Vehículos</legend><table><thead><tr>"
        [ :kind, :model, :purchase_date, :table_order ].each {|x| aux =  aux + "<th>#{Vehicle.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.vehicles.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :kind, :model, :purchase_date, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Otros Bienes Inmuebles</legend><table><thead><tr>"
        [ :types, :purchase_date, :table_order ].each {|x| aux =  aux + "<th>#{OtherPersonalProperty.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.other_personal_properties.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :types, :purchase_date, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>" }
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Deudas</legend><table><thead><tr>"
        [ :kind, :import, :observations, :table_order ].each {|x| aux =  aux + "<th>#{Debt.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.debts.sort_by{|t| t.table_order.to_i}.each do |l|
            aux =  aux + "<tr>"
            [ :kind, :import, :observations, :table_order ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>"}
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux = aux + "<fieldset><legend>Información tributaria</legend><table><thead><tr>"
        [ :tax, :fiscal_data, :import, :observation ].each {|x| aux =  aux + "<th>#{TaxDatum.human_attribute_name(x)}</th>" }
        aux = aux + "</tr></thead><tbody>"
        asset.tax_data.each do |l|
            aux =  aux + "<tr>"
            [ :tax, :fiscal_data, :import, :observation ].each {|x| aux =  aux + "<td>#{l.try(x)}</td>"}
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux.html_safe
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
        ""
    end

    def self.transformDataHash(notification)
        fecAlta = notification.try(:content_data).try{|x| x['decgFecAlta']}
        fecRegistro = notification.try(:content_data).try{|x| x['decgFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta
        person = Person.find_by(personal_code: notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']} : notification.try(:content_data).try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{notification.try(:content_data).try{|x| x['dperNombre']}}%", "%#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}%")
        end
        personal_code = person.blank? ? notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : notification.try(:content_data).try{|x| x['dperNumper']}.to_s : person.personal_code

        {
            :person_type => notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : I18n.t("person_abrev.#{notification.try(:content_data).try{|x| x['gperAbrev']}}"),
            :period_name_notification => "#{notification.try(:content_data).try{|x| x['tdNombre']}} #{fecFinal.blank? || notification.try(:content_data).try{|x| x['tdNombre']} !="Anual" ? '' : notification.try(:content_data).try{|x| x['decgYear']}}",
            :declaration_date => fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y"),
            :person_name => "#{notification.try(:content_data).try{|x| x['dperNombre']}}".upcase,
            :person_last_name => "#{notification.try(:content_data).try{|x| x['dperApellido1']}} #{notification.try(:content_data).try{|x| x['dperApellido2']}}".upcase,
            :person_identificator => personal_code.to_s
        }
    rescue
        {}
    end

    def self.transformData(notification, actualiza = false)
        contenido = notification.try(:content_data)
        return {} if contenido.blank?

        fecAlta = notification.try(:content_data).try{|x| x['decgFecAlta']}
        fecRegistro = notification.try(:content_data).try{|x| x['decgFecRegistro']}
        fecFinal = fecAlta.blank? && fecRegistro.blank? ? '' : fecRegistro.blank? ? fecAlta : fecAlta.blank? ? fecRegistro : fecRegistro >= fecAlta ? fecRegistro : fecAlta

        temporality = Temporality.find_by(name: contenido.try{|x| x['tdNombre']})

        asset = AssetsDeclaration.new(
            temporality: temporality,
            declaration_date: fecFinal.blank? ? '' : Time.at(fecFinal/1000).strftime("%d/%m/%Y"),
            year_period: fecFinal.blank? || temporality.try(:name).to_s !="Anual" ? '' : contenido.try{|x| x['decgYear']}
        )
        person_type = contenido.try{|x| x['gperAbrev']}
        person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
        if person.blank?
            person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
        end

        if person.blank?
            person = Person.new(name: contenido.try{|x| x['dperNombre']},
                sex: contenido.try{|x| x['dperSexo']},
                document_nif: contenido.try{|x| x['dperDocumento']},
                document_type: DocumentType.find_by(name: 'DNI'),
                last_name: "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}",
                personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
            job = JobLevel.new(person: person, person_type: person_type == 'C' ? 'councillor' : person_type == 'R' ? 'directors' : person_type == 'F' ? 'public_worker' : person_type == 'E' ? 'temporary_worker' : 'spokesperson')
            if job.councillor?
                corporation = Corporation.find_by(name: contenido.try{|x| x['cpNombre']}.gsub('/','-'))
                councillor = CouncillorsCorporation.new(job_level: job, corporation: corporation)
                job.councillors_corporations << councillor
                asset.person_level = councillor
            else
                asset.person_level = job
            end
        else
            case person_type
            when "C"
                corporation = Corporation.find_by(name: contenido.try{|x| x['cpNombre']}.gsub('/','-'))
                asset.person_level = person.councillors[0].councillors_corporations.corporation(corporation)[0]
            when "R"
                asset.person_level = person.directors[0]
            when "F"
                asset.person_level = person.public_workers[0]
            when "E"
                asset.person_level = person.temporary_workers[0]
            when "V"
                asset.person_level = person.spokespeople[0]
            end
        end

        if asset.person_level.blank?
            job = JobLevel.new(person: person, person_type: person_type == 'C' ? 'councillor' : person_type == 'R' ? 'directors' : person_type == 'F' ? 'public_worker' : person_type == 'E' ? 'temporary_worker' : 'spokesperson')
            if job.councillor?
                corporation = Corporation.find_by(name: contenido.try{|x| x['cpNombre']})
                if corporation.blank?
                    corporation = Corporation.new(name: contenido.try{|x| x['cpNombre']})
                end
                councillor = CouncillorsCorporation.new(job_level: job, corporation: corporation)
                job.councillors_corporations << councillor
                asset.person_level = councillor
            else
                asset.person_level = job
            end
        end

        if !contenido.try{|x| x['patrimonioInmobiliario']}.blank?
            contenido.try{|x| x['patrimonioInmobiliario']}.each do |l|

                aux = RealEstateProperty.new(
                    assets_declaration: asset,
                    straight_type: l['tipoDerecho'],
                    adquisition_title: l['tituloAdquisicion'], municipality: l['municipio'],
                    purchase_date: "#{l['mes']}/#{l['anyo']}",
                    participation_percentage: l['participacion'], kind:  l['clase'],
                    mounth: l['mes'], year: l['anyo'], cadastral_value: l['valorCatastral'], observations: l['observaciones'],
                    table_order: l['orden'])
                    asset.real_estate_properties << aux
            end
        end

        if !contenido.try{|x| x['depositosCuenta']}.blank?
            contenido.try{|x| x['depositosCuenta']}.each do |l|
                aux = AccountDeposit.new(assets_declaration: asset,kind: l['dpClase'], entity: l['dpEntidad'], balance: l['dpSaldo'], table_order: l['dpOrden'])
                asset.account_deposits << aux
            end
        end

        if !contenido.try{|x| x['otrosDepositosCuenta']}.blank?
            contenido.try{|x| x['otrosDepositosCuenta']}.each do |l|
                aux = OtherDeposit.new(
                    assets_declaration: asset,types: l['optrTipos'], description: l['optrDescripcion'],
                    mounth: l['optrMes'], year: l['optrAnyo'],
                    purchase_date: "#{l['optrMes']}/#{l['optrAnyo']}",
                    value: "#{l['optrValor']} (#{l['optrTipoUnidad']})",
                    table_order: l['optrOrden'])
                    asset.other_deposits << aux
            end
        end

        if !contenido.try{|x| x['vehiculos']}.blank?
            contenido.try{|x| x['vehiculos']}.each do |l|
                aux = Vehicle.new(assets_declaration: asset,kind: l['vehClase'], model: l['vehMarcaMod'],
                    purchase_date: "#{l['vehMes']}/#{l['vehAnyo']}",
                    mounth: l['vehMes'], year: l['vehAnyo'],
                    table_order: l['vehOrden'])
                asset.vehicles << aux
            end
        end

        if !contenido.try{|x| x['otrosBienesMuebles']}.blank?
            contenido.try{|x| x['otrosBienesMuebles']}.each do |l|
                aux = OtherPersonalProperty.new(assets_declaration: asset,types: l['obmTipos'], mounth: l['obmMes'], year: l['obmAnyo'], purchase_date: "#{l['obmMes']}/#{l['obmAnyo']}", table_order: l['obmOrden'])
                asset.other_personal_properties << aux
            end
        end

        if !contenido.try{|x| x['deudas']}.blank?
            contenido.try{|x| x['deudas']}.each do |l|
                aux = Debt.new(assets_declaration: asset,kind: l['deuClase'], observations: l['deuDesc'], import: l['deuImporte'], table_order: l['deuOrden'])
                asset.debts << aux
            end
        end

        if !contenido.try{|x| x['informacionTributaria']}.blank?
            contenido.try{|x| x['informacionTributaria']}.each do |l|
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.irpf"), fiscal_data: I18n.t("tax_data.irpf_big"), import: l['inftIrpfBigImporte'],observation: l['inftIrpfBigObserv'])
                asset.tax_data << aux
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.irpf"), fiscal_data: I18n.t("tax_data.irpf_bi"), import: l['inftIrpfBiImporte'],observation: l['inftIrpfBiObserv'])
                asset.tax_data << aux
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.irpf"), fiscal_data: I18n.t("tax_data.irpf_deduc_a"), import: l['inftIrpfDeducImporteA'],observation: l['inftIrpfDeducObservA'])
                asset.tax_data << aux
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.irpf"), fiscal_data: I18n.t("tax_data.irpf_deduc_b"), import: l['inftIrpfDeducImporteB'],observation: l['inftIrpfDeducObservB'])
                asset.tax_data << aux
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.isp"), fiscal_data: I18n.t("tax_data.isp_bi"), import: l['inftIspBiImporte'],observation: l['inftIspBiObserv'])
                asset.tax_data << aux
                aux = TaxDatum.new(assets_declaration: asset,tax: I18n.t("tax_data.is"), fiscal_data: I18n.t("tax_data.is_bi"), import: l['inftIsBiImporte'],observation: l['inftIsBiObserv'])
                asset.tax_data << aux
            end
        end

        asset
    # rescue
    #     {}
    end

    private

    def unique_declaration

        asset = AssetsDeclaration.where(temporality_id: self.temporality_id, year_period: self.year_period, editable: false, person_level_id: self.person_level_id, person_level_type: self.person_level_type)

        if !self.id.blank?
            asset = asset.where("id not in (?)", self.id)
        end
        if self.editable == false && !asset.blank?
            self.errors.add(:temporality, I18n.t('assets_declaration.error_temporalidad'))
        end
    end

    def editable_declaration
        asset = AssetsDeclaration.where(temporality_id: self.temporality_id, year_period: self.year_period, editable: false, person_level_id: self.person_level_id, person_level_type: self.person_level_type).order(declaration_date: :desc).first
        if asset.blank? && self.editable == true
            self.errors.add(:editable, I18n.t('assets_declaration.error_editable'))
        elsif self.try(:declaration_date).blank? || !asset.blank? && asset.declaration_date > self.declaration_date && self.id.blank?
            self.errors.add(:declaration_date, I18n.t('assets_declaration.error_declaration_date', date: asset.declaration_date))
        end
    end

end
