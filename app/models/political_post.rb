class PoliticalPost < ActiveRecord::Base
    include Validator

    belongs_to :profile

    validate :not_blank

    def not_blank
        if self.start_year.blank? && self.position.blank? && self.entity.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
end
