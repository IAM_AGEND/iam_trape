require 'active_record/diff'
class CompatibilityActivity < ActiveRecord::Base
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]
    
    belongs_to :person_level, :polymorphic => true, touch: true

    def self.valid_column_type
        [:resolution_date, :compatibility_description, :public_private]
    end

end