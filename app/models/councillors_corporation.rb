require 'active_record/diff'
class CouncillorsCorporation < ActiveRecord::Base  
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]  
    belongs_to :job_level, touch: true
    belongs_to :corporation
    belongs_to :electoral_list
    belongs_to :appointment, dependent: :destroy, touch: true
    belongs_to :profile, dependent: :destroy
    has_many :organisms
    has_many :dedications
    has_many :activities_declarations,  -> { sort_for_list }, as: :person_level, dependent: :destroy
    has_many :assets_declarations,  -> { sort_for_list }, as: :person_level, dependent: :destroy
    has_many :duties, as: :person_level, dependent: :destroy
    has_many :termination_authorizations, as: :person_level, dependent: :destroy
    has_many :compatibility_activities, as: :person_level, dependent: :destroy

    accepts_nested_attributes_for :profile,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :appointment,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :activities_declarations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :assets_declarations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :organisms, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :dedications, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :termination_authorizations,reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :compatibility_activities,reject_if: :all_blank, allow_destroy: true

    validates_associated :profile
    validates_associated :appointment
    validates_associated :activities_declarations
    validates_associated :assets_declarations
    validates_uniqueness_of :corporation, scope: [:job_level_id, :corporation_id]

    validates :corporation, presence: true

    scope :corporation, -> (corporation) { where(corporation: corporation) }
    scope :working, -> { joins(:appointment).where("appointments.end_date IS NULL")}
    scope :not_working, -> { joins(:appointment).where("appointments.end_date IS NOT NULL")}

    def backwards_name
        "#{job_level.try(:backwards_name)} (#{corporation.try(:name)})".try { |tx| tx.mb_chars.upcase}
    end

    def type_person
        "#{I18n.t('person.titles.councillor')} (#{corporation.try(:name)})"
    end

    def person
        job_level.try(:person)
    end

    def self.valid_column_type(type)
        case type.to_s
        when "councillors"
            []
        when "directors"
            []
        when "public_workers"
            []
        when "temporary_workers"
            []
        when "spokespeople"
            []
        else
            []
        end
    end

end
