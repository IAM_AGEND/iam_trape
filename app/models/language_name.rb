class LanguageName < ActiveRecord::Base
    validates :name, uniqueness: true

    default_scope { order(name: :asc) }
end
