class Log < ActiveRecord::Base
    belongs_to :administrator
   

    self.record_timestamps = false

    def author_email
        administrator.try(:email)
    end
    
    def translated_successful
        I18n.t("shared.#{successful}")
    end

    def log_parent
        Log.find_by(id: self.log_id)
    end

    def logs_children
        Log.where(log_id: self.id)
    end
end
