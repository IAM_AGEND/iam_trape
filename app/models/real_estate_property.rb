class RealEstateProperty < ActiveRecord::Base
    include AssetsCommon

    belongs_to :assets_declaration, touch: true

    validate :not_blank

    def not_blank
        if self.kind.blank? && self.straight_type.blank? && self.adquisition_title.blank? && self.municipality.blank? && self.participation_percentage.blank? && self.cadastral_value.blank?
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end

    def get_cadastral_value
        get_currency_value(self.try(:cadastral_value))
    end
end
