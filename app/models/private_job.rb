class PrivateJob < ActiveRecord::Base
    include Validator

    belongs_to :profile

    validate :not_blank


    def not_blank
        if self.entity.blank? && self.position.blank? && self.start_year.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
end
