module Validator

  def date_greather
      if (!try(:start_date).blank? && !try(:end_date).blank?) && try(:start_date) > try(:end_date)
          errors.add(:start_date, I18n.t('errors.messages.invalid_date'))
          errors.add(:end_date, I18n.t('errors.messages.invalid_date'))
          false
      else
          true
      end
  end

    def year_greather
        if (!try(:start_year).blank? && !try(:end_year).blank?) && try(:start_year) > try(:end_year)
            errors.add(:start_year, I18n.t('errors.messages.invalid_date'))
            errors.add(:end_year, I18n.t('errors.messages.invalid_date'))
            false
        else
            true
        end
    end

end
