module ActivitiesCommon
    include Validator

    def period_name
        if !self.activities_declaration.try(:temporality).try(:name).blank?
        if self.activities_declaration.try(:temporality).try(:name).to_s == "Anual"
            "#{ self.activities_declaration.try(:temporality).try(:name)} #{self.activities_declaration.try(:year_period)}"
        else
            "#{self.activities_declaration.try(:temporality).try(:name)}"
        end
        else
        ""
        end
    end

    def declaration_date
        self.activities_declaration.try(:declaration_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    ###############################################################################
    #   FIELDS FOR CSV OR XLS
    ###############################################################################
    def person_identificator
        self.activities_declaration.try(:person_level).try(:person).try(:personal_code)
    end

    def person_fist_name
        self.activities_declaration.try(:person_level).try(:person).try(:name).to_s.upcase
    end

    def person_first_last_name
        return "" if self.activities_declaration.try(:person_level).try(:person).try(:last_name).blank?
        aux =self.activities_declaration.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat = concat + " " + s.to_s.upcase
                else 
                    concat = concat + " " + s.to_s.upcase
                    break
                end
            end
            concat.strip
        else
            aux[0].to_s.upcase
        end
    end

    def person_second_last_name
        return "" if self.activities_declaration.try(:person_level).try(:person).try(:last_name).blank?

        aux =self.activities_declaration.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat1 = ""
            concat2 = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat1 = concat1 + " " + s.to_s.upcase
                else 
                    concat1 = concat1 + " " + s.to_s.upcase
                    if concat2.blank?
                        concat2 = concat1
                        concat1 = ""
                    else
                        break
                    end
                end
            end
            concat1.strip
        else
            aux[1].to_s.upcase
        end
    end

    def person_corporation
        self.activities_declaration.try(:person_level).try(:corporation).try(:name)
    end

    def type
        "Declaración de actividades"
    end

    def appointment_start_date
        self.activities_declaration.person_level.try(:appointment).try(:start_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    def appointment_end_date
        self.activities_declaration.person_level.try(:appointment).try(:end_date).try{|x| x.strftime("%d/%m/%Y")}
    end
end