module AssetsCommon
    include ActionView::Helpers::NumberHelper

    def period_name
        if !self.assets_declaration.try(:temporality).try(:name).blank?
            if self.assets_declaration.try(:temporality).try(:name).to_s == "Anual"
                "#{ self.assets_declaration.try(:temporality).try(:name)} #{self.assets_declaration.try(:year_period)}"
            else
                "#{self.assets_declaration.try(:temporality).try(:name)}"
            end
        else
            ""
        end
    end

    def declaration_date
        self.assets_declaration.try(:declaration_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    ###############################################################################
    #   FIELDS FOR CSV OR XLS
    ###############################################################################
    def person_identificator
        self.assets_declaration.try(:person_level).try(:person).try(:personal_code)
    end

    def person_fist_name
        self.assets_declaration.try(:person_level).try(:person).try(:name).to_s.upcase
    end

    def person_corporation
        self.assets_declaration.try(:person_level).try(:corporation).try(:name)
    end

    def person_first_last_name
        return "" if self.assets_declaration.try(:person_level).try(:person).try(:last_name).blank?
        aux =self.assets_declaration.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat = concat + " " + s.to_s.upcase
                else 
                    concat = concat + " " + s.to_s.upcase
                    break
                end
            end
            concat.strip
        else
            aux[0].to_s.upcase
        end
    end

    def person_second_last_name
        return "" if self.assets_declaration.try(:person_level).try(:person).try(:last_name).blank?

        aux =self.assets_declaration.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat1 = ""
            concat2 = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat1 = concat1 + " " + s.to_s.upcase
                else 
                    concat1 = concat1 + " " + s.to_s.upcase
                    if concat2.blank?
                        concat2 = concat1
                        concat1 = ""
                    else
                        break
                    end
                end
            end
            concat1.strip
        else
            aux[1].to_s.upcase
        end
    end

    private

    def get_currency_value(value)
        return "" if value.blank?
        number_to_currency(value, precision: 2)
    end

    def type
        "Declaración de bienes"
    end

    def appointment_start_date
        self.assets_declaration.person_level.try(:appointment).try(:start_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    def appointment_end_date
        self.assets_declaration.person_level.try(:appointment).try(:end_date).try{|x| x.strftime("%d/%m/%Y")}
    end
end