module Declarations

    
    def period_name
        if !self.try(:temporality).try(:name).blank?
          if self.try(:temporality).try(:name).to_s == "Anual"
            "#{self.try(:temporality).try(:name)} #{self.try(:year_period)}"
          else
            "#{self.try(:temporality).try(:name)}"
          end
        else
          ""
        end
    end

    def person_type
        self.try(:person_level).try(:type_person)
    end
    
    
    def person_name
        self.try(:person_level).try(:person).try(:name).to_s.upcase
    end

    def person_last_name
        self.try(:person_level).try(:person).try(:last_name).to_s.upcase
    end

   
    def person_appointment
        self.try(:person_level).try(:appointment).try(:position)
    end


    def declaration_date_format
        self.try(:declaration_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    ###############################################################################
    #   FIELDS FOR CSV OR XLS
    ###############################################################################
    def person_identificator
        self.try(:person_level).try(:person).try(:personal_code)
    end

    def person_fist_name
        self.try(:person_level).try(:person).try(:name).to_s.upcase
    end

    def person_first_last_name
        return "" if self.try(:person_level).try(:person).try(:last_name).blank?
        aux =self.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat = concat + " " + s.to_s.upcase
                else 
                    concat = concat + " " + s.to_s.upcase
                    break
                end
            end
            concat.strip
        else
            aux[0].to_s.upcase
        end
    end

    def person_second_last_name
        return "" if self.try(:person_level).try(:person).try(:last_name).blank?

        aux =self.try(:person_level).try(:person).try(:last_name).split(" ")
        if aux.length > 2 
            concat1 = ""
            concat2 = ""
            aux.each do |s|
                if ['de','la','lo','y','del','las','los','le','les'].include?(s.to_s.downcase)
                    concat1 = concat1 + " " + s.to_s.upcase
                else 
                    concat1 = concat1 + " " + s.to_s.upcase
                    if concat2.blank?
                        concat2 = concat1
                        concat1 = ""
                    else
                        break
                    end
                end
            end
            concat1.strip
        else
            aux[1].to_s.upcase
        end
    end


    def person_corporation
        self.try(:person_level).try(:corporation).try(:name)
    end

    def should_display_declarations?
        job = self.person_level
        if job.model_name.to_s == "CouncillorsCorporation"
            job.try(:job_level).try {|x| x.should_display_declarations?(job.try(:corporation))}
        else
            job.try {|x| x.should_display_declarations?}
        end
    end

    def type
        self.model_name == "ActivitiesDeclaration" ? "Declaración de actividades" : "Declaración de bienes"
    end

    def appointment_start_date
        self.person_level.try(:appointment).try(:start_date).try{|x| x.strftime("%d/%m/%Y")}
    end

    def appointment_end_date
        self.person_level.try(:appointment).try(:end_date).try{|x| x.strftime("%d/%m/%Y")}
    end
end