
module OrdersCommon


    def valid_order(table_model)
        orders = []
        table_model.each do |row|
            if !row.table_order.blank? && row.table_order.to_i > 0
                if orders.blank? 
                    orders.push(row.table_order.to_i)
                else
                    if orders.include?(row.table_order.to_i)
                        errors.add(row.class.name == "Information" ? :publications_data : row.class.name.underscore.pluralize, ". No pueden repetirse los valores de ordenación.")
                        return false
                    else
                        orders.push(row.table_order.to_i)
                    end
                end
            elsif !row.table_order.blank? && row.table_order.to_i < 0
                errors.add(row.class.name == "Information" ? :publications_data : row.class.name.underscore.pluralize, ". No pueden introducirse valores negativos de ordenación.")
                return false
            end
        end
        true
    end

end