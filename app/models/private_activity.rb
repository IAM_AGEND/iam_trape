class PrivateActivity < ActiveRecord::Base
    include ActivitiesCommon

    belongs_to :activities_declaration, touch: true

    #validate :not_blank

    def not_blank
        if self.private_activity_type.blank? && self.description.blank? && self.position.blank? && self.entity.blank? 
            errors.add(:data, "No puede estar en blanco")
            return false
        end
        true
    end
end
