class TextContent < ActiveRecord::Base
    validates :section, uniqueness: true
    validates :content, length: { maximum: 250}

    def self.get_text_section(section)
        aux = TextContent.find_by("section = ?", section.to_s)
        aux.blank? ? "" : aux.content
    end
end
