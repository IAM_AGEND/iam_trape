class Language < ActiveRecord::Base
    belongs_to :profile
    belongs_to :language_name
    belongs_to :language_level

    validates :name, presence: true 
    validate :duplicated_name

    private

    def duplicated_name
        exist = Language.where(name: self.name, profile_id: self.profile_id)
        if !exist.blank? &&  exist.count > 1
            errors.add(:name, "Ya existe este idioma")
        end
    end
end
