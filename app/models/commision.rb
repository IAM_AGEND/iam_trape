class Commision < ActiveRecord::Base   

    belongs_to :appointment
    validates :commision, presence: :true, length: { maximum: 300 }

end