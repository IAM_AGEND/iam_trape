class PublicAdministration < ActiveRecord::Base
    validates :administration_code, uniqueness: true
end
