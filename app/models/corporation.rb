require 'active_record/diff'
class Corporation < ActiveRecord::Base
    include ActiveRecord::Diff
    diff :exclude => [:created_at, :updated_at]
    has_many :corporations_electoral_lists, dependent: :destroy
    has_many :councillors_corporations, dependent: :destroy

    validates :name, uniqueness: true
    validates :name, :start_year, :end_year, presence: true

    scope :active, -> { where(active: true) }
    scope :inactive, -> { where(active: false) }

    accepts_nested_attributes_for :corporations_electoral_lists, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :councillors_corporations, :allow_destroy => true, reject_if: :all_blank

    def self.main_columns
        [
            :name,
            :election_date,
            :full_constitutional_date,
            :end_corporation_date,
            :councillors_num,
            :active
        ]
    end

    def self.especific_columns
        %w{
            name
            election_date
            full_constitutional_date
            end_corporation_date
            councillors_num
            active_formated
            electoral_asociated
        }
    end

    def active_formated
        I18n.t("booleans.#{active}")
    end

    def electoral_asociated
        aux = ''
        corporations_electoral_lists.each do |cl|            
            aux = aux.blank? ? aux + cl.electoral_list.long_name : aux + ", " + cl.electoral_list.long_name
        end

        aux
    end

    def self.getNotificationData(notification)
        aux = ''
        corporation = self.transformData(notification)

        aux = aux + "<h1>Vista de corporación</h1><br>"
        aux = aux + "<fieldset><legend>Datos de corporación</legend><table><thead><tr>"
        [
            :name, :description, :start_year, :end_year, :election_date,   :full_constitutional_date,
            :end_corporation_date, :councillors_num,  :active
        ].each do |x|
            aux =  aux + "<th>#{Corporation.human_attribute_name(x)}</th>"
        end
        aux = aux + "</tr></thead><tbody><tr>"
        [
            :name, :description, :start_year, :end_year, :election_date,   :full_constitutional_date,
            :end_corporation_date, :councillors_num,  :active
        ].each do |x|
            aux =  aux + "<td>#{corporation.try(x)}</td>"
        end
        aux = aux + "</tr></tbody></table></fieldset><fieldset><legend>Afiliaciones</legend><table><thead><tr>"
        aux =  aux + "<th>#{ElectoralList.human_attribute_name(:name)}</th>"
        aux = aux + "</tr></thead><tbody>"
        corporation.corporations_electoral_lists.each do |l|
            aux =  aux + "<tr>"
            aux =  aux + "<td>#{l.electoral_list.try(:name)}</td>"
            aux =  aux + "</tr>"
        end
        aux = aux + "</tbody></table></fieldset>"

        aux.html_safe
    end
    
    

    def self.transformData(notification)
        corporation = Corporation.new(
            name: notification.try(:content_data).try{|x| x['cpNombre'].gsub('/','-')},
            description: notification.try(:content_data).try{|x| x['cpObserv']},
            active: notification.try(:content_data).try{|x| x['cpActiva']},
            end_year: notification.try(:content_data).try{|x| x['cpAnioFin']},
            start_year: notification.try(:content_data).try{|x| x['cpAnioInicio']},
            councillors_num: notification.try(:content_data).try{|x| x['cpNumConcej']},
            end_corporation_date: notification.try(:content_data).try{|x| x['cpFechaFinal']}.blank? ? '' : Time.at(notification.try(:content_data).try{|x| x['cpFechaFinal']}/1000).strftime("%d/%m/%Y"),
            election_date: notification.try(:content_data).try{|x| x['cpFechaElecciones']}.blank? ? '' : Time.at(notification.try(:content_data).try{|x| x['cpFechaElecciones']}/1000).strftime("%d/%m/%Y"),
            full_constitutional_date: notification.try(:content_data).try{|x| x['cpFechaPlenoConst']}.blank? ? '' : Time.at(notification.try(:content_data).try{|x| x['cpFechaPlenoConst']}/1000).strftime("%d/%m/%Y"),
        )

        if !notification.try(:content_data).try{|x| x['listasElectorales']}.blank?
            notification.try(:content_data).try{|x| x['listasElectorales']}.each do |l|
                aux_l = ElectoralList.find_by("long_name LIKE '%#{ l['leNombreLista']}%' OR name Like '%#{ l['leNombreLista']}%'")
                if aux_l.blank?
                    aux_l = ElectoralList.new(name: l['leNombreLista'], long_name: l['leNombreLista'])
                end
                aux_union = CorporationsElectoralList.create(corporation: corporation, electoral_list: aux_l)
                corporation.corporations_electoral_lists << aux_union                        
            end
        end

        corporation
    end
end
