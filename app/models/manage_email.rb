class ManageEmail < ActiveRecord::Base
    validate :valid_sender
    validate :valid_cc
    validate :valid_cco

    def cc
        return "" if self.fields_cc.blank?
        generate_formated(self.fields_cc)
    rescue
        ""
    end

    def cco
        return "" if self.fields_cco.blank?
        generate_formated(self.fields_cco)
    rescue
        ""
    end

    private 

    def valid_sender
        reg=/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
        if !self.sender.blank? && self.sender.match(reg).to_s!=self.sender.to_s
            self.errors.add(:sender, "No tiene el formato correcto de email")
        end
    end

    def valid_cc
        validator_c(:fields_cc)
    end

    def valid_cco
        validator_c(:fields_cco)
    end

    def validator_c(field)
        reg=/(\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z)|(\A(([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+;)+(([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+)\z)/i
        self.try(field).each do |k,v|
            if !v.blank? && reg.match(v).to_s!=v.to_s
                self.errors.add(field, "No tiene el formato correcto de email")
            end
        end
    end

    def generate_formated(h=nil)
        return "" if h.blank?
        formated_data="<ul>"
        h.each do |k, v|
            formated_data=formated_data+"<li><b>"+I18n.t("person.titles.#{k}")+":</b> "+v+"</li>"
        end
        formated_data=formated_data+"</ul>"
    end
end
