class Notification < ActiveRecord::Base
  extend Admin::NotificationsHelper
  belongs_to :administrator

  scope :with_igonre,    -> { where("\"ignore\"= true") }
  scope :with_permit,    -> { where(permit: true) }
  scope :pending,        -> { where("\"ignore\" = false AND permit = false")}
  scope :historic,       -> { where("\"ignore\" = true OR permit = true") }
  scope :with_type_data, -> (type){ where(type_data: type) }

  validate :change_state

  def type_action
      return Notification.human_attribute_name(:permit) if self.permit
      return Notification.human_attribute_name(:ignore) if self.ignore
  end

  def content_data_formated(field)
    return '' if field.blank?
    begin
      data_show = self.try(:type_data).constantize.transformData(self)
    rescue
      data_show = {}
    end
    field.blank? || data_show.try(field).blank? ? '' : data_show.try(field)
  end

  def content_data_hash_formated(field)
    return '' if field.blank?
    begin
      data_show = self.try(:type_data).constantize.transformDataHash(self)
    rescue
      data_show = {}
    end
    data_show.try{|x| x[field.to_sym]}
  end

  def author
      self.administrator.blank? ? I18n.t('administrator_system') : self.administrator.email
  end

  def historic?
      self.permit || self.ignore
  end

  def self.data_redirect
    {'Profile' => 'profiles', 'AssetsDeclaration' => 'assets', 'ActivitiesDeclaration' => 'activities', 'Person' => 'person'}
  end

  private 

  def change_state
    self.permit && self.ignore ? false : true
  end
end
