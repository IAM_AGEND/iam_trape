class Admin::DeleteDataController < Admin::LogsController
  before_action :set_logs, only: [:index, :delete_information]
  before_action :load_data, only: [:index, :delete_information]
  before_action :load_form_data, only: [:index, :delete_information]

  def index
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @delete_data = DeleteData.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def delete_information
    @delete_data = DeleteData.new
    attrs = (params[:delete_data].blank? || !params.has_key?(:delete_data)) ? {} : delete_data_params

    if validate_params(attrs)
      destroy_information(attrs)
    else
      render :index
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

  def delete_data_params
    params.require(:delete_data).permit(:delete,:period, :corporation,:type_information, :people, :type_people)
  end

  def generate_period(job=nil,type=nil)
    @periods ||= {}
    if !type.blank? && !job.blank?
      job.try(type).includes(:temporality).preload(:temporality).each do |dec|
        key = dec.try(:temporality).try(:name)
        if key.to_s == "Anual"
          @periods.merge!({dec.try(:year_period).to_s=>dec.try(:year_period).to_s})
        else
          @periods.merge!({key=>key})
        end
      end
    end
  end

  def generate_period_list(jobs=nil,type=nil,person_type='')
    @periods ||= {}
    if !type.blank? && !jobs.blank?
      type.all.includes(:temporality).preload(:temporality).where("person_level_type = ? and person_level_id in (?)",(person_type == 'councillor' ? "CouncillorsCorporation" : "JobLevel"),jobs.select(:id)).each do |dec|
        key = dec.try(:temporality).try(:name)
        if key.to_s == "Anual"
          @periods.merge!({dec.try(:year_period).to_s=>dec.try(:year_period).to_s})
        else
          @periods.merge!({key=>key})
        end
      end
    end
  end

  def load_data
    @delete_data = DeleteData.new
    @job_levels = {}
    @periods = {}

    @type_people = {
      I18n.t("admin.dashboard.councillor")=>"councillor" ,
      I18n.t("admin.dashboard.director")=>"director"
    }
    @type_informations = {}
    if !params[:delete_data].blank? && params[:delete_data][:delete].to_s != "complete"
      @type_informations.merge!({
        I18n.t("admin.dashboard.profile")=>"profile"
      })
    end
    @type_informations.merge!({
      I18n.t("admin.dashboard.assets_declaration")=>"assets_declaration",
      I18n.t("admin.dashboard.activities_declaration")=>"activities_declaration"
    })

    @attrs = (params[:delete_data].blank? || !params.has_key?(:delete_data)) ? {} : delete_data_params
    if !@attrs[:type_people].blank?

      if @attrs[:type_people].to_s=="director"
        @job_levels = JobLevel.joins(:person).preload(:person).where(person_type: @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
      elsif @attrs[:type_people].to_s=="councillor"
        @job_levels = CouncillorsCorporation.joins(:job_level => [:person]).preload(:corporation,:job_level => [:person]).where("job_levels.person_type=?", @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
      end

      if !@attrs[:type_information].blank?
        @periods = {}
        case @attrs[:type_information]
        when "profile"
          @job_levels = @job_levels.where("job_levels.id in (?)", JobLevel.joins(:person =>[:profile]).preload(:person => [:profile]).all.uniq.select("job_levels.id"))
        when "assets_declaration"
          if @attrs[:type_people].to_s=="director"
            @job_levels = @job_levels.where("job_levels.id in (?)", JobLevel.joins(:assets_declarations).preload(:assets_declaration).all.uniq.select("job_levels.id"))
          elsif @attrs[:type_people].to_s=="councillor"
            @job_levels = @job_levels.where("councillors_corporations.id in (?)", CouncillorsCorporation.joins(:assets_declarations).preload(:assets_declaration).all.uniq.select("councillors_corporations.id"))
          end
        when "activities_declaration"
          if @attrs[:type_people].to_s=="director"
            @job_levels = @job_levels.where("job_levels.id in (?)", JobLevel.joins(:activities_declarations).preload(:activities_declarations).all.uniq.select("job_levels.id"))
          elsif @attrs[:type_people].to_s=="councillor"
            @job_levels = @job_levels.where("councillors_corporations.id in (?)", CouncillorsCorporation.joins(:activities_declarations).preload(:activities_declarations).all.uniq.select("councillors_corporations.id"))
          end
        else
        end

        unless @attrs[:people].blank?
          if @attrs[:type_people].to_s=="director"
            job = JobLevel.includes(:person).find(@attrs[:people])
          elsif @attrs[:type_people].to_s=="councillor"
            job = CouncillorsCorporation.includes(:corporation,:job_level => [:person]).find(@attrs[:people])
          end
          case @attrs[:type_information]
          when "assets_declaration"
            generate_period(job, :assets_declarations)
          when "activities_declaration"
            generate_period(job, :activities_declarations)
          else
          end
        else
          case @attrs[:type_information]
          when "assets_declaration"
            generate_period_list(@job_levels, AssetsDeclaration,@attrs[:type_people].to_s)
          when "activities_declaration"
            generate_period_list(@job_levels, ActivitiesDeclaration,@attrs[:type_people].to_s)
          else
          end
        end
        @periods = @periods.sort.to_h
      end


    end
    @job_levels = @job_levels.sort_by {|x| x.backwards_name}
  end

  def validate_params(attrs)
    errors=false
    [:delete,:type_information,:type_people].each do |field|
      if attrs[field].blank? || !attrs.has_key?(field)
        @delete_data.errors.add(field, I18n.t('admin.delete_data.errors.blank'))
        errors = true
      end
    end

    if (attrs[:period].blank? || !attrs.has_key?(:period)) && attrs[:type_information].to_s!="profile"
      @delete_data.errors.add(:period, I18n.t('admin.delete_data.errors.blank'))
      errors = true
    end

    if (attrs[:corporation].blank? || !attrs.has_key?(:corporation)) && attrs[:delete].to_s=="complete" && attrs[:type_people].to_s == "councillor"
      @delete_data.errors.add(:corporation, I18n.t('admin.delete_data.errors.blank'))
      errors = true
    end

    if (attrs[:people].blank? || !attrs.has_key?(:people)) &&  attrs[:delete].to_s=="single"
      @delete_data.errors.add(:people, I18n.t('admin.delete_data.errors.blank'))
      errors = true
    end

    !errors
  end

  def set_logs
    @logs=DeleteData.all.order(created_at: :desc).page(params[:page])
  end

  def destroy_information(params)
    @name_person = ""
    @num_succesfull = 0
    @num_errors = 0
    @mesagges = ""
    if params[:delete].to_s == "complete"
      @job_levels.each do |job|
        delete_single(job, params )
      end
    elsif params[:delete].to_s == "single"
      job = JobLevel.find(params[:people]) if params[:type_people].to_s == "director"
      job = CouncillorsCorporation.joins(:job_level => [:person]).find(params[:people]) if params[:type_people].to_s == "councillor"
      @name_person = job.try(:backwards_name)
      delete_single(job, params)
    end

    if @num_errors > 0 && @num_succesfull == 0
      successful=false
    elsif @num_succesfull > 0 && @num_errors == 0
      successful=true
    else
      successful=false
    end

    @delete_data.assign_attributes(
      log: I18n.t("admin.delete_data.logs",
        delete: I18n.t("admin.dashboard.#{params[:delete]}"),
        author_email: current_administrator.try(:email),
        created_at: Time.zone.now,
        type_people: I18n.t("admin.dashboard.#{params[:type_people]}"),
        type_information: I18n.t("admin.dashboard.#{params[:type_information]}"),
        people: params[:people].blank? ? "" : @name_person,
        period: params[:type_information].to_s=="profile" ? "" : params[:period],
        corporation: (params[:type_people].to_s == "councillor" &&  params[:delete].to_s=="complete") ? Corporation.find(params[:corporation]).try(:name) : "",
        translated_successful: I18n.t("shared.#{successful}"),
        log: successful ? I18n.t("admin.dashboard.message_success") : I18n.t("admin.dashboard.message_errors")+"<br>"+@mesagges),
      successful: successful,
      administrator: current_administrator,
      created_at: Time.zone.now)

      if @delete_data.save!
        redirect_to admin_delete_datum_path(@delete_data)
      else
        render :index
      end
  end

  def delete_single(job, params)
    case params[:type_information]
    when "profile"
      if job.try(:person).try(:profile).try {|p| p.destroy}
        @num_succesfull = @num_succesfull +1
        Audit.create!(administrator: current_administrator,
          person: job.try(:person),
          action: I18n.t('audits.delete'),
          description: I18n.t('audits.delete_profile', full_name: job.try(:backwards_name)))
      else
        job.errors.full_messages.each {|men| @mesagges = @mesagges + "<br>" + men}
        @num_errors = @num_errors +1
      end
    when "assets_declaration"
      temporality = Temporality.find_by(name: params[:period])
      if temporality.blank?
        assets = job.try(:assets_declarations).where(temporality: Temporality.find_by(name: "Anual"), year_period: params[:period])
      else
        assets = job.try(:assets_declarations).where(temporality: temporality)
      end
      assets.each do |dec|
        if dec.destroy
          @num_succesfull = @num_succesfull +1
          Audit.create!(administrator: current_administrator,
            person: job.try(:person),
            action: I18n.t('audits.delete'),
            description: I18n.t('audits.delete_asset', full_name: job.try(:backwards_name),
            period: params[:period]))
        else
          dec.errors.full_messages.each {|men| @mesagges = @mesagges + "<br>" + men}
          @num_errors = @num_errors +1
        end
      end
    when "activities_declaration"
      temporality = Temporality.find_by(name: params[:period])
      if temporality.blank?
        activities = job.try(:activities_declarations).where(temporality: Temporality.find_by(name: "Anual"), year_period: params[:period])
      else
        activities = job.try(:activities_declarations).where(temporality: temporality)
      end
      activities.each do |dec|
        period_name = dec.period_name
        if dec.destroy
          @num_succesfull = @num_succesfull +1
          Audit.create!(administrator: current_administrator,
            person: job.try(:person),
            action: I18n.t('audits.delete'),
            description: I18n.t('audits.delete_activity', full_name: job.try(:backwards_name),
            period: period_name))
        else
          dec.errors.full_messages.each {|men| @mesagges = @mesagges + "<br>" + men}
          @num_errors = @num_errors +1
        end
      end
    else
    end
    job = job.try(:job_level) if params[:type_people].to_s == "councillor"
    job.updated_at = Time.zone.now
    job.save
  end
end
