class Admin::PeopleController < Admin::BaseController

  before_action :load_search, only: [:index]
  before_action :load_admin_person, only: [ :edit, :update, :show,:audits, :show_hide, :destroy, :show_unhide]
  #after_action :update_cumplimented_profile, only: [:update,:create]
  #after_action :change_job_update, only: [:update,:create]

  respond_to :html, :js, :json
  require 'csv'
  require 'stringio'

  def index
    if session[:search_params].blank? && params.has_key?(:name) || params.has_key?(:name)
      session[:search_params] = params
    elsif !session[:search_params].blank? && !params.has_key?(:name)
      params = session[:search_params]
    end

    search(session[:search_params] || params)

    respond_to do |format|
      format.csv { stream_csv_report  }
      format.xls { stream_xls_report  }
      format.html
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def new
    @person = Person.new
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @person = Person.new(person_params_strong)
    if @person.save
      if @person.profile.blank?
        @person.profile = Profile.new(cumplimented: false, updated_at: Time.zone.now)
      else
        @person.profile.cumplimented = @person.profile.has_profile?
      end
      @person.save
      Audit.create!(administrator: current_administrator,
                    person: @person,
                    action: I18n.t('audits.create'),
                    description: I18n.t('audits.create_description', full_name: @person.backwards_name))
      update_cumplimented_profile
      redirect_to admin_people_path, notice: I18n.t("people.notice.created")
    else
      render :new
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update
    @person.assign_attributes(person_params_strong)
    if @person.save
      if @person.profile.blank?
        @person.profile = Profile.new(cumplimented: false, updated_at: Time.zone.now)
      else
        @person.profile.cumplimented = @person.profile.has_profile?
      end
      @person.save
      Audit.create!(administrator: current_administrator,
                    person: @person,
                    action: I18n.t('audits.update'),
                    description: I18n.t('audits.update_description', full_name: @person.backwards_name))
      setAuditHideUnhide(params[:hide_reason],params[:unhide_reason])
      update_cumplimented_profile

      if params[:public_data].blank?
        flash[:notice] = I18n.t('people.notice.updated')
        redirect_to admin_people_path
      else
        redirect_to person_path(@job_level, corporation: params[:corporation]), notice: I18n.t('people.notice.updated')
      end
    else
      render :edit
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to admin_people_path, alert: "No se puede modificar esta persona debido a que no tiene correctamente los datos cargados"
  end

  def destroy
    @job_level.destroy if !@job_level.blank?
    @person.destroy if @person.job_levels.blank?
    redirect_to admin_people_path, notice: I18n.t('people.notice.deleted')
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show_hide
    respond_to do |format|
      format.js { render 'popup_hide' }
      format.html
    end
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def hide
    @job_level = JobLevel.friendly.find(params[:person_id])
    @job_level.change_working("hide",current_administrator, params[:hidden_reason], parse_date_at(params[:hidden_at]), params[:corporation])

    if params[:public_data].blank?
      redirect_to admin_people_path, notice: I18n.t('people.notice.hidden')
    else
      redirect_to person_path(@job_level, corporation:  params[:corporation]), notice: I18n.t('people.notice.hidden')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show_unhide
    respond_to do |format|
      format.js { render 'popup_unhide' }
      format.html
    end
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def unhide
    @job_level = JobLevel.friendly.find(params[:person_id])
    @job_level.change_working("unhide",current_administrator, params[:unhidden_reason], parse_date_at(params[:unhidden_at]), params[:corporation])

    if params[:public_data].blank?
      redirect_to admin_people_path, notice: I18n.t('people.notice.unhidden')
    else
      redirect_to person_path(@job_level, corporation:  params[:corporation]), notice: I18n.t('people.notice.unhidden')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def audits
    @audits= Audit.where(person_id: @person.id).order(created_at: :desc)
  rescue => e
    begin
      Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def portrait_delete
    profile = Profile.find(params[:id])
    job_level = JobLevel.friendly.find(params[:job_id])

    if profile.try(:portrait).try{|p| p.destroy}
      profile.try(:portrait).try{|p| p.clear}
      profile.portrait = nil
      if profile.save
        redirect_to edit_admin_person_path(job_level,public_data: params[:public_data], corporation: params[:corporation]), notice: I18n.t('people.notice.delete_image')
      else
        redirect_to edit_admin_person_path(job_level, public_data: params[:public_data], corporation: params[:corporation]), alert: I18n.t('people.alert.error_delete_image')
      end
    else
      redirect_to edit_admin_person_path(job_level, public_data: params[:public_data], corporation: params[:corporation]), alert: I18n.t('people.alert.error_delete_image')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

    def update_cumplimented_profile
      profile = @person.try(:profile)
      if !profile.blank?
        profile.cumplimented = profile.try{|p| p.has_profile?}.blank? ? false : profile.try{|p| p.has_profile?}
        profile.save
      end
    end

    def stream_csv_report
      query = @people.map {|person| {job_id: person['id'], councillor_id: person['councillor_id']}}

      stream_file("personas", "csv") do |stream|
        JobLevel.stream_query_rows(query) do |row_from_db|
          stream.write row_from_db
        end
      end
    end

    def stream_xls_report
      query = @people.map {|person| {job_id: person['id'], councillor_id: person['councillor_id']}}
      buffer = StringIO.new
      book = Spreadsheet::Workbook.new
      sheet = book.create_worksheet
      sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold

      index = 0
      JobLevel.stream_query_rows(query) do |row_from_db|
        sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
        index += 1
      end

      book.write(buffer)
      buffer.rewind
      send_data buffer.read
    end

    def stream_file(filename, extension)
      response.headers["Content-Type"] = "application/octet-stream"
      response.headers["Content-Disposition"] = "attachment; filename=#{filename}.#{extension}"

      yield response.stream
    ensure
      response.stream.close
    end

    def load_admin_person
      if params[:is_person].to_s=="true"
        @person = Person.find(params[:id])
        @job_level = nil
      else
        @job_level = JobLevel.friendly.find(params[:id])
        @person = @job_level.try(:person)
      end
    end

    def parse_date_at(date_at)
      return date_at if date_at.respond_to?(:year)
      begin
        date_at = DateTime.strptime(date_at, '%d/%m/%y %H:%M:%S')
      rescue
        date_at = Time.zone.parse(date_at)
      end
    end

    def load_search
      @status_type ||= {
        I18n.t("people.status.active")=>I18n.t("people.status.active"),
        I18n.t("people.status.inactive_show")=>I18n.t("people.status.inactive_show"),
        I18n.t("people.status.inactive_hidden")=>I18n.t("people.status.inactive_hidden")}

      @people_type ||= {
        I18n.t('admin.dashboard.councillor') => 'councillor',
        I18n.t('admin.dashboard.director') => 'director',
        I18n.t('admin.dashboard.temporary_worker') => 'temporary_worker',
        I18n.t('admin.dashboard.public_worker') => 'public_worker',
        I18n.t('admin.dashboard.spokesperson') => 'spokesperson' }
      @corporations = {}
      Corporation.all.order(name: :asc).each do |c|
        @corporations.merge!({c.name => c.id})
      end
    end

    def search(params_search)
      @people = ActiveRecord::Base.connection.exec_query("
        (SELECT
          job_levels.slug as \"slug\",
          (select people.personal_code FROM people where people.id =job_levels.person_id) as \"personal_code\" ,
          CAST(to_char(job_levels.updated_at,'yyyy-MM-dd HH24:MI') as varchar) as \"updated_at\",
          (select TRIM(from UPPER(people.name)) FROM people where people.id =job_levels.person_id) as \"name\" ,
          (select TRIM(from UPPER(people.last_name)) FROM people where people.id =job_levels.person_id) as \"last_name\" ,
          (select CONCAT(TRIM(from UPPER(people.last_name)),', ',TRIM(from UPPER(people.name))) FROM people where people.id =job_levels.person_id) as \"full_name\" ,
          job_levels.person_type as \"person_type\",
          job_levels.id as \"id\",
          CAST((select to_char(profiles.updated_at,'yyyy-MM-dd HH24:MI') from profiles, people where profiles.id=people.profile_id and people.id = job_levels.person_id) as varchar)  as \"profile_at\",
          (SELECT appointments.position from appointments where appointments.id = job_levels.appointment_id  ) as \"position\",
          (SELECT appointments.start_date from appointments where appointments.id = job_levels.appointment_id  ) as \"start_date\",
          (SELECT appointments.end_date from appointments where appointments.id = job_levels.appointment_id  ) as \"end_date\",
          '' as \"corporation\",
          null as \"corporation_id\",
          null as \"councillor_id\"
          from job_levels where (job_levels.person_type != 'councillor' or job_levels.person_type is null)  and (select CONCAT(TRIM(from UPPER(people.last_name)),', ',TRIM(from UPPER(people.name))) FROM people where people.id =job_levels.person_id) is not null)

UNION

        (SELECT
          (select job_levels.slug from job_levels where job_levels.id = councillors_corporations.job_level_id) as \"slug\",
          (select people.personal_code FROM people, job_levels where people.id =job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) as \"personal_code\" ,
          (Select CAST(to_char(job_levels.updated_at,'yyyy-MM-dd HH24:MI') as varchar) from job_levels where job_levels.id = councillors_corporations.job_level_id) as \"updated_at\",
          (select TRIM(from UPPER(people.name)) FROM people, job_levels where people.id =job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) as \"name\" ,
          (select TRIM(from UPPER(people.last_name)) FROM people, job_levels where people.id =job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) as \"last_name\" ,
          (select CONCAT(TRIM(from UPPER(people.last_name)),', ',TRIM(from UPPER(people.name))) FROM people, job_levels where people.id =job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) as \"full_name\" ,
          (select job_levels.person_type from job_levels where job_levels.id = councillors_corporations.job_level_id) as \"person_type\",
          (select job_levels.id from job_levels where job_levels.id = councillors_corporations.job_level_id) as \"id\",
          CAST((select to_char(profiles.updated_at,'yyyy-MM-dd HH24:MI') from profiles, people, job_levels where profiles.id=people.profile_id and people.id = job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) as varchar)  as \"profile_at\",
          (SELECT appointments.position from appointments where appointments.id = councillors_corporations.appointment_id  ) as \"position\",
          (SELECT appointments.start_date from appointments where appointments.id = councillors_corporations.appointment_id  ) as \"start_date\",
          (SELECT appointments.end_date from appointments where appointments.id = councillors_corporations.appointment_id  ) as \"end_date\",
          (SELECT corporations.name::varchar from corporations where corporations.id = councillors_corporations.corporation_id) as \"corporation\",
          (SELECT corporations.id::int from corporations where corporations.id = councillors_corporations.corporation_id) as \"corporation_id\",
          councillors_corporations.id::int as \"councillor_id\"
          from councillors_corporations WHERE (select CONCAT(TRIM(from UPPER(people.last_name)),', ',TRIM(from UPPER(people.name))) FROM people, job_levels where people.id =job_levels.person_id and job_levels.id = councillors_corporations.job_level_id) is not null)
           UNION
select p.id::varchar as \"slug\",
p.personal_code  as \"personal_code\" ,
         '' as \"updated_at\",
           TRIM(from UPPER(p.name))  as \"name\" ,
          TRIM(from UPPER(p.last_name))  as \"last_name\" ,
           CONCAT(TRIM(from UPPER(p.last_name)),', ',TRIM(from UPPER(p.name)))  as \"full_name\" ,
          '' as \"person_type\",
          null as \"id\",
          CAST((select to_char(profiles.updated_at,'yyyy-MM-dd HH24:MI') from profiles where profiles.id=p.profile_id) as varchar)  as \"profile_at\",
          '' as \"position\",
          null as \"start_date\",
          null as \"end_date\",
          '' as \"corporation\",
          null as \"corporation_id\",
          null as \"councillor_id\"
          from people p where p.id not in (select p2.id  from job_levels jl,people p2 where p2.id=jl.person_id)
        ")

      @people = @people.to_a.sort_by {|o| o['full_name'].blank? ? "" : o['full_name']}

      if !params_search.blank? && params_search[:export_all].to_s != "true"
        begin
          @people = @people.select{ |people| people['personal_code'].to_i == params_search["personal_code"].to_i} unless params_search["personal_code"].blank?
        rescue
        end
        begin
          @people = @people.select{ |people| I18n.transliterate(people['name'].downcase.to_s).include?(I18n.transliterate(params_search["name"].downcase.to_s))} unless params_search["name"].blank?
        rescue
        end
        begin
          @people = @people.select{ |people| I18n.transliterate(people['last_name'].downcase.to_s).include?(I18n.transliterate(params_search["last_name"].downcase.to_s))} unless params_search["last_name"].blank?
        rescue
        end
        begin
          @people = @people.select{ |people| I18n.transliterate(people['full_name'].downcase.to_s.gsub(',','').to_s).include?(I18n.transliterate(params_search["full_name"].downcase.to_s.gsub(',','').to_s))} unless params_search["full_name"].blank?
        rescue
        end
        begin
          @people = @people.select{ |people| I18n.transliterate(people['position'].downcase.to_s).include?(I18n.transliterate(params_search["area"].downcase.to_s))} unless params_search["area"].blank?
        rescue
        end
        begin
          @people = @people.select{ |people| people['person_type'] == params_search["person_type"]} unless params_search["person_type"].blank?
        rescue
        end

        begin
          @people = @people.select{ |people| people['corporation_id'].to_i == params_search["corporation"].to_i} unless params_search["corporation"].blank?
        rescue
        end
        begin
          if !params_search["start_date"].blank? && !params_search["end_date"].blank?
            if date_from_less_than_date_to?(start_date: params_search["start_date"], end_date: params_search["end_date"], filter: "updated_at", param_name: "start_date_less_than_end_date")
              @people = @people.select{ |people| !people['updated_at'].blank? &&  people['updated_at'].to_date >= params_search["start_date"].to_date  && people['updated_at'].to_date <= params_search["end_date"].to_date}
            end
          elsif !params_search["start_date"].blank?
            if date_format_valid?(date: params_search["start_date"], filter: "updated_at", param_name: "start_date")
              @people = @people.select{ |people| !people['updated_at'].blank? && people['updated_at'].to_date >=params_search["start_date"].to_date }
            end
          elsif !params_search["end_date"].blank?
            if date_format_valid?(date: params_search["end_date"], filter: "updated_at", param_name: "end_date")
              @people = @people.select{ |people| !people['updated_at'].blank? && people['updated_at'].to_date <= params_search["end_date"].to_date }
            end
          end
        rescue
        end

        begin
          if !params_search["start_incorporate_date"].blank? && !params_search["end_incorporate_date"].blank?
            if date_from_less_than_date_to?(start_date: params_search["start_incorporate_date"], end_date: params_search["end_incorporate_date"], filter: "incorporate", param_name: "start_date_less_than_end_date")
              @people = @people.select{ |people| !people['start_date'].blank? && people['start_date'].to_date >= params_search["start_incorporate_date"].to_date &&  people['start_date'].to_date <= params_search["end_incorporate_date"].to_date }
            end
          elsif !params_search["start_incorporate_date"].blank?
            if date_format_valid?(date: params_search["start_incorporate_date"], filter: "incorporate", param_name: "start_date")
              @people = @people.select{ |people| !people['start_date'].blank? && people['start_date'].to_date >=params_search["start_incorporate_date"].to_date }
            end
          elsif !params_search["end_incorporate_date"].blank?
            if date_format_valid?(date: params_search["end_incorporate_date"], filter: "incorporate", param_name: "end_date")
              @people = @people.select{ |people| !people['start_date'].blank? && people['start_date'].to_date <= params_search["end_incorporate_date"].to_date }
            end
          end
        rescue
        end

        begin
          if !params_search["start_cessation_date"].blank? && !params_search["end_cessation_date"].blank?
            if date_from_less_than_date_to?(start_date: params_search["start_cessation_date"], end_date: params_search["end_cessation_date"], filter: "cessation", param_name: "start_date_less_than_end_date")
              @people = @people.select{ |people| !people['end_date'].blank? && people['end_date'].to_date >= params_search["start_cessation_date"].to_date &&  people['end_date'].to_date <= params_search["end_cessation_date"].to_date }
            end
          elsif !params_search["start_cessation_date"].blank?
            if date_format_valid?(date: params_search["start_cessation_date"], filter: "cessation", param_name: "start_date")
              @people = @people.select{ |people| !people['end_date'].blank? && people['end_date'].to_date >= params_search["start_cessation_date"].to_date }
            end
          elsif !params_search["end_cessation_date"].blank?
            if date_format_valid?(date: params_search["end_cessation_date"], filter: "cessation", param_name: "end_date")
              @people = @people.select{ |people| !people['end_date'].blank? && people['end_date'].to_date <= params_search["end_cessation_date"].to_date }
            end
          end
        rescue
        end

        begin
          if !params_search["status"].blank?
            if params_search["status"].to_s ==  I18n.t("people.status.active")
              @people = @people.select{ |people| I18n.transliterate(JobLevel.get_active(people).downcase.to_s).include?(I18n.transliterate(I18n.t("people.status.active").downcase.to_s))}
            elsif params_search["status"].to_s ==  I18n.t("people.status.inactive_show")
              @people = @people.select{ |people| I18n.transliterate(JobLevel.get_active(people).downcase.to_s).include?(I18n.transliterate(I18n.t("people.status.inactive_show").downcase.to_s))}
            else
              @people = @people.select{ |people| I18n.transliterate(JobLevel.get_active(people).downcase.to_s).include?(I18n.transliterate(I18n.t("people.status.inactive_hidden").downcase.to_s))}
            end
          end
        rescue
        end
      end

      @people_pag = Kaminari.paginate_array(@people).page(!params_search.blank? ? params_search["page"] : params[:page] || 1).per(50)
    end

    def date_format_valid?(date:, filter:, param_name:)
      valid_date = valid_date?(date: date)
      unless valid_date
        flash.now[:alert] = t("filter_people.errors.#{filter}.#{param_name}")
      end
      valid_date
    end

    def date_from_less_than_date_to?(start_date:, end_date:, filter:, param_name:)
      date_from_less_than_date_to = Date.parse(start_date) <= Date.parse(end_date)
      unless date_from_less_than_date_to
        flash.now[:alert] = t("filter_people.errors.#{filter}.#{param_name}")
      end
      date_from_less_than_date_to
    end

    def valid_date?(date:)
      Date.strptime(date.match(/^\d{4}-\d{2}-\d{2}$/).to_s, '%Y-%m-%d') rescue false
    end

    def setAuditHideUnhide(hide_reason,unhide_reason)
      if !hide_reason.blank? && !@person.leaving_date.blank?
        Audit.create!(administrator: current_administrator,
          person: @person,
          action: I18n.t('audits.hide'),
          description: hide_reason)
      elsif !unhide_reason.blank? && @person.leaving_date.blank?
        Audit.create!(administrator: current_administrator,
          person: @person,
          action: I18n.t('audits.unhide'),
          description: unhide_reason)
      end
    end

    def profile_permit_params
      [profile_attributes: [
        :id, :email, :twitter, :facebook, :calendar_url, :updated_at, :portrait, :_destroy,
        {
          studies_attributes: [
            :id, :_destroy, :official_degree, :center, :start_year, :end_year, :education_level, :table_order
          ]
        },
        {
          courses_attributes: [ :id, :_destroy, :title, :center, :start_year, :end_year, :table_order]
        },
        {
          languages_attributes: [ :id, :_destroy, :name, :level, :table_order ]
        },
        {
          public_jobs_attributes: [
            :id, :_destroy, :body_scale, :init_year, :consolidation_degree, :position,
            :public_administration, :start_year, :end_year, :table_order
          ]
        },
        {
          private_jobs_attributes: [:id, :_destroy, :position, :entity, :start_year, :end_year, :table_order]
        },
        {
          political_posts_attributes: [:id, :_destroy, :position, :entity, :start_year, :end_year, :table_order]
        },
        {
          publications_attributes: [information_permit_params, :table_order ]
        },
        {
          studies_comments_attributes: [information_permit_params]
        },
        {
          courses_comments_attributes: [information_permit_params]
        },
        {
          career_comments_attributes: [information_permit_params]
        },
        {
          political_posts_comments_attributes: [information_permit_params]
        },
        {
          theacher_activity_attributes: [information_permit_params]
        },
        {
          special_mentions_attributes: [information_permit_params]
        },
        {
          other_attributes: [information_permit_params]
        },
        {
          informations_attributes: [ :id, :information ]
        }
      ]]
    end

    def information_permit_params
      [:id, :_destroy, :information_at, :information, :information_type_id, :information_type]
    end

    def appointment_permit_params
      [appointment_attributes: [
        :id, :_destroy, :position, :position_alt, :unit, :area, :start_date, :end_date,
        :possession_date, :description_possession, :email,:url_form, :functions, :url_possession,
        :retribution, :retribution_year, :observations, :transparency_link, :other_expenses,
        :gifts, :description, charges_permit_params, commisions_permit_params
      ]]
    end

    def charges_permit_params
      [charges_attributes: [:id, :charge, :_destroy]]
    end

    def commisions_permit_params
      [commisions_attributes: [:id, :commision, :_destroy]]
    end

    def job_level_permit_params
      [:id, :_destroy, :updated_at, :person_type, :slug]
    end

    def corporation_permit_params
      [councillors_corporations_attributes: [
            :id, :_destroy, :corporation_id, :order_num, :electoral_list_id,
            appointment_permit_params, activities_declarations_permit_params, assets_declarations_permit_params, organisms_permit_params,
            dedications_permit_params, termination_authorizations_permit_params, compatibility_activities_permit_params
      ]]
    end

    def organisms_permit_params
      [organisms_attributes: [:id, :title, :_destroy]]
    end

    def dedications_permit_params
      [dedications_attributes: [:id, :start_date, :end_date, :percentage, :description, :_destroy]]
    end

    def activities_declarations_permit_params
      [activities_declarations_attributes: [
        :id, :_destroy, :declaration_date, :year_period, :temporality_id, :editable,
        {
          public_activities_attributes: [ :id, :_destroy, :entity, :position, :start_date, :end_date, :table_order ]
        },
        {
          private_activities_attributes: [:id, :_destroy, :private_activity_type, :description, :entity,
            :position, :start_date, :end_date, :table_order ]
        },
        {
          other_activities_attributes: [ :id, :_destroy, :description, :start_date, :end_date, :table_order]
        }
      ]]
    end

    def termination_authorizations_permit_params
      [termination_authorizations_attributes: [
        :id, :_destroy, :authorization_date, :authorization_description
      ]]
    end

    def compatibility_activities_permit_params
      [compatibility_activities_attributes: [
        :id, :_destroy, :resolution_date, :compatibility_description, :public_private
      ]]
    end

    def assets_declarations_permit_params
      [assets_declarations_attributes: [
        :id, :_destroy, :declaration_date, :year_period, :temporality_id, :editable,
        {
          real_estate_properties_attributes: [
            :id, :_destroy, :kind, :straight_type, :adquisition_title, :municipality, :participation_percentage,
            :mounth, :year, :cadastral_value, :observations, :purchase_date, :table_order
          ]
        },
        {
          vehicles_attributes: [ :id, :_destroy, :kind, :model, :mounth, :year, :purchase_date, :table_order ]
        },
        {
          other_personal_properties_attributes: [ :id, :_destroy, :types, :mounth, :year, :description, :purchase_date, :table_order]
        },
        {
          account_deposits_attributes: [ :id, :_destroy, :kind, :entity, :balance, :table_order]
        },
        {
          debts_attributes: [ :id, :_destroy, :kind, :import, :observations, :table_order]
        },
        {
          tax_data_attributes: [
            :id, :_destroy, :import, :observation, :tax, :fiscal_data, :table_order
            # :irpf_big_import, :irpf_big_observation, :irpf_bi_import, :irpf_bi_observation,
            # :irpf_deduc_import_a, :irpf_deduc_observation_a, :irpf_deduc_import_b, :irpf_deduc_observation_b,
            # :isp_bi_import, :isp_bi_observation, :is_bi_import, :is_bi_observation
          ]
        },
        {
          other_deposits_attributes: [:id, :_destroy, :types, :mounth, :year, :value, :description, :purchase_date, :table_order ]
        }
      ]]
    end

    def person_params_strong
      params.require(:person).permit(
        :id, :name, :last_name, :personal_code, :sex, :document_type_id, :document_nif,:profile_id, profile_permit_params,
        {
          councillors_attributes: [ job_level_permit_params, corporation_permit_params ]
        },
        {
          directors_attributes: [
            job_level_permit_params,  appointment_permit_params, activities_declarations_permit_params, assets_declarations_permit_params,
            termination_authorizations_permit_params, compatibility_activities_permit_params
          ]
        },
        {
          public_workers_attributes: [
            job_level_permit_params, appointment_permit_params, activities_declarations_permit_params, assets_declarations_permit_params
          ]
        },
        {
          temporary_workers_attributes: [
            job_level_permit_params, appointment_permit_params, activities_declarations_permit_params, assets_declarations_permit_params
          ]
        },
        {
          spokespeople_attributes: [
            job_level_permit_params, appointment_permit_params, activities_declarations_permit_params, assets_declarations_permit_params
          ]
        }
      )
    end
end
