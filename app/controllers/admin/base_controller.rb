class Admin::BaseController < ApplicationController
  helper_method :sort_column, :sort_direction

  before_action :authenticate_administrator!

  layout 'admin'

  private

    def full_feature?
      true
    end

    def sort_column
      params[:sort] ? params[:sort] : ""
    end
    
    def sort_direction
      !params[:direction].blank? ? params[:direction].to_s : "asc"
    end

    def stream_file(filename, extension)
      response.headers["Content-Type"] = "application/octet-stream"
      response.headers["Content-Disposition"] = "attachment; filename=#{filename}.#{extension}"
  
      yield response.stream
    ensure
      response.stream.close
    end

end
