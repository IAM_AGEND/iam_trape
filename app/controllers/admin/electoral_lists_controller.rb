class Admin::ElectoralListsController < Admin::BaseController
    before_action :load_data, only: [:new, :create, :edit, :update, :destroy]

    def index
        @electoral_lists = ElectoralList.all.order(name: :asc)
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def new
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def create
        @electoral_list.assign_attributes(electoral_list_params)
        if @electoral_list.save
            redirect_to admin_electoral_lists_path, notice: I18n.t('admin.electoral_lists.success_insert')
        else
            render :new
        end
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def edit
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def update
        if @electoral_list.update(electoral_list_params)
            redirect_to admin_electoral_lists_path, notice: I18n.t('admin.electoral_lists.success_update')
        else
            render :edit
        end
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def destroy
        if @electoral_list.destroy
            redirect_to admin_electoral_lists_path, notice: I18n.t('admin.electoral_lists.success_destroy')
        else
            redirect_to admin_electoral_lists_path, alert: I18n.t('admin.electoral_lists.error_destroy')
        end
    rescue => e
        begin
          Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def load_data
        @electoral_list = params[:id].blank? ? ElectoralList.new : ElectoralList.find(params[:id])
    end

    def electoral_list_params
        params.require(:electoral_list).permit(:name, :logo, :long_name)
    end
end