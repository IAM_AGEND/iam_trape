class Admin::CorporationsController < Admin::BaseController
    before_action :load_data, only: [:new, :create, :edit, :update, :destroy]

    def index
        @corporations = Corporation.all.order(name: :asc)
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def new
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def edit
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def create
        @corporation.name = params_corporations[:start_year]+"-"+params_corporations[:end_year]
        @corporation.assign_attributes(params_corporations)
        if @corporation.save
            redirect_to admin_corporations_path, notice: I18n.t('admin.corporations.success_insert')
        else
            render :new
        end
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def update
        @corporation.name = params_corporations[:start_year]+"-"+params_corporations[:end_year]
        if @corporation.update(params_corporations)
            redirect_to admin_corporations_path, notice: I18n.t('admin.corporations.success_update')
        else
            render :edit
        end
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def destroy
        if @corporation.destroy
            redirect_to admin_corporations_path, notice: I18n.t('admin.corporations.success_destroy')
        else
            redirect_to admin_corporations_path, alert: I18n.t('admin.corporations.error_destroy')
        end
    rescue => e
        begin
          Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def params_corporations
        params.require(:corporation).permit(
            :name, 
            :election_date,
            :start_year, 
            :end_year, 
            :full_constitutional_date, 
            :end_corporation_date,
            :councillors_num,
            :descriptions,
            :active,
            {
              corporations_electoral_lists_attributes: [
                :id,
                :_destroy,
                :corporation_id,
                :electoral_list_id,
                :order
              ]
            })
    end

    def load_data
        @corporation = params[:id].blank? ? Corporation.new : Corporation.find(params[:id])
    end

end