require 'excel_importers/assets'

class Admin::AssetsUploadsController < Admin::LogsController
  before_action :load_export_data, only: [:index, :export]

  def index
    @assets_uploads = AssetsUpload.includes(:administrator).all.order(created_at: :desc).page(params[:page]).preload(:administrator)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    attrs = assets_upload_params.merge(author: current_administrator,
                                       check_for_file: true)

    if validate_params(attrs)
      StringIO.open do |strio|
        logger = ExcelImporters::Base.new_default_logger(strio)
        importer = ExcelImporters::Assets.new(
          attrs[:file].tempfile,
          attrs[:period],
          attrs[:corporation],
          attrs[:job],
          logger)
        
        successful = true
        if importer.import
          flash[:notice] = t('admin.assets_uploads.create.no_errors')
        else
          successful = false
          flash[:alert] = t('admin.assets_uploads.create.errors')
        end

        logger.close

        @assets_upload.assign_attributes(
          log: I18n.t('admin.assets_uploads.logs', 
            log: strio.string, 
            created_at: Time.zone.now, 
            period: attrs[:period],
            author_email: current_administrator.try(:email), 
            original_filename: attrs[:file].try(:original_filename),
            file_format: importer.file_format,
            translated_successful: I18n.t("shared.#{successful}")),
          emails: importer.emails,
          successful: successful,
          administrator: attrs[:author],
          created_at: Time.zone.now
        )
      end

      if @assets_upload.save!
        redirect_to admin_assets_upload_path(@assets_upload)
      else
        render :new
      end
    else
      render :new
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @assets_upload = AssetsUpload.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show_mailer
    @asset_mailer = AssetsEmailLog.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def send_mailer
    asset_upload = AssetsUpload.find(params[:id])
    asset_mailer = AssetsEmailLog.new
    generate_log_email(params[:emails],asset_upload,asset_mailer)
    
    if !asset_mailer.blank? && asset_mailer.save
      redirect_to show_mailer_admin_assets_upload_path(asset_mailer), notice: I18n.t('logs.success')
    else
      redirect_to admin_assets_upload_path(asset_upload, section_m: params[:section_m]), alert: I18n.t('logs.error')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def export
    attrs = (params[:export_data].blank? || !params.has_key?(:export_data)) ? {} : export_data_params

    if validate_params_export(attrs)
      export_information(attrs)
    else
      params[:export] = true
      render :index
    end
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def assets_upload_params
      params.require(:assets_upload).permit(:file, :period, :corporation, :job)
    end

    def load_data
      @assets_upload = AssetsUpload.new
    end

    def load_export_data
      @job_levels = {}
      @periods = {}   
      @corporation_list = Corporation.all.order(name: :asc)

      @type_people = {
        I18n.t("admin.dashboard.councillor")=>"councillor" ,
        I18n.t("admin.dashboard.director")=>"director"
      }
     
      @attrs = (params[:export_data].blank? || !params.has_key?(:export_data)) ? {} : export_data_params
      if !@attrs[:type_people].blank?
        if @attrs[:type_people].to_s=="director"
          @job_levels = JobLevel.joins(:person).preload(:person).where(person_type: @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
        elsif @attrs[:type_people].to_s=="councillor"
          @job_levels = CouncillorsCorporation.joins(:job_level => [:person]).preload(:corporation,:job_level => [:person]).where("job_levels.person_type=?", @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
        end
      end

      unless @attrs[:export_type] == 'complete'
        unless @attrs[:people].blank?
          if @attrs[:type_people].to_s=="director"
            @job_levels = @job_levels.where("job_levels.id in (?)", JobLevel.joins(:assets_declarations).preload(:assets_declarations).all.uniq.select("job_levels.id"))
            job = JobLevel.find(params[:export_data][:people])
          elsif @attrs[:type_people].to_s=="councillor"
            job = CouncillorsCorporation.find(params[:export_data][:people])
            @job_levels = @job_levels.where("councillors_corporations.id in (?)", CouncillorsCorporation.joins(:assets_declarations).preload(:assets_declarations).all.uniq.select("councillors_corporations.id"))
          end
          generate_period(job, :assets_declarations)
        end
      else
        if @attrs[:type_people].to_s=="director"
          @job_levels = JobLevel.joins(:person).preload(:person).where(person_type: @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
        elsif @attrs[:type_people].to_s=="councillor"
          @job_levels = CouncillorsCorporation.joins(:job_level => [:person]).preload(:corporation,:job_level => [:person]).where("job_levels.person_type=?", @attrs[:type_people]).order("people.name ASC, people.last_name ASC")
        end
        @job_levels.each {|job| generate_period(job, :assets_declarations) }
      end
      @periods = @periods.sort
      @job_levels = @job_levels.sort_by {|x| x.backwards_name}
    end

    def validate_params(attrs)
      errors=false
      [:file,:period, :job].each do |p|
        if attrs[p].blank? || !attrs.has_key?(p)
          @assets_upload.errors.add(p, I18n.t('admin.assets_uploads.errors.blank'))
          errors = true 
        end
      end

      !errors
    end

    def validate_params_export(attrs)
      errors=false
      [:export_type,:type_people, :period].each do |field|
        if attrs[field].blank? || !attrs.has_key?(field)
          @assets_upload.errors.add(field, I18n.t('admin.assets_uploads.errors.blank'))
          errors = true 
        end
      end

      if (attrs[:corporation].blank? || !attrs.has_key?(:corporation)) && attrs[:type_people].to_s == "councillor"
        @assets_upload.errors.add(:corporation, I18n.t('admin.assets_uploads.errors.blank'))
        errors = true
      end

      if (attrs[:people].blank? || !attrs.has_key?(:people)) &&  attrs[:export].to_s=="single"
        @assets_upload.errors.add(:people, I18n.t('admin.assets_uploads.errors.blank'))
        errors = true 
      end
      !errors
    end

    def export_data_params
      params.require(:export_data).permit(:export_type,:period, :corporation, :people, :type_people)
    end

    def export_information (params)
      buffer = StringIO.new
      book = Spreadsheet::Workbook.new 
      (1..8).each do |i|
        sheet = book.create_worksheet :name => I18n.t("assets_exporter.sheet#{i}")
        sheet.row(0).default_format = Spreadsheet::Format.new color: :white, pattern: 1, pattern_fg_color:  :xls_color_54 , weight: :bold
      
        index = 0
        AssetsUpload.stream_query_rows(params, i.to_i) do |row_from_db|
          sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
          index += 1
        end
      end
    
      book.write(buffer)
      buffer.rewind
      send_data buffer.read, :filename => "DeclaracionBienes.xls", :type =>  "application/vnd.ms-excel"
    end

    def generate_period(job=nil,type=nil)
      if !type.blank? && !job.blank?
        job.try(type).each do |dec|
          key = dec.try(:temporality).try(:name)
          if key.to_s == "Anual"
            @periods.merge!({dec.try(:year_period).to_s=>dec.try(:year_period).to_s})
          else
            @periods.merge!({key=>key}) 
          end
        end
      end
    end
end
