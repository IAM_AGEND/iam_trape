require 'inper_ws'
require 'notifications_importer'

class Admin::NotificationsController < Admin::BaseController
  before_action :loadNotification, only: [:show, :permit, :reject, :publicable, :insert_reject]
  before_action :loadTypePerson, only: [:profiles,:assets,:activities,:profiles_historic,:assets_historic,:activities_historic, :person, :person_historic]
  include ApplicationHelper

  respond_to :html, :js, :json
  require 'csv'
  require 'stringio'

  def profiles
    setNotifications('Profile', 'pending')
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def assets
    setNotifications('AssetsDeclaration', 'pending')
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def activities
    setNotifications('ActivitiesDeclaration', 'pending')
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def profiles_historic
    setNotifications('Profile', 'historic')
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def assets_historic
    setNotifications('AssetsDeclaration', 'historic')
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def activities_historic
    setNotifications('ActivitiesDeclaration', 'historic')
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def person
    setNotifications('Person', 'pending')
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def person_historic
    setNotifications('Person', 'historic')
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    case @notification.try(:type_data)
    when 'Profile'
      @data_show = Profile.getNotificationData(@notification)
      @path_content= @notification.historic? ? profiles_historic_admin_notifications_path : profiles_admin_notifications_path
    when 'AssetsDeclaration'
      @data_show = AssetsDeclaration.getNotificationData(@notification)
      @path_content= @notification.historic? ? assets_historic_admin_notifications_path : assets_admin_notifications_path
    when 'ActivitiesDeclaration'
      @data_show = ActivitiesDeclaration.getNotificationData(@notification)
      @path_content= @notification.historic? ? activities_historic_admin_notifications_path : activities_admin_notifications_path
    when 'Person'
      @data_show = Person.getNotificationData(@notification)
      @path_content= @notification.historic? ? person_historic_admin_notifications_path : person_admin_notifications_path
    else
      @data_show = ''
    end
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update_data#this on the botton update
    NotificationsImporter.new().import(false)
    redirect_to profiles_admin_notifications_path, notice: I18n.t('notifications.succes_update')
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to profiles_admin_notifications_path, alert: I18n.t('notifications.reject_update')
  end

  def permit
    importer = NotificationsImporter.new()
    permitido = false
    @notification.no_publish_reason = nil
    case @notification.try(:type_data)
    when 'Profile'
      permitido = importer.saveProfileData(@notification, false, current_administrator)
    when 'AssetsDeclaration'
      permitido = importer.saveAssetsDeclarationData(@notification, false, current_administrator)
    when 'ActivitiesDeclaration'
      permitido = importer.saveActivitiesDeclarationData(@notification, false, current_administrator)
    when 'Person'
      permitido = importer.savePersonData(@notification, false, current_administrator)
    end

    @notification.permit = true
    @notification.administrator = current_administrator
    @notification.change_state_at = Time.zone.now
    @notification.send_feedback = false

    if permitido && @notification.save
      ws = INPERWS.new
      person_aux_types = {
          'C' => 'councillors',
          'R' => 'directors',
          'F' => 'public_workers',
          'E' => 'temporary_workers',
          'V' => 'spokespeople' }
      type = @notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : person_aux_types[@notification.try(:content_data).try{|x| x['gperAbrev']}]
      person_identificator = @notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? @notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : @notification.try(:content_data).try{|x| x['dperNumper']}.to_s
      aux_person = Person.find_by(personal_code: person_identificator)

      case @notification.try(:type_data)
      when 'Profile'
        begin
          @notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{@notification.try(:content_data)['dpfId']} / "
          inper_response = ws.setWSPerfilesPublicado(@notification.try(:content_data)['dpfId'], @notification.change_state_at, 0,
                                                    "Se han publicado los datos en TRAPE con fecha #{@notification.change_state_at}")
          begin
            respuesta= JSON.parse(inper_response)
            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
              @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
              @notification.motive = ""
              @notification.send_feedback = true
            else
              @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
              @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
            end
          rescue => e
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = e.message
          end
        rescue => e
          @notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{@notification.try(:content_data)['dpfId']} / No se ha realizado el feedback"
          @notification.motive = e.message
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
        begin
          if !Rails.env.production?
            mailers_test.each do |mail|
              Mailer.profile_upload(type,mail).deliver_now
            end
          else
            if !aux_person.profile.email.blank?
              Mailer.profile_upload(type,aux_person.profile.email).deliver_now
            elsif !aux_person.profile.personal_email.blank?
              Mailer.profile_upload(type,aux_person.profile.personal_email).deliver_now
            end
          end
        rescue => e
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
      when 'AssetsDeclaration'
        begin
          @notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / "
          inper_response = ws.setWSDeclaracionPublicado(@notification.try(:content_data)["decg_id"],@notification.change_state_at,0,
                                                        "Se han publicado los datos en TRAPE con fecha #{@notification.change_state_at}").to_s
          begin
            respuesta= JSON.parse(inper_response)
            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
              @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
              @notification.motive = ""
              @notification.send_feedback = true
            else
              @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
              @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
            end
          rescue => e
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = e.message
          end
        rescue => e
          @notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / No se ha realizado el feedback"
          @notification.motive = e.message
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
        begin
          if !Rails.env.production?
            mailers_test.each do |mail|
              Mailer.assets_upload(type,mail).deliver_now
            end
          else
            if !aux_person.profile.email.blank?
              Mailer.assets_upload(type,aux_person.profile.email).deliver_now
            elsif !aux_person.profile.personal_email.blank?
              Mailer.assets_upload(type,aux_person.profile.personal_email).deliver_now
            end
          end
        rescue => e
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
      when 'ActivitiesDeclaration'
        begin
          @notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{@notification.try(:content_data)['decgId']} / "
          inper_response = ws.setWSDeclaracionPublicado(@notification.try(:content_data)["decgId"],@notification.change_state_at,0,
                                                        "Se han publicado los datos en TRAPE con fecha #{@notification.change_state_at}")
          begin
            respuesta= JSON.parse(inper_response)
            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
              @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
              @notification.motive = ""
              @notification.send_feedback = true
            else
              @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
              @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
            end
          rescue => e
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = e.message
          end
        rescue => e
          @notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{@notification.try(:content_data)['decgId']} / No se ha realizado el feedback"
          @notification.motive = e.message
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
        begin
          if !Rails.env.production?
            mailers_test.each do |mail|
              Mailer.activities_upload(type,mail).deliver_now
            end
          else
            if !aux_person.profile.email.blank?
              Mailer.activities_upload(type,aux_person.profile.email).deliver_now
            elsif !aux_person.profile.personal_email.blank?
              Mailer.activities_upload(type,aux_person.profile.personal_email).deliver_now
            end
          end
        rescue => e
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
      end

      @notification.save
      redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), notice: I18n.t('notifications.succes_permit')
    else
      redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), alert: I18n.t('notifications.error_permit')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    @notification.no_publish_reason = "ERROR: #{e}"
    @notification.save
    redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), alert: I18n.t('notifications.error_permit')
  end

  def insert_reject
    case @notification.try(:type_data)
    when 'Profile'
      @path_content= @notification.historic? ? profiles_historic_admin_notifications_path : profiles_admin_notifications_path
    when 'AssetsDeclaration'
      @path_content= @notification.historic? ? assets_historic_admin_notifications_path : assets_admin_notifications_path
    when 'ActivitiesDeclaration'
      @path_content= @notification.historic? ? activities_historic_admin_notifications_path : activities_admin_notifications_path
    when 'Person'
      @path_content= @notification.historic? ? person_historic_admin_notifications_path : person_admin_notifications_path
    end
  rescue => e
    begin
      Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def reject
    @notification.ignore = true
    @notification.administrator = current_administrator
    @notification.change_state_at = Time.zone.now
    @notification.no_publish_reason = params[:reason]
    @notification.send_feedback = false
    ws = INPERWS.new

    case @notification.try(:type_data)
    when 'Profile'
      begin
        inper_response = ws.setWSPerfilesPublicado(@notification.try(:content_data)['dpfId'],@notification.change_state_at,1,@notification.no_publish_reason)
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{@notification.try(:content_data)['dpfId']} / Razón: #{@notification.no_publish_reason} / "
        begin
          respuesta= JSON.parse(inper_response)
          if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
            @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
            @notification.motive = ""
            @notification.send_feedback = true
          else
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
          end
        rescue => e
          @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
          @notification.motive = e.message
        end
      rescue => e
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{@notification.try(:content_data)['dpfId']} / Razón: #{@notification.no_publish_reason} / No se ha realizado el feedback"
        @notification.motive = e.message
        begin
          logger.error("ERROR: #{e}")
        rescue
        end
      end
    when 'AssetsDeclaration'
      begin
        inper_response = ws.setWSDeclaracionPublicado(@notification.try(:content_data)["decg_id"],@notification.change_state_at,1,@notification.no_publish_reason).to_s
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / Razón: #{@notification.no_publish_reason} / "
        begin
          respuesta= JSON.parse(inper_response)
          if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
            @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
            @notification.motive = ""
            @notification.send_feedback = true
          else
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
          end
        rescue => e
          @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
          @notification.motive = e.message
        end
      rescue => e
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / Razón: #{@notification.no_publish_reason} / No se ha realizado el feedback"
        @notification.motive = e.message
        begin
          logger.error("ERROR: #{e}")
        rescue
        end
      end
    when 'ActivitiesDeclaration'
      begin
        inper_response = ws.setWSDeclaracionPublicado(@notification.try(:content_data)["decgId"],@notification.change_state_at,1,@notification.no_publish_reason)
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{@notification.try(:content_data)['decgId']} / Razón: #{@notification.no_publish_reason} / "
        begin
          respuesta= JSON.parse(inper_response)
          if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
            @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
            @notification.motive = ""
            @notification.send_feedback = true
          else
            @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
            @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
          end
        rescue => e
          @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
          @notification.motive = e.message
        end
      rescue => e
        @notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{@notification.try(:content_data)['decgId']} / Razón: #{@notification.no_publish_reason} / No se ha realizado el feedback"
        @notification.motive = e.message
        begin
          logger.error("ERROR: #{e}")
        rescue
        end
      end
    when 'Person'
      begin
        @notification.no_publish_reason = "Se han rechazado los datos. #{@notification.no_publish_reason}"
      rescue => e
        @notification.no_publish_reason = "Se han rechazado los datos. #{@notification.no_publish_reason}"
        begin
          logger.error("ERROR: #{e}")
        rescue
        end
      end
    end

    if @notification.save
      redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), notice: I18n.t('notifications.succes_reject')
    else
      redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), alert: I18n.t('notifications.error_reject')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00013: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    @notification.no_publish_reason = "ERROR: #{e}"
    @notification.save
    redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}_admin_notifications_path"), alert: I18n.t('notifications.error_reject')
  end

  def publicable
    job_level = ""
    corporation_name = ""
    contenido = @notification.try(:content_data)



    case @notification.try(:type_data)
    when 'Profile'
      person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']} : contenido.try{|x| x['dperNumper']})
      if person.blank?
          person =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
      end
      person_type = contenido.try{|x| x['gperAbrev']}
      case person_type
      when 'V'
        job_level = person.try(:spokespeople).try{|x| x[0]}
      when 'E'
        job_level = person.try(:temporary_workers).try{|x| x[0]}
      when 'R'
        job_level = person.try(:directors).try{|x| x[0]}
      when 'F'
        job_level = person.try(:public_workers).try{|x| x[0]}
      when 'C'
        corporation_name = contenido['ultimoConcejal'][0]['corporacion']['cpNombre'].gsub('/','-').to_s
        job_level = person.try(:councillors).try{|x| x[0]}
      end
    when 'AssetsDeclaration'
      aux = AssetsDeclaration.transformData(@notification)
      person_type = contenido.try{|x| x['gperAbrev']}

      case person_type
      when 'V'
        job_level = aux.try(:person_level)
      when 'E'
        job_level = aux.try(:person_level)
      when 'R'
        job_level = aux.try(:person_level)
      when 'F'
        job_level = aux.try(:person_level)
      when 'C'
        job_level = aux.try(:person_level).try(:job_level)
        corporation_name =contenido.try{|x| x['cpNombre']}.gsub('/','-')
      end
    when 'ActivitiesDeclaration'
      aux = ActivitiesDeclaration.transformData(@notification)
      person_type = contenido.try{|x| x['gperAbrev']}

      case person_type
      when 'V'
        job_level = aux.try(:person_level)
      when 'E'
        job_level = aux.try(:person_level)
      when 'R'
        job_level = aux.try(:person_level)
      when 'F'
        job_level = aux.try(:person_level)
      when 'C'
        job_level = aux.try(:person_level).try(:job_level)
        corporation_name =contenido.try{|x| x['cpNombre']}.gsub('/','-')
      end
    when 'Person'
      aux =  Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_i : contenido.try{|x| x['dperNumper']})
      if aux.blank?
        aux =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
              "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
      end
      person_type = @notification.try(:action_abrev)

      case person_type
      when 'V'
        job_level = aux.try(:spokespeople).first
      when 'E'
        job_level = aux.try(:temporary_workers).first
      when 'R'
        job_level = aux.try(:directors).first
      when 'F'
        job_level = aux.try(:public_workers).first
      when 'C'
        job_level = aux.try(:councillors).first
        corporation_name = contenido.try{|x| x['concejal']}['corporacion'].try{|x| x['cpNombre'].gsub('/','-')}
      end
    end

    if job_level.blank?
      redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}#{"_historic" if @notification.historic?}_admin_notifications_path"), alert: I18n.t('notifications.error_publicable')
    else
      begin
        logger.error(person_path(job_level,corporation: corporation_name))
        rescue
        end
      redirect_to person_path(job_level,corporation: corporation_name)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00014: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to send("#{Notification.data_redirect[@notification.try(:type_data)]}#{"_historic" if @notification.historic?}_admin_notifications_path"), alert: I18n.t('notifications.error_publicable')
  end

  def forward
    ws = INPERWS.new
    notification = Notification.find_by(id: params[:id])
    if !notification.blank? && notification.try(:no_publish_reason).to_s.include?("No se ha realizado el feedback")
      case notification.try(:type_data)
      when 'Profile'
        begin
            if notification.permit
                notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / "
                inper_response = ws.setWSPerfilesPublicado(notification.try(:content_data)['dpfId'], notification.change_state_at, 0,
                                                            "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}")
            else
                inper_response = ws.setWSPerfilesPublicado(notification.try(:content_data)['dpfId'],notification.change_state_at,1,notification.no_publish_reason)
                notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / Razón: #{notification.no_publish_reason} / "
            end
            begin
                respuesta= JSON.parse(inper_response)
                if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                    notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                    notification.motive = ""
                    notification.send_feedback = true
                else
                    notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                    notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                end
            rescue => e
                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                notification.motive = e.message
            end
        rescue => e
            if notification.permit
                notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / No se ha realizado el feedback"
            else
                notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
            end
            notification.motive = e.message
            begin
              logger.error("ERROR: #{e}")
            rescue
            end
        end
      when 'AssetsDeclaration'
          begin
              if notification.permit
                  notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / "
                  inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decg_id"],notification.change_state_at,0,
                                                                "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}").to_s
              else
                  inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)['decg_id'],notification.change_state_at,1,notification.no_publish_reason)
                  notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} /  Razón: #{notification.no_publish_reason} / "
              end
              begin
                  respuesta= JSON.parse(inper_response)
                  if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                      notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                      notification.motive = ""
                      notification.send_feedback = true
                  else
                      notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                      notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                  end
              rescue => e
                  notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                  notification.motive = e.message
              end
          rescue => e
              if notification.permit
                  notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / No se ha realizado el feedback"
              else
                  notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
              end
              notification.motive = e.message
              begin
                logger.error("ERROR: #{e}")
              rescue
              end
          end
      when 'ActivitiesDeclaration'
          begin
              if notification.permit
                  notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / "
                  inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decgId"],notification.change_state_at,0,
                                                                "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}").to_s
              else
                  inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)['decgId'],notification.change_state_at,1,notification.no_publish_reason)
                  notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} /  Razón: #{notification.no_publish_reason} / "
              end
              begin
                  respuesta= JSON.parse(inper_response)
                  if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                      notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                      notification.motive = ""
                      notification.send_feedback = true
                  else
                      notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                      notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                  end
              rescue => e
                  notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                  notification.motive = e.message
              end
          rescue => e
              if notification.permit
                  notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / No se ha realizado el feedback"
              else
                  notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
              end
              notification.motive = e.message
              begin
                logger.error("ERROR: #{e}")
              rescue
              end
          end
      end
      if !notification.send_feedback
          notification.forwards = notification.forwards + 1
      end

      notification.save

      begin
        umbral = ::DatePreference.find_by(code: "notification_pending_alert").try(:content_data).to_i
      rescue
          umbral=nil
      end

      if !umbral.blank? && umbral <= notification.forwards.to_i
        begin
          Mailer.notification_alerts(notification).deliver_now
        rescue => e
          begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
      end
    end

    case notification.try(:type_data)
    when 'Profile'
      redirect_to profiles_historic_admin_notifications_path
    when 'AssetsDeclaration'
      redirect_to assets_historic_admin_notifications_path
    when 'ActivitiesDeclaration'
      redirect_to activities_historic_admin_notifications_path
    when 'Person'
      redirect_to person_historic_admin_notifications_path
    else
      redirect_to profiles_historic_admin_notifications_path
    end
  rescue => e
    begin
      Rails.logger.error("COD-00015: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    flash[:error]= "No se ha podido realizar el proceso"
    case notification.try(:type_data)
    when 'Profile'
      redirect_to profiles_historic_admin_notifications_path
    when 'AssetsDeclaration'
      redirect_to assets_historic_admin_notifications_path
    when 'ActivitiesDeclaration'
      redirect_to activities_historic_admin_notifications_path
    when 'Person'
      redirect_to person_historic_admin_notifications_path
    else
      redirect_to profiles_historic_admin_notifications_path
    end
  end

  private

  def setNotifications(x, type)
    aux_params=params
    case x
    when 'Profile'
      @path_content= type.to_s == 'pending' ? profiles_admin_notifications_path : profiles_historic_admin_notifications_path
      @field_traduction = 'update_profile'
    when 'AssetsDeclaration'
      @path_content= type.to_s == 'pending' ? assets_admin_notifications_path : assets_historic_admin_notifications_path
      @field_traduction = 'declaration_date'
    when 'ActivitiesDeclaration'
      @path_content= type.to_s == 'pending' ? activities_admin_notifications_path : activities_historic_admin_notifications_path
      @field_traduction = 'declaration_date'
    when 'Person'
      @path_content= type.to_s == 'pending' ? person_admin_notifications_path : person_historic_admin_notifications_path
      @field_traduction = 'update_profile'
    end

    if session["search_params_not_#{x}_#{type.to_s}".to_sym].blank? && aux_params.has_key?(:name) || aux_params.has_key?(:name)
     session["search_params_not_#{x}_#{type.to_s}".to_sym] = aux_params
    elsif !session["search_params_not_#{x}_#{type.to_s}".to_sym].blank? && !aux_params.has_key?(:name)
      aux_params = session["search_params_not_#{x}_#{type.to_s}".to_sym]
    end

    if type.to_s == 'pending'
      if aux_params[:sort].blank?
        @notifications = Notification.pending.with_type_data(x).order(created_at: :desc)
      elsif !(["created_at","change_state_at","no_publish_reason","type_action","administrator"].include? aux_params[:sort].to_s )
        @notifications = Notification.pending.with_type_data(x)
      else
        @notifications = Notification.pending.with_type_data(x).order("#{aux_params[:sort].to_s} #{aux_params[:direction].to_s}")
      end
    else
      if aux_params[:sort].blank?
        @notifications = Notification.historic.with_type_data(x).order(change_state_at: :desc)
      elsif !(["created_at","change_state_at","no_publish_reason"].include? aux_params[:sort].to_s )
        @notifications = Notification.historic.with_type_data(x)
      else
        @notifications = Notification.historic.with_type_data(x).order("#{aux_params[:sort].to_s} #{aux_params[:direction].to_s}")
      end
    end

    search(session["search_params_not_#{x}_#{type.to_s}".to_sym] || aux_params, x, type)
  end

  def search(parameters, x, type)
    parameters ||= {}
    parameters = parameters.transform_keys(&:to_sym)
    @section_params = parameters
    @sort = parameters[:sort].blank? ? nil : parameters[:sort]
    @direction = parameters[:direction].blank? ? nil : parameters[:direction]

    if params[:format].to_s != 'xls' && params[:export_all].blank? ||  params[:export_all].to_s == "false"
      if !(["created_at","change_state_at","no_publish_reason"].include? @sort.to_s)
        if @sort.to_s == "updated_at" ||  @sort.to_s == "declaration_date" || @sort.to_s == "notification_type_date"
          sorted = @notifications.sort { |x, y| (x.content_data_hash_formated(@sort.try{|z| z.to_sym}).blank? ? Date.parse("01/01/1700") : Date.parse(x.content_data_hash_formated(@sort.try{|z| z.to_sym}))) <=>
          (y.content_data_hash_formated(@sort.try{|z| z.to_sym}).blank? ? Date.parse("01/01/1700") : Date.parse(y.content_data_hash_formated(@sort.try{|z| z.to_sym}))) }
        elsif @sort.to_s == "personal_code" || @sort.to_s == "person_identificator"
          sorted = @notifications.sort { |x, y| x.content_data_hash_formated(@sort.try{|y| y.to_sym}).to_i <=> y.content_data_hash_formated(@sort.try{|y| y.to_sym}).to_i }
        elsif @sort.to_s == "type_action"
          sorted = @notifications.sort_by { |x| x.type_action }
        elsif @sort.to_s == "administrator"
          sorted = @notifications.sort_by { |x| x.administrator }
        else
          sorted = @notifications.sort { |x, y| x.content_data_hash_formated(@sort.try{|y| y.to_sym}) <=> y.content_data_hash_formated(@sort.try{|y| y.to_sym}) }
        end
        @notifications = @direction.blank? || @direction.to_s == 'asc' ? sorted : sorted.reverse
      end

      if type.to_s == 'historic'
        if params[:archived].blank? || params[:archived] == "manually_reviewed"
          @notifications = @notifications.select { |n| n.administrator_id != nil }
        elsif params[:archived] == "auto_reviewed"
          @notifications = @notifications.select { |n| n.administrator_id == nil }
        end
      end

      if !parameters[:name].blank?
        aux = ['Profile', 'Person'].include?(x) ? :name : :person_name
        @notifications = @notifications.select {|n| I18n.transliterate(n.content_data_hash_formated(aux)).try(:downcase).include?(I18n.transliterate(parameters[:name].strip).try(:downcase))}
      end
      if !parameters[:last_name].blank?
        aux = ['Profile', 'Person'].include?(x) ? :last_name : :person_last_name
        @notifications = @notifications.select {|n| I18n.transliterate(n.content_data_hash_formated(aux)).try(:downcase).include?(I18n.transliterate(parameters[:last_name].strip).try(:downcase))}
      end
      if !parameters[:type_person].blank?
        aux = x == 'Person' ? :type_job : :person_type
        @notifications = @notifications.select {|n|  n.content_data_hash_formated(aux).to_s == I18n.translate("person_abrev.#{parameters[:type_person].to_s}")}
      end

      if type.to_s == 'pending' && (!parameters[:not_start_date].blank? || !parameters[:not_end_date].blank?)
        if !parameters[:not_start_date].blank? && !parameters[:not_end_date].blank?
          if date_from_less_than_date_to?(start_date: parameters[:not_start_date], end_date: parameters[:not_end_date], filter: "not", param_name: "start_date_less_than_end_date")
            @notifications = @notifications.select {|n| n.created_at.to_date >= parameters[:not_start_date].to_date && n.created_at.to_date <= parameters[:not_end_date].to_date}
          end
        elsif !parameters[:not_start_date].blank? && parameters[:not_end_date].blank?
          if date_format_valid?(date: parameters[:not_start_date], filter: "not", param_name: "start_date")
            @notifications = @notifications.select {|n| n.created_at.to_date >= parameters[:not_start_date].to_date}
          end
        else
          if date_format_valid?(date: parameters[:not_end_date], filter: "not", param_name: "end_date")
            @notifications = @notifications.select {|n| n.created_at.to_date <= parameters[:not_end_date].to_date}
          end
        end
      end

      if !parameters[:personal_code].blank?
        aux = ['Person','Profile'].include?(x) ? :personal_code : :person_identificator
        @notifications = @notifications.select {|n|  n.content_data_hash_formated(aux).to_s.include?(I18n.transliterate(parameters[:personal_code].strip))}
      end

      if !parameters[:update_start_date].blank? || !parameters[:update_end_date].blank?
        aux= x == 'Profile' ? :updated_at : x== 'Person' ? :notification_type_date : :declaration_date

        if !parameters[:update_start_date].blank? && !parameters[:update_end_date].blank?
          if date_from_less_than_date_to?(start_date: parameters[:update_start_date], end_date: parameters[:update_end_date], filter: "update", param_name: "start_date_less_than_end_date")
            @notifications = @notifications.select {|n| (!n.content_data_hash_formated(aux).blank? ? Date.parse(n.content_data_hash_formated(aux).to_s) : Date.parse("01/01/1700"))  >= parameters[:update_start_date].to_date && (!n.content_data_hash_formated(aux).blank? ? Date.parse(n.content_data_hash_formated(aux).to_s) : Date.parse("01/01/1700")) <= parameters[:update_end_date].to_date}
          end
        elsif !parameters[:update_start_date].blank? && parameters[:update_end_date].blank?
          if date_format_valid?(date: parameters[:update_start_date], filter: "update", param_name: "start_date")
            @notifications = @notifications.select {|n| (!n.content_data_hash_formated(aux).blank? ? Date.parse(n.content_data_hash_formated(aux).to_s) : Date.parse("01/01/1700") ) >= parameters[:update_start_date].to_date}
          end
        else
          if date_format_valid?(date: parameters[:update_end_date], filter: "update", param_name: "end_date")
            @notifications = @notifications.select {|n| (n.content_data_hash_formated(aux).blank? ? Date.parse("01/01/1700") : Date.parse(n.content_data_hash_formated(aux).to_s)) <= parameters[:update_end_date].to_date}
          end
        end
      end

      if type.to_s == 'historic' && (!parameters[:process_start_date].blank? || !parameters[:process_end_date].blank?)
        if !parameters[:process_start_date].blank? && !parameters[:process_end_date].blank?
          if date_from_less_than_date_to?(start_date: parameters[:process_start_date], end_date: parameters[:process_end_date], filter: "process", param_name: "start_date_less_than_end_date")
            @notifications = @notifications.select {|n| n.change_state_at.to_date >= parameters[:process_start_date].to_date && n.change_state_at.to_date <= parameters[:process_end_date].to_date}
          end
        elsif !parameters[:process_start_date].blank? && parameters[:process_end_date].blank?
          if date_format_valid?(date: parameters[:process_start_date], filter: "process", param_name: "start_date")
            @notifications = @notifications.select {|n| n.change_state_at.to_date >= parameters[:process_start_date].to_date}
          end
        elsif !parameters[:process_end_date].blank?
          if date_format_valid?(date: parameters[:process_end_date], filter: "process", param_name: "end_date")
            @notifications = @notifications.select {|n| n.change_state_at.to_date <= parameters[:process_end_date].to_date}
          end
        end
      end
    end
    if params[:format].to_s != 'xls' || params[:format].blank? || parameters.blank?
      @notifications = Kaminari.paginate_array(@notifications).page(parameters[:page] || 1).per(30)
    end

    respond_to do |format|
      format.csv { stream_csv_report(type) }
      format.xls { stream_xls_report(type)  }
      format.html
    end
  end

  def loadNotification
    @notification = Notification.find(params[:id])
  end

  def loadTypePerson
    @person_types ||= {
      I18n.t('person_abrev.C') => 'C',
      I18n.t('person_abrev.R') => 'R',
      I18n.t('person_abrev.F') => 'F',
      I18n.t('person_abrev.E') => 'E',
      I18n.t('person_abrev.V') => 'V' }
  end

  def stream_csv_report(type)
    stream_file("notificaciones", "csv") do |stream|
      Notification.stream_query_rows(@notifications, params, type) do |row_from_db|
        stream.write row_from_db
      end
    end
  end

  def stream_xls_report(type)
    buffer = StringIO.new
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet
    sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    # if params[:export_all].to_s == "false"
    #   sheet.row(1).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    #   sheet.row(4).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    # end

    index = 0
    Notification.stream_query_rows(@notifications, params, type) do |row_from_db|
      sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
      index += 1
    end

    book.write(buffer)
    buffer.rewind
    send_data buffer.read
  end

  def mailers_test
    ["rupertomj@madrid.es", "aguerozmf@madrid.es", "ruizmmc@madrid.es", "ajmorenoh@minsait.com"]
  end
  def date_format_valid?(date:, filter:, param_name:)
    valid_date = valid_date?(date: date)
      unless valid_date
        flash.now[:alert] = t("filter_notifications.errors.#{filter}.#{param_name}")
      end
    valid_date
  end

  def date_from_less_than_date_to?(start_date:, end_date:, filter:, param_name:)
    date_from_less_than_date_to = Date.parse(start_date) <= Date.parse(end_date)
    unless date_from_less_than_date_to
      flash.now[:alert] = t("filter_notifications.errors.#{filter}.#{param_name}")
    end
    date_from_less_than_date_to
  end

  def valid_date?(date:)
    Date.strptime(date.match(/^\d{4}-\d{2}-\d{2}$/).to_s, '%Y-%m-%d') rescue false
  end

end
