require 'stringio'
require 'logger'
include DeclarationsHelper

class Admin::LogsController < Admin::BaseController
    before_action :load_form_data, only: [:new, :create]
    before_action :load_data, only: [:new, :create, :index, :export]

    def new
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def load_form_data
        @corporation_list = Corporation.all.order(name: :asc)
        @periods_list = DeclarationsHelper.periods_list
        @list_jobs = { 
            I18n.t('people.titles.director') => 'director', 
            I18n.t('people.titles.councillor') => 'councillor'
        }.sort.to_h
    end

    def generate_log_email(emails_total=nil,upload=nil,email_log=nil)
        if upload.blank? || email_log.blank? || emails_total.blank?
            email_log=nil
        else
            emails_total= JSON.parse(emails_total)
            emails= ""
            logger=""
            enviados=0
            emails_total = emails_total.uniq
            emails_total.each do |email|
                identificador = email[2].blank? ? Person.joins(:profile).find_by("profiles.email=?", email[0]).try(:personal_code) : email[2]
                identificador = "-" if identificador.blank?
                
                begin
                    case email_log.type.to_s
                    when "ProfilesEmailLog"
                        Mailer.profile_upload(email[1],email[0]).deliver_now
                    when "AssetsEmailLog"
                        Mailer.assets_upload(email[1],email[0]).deliver_now
                    when "ActivitiesEmailLog"
                        Mailer.activities_upload(email[1],email[0]).deliver_now
                    else
                    end
                    emails=emails + email[0]+ "(#{I18n.t("person.titles.#{email[1]}")})" + "; "
                    logger=logger+I18n.t("logs.data.info",email: email[0]+ "(#{I18n.t("person.titles.#{email[1]}")})", identificador: identificador)
                    enviados = enviados + 1
                rescue
                    logger=logger+I18n.t("logs.data.error",email: email[0]+ "(#{I18n.t("person.titles.#{email[1]}")})", identificador: identificador)
                end
            end
            logger=logger+I18n.t("logs.data.result",enviados: enviados, total: emails_total.count-enviados)
            successful = enviados != 0 

            email_log.assign_attributes(
                log: I18n.t('logs.data_log', 
                    log: logger, 
                    created_at: Time.zone.now, 
                    author_email: current_administrator.try(:email), 
                    emails: emails,
                    type_email: I18n.t("logs.profiles"),
                    translated_successful: I18n.t("shared.#{successful}")),
                emails: nil,
                successful: successful,
                log_id: upload.try(:id),
                administrator: current_administrator,
                created_at: Time.zone.now
            )
        end
    end


end