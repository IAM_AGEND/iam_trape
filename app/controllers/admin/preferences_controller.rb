class Admin::PreferencesController < Admin::BaseController
    def index
        @text_contents = TextContent.all.sort_by {|text| TextContent.human_attribute_name(text.section)}
        @data_preferences = DataPreference.all.sort_by {|data| data.title}
        @manage_emails = ManageEmail.all.sort_by {|mail| I18n.t("manage_email.types.#{mail.type_data}")}
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end