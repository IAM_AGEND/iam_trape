require 'excel_importers/profile'

class Admin::ProfileUploadsController < Admin::LogsController
  include ExcelImporters

  def index
    @profile_uploads = ProfileUpload.includes(:administrator).all.order(created_at: :desc).page(params[:page]).preload(:administrator)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    attrs = profile_upload_params.merge(author: current_administrator,
                                        check_for_file: true)

    if validate_params(attrs)
      StringIO.open do |strio|
        logger = ExcelImporters::Base.new_default_logger(strio)
        importer = ExcelImporters::ProfileImport.new( 
          attrs[:file].tempfile,
          header_field: 'Fecha',
          corporation: attrs[:corporation],
          logger: logger,
          author: current_administrator)
                                               
        successful = true
        if importer.import
          flash[:notice] = t('admin.profile_uploads.create.no_errors')
        else
          successful = false
          flash[:alert] = t('admin.profile_uploads.create.errors')
        end

        logger.close

        @profile_upload.assign_attributes(
          log: I18n.t('admin.profile_uploads.logs', 
            log: strio.string, 
            created_at: Time.zone.now, 
            author_email: current_administrator.try(:email), 
            original_filename: attrs[:file].try(:original_filename),
            file_format: importer.file_format,
            translated_successful: I18n.t("shared.#{successful}")),
          emails: importer.emails,
          successful: successful,
          administrator: attrs[:author],
          created_at: Time.zone.now
        )
      end

      if @profile_upload.save
        redirect_to admin_profile_upload_path(@profile_upload)
      else
        render :new
      end
    else
      render :new
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @profile_upload= ProfileUpload.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show_mailer
    @profile_mailer = ProfilesEmailLog.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def send_mailer
    profile_upload = ProfileUpload.find(params[:id])
    profiles_email = ProfilesEmailLog.new
    
    generate_log_email(params[:emails],profile_upload,profiles_email)
    
    if !profiles_email.blank? && profiles_email.save
      redirect_to show_mailer_admin_profile_upload_path(profiles_email), notice: I18n.t('logs.success')
    else
      redirect_to admin_profile_upload_path(profile_upload, section_m: params[:section_m]), alert: I18n.t('logs.error')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def profile_upload_params
      params.require(:profile_upload).permit(:file,:corporation)
    end

    def load_data
      @profile_upload = ProfileUpload.new
    end

    def validate_params(attrs)
      errors=false
      if attrs[:file].blank? || !attrs.has_key?(:file)
        @profile_upload.errors.add(:file, I18n.t('admin.profile_uploads.errors.blank'))
        errors = true 
      end
      !errors
    end
end
