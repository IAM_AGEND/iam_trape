class Admin::DataPreferencesController < Admin::BaseController
  def update_all
    returned_success=true
    data_preference_params[:info].each do |k,v|
      preference = DataPreference.find(v[:id])
      unless preference.blank?
        preference.content_data = v[:content_data]
        unless preference.save
          returned_success=false
          break
        end
      end
    end
   
    if returned_success
      `whenever -i cellar`
      redirect_to admin_preferences_path(generic: true), notice: I18n.t('data_preference.flash.success')
    else
      redirect_to admin_preferences_path(generic: true), alert: I18n.t('data_preference.flash.error')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end


  private
    def data_preference_params
      params.require(:datas).permit({
        info: [
          :id,
          :content_data
        ]
      })
    end
end
