class Admin::ManageEmailsController < Admin::BaseController
  before_action :set_manage_email, only: [:edit, :update]

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
  
  def update
    if @manage_email.update(manage_email_params)
      redirect_to admin_preferences_path, notice: I18n.t('manage_emails.success')
    else
      render :edit
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end  

  private
   
    def set_manage_email
      @manage_email = ManageEmail.find(params[:id])
    end

    def params_c
      [:councillors,:directors,:public_workers,:temporary_workers,:spokespeople]
    end

    def manage_email_params
      params.require(:manage_email).permit(:sender, :subject, :email_body, fields_cc: params_c, fields_cco: params_c)
    end
end
