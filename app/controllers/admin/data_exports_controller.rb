class Admin::DataExportsController < Admin::BaseController
    respond_to :html

    def index
        preference = DataPreference.find_by(:code => "councillors_publication_year")
        date_years = preference.blank? ? 4 : preference.type_data.to_i==0 ? preference.content_data.to_i : 4      
        @corporations = Corporation.where("end_corporation_date >= ?", Time.zone.now - date_years.years).order(name: :desc)
       
        charge_data
        
        respond_to do |format|
            format.html
            format.xls { download_xls  }
        end
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def download_xls
        aux = {}
        aux.merge!({corporation: params[:corporation]}) if !params[:corporation].blank?
        if params[:type].to_s == "directors"
            aux.merge!({directors: true}) 
        else
            aux.merge!({councillors: true}) 
        end
        params.merge!(aux)

        if File.exist?("#{Rails.root}/public/export/#{params[:url]}")
            flash[:notice] = I18n.t('data_exports.success_download')
            send_file("#{Rails.root}/public/export/#{params[:url]}")
        else
            flash[:alert] = I18n.t('data_exports.error_download')
            redirect_to admin_data_exports_path(aux)
        end
    end

    def charge_data
        @exports = {directors: [], councillors: []}
       
        ["Inicial", "Final", "Anual"].each do |p|
            if p == "Anual"
                periods = {}
                AssetsDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Anual'").find_each do |dec|
                  periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
                end
                periods.each do |k,v|
                    @exports[:directors].push({period: "#{p} #{k}", corporation: nil, content: [{path: "assets/Bienes#{p}_Directivos_#{k}.xls", type: "assets"}]})
                end

                periods = {}
                ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Anual'").find_each do |dec|
                  periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
                end
                periods.each do |k,v|
                    if  @exports[:directors].find {|x| x[:period] == "#{p} #{k}"}.blank?
                        @exports[:directors].push({period: "#{p} #{k}",corporation: nil, content: [{path: "activities/Actividades#{p}_Directivos_#{k}.xls", type: "activities"}]})
                    else
                        @exports[:directors].find {|x| x[:period] == "#{p} #{k}" && x[:corporation] == nil}[:content].push({path: "activities/Actividades#{p}_Directivos_#{k}.xls", type: "activities"})
                    end
                end
            else
                @exports[:directors].push({period: p, corporation: nil,content: [{path: "assets/Bienes#{p}_Directivos.xls", type: "assets"}, {path: "activities/Actividades#{p}_Directivos.xls", type: "activities"}]})
            end

            @corporations.each do |corp|
                if p == "Anual"
                    periods = {}
                    AssetsDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id = #{corp.id}").where("temporalities.name = 'Anual'").find_each do |dec|
                        periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
                    end
                
                    periods.each do |k,v|
                        @exports[:councillors].push({period: "#{p} #{k}", corporation: corp.id, content: [{path: "assets/Bienes#{p}_Concejales_#{corp.name}_#{k}.xls", type: "assets"}]})
                    end

                    periods = {}
                    ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id = #{corp.id}").where("temporalities.name = 'Anual'").find_each do |dec|
                        periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
                    end
                
                    periods.each do |k,v|
                        if  @exports[:councillors].find {|x| x[:period] == "#{p} #{k}"}.blank?
                            @exports[:councillors].push({period: "#{p} #{k}",corporation: corp.id, content: [{path: "activities/Actividades#{p}_Concejales_#{corp.name}_#{k}.xls", type: "activities"}]})
                        else
                            @exports[:councillors].find {|x| x[:period] == "#{p} #{k}" && x[:corporation] == corp.id}[:content].push({path: "activities/Actividades#{p}_Concejales_#{corp.name}_#{k}.xls", type: "activities"})

                        end
                    end
                else
                    @exports[:councillors].push({period: p, corporation: corp.id,content: [ {path: "assets/Bienes#{p}_Concejales_#{corp.name}.xls", type: "assets"}, {path: "activities/Actividades#{p}_Concejales_#{corp.name}.xls", type: "activities"}]})
                end
            end
        end
    end
end