class Admin::TextContentsController < Admin::BaseController
  before_action :set_text_content, only: [:edit, :update]

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update
    if @text_content.update(text_content_params)
      redirect_to admin_preferences_path(text_content: true), notice: I18n.t('text_contents.success')
    else
      render :edit 
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def set_text_content
      @text_content = TextContent.find(params[:id])
    end

    def text_content_params
      params.require(:text_content).permit(:id, :section, :content)
    end
end
