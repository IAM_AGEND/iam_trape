class Admin::StatisticsController < Admin::BaseController
  respond_to :html, :js, :json
  require 'csv'
  require 'stringio'
  require 'duties_importer'

  before_action :load_profiles_data, only: :profiles
  before_action :load_councillors_data, only: :councillors
  before_action :load_directors_data, only: :directors

  def profiles
    respond_to do |format|
      format.html
      format.csv { to_csv(params) }
      format.xls { to_xls(params) }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def councillors
    respond_to do |format|
      format.html
      format.csv { to_csv(params) }
      format.xls { to_xls(params) }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def directors
    respond_to do |format|
      format.html
      format.csv { to_csv(params) }
      format.xls { to_xls(params) }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update_data
    Profile.where(cumplimented: false).each {|p|  p.update_attributes(cumplimented: p.has_profile?) }
    DutiesImporter.new().import

    if params[:type].blank?
      redirect_to profiles_admin_statistics_path, notice: I18n.t('statistics.success_update')
    else
      case params[:type].to_s
      when 'profiles'
        redirect_to profiles_admin_statistics_path, notice: I18n.t('statistics.success_update')
      when 'directors'
        redirect_to directors_admin_statistics_path, notice: I18n.t('statistics.success_update')
      when 'councillors'
        redirect_to councillors_admin_statistics_path, notice: I18n.t('statistics.success_update')
      end
    end
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    if params[:type].blank?
      redirect_to profiles_admin_statistics_path, alert: I18n.t('statistics.error_update')
    else
      case params[:type].to_s
      when 'profiles'
        redirect_to profiles_admin_statistics_path, alert: I18n.t('statistics.error_update')
      when 'directors'
        redirect_to directors_admin_statistics_path, alert: I18n.t('statistics.error_update')
      when 'councillors'
        redirect_to councillors_admin_statistics_path, alert: I18n.t('statistics.error_update')
      end
    end
  end

  private

  def getFileName(parametros)
    export_name = ''
    case parametros[:section]
    when 'profiles'
      data = Admin::StatisticsHelper.file_name
      export_name = data[parametros[:section]][parametros[:person]][parametros[:part]]
    when 'directors'
      year = !parametros[:temporality].blank? && parametros[:temporality].include?("Anual") ? parametros[:temporality].split(" ")[1] : nil
      data = Admin::StatisticsHelper.file_name(year)

      active = parametros[:active].blank? ? 'inactive' : 'active'
      temporality = parametros[:declaration] == 'Total' ? 'total' : year.blank? && !parametros[:temporality].downcase.strip.blank? ? parametros[:temporality].downcase.strip : 'anual'
      declaration = parametros[:declaration] == "ActivitiesDeclaration" ? 'activity' : parametros[:declaration] == 'Total' ? 'total' : 'asset'
      export_name = data[parametros[:section]][active][temporality][declaration][parametros[:part]]
    when 'corporation'
      year = !parametros[:temporality].blank? && parametros[:temporality].include?("Anual") ? parametros[:temporality].split(" ")[1] : nil
      corporation_name=Corporation.find(params[:corporation]).name.gsub('-','_')
      data = Admin::StatisticsHelper.file_name(year,corporation_name)

      active = parametros[:active].blank? ? 'inactive' : 'active'
      temporality = parametros[:declaration] == 'Total' ? 'total' : year.blank? && !parametros[:temporality].downcase.strip.blank? ? parametros[:temporality].downcase.strip : 'anual'
      declaration = parametros[:declaration] == "ActivitiesDeclaration" ? 'activity' : parametros[:declaration] == 'Total' ? 'total' : 'asset'
      export_name = data[parametros[:section]][active][temporality][declaration][parametros[:part]]
    end

    export_name
  end

  def to_csv(parametros)
    name = parametros[:editable].blank? || !parametros[:editable] ? getFileName(parametros) : "#{getFileName(parametros)}_modificación".gsub(/_act_|_ina_/, "_")

    stream_file(name, "csv") do |stream|
      case parametros[:section]
      when 'profiles'
        JobLevel.stream_query_rows_statistics(parametros[:section], parametros[:person], parametros[:part]) do |row_from_db|
          stream.write row_from_db
        end
      when 'directors'
        JobLevel.stream_query_rows_directors(parametros[:active], parametros[:declaration], parametros[:temporality], parametros[:part],  parametros[:editable]) do |row_from_db|
          stream.write row_from_db
        end
      when 'corporation'
        JobLevel.stream_query_rows_councillors(parametros[:active], parametros[:declaration], parametros[:temporality], parametros[:part], parametros[:corporation],  parametros[:editable]) do |row_from_db|
          stream.write row_from_db
        end
      end
    end
  end

  def to_xls(parametros)
    buffer = StringIO.new
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet
    sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    name = parametros[:editable].blank? || !parametros[:editable] ? getFileName(parametros) : "#{getFileName(parametros)}_modificación".gsub(/_act_|_ina_/, "_")
    index = 0
    case parametros[:section]
    when 'profiles'
      JobLevel.stream_query_rows_statistics(parametros[:section], parametros[:person], parametros[:part]) do |row_from_db|
        sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
        index += 1
      end
    when 'directors'
      JobLevel.stream_query_rows_directors(parametros[:active], parametros[:declaration], parametros[:temporality], parametros[:part], parametros[:editable]) do |row_from_db|
        sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
        index += 1
      end
    when 'corporation'
      JobLevel.stream_query_rows_councillors(parametros[:active], parametros[:declaration], parametros[:temporality], parametros[:part], parametros[:corporation], parametros[:editable]) do |row_from_db|
        sheet.row(index).concat row_from_db.b.encode!("UTF-8","ISO-8859-1").gsub("\n",'').split(/;/)
        index += 1
      end
    end

    book.write(buffer)
    buffer.rewind
    send_data buffer.read, :filename => "#{name}.xls", :type =>  "application/vnd.ms-excel"
  end

  def load_profiles_data
    @profiles_count = []
    ['councillor', 'director', 'public_worker', 'temporary_worker', 'spokesperson', 'total'].each do |type|
      @profiles_count.push(type: type, all: 0, profile: 0, unprofile: 0)
    end

    begin
      councillors = JobLevel.councillors_working_without.uniq.count
      councillors_profile = JobLevel.councillors_working_without_profile.uniq.count
      @profiles_count.select {|x| x[:type] == 'councillor'}[0][:all] = councillors
      @profiles_count.select {|x| x[:type] == 'councillor'}[0][:profile] = councillors_profile
      @profiles_count.select {|x| x[:type] == 'councillor'}[0][:unprofile] = councillors - councillors_profile

      JobLevel.working.uniq.group_by { |t| t.person_type }.each do |job|
        if !@profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}.blank?
          @profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}[0][:all] = job[1].count
        end
      end

      JobLevel.working_profile_others.uniq.group_by { |t| t.person_type }.each do |job|
        if !@profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}.blank?
          @profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}[0][:profile] = job[1].count
          @profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}[0][:unprofile] = @profiles_count.select {|x| job[0]!= 'councillor' && x[:type] == job[0]}[0][:all] - job[1].count
        end
      end

      @profiles_count.select {|x| x[:type] != 'total'}.each do |count|
        [:all, :profile, :unprofile].each do |ite|
          @profiles_count.select {|x| x[:type] == 'total'}[0][ite] =  @profiles_count.select {|x| x[:type] == 'total'}[0][ite] + count[ite]
        end
      end
    rescue => e
    end
  end

  def load_directors_data
    @data_directors_active = []
    @data_directors_inactive = []
    @data_directors_editable = []

    Duty.joins(:temporality).where("duties.person_level_type ='JobLevel' and temporalities.id = duties.temporality_id and (cast(duties.year as integer) >= 2017 or duties.year is null or duties.year ='')").sort_for_temporality.each do |d|
      if !d.declaration.blank? && d.declaration.editable
        if @data_directors_editable.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
          @data_directors_editable.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, editable:   1, all: 1})
        else
          @data_directors_editable.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:editable] = @data_directors_editable.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:editable] +1
          @data_directors_editable.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_directors_editable.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
        end
      elsif d.person_level.try(:appointment).try(:end_date).blank? || d.person_level.try(:appointment).try(:end_date).try(:to_date) > Date.today
          if @data_directors_active.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
            @data_directors_active.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, with:  d.declaration.blank? ? 0 : 1, without:  d.declaration.blank? ? 1 : 0, all: 1})
          else
            @data_directors_active.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] = @data_directors_active.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] +1
            @data_directors_active.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_directors_active.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
          end
      else
        if @data_directors_inactive.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
          @data_directors_inactive.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, with:  d.declaration.blank? ? 0 : 1, without:  d.declaration.blank? ? 1 : 0, all: 1})
        else
          @data_directors_inactive.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] = @data_directors_inactive.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] +1
          @data_directors_inactive.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_directors_inactive.select {|x| x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
        end
      end
    end
  rescue
    @data_directors_active = []
    @data_directors_inactive = []
    @data_directors_editable = []
  end

  def load_councillors_data
    @data_councillors_active = []
    @data_councillors_inactive = []
    @data_councillors_editable = []

    Corporation.all.each do |c|
      Duty.joins(:temporality).where("duties.person_level_type ='CouncillorsCorporation' and duties.person_level_id in (?) and temporalities.id = duties.temporality_id and (cast(duties.year as integer) >= 2017 or duties.year is null or duties.year ='') ", CouncillorsCorporation.where("corporation_id =?",c.id).select(:id)).sort_for_temporality.each do |d|
        if !d.declaration.blank? && d.declaration.editable
          if @data_councillors_editable.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
            @data_councillors_editable.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, editable:   1, all: 1, corporation: c.id})
          else
            @data_councillors_editable.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:editable] = @data_councillors_editable.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:editable] +1
            @data_councillors_editable.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_councillors_editable.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
          end
        elsif d.person_level.try(:appointment).try(:end_date).blank? || d.person_level.try(:appointment).try(:end_date).try(:to_date) > Date.today
            if @data_councillors_active.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
              @data_councillors_active.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, with:  d.declaration.blank? ? 0 : 1, without:  d.declaration.blank? ? 1 : 0, all: 1,corporation: c.id})
            else
              @data_councillors_active.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] = @data_councillors_active.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] +1
              @data_councillors_active.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_councillors_active.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
            end
        else
          if @data_councillors_inactive.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}.blank?
            @data_councillors_inactive.push({temporality: "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}", declaration: d.type_duty, with:  d.declaration.blank? ? 0 : 1, without:  d.declaration.blank? ? 1 : 0, all: 1,corporation: c.id})
          else
            @data_councillors_inactive.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] = @data_councillors_inactive.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][d.declaration.blank? ? :without : :with] +1
            @data_councillors_inactive.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] = @data_councillors_inactive.select {|x| x[:corporation]==c.id && x[:temporality] == "#{d.temporality.name} #{"- #{d.year}" if !d.year.blank?}" && x[:declaration] == d.type_duty}[0][:all] +1
          end
        end
      end
    end
  rescue
    @data_councillors_active = []
    @data_councillors_inactive = []
    @data_councillors_editable = []
  end
end
