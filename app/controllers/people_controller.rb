class PeopleController < ApplicationController
  before_action :load_person_and_declarations, only: :show
  before_action :load_data_councillors, only: [:councillors,:councillors_not_work]
  before_action :get_calendar, only: [:show]
  before_action :load_data_events, only: :show

  def index
    redirect_to councillors_people_path
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def councillors
    @councillors_working = @councillors.preload(:person,:councillors_corporations => [:appointment,:corporation, :electoral_list]).councillors_working(@corporation_now.try(:id)).grouped_by_electoral_list(@corporation_now.try(:id))
    @text_working = TextContent.get_text_section("actual_concillors")    
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def councillors_not_work
    @councillors_not_work = @councillors.councillors_not_working(@corporation_now.try(:id)).preload(:councillors_corporations => [:appointment,:corporation, :electoral_list])
    @text_not_working = TextContent.get_text_section("actual_concillors_not_working")
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def councillors_other
    @corporations_old = Corporation.inactive
    @councillors = JobLevel.joins(:person).includes(:councillors_corporations => [:appointment,:corporation, :electoral_list]).preload(:person).councillors
    @text_other_corporation= {}
    @corporations_old.each do |corporation|
      @text_other_corporation.merge!({corporation.try(:name) => TextContent.get_text_section("old_corporation_#{corporation.try(:name)}")})
    end
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def directors
    @directors_working = JobLevel.joins(:person).includes(:appointment).preload(:person).directors.working.grouped_by_initial
    @text_working = TextContent.get_text_section("directors")
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def directors_not_work
    @directors_not_working = JobLevel.joins(:person).includes(:appointment).preload(:person).directors.not_working.no_visible_director.grouped_by_initial
    @text_not_working = TextContent.get_text_section("directors_not_working")
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def temporary_workers
    @temporary_workers_working = JobLevel.joins(:person).includes(:appointment).preload(:person).temporary_workers.working.grouped_by_initial
    
    @text_not_data = TextContent.get_text_section("temporary_workers_no_data")
    @text_working = TextContent.get_text_section("temporary_workers")
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def public_workers
    @public_workers_working = JobLevel.joins(:person).includes(:appointment).preload(:person).public_workers.working.grouped_by_initial
    
    @text_not_data = TextContent.get_text_section("public_workers_no_data")
    @text_working = TextContent.get_text_section("public_workers")
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def spokespeople
    @spokespeople_working = JobLevel.joins(:person).includes(:appointment).spokespeople.working.grouped_by_initial
    
    @text_not_data = TextContent.get_text_section("spokespeople_no_data")
    @text_working = TextContent.get_text_section("spokespeople")
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @preference = DataPreference.find_by(:code => "link_information_retribuction")
    @preference_transparency = DataPreference.find_by(:code => "name_link_councillor_contact")
    @preference_retribution = DataPreference.find_by(:code => "name_link_information_retribuction")
    @url_possession_text = DataPreference.find_by(:code => "url_possession_text").try(:content_data)

    if @job_level.director? && !@job_level.director_active?
      redirect_to directors_people_path 
    elsif (@job_level.spokesperson? && @job_level.not_working?)
      redirect_to spokespeople_people_path 
    elsif (@job_level.public_worker? && @job_level.not_working?)
      redirect_to public_workers_people_path 
    elsif (@job_level.temporary_worker? && @job_level.not_working?)
      redirect_to temporary_workers_people_path 
    end
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to root_path, alert: "No es posible acceder a la ruta indicada"
  end

  private

    def load_person_and_declarations
      
      @job_level = JobLevel.friendly.find(params[:id])
      redirect_to root_path if @job_level.blank?
      @person = Person.find(@job_level.person.id)
      @corporation = params[:corporation].blank? ? nil : Corporation.find_by(name: params[:corporation])
      @appointment = @job_level.get_appointment(@corporation)
      @councillor = CouncillorsCorporation.includes(:appointment,:electoral_list,:corporation,:organisms).preload(:appointment,:electoral_list,:corporation,:organisms).find_by(corporation_id: @corporation.try(:id), job_level_id: @job_level.try(:id))
      
      
      @assets_declarations = @job_level.get_assets_declarations(@corporation).try{ |ad| ad.order(:declaration_date)}
      @activities_declarations = @job_level.get_activities_declarations(@corporation).try{ |ad| ad.order(:declaration_date)}

      #authorize_administrators if (!@job_level.not_working?(@corporation) && (!@job_level.councillor? && !@job_level.director?))
    rescue => e
      begin
        Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      redirect_to root_path , error: "No se puede acceder a la página solicitada"
    end

    def full_feature?
      true
    end

    def load_data_councillors
      @corporation_now = Corporation.active.first   
      @councillors = JobLevel.joins(:person).includes(:councillors_corporations => [:appointment,:corporation, :electoral_list]).preload(:person).councillors
    rescue => e
      begin
        Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def load_data_events      
      require 'rest-client'
      require 'json'
      if @person && !@person.personal_code.blank?
        if !params[:select].blank? && !params[:select][:month].blank? && !params[:select][:year].blank?
          start_date = "01-#{params[:select][:month]}-#{params[:select][:year]}"
          end_date = "#{Date.new(params[:select][:year].to_i, params[:select][:month].to_i ,-1).day}-#{params[:select][:month]}-#{params[:select][:year]}"
        else
          start_date = Date.today.beginning_of_month.to_s
          end_date = Date.today.end_of_month.to_s
        end
        response = RestClient.post("#{Rails.application.secrets.agend_url}/ws/agends/#{@person.personal_code}", {api: Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: start_date, end_date: end_date})
       
        if response.code == 200
          @events = JSON.parse(response)
          return @events
        else
          p "error"
        end
      end      
    rescue => e
      begin
        Rails.logger.error("COD-00013: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def get_calendar
      @current_agend_calendar = !params[:set_date_calendar].blank? ? Time.zone.parse(params[:set_date_calendar]).to_date : Time.zone.now.to_date 
      @current_date_calendar = Time.zone.now.to_date

      if !params[:select].blank? && !params[:select][:year].blank? 
        params[:select][:year] = 2019 if params[:select][:year].to_i < 2019
        params[:select][:month] = 6 if params[:select][:year].to_i == 2019 && !params[:select][:month].blank? && params[:select][:month].to_i < 6
        @current_date_calendar = @current_date_calendar.change(year: params[:select][:year].to_i)
      end

      if !params[:select].blank? && !params[:select][:month].blank? 
        @current_date_calendar = @current_date_calendar.change(month: params[:select][:month].to_i)
      end

      if !params[:select_calendar].blank? && !params[:select_calendar][:month].blank? 
        @current_agend_calendar = Date.new(params[:select_calendar][:year].to_i , params[:select_calendar][:month].to_i, -1)
      end
      if !params[:select_calendar].blank? && !params[:select_calendar][:year].blank? 
        @current_agend_calendar = @current_agend_calendar.change(year: params[:select_calendar][:year].to_i)
      end
      if !DataPreference.find_by(title: "show_calendar").try(:content_data).blank? && params[:tab_calendar].blank?
        params.merge!({:tab_calendar => DataPreference.find_by(title: "show_calendar").try(:content_data), :set_date =>  @current_date_calendar.to_s})
      end
    rescue => e
      begin
        Rails.logger.error("COD-00014: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

end
