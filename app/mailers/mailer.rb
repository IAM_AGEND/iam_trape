class Mailer < ApplicationMailer

    def activities_upload(job=nil,email=nil)
        return if email.blank? || job.blank?
        manage_email= ManageEmail.find_by(:type_data => "ActivitiesUpload")
        return if manage_email.blank?
        sender_email(email, job, manage_email)
    end

    def assets_upload(job=nil,email=nil)
        return if email.blank? || job.blank?
        manage_email= ManageEmail.find_by(:type_data => "AssetsUpload")
        return if manage_email.blank?       
        sender_email(email, job, manage_email)
    end

    def profile_upload(job=nil,email=nil)        
        return if email.blank? || job.blank?
        manage_email= ManageEmail.find_by(:type_data => "ProfilesUpload")
        return if manage_email.blank?
        sender_email(email, job, manage_email)
    end

    def notification_alerts(notification)
        emails = DataPreference.find_by(code: "alert_senders").try(:content_data)
        @body_email= "Se ha alcanzado el límite de reintentos de envío de feedback a la aplicación INPER \n para la notificación: #{notification.id}."
        mail(to: emails, from: "transparenciaydatos@madrid.es", subject: "Límite de envío de feedback alcanzado")
    end

    def pending_alerts(type)
        emails = DataPreference.find_by(code: "alert_senders").try(:content_data)
        @body_email= "Se ha alcanzado el umbral de notificaciones de #{type} pendientes."
        mail(to: emails, from: "transparenciaydatos@madrid.es", subject: "Límite de notificaciones pendientes alcanzado")
    end
    
    private

    def sender_email(email, job, manage)
        @body_email=manage.try(:email_body)
        mail(to: email,from: manage.try(:sender), subject: manage.try(:subject), cc: manage.try{|m| m.fields_cc[job]}, bcc: manage.try{|m| m.fields_cco[job]})
    end

    

end
