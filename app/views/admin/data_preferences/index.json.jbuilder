json.array!(@data_preferences) do |data_preference|
  json.extract! data_preference, :id
  json.url data_preference_url(data_preference, format: :json)
end
