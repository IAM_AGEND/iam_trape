App.Toggler =
  initialize: ->
    $('[data-toggle]').on 'click', ->
      $($(this).data('toggle')).toggle()
      if($(this).data('hide')!="")
        $($(this).data('hide')).hide()
      false # needed to avoid scroll up when clicking link

