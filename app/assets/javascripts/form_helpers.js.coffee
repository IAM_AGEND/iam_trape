form_helpers = ->
  $('a[data-add-to]').click ->
    $this = $(this)
    source_selector = $this.data('add-from')
    destination_selector = $this.data('add-to')
    no_repeat = $this.data('no-repeat')

    random_id = Math.floor(Math.random() * (100000 - 100)) + 100;
    source_body = $(source_selector).html().split("{id}").join(random_id)

    if !no_repeat || (no_repeat && $(destination_selector).children().length <=0) 
      $(source_body).appendTo($(destination_selector))
    false

  $(document).on 'click', '[data-delete-parent]', ->
    $this = $(this)
    $this.parents($this.data('delete-parent')).hide()
    $this.parents($this.data('delete-parent')).children()[0].value = true;
    false

  $('.person_portrait').on 'change', (event) ->
    files = event.target.files
    image = files[0]
    reader = new FileReader

    reader.onload = (file) ->
      img = new Image
      img.src = file.target.result
      $('#image_preview').html img
      return

    reader.readAsDataURL image
  $('#electoral_list_logo').on 'change', (event) ->
    files = event.target.files
    image = files[0]
    reader = new FileReader

    reader.onload = (file) ->
      img = new Image
      img.src = file.target.result
      $('#image_preview').html img
      return

    reader.readAsDataURL image
  
  $('#corporation_start_year').on 'change', (event) ->
    $('#corporation_end_year').val(parseInt($('#corporation_start_year').val())+4)
    $('#corporation_name').val(parseInt($('#corporation_start_year').val())+'-'+parseInt($('#corporation_end_year').val()))

 
$(document).ready(form_helpers)
$(document).on('page:load', form_helpers)
