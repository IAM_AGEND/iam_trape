// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require app
//= require jquery
//= require jquery_ujs
//= require foundation
//= require init-foundation
//= require program-accomplishment
//= require turbolinks
//= require trix
//= require linkable
//= require form_helpers
//= require toggler
//= require jquery_nested_form
//= require foundation-datepicker
//= require foundation-datepicker-locales/foundation-datepicker.es.js
//= require tinymce
//= require formatter

$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert', function() {
  
  tinymce.init({
    selector : "textarea:not(.mceNoEditor)",
    menubar: false,
    plugins: "advlist autolink autosave link image lists charmap wordcount media code contextmenu textcolor paste table",
    toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | bullist numlist | outdent indent blockquote | link image media | forecolor | table | subscript superscript | charmap | code",
    language : 'es'
  });

  $('.fdatepicker').fdatepicker({
		format: 'mm/dd/yyyy hh:ii',
		disableDblClickSelection: true,
		language: 'es',
		pickTime: true
	});
});

var initialize_modules = function() {
  App.Toggler.initialize()
}

function clean_search() {
  $('#name').val("");
  $('#last_name').val("");
  $('#full_name').val("");
  $('#area').val("");
  $('#start_date').val("");
  $('#end_date').val("");
  $('#start_incorporate_date').val("");
  $('#end_incorporate_date').val("");
  $('#start_cessation_date').val("");
  $('#end_cessation_date').val("");
  $('#corporation').val("");
  $('#status').val("");
  $('#person_type').val("");
  $('#type_person').val("");
  $('#process_start_date').val("");
  $('#process_end_date').val("");
  $('#not_start_date').val("");
  $('#not_end_date').val("");
  $('#update_end_date').val("");
  $('#update_start_date').val("");
  $('#personal_code').val("");
  $('#archived').val("");
  if (this.form) {
    this.form.submit();
  } else {
    $('#form').submit();
  }
}



function validate_dates(start,end, type){
  if (start.val() != "" && end.val() != "" && start.val() > end.val() && type==0) {
    start.val(end.val());
  } else if(start.val() != "" && end.val() != "" && start.val() > end.val() && type==1) {
    end.val(start.val());
  }
}

$(function(){
  Turbolinks.enableProgressBar()

  $(document).ready(initialize_modules);
  $(document).on('page:load', initialize_modules);
  $(document).on('ajax:complete', initialize_modules);
});

function reload_form(url) {
  if($('#delete_data_delete_single').prop("checked")){
    params="delete_data[delete]="+encodeURI($('#delete_data_delete_single').val());
  }else if($('#delete_data_delete_complete').prop("checked")) {
    params="delete_data[delete]="+encodeURI($('#delete_data_delete_complete').val());
  }

  if($('#delete_data_type_people').val()){
    params=params+"&"+"delete_data[type_people]="+encodeURI($('#delete_data_type_people').val());
  }

  if($('#delete_data_type_information').val()){
    params=params+"&"+"delete_data[type_information]="+encodeURI($('#delete_data_type_information').val());
  }

  if($('#delete_data_people').val()){
    params=params+"&"+"delete_data[people]="+encodeURI($('#delete_data_people').val());
  }

  if($('#delete_data_period').val()){
    params=params+"&"+"delete_data[period]="+encodeURI($('#delete_data_period').val());
  }

  if($('#delete_data_corporation').val()){
    params=params+"&"+"delete_data[corporation]="+encodeURI($('#delete_data_corporation').val());
  }

  $('#recarga').load(url+"admin/delete_data?"+params+" #recarga");

}

function reload_form_export_csv(url) {
  console.log($('#export_type_xls').prop("checked"));
  console.log($('#export_type_csv').prop("checked"));
  /*if($('#export_type_xls').prop("checked")){
    params="export[type]="+encodeURI($('#export_type_xls').val());
  }else if($('#export_type_csv').prop("checked")) {
    params="export[type]="+encodeURI($('#export_type_csv').val());
  }
  $('#recarga').load(url+"admin/statistics?"+params+" #recarga");*/
}

function reload_form_export_directors_csv(url) {
  console.log($('#export_type_xls').prop("checked"));
  console.log($('#export_type_csv').prop("checked"));
  if($('#export_type_xls').prop("checked")){
    params="export[type]="+encodeURI($('#export_type_xls').val());
  }else if($('#export_type_csv').prop("checked")) {
    params="export[type]="+encodeURI($('#export_type_csv').val());
  }
  $('#recarga_directors').load(url+"admin/statistics?"+params+" #recarga_directors");
}



function reload_form_export(url, site) {
  if($('#export_data_export_type_single').prop("checked")){
    params="export_data[export_type]="+encodeURI($('#export_data_export_type_single').val());
  }else if($('#export_data_export_type_complete').prop("checked")) {
    params="export_data[export_type]="+encodeURI($('#export_data_export_type_complete').val());
  }

  if($('#export_data_type_people').val()){
    params=params+"&"+"export_data[type_people]="+encodeURI($('#export_data_type_people').val());
  }

  if($('#export_data_people').val()){
    params=params+"&"+"export_data[people]="+encodeURI($('#export_data_people').val());
  }

  if($('#export_data_period').val()){
    params=params+"&"+"export_data[period]="+encodeURI($('#export_data_period').val());
  }

  if($('#export_data_corporation').val()){
    params=params+"&"+"export_data[corporation]="+encodeURI($('#export_data_corporation').val());
  }
  $('#recarga').load(url+"admin/"+site+"/?export=true&"+params+" #recarga");
}

function reload_form_declaration(url, obj) {

  var params = ""
  if(obj.val()){
    params=params+"activities_upload[job]="+encodeURI(obj.val());
    $('#recarga').load(url+"admin/activities_uploads/new?"+params+" #recarga");
  } else {
    $('#recarga').load(url+"admin/activities_uploads/new #recarga");
  }

}

function reload_form_declaration_asset(url, obj) {

  var params = ""
  if(obj.val()){
    params=params+"assets_upload[job]="+encodeURI(obj.val());
    $('#recarga').load(url+"admin/assets_uploads/new?"+params+" #recarga");
  } else {
    $('#recarga').load(url+"admin/assets_uploads/new #recarga");
  }

}

function show_button_add(obj, type) {
  if (parseInt(type) == 0) {
    obj.hide();
  }else if (parseInt(type) == 1) {
    obj.show();
  }
}

$(document).ready(function(){

	$("#added_councillors").click(function(){
    $("#data_councillor").addClass("data_exist");
    $("#data_form_job_levels").addClass("data_exist");
  });
  
	$("#added_directors").click(function(){
		$("#data_director").addClass("data_exist");
    $("#data_form_job_levels").addClass("data_exist");
  });
  
	$("#added_temporary_workers").click(function(){
		$("#data_temporary_worker").addClass("data_exist");
    $("#data_form_job_levels").addClass("data_exist");
  });
  
	$("#added_public_workers").click(function(){
		$("#data_public_worker").addClass("data_exist");
    $("#data_form_job_levels").addClass("data_exist");
  });
  
	$("#added_spokespeople").click(function(){
		$("#data_spokespeople").addClass("data_exist");
    $("#data_form_job_levels").addClass("data_exist");
  });
});

function delete_data_job_levels(){
  if ($("#data_councillor").hasClass("data_exist") == false && $("#data_director").hasClass("data_exist") == false && $("#data_temorary_worker").hasClass("data_exist") == false && $("#data_public_worker").hasClass("data_exist") == false && $("#data_spokespeople").hasClass("data_exist") == false){
    $("#data_form_job_levels").removeClass("data_exist");
  }
}

function show_tab(obj, header, array_num) {
  var aux= [];
  var aux_header=[];
  for (var i = 0; i < array_num.length; i++) {
    aux.push($('#tab_corporation_'+array_num[i]));
    aux_header.push($('#tab_header_corporation_'+array_num[i]));
    
  }
  show_tab_single(obj, header, aux, aux_header);
}

function show_tab_single(obj, header, others, others_header) {
  if(!obj.is(":visible")) {
    for(var i=0; i< others.length;i++) {
      others[i].hide();
      others[i].removeClass('active');
      others_header[i].removeClass('active');
    }
    obj.show();
    header.addClass('active');
    obj.addClass('active');
  }
}




function change_image(div,image_div ){
  files = div.prop('files');
  image = files[0];
  reader = new FileReader;

  reader.onload = function(file) {
    img = new Image;
    img.src = file.target.result;
    image_div.html(img);
    return;
  }

  reader.readAsDataURL(image)
}

function change_visibility_year(div, input_year, id) {
  if (parseInt(div.val()) == parseInt(id)) {
    input_year.show();
  } else {
    input_year.hide();
  }
}

function addtabs(hidden, headers){
  headers.html("");
  others_aux = []
  others_header_aux = []
  hidden.each(function (i,el){
    if( $("#"+el.id).attr("name") != "delete") {
      others_aux.push("$('#"+el.id+"')");
      others_header_aux.push("$('#"+el.id+"_header')");
    }
  });

  

  hidden.each(function (i,el){
    if( $("#"+el.id).attr("name") != "delete") {
      headers.append("<li id='"+el.id+"_header' class='tab-title active' role='presentation'><a href='#"+el.id+"' onclick=\"show_tab_single($('#"+el.id+"'),$('#"+el.id+"_header'), ["+others_aux+"], ["+others_header_aux+"])\">"+$("#"+el.id).attr("name")+"</a></li>");

      if(hidden.length-1 == i){
        $("#"+el.id).show();
        $("#"+el.id).addClass('active');
        $("#"+el.id+"_header").addClass('active');
      } else {
        $("#"+el.id).hide();
        $("#"+el.id).removeClass('active');
        $("#"+el.id+"_header").removeClass('active');
      }
    }
  })

 
}

function deletetabs(el) {
  $("#"+el.id+"_header").remove();
  $("#"+el.id).attr('name','delete');
}

window.nestedFormEvents.insertFields = function(content, assoc, link) {
  var target = $(link).data('target');
  var identificador = $(link).data('identificador');

  if(identificador) {
    content = content.replace(/councillor_id_[0-9]*/g, identificador)
  }
  if (target) {
    return $(content).appendTo($(target));
  } else {
    return $(content).insertBefore(link);
  }
}


function change_param(id,b, url) {
  $.ajax({
    type: 'get',
    url: url+id,
    data: {"section_m": b},
    success: function (data, status) {
      window.history.pushState(data, "Title",url+id+"?section_m="+b);
    }});
}

function change_param_parametrize(param, section) {
  data_send="{}"
  data_url=""

  if (param.length !=0) {
    if (typeof param[1] === 'string') { 
      data_send = "{\""+param[0]+"\": \""+param[1]+"\" }";
    } else {
      data_send = "{\""+param[0]+"\": "+param[1]+" }";
    }
    if (section.includes("?")) {
      data_url = "&"+param[0]+"="+param[1];
    } else {
      section = section + '/'
      data_url = "?"+param[0]+"="+param[1];
    }
  }
  data_send=JSON.parse(data_send)
  $.ajax({
    type: 'get',
    url: '/admin/'+section + data_url,
    data: data_send,
    success: function (data, status) {
      
      window.history.pushState(data, "Title",'/admin/'+section+data_url);
      window.location.reload()
    }});
}

function change_param_parametrize_url(param, section, div) {
  data_send="{}"
  data_url=""

  if (param.length !=0) {
    if (typeof param[1] === 'string') { 
      data_send = "{\""+param[0]+"\": \""+param[1]+"\" }";
    } else {
      data_send = "{\""+param[0]+"\": "+param[1]+" }";
    }
    if (section.includes("?")) {
      data_url = "&"+param[0]+"="+param[1];
    } else {
      section = section + '/'
      data_url = "?"+param[0]+"="+param[1];
    }
  }

  envio = section.split('/')
  aux = '';
  if(envio.length > 1) {
    aux = "/" + section.split('/')[1];
  }
  
  $(div).load(window.location.href+aux+data_url+" "+div);
  data_send=JSON.parse(data_send)
  $.ajax({
    type: 'get',
    url: '/admin/'+section + data_url,
    data: data_send,
    success: function (data, status) {
      
      window.history.pushState(data, "Title",'/admin/'+section+data_url);
    }});
}

function send_data_email(url) {
  emails=[];
  $('input[name^="emails[]"]').each(function(){
    if ($(this).prop("checked")) {
      emails.push($(this).val().split(' '));
    }
  })

  if(emails.length == 0) {
    loc=""
    emails = ""
  } else{
    loc="&emails="+encodeURIComponent(JSON.stringify(emails))
  }


  $.ajax({
    type: 'get',
    url: window.location.href,
    data: {"emails": emails},
    success: function (data, status) {
      $('#recarga').load(window.location.href+loc+" #recarga");
      if(emails.length > 0) {
        $('#send').attr("disabled", false);
      } else {
        $('#send').attr("disabled", true);
      }
    }});
}
