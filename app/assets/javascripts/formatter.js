var regex_email_g = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
var errors_emails={};


function check_format_email(email) {
    regex_email = new RegExp("^"+regex_email_g.source+"$", "g")
    return regex_email.test(email)
}

function check_format_multiple_email(emails) {
    regex_email_multiple = new RegExp("(^"+regex_email_g.source+"$)|(^("+regex_email_g.source+";)+"+regex_email_g.source+"$)","g")
    return regex_email_multiple.test(emails)
}

function change_input_email(element,multiple) {
    var aux=false
    if(multiple){
        aux=check_format_multiple_email(element.val());
    }else {
        aux=check_format_email(element.val());
    }

    if(aux==true || element.val()==""){
        errors_emails[element.attr('id')]=false
        element.css("border-color","");
    }else{
        errors_emails[element.attr('id')]=true
        element.css("border-color","red");
    }
    var block=false;
    $.each(errors_emails, function(k,v){
        if(v) { 
            block=true;
            return; 
        }
    });

    if(block==true) {
        $("#btn_submit").prop("disabled", true);
    }else{
        $("#btn_submit").prop("disabled", false);
    }
}