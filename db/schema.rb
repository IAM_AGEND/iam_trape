# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20230309130046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "unaccent"

  create_table "account_deposits", force: :cascade do |t|
    t.integer  "assets_declaration_id", null: false
    t.string   "kind"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.float    "balance"
    t.string   "entity"
    t.integer  "table_order"
  end

  create_table "activities_declarations", force: :cascade do |t|
    t.date     "declaration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "temporality_id"
    t.integer  "year_period"
    t.integer  "person_level_id"
    t.string   "person_level_type"
    t.boolean  "editable",          default: false
  end

  add_index "activities_declarations", ["person_level_type", "person_level_id"], name: "index_activity_person_l", using: :btree

  create_table "administrators", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "administrators", ["email"], name: "index_administrators_on_email", unique: true, using: :btree
  add_index "administrators", ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true, using: :btree

  create_table "appointments", force: :cascade do |t|
    t.string   "area"
    t.string   "position"
    t.string   "unit"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "position_alt"
    t.string   "politic_group"
    t.string   "district"
    t.string   "corporation"
    t.string   "juntamd"
    t.datetime "possession_date"
    t.text     "description_possession"
    t.text     "description"
    t.string   "email"
    t.string   "url_possession"
    t.string   "functions"
    t.text     "observations"
    t.integer  "retribution_year"
    t.string   "transparency_link"
    t.text     "other_expenses"
    t.text     "gifts"
    t.string   "retribution"
    t.string   "url_form"
  end

  create_table "assets_declarations", force: :cascade do |t|
    t.date     "declaration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "temporality_id"
    t.integer  "person_level_id"
    t.string   "person_level_type"
    t.integer  "year_period"
    t.boolean  "editable",          default: false
  end

  add_index "assets_declarations", ["person_level_type", "person_level_id"], name: "index_asset_person_l", using: :btree

  create_table "audits", force: :cascade do |t|
    t.integer  "person_id",        null: false
    t.integer  "administrator_id"
    t.string   "action"
    t.text     "description"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "charges", force: :cascade do |t|
    t.text     "charge"
    t.integer  "appointment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commisions", force: :cascade do |t|
    t.text     "commision"
    t.integer  "appointment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "compatibility_activities", force: :cascade do |t|
    t.datetime "resolution_date"
    t.text     "compatibility_description"
    t.string   "public_private"
    t.integer  "person_level_id"
    t.string   "person_level_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "compatibility_activities", ["person_level_type", "person_level_id"], name: "index_compatibility_activities_person_l", using: :btree

  create_table "corporations", force: :cascade do |t|
    t.string   "name",                                    null: false
    t.date     "election_date"
    t.integer  "start_year",                              null: false
    t.integer  "end_year",                                null: false
    t.date     "full_constitutional_date"
    t.date     "end_corporation_date"
    t.integer  "councillors_num",          default: 0
    t.text     "description"
    t.boolean  "active",                   default: true
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "corporations", ["name"], name: "index_corporations_on_name", unique: true, using: :btree

  create_table "corporations_electoral_lists", force: :cascade do |t|
    t.integer  "corporation_id"
    t.integer  "electoral_list_id"
    t.integer  "order",             default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "councillors_corporations", force: :cascade do |t|
    t.integer  "job_level_id"
    t.integer  "corporation_id"
    t.integer  "electoral_list_id"
    t.integer  "order_num",         default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "appointment_id"
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "profile_id",  null: false
    t.integer  "start_year"
    t.integer  "end_year"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "title"
    t.string   "center"
    t.integer  "table_order"
  end

  create_table "data_preferences", force: :cascade do |t|
    t.string   "title",                    null: false
    t.text     "content_data"
    t.integer  "type_data",    default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "code"
  end

  create_table "debts", force: :cascade do |t|
    t.integer  "assets_declaration_id", null: false
    t.string   "kind"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.text     "observations"
    t.float    "import"
    t.integer  "table_order"
  end

  create_table "dedications", force: :cascade do |t|
    t.integer  "councillors_corporation_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "percentage"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delete_logs", force: :cascade do |t|
    t.integer  "administrator_id", null: false
    t.string   "action"
    t.string   "result"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "document_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "document_types", ["name"], name: "index_document_types_on_name", unique: true, using: :btree

  create_table "duties", force: :cascade do |t|
    t.integer "declaration_id"
    t.string  "declaration_type"
    t.integer "person_level_id"
    t.string  "person_level_type"
    t.date    "start_date"
    t.date    "limit_date"
    t.date    "end_date"
    t.string  "type_duty"
    t.string  "year"
    t.integer "temporality_id"
  end

  add_index "duties", ["declaration_type", "declaration_id"], name: "index_duties_on_declaration_type_and_declaration_id", using: :btree
  add_index "duties", ["person_level_type", "person_level_id"], name: "index_duties_on_person_level_type_and_person_level_id", using: :btree

  create_table "electoral_lists", force: :cascade do |t|
    t.string   "name"
    t.string   "long_name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  add_index "electoral_lists", ["long_name"], name: "index_electoral_lists_on_long_name", unique: true, using: :btree
  add_index "electoral_lists", ["name"], name: "index_electoral_lists_on_name", unique: true, using: :btree

  create_table "file_uploads", force: :cascade do |t|
    t.string   "type"
    t.string   "original_filename"
    t.string   "file_format"
    t.text     "log"
    t.boolean  "successful"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "author_id"
    t.string   "period"
  end

  add_index "file_uploads", ["author_id"], name: "index_file_uploads_on_author_id", using: :btree

  create_table "information", force: :cascade do |t|
    t.integer  "profile_id"
    t.integer  "information_type_id"
    t.text     "information"
    t.date     "information_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "table_order"
  end

  create_table "information_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "information_types", ["name"], name: "index_information_types_on_name", unique: true, using: :btree

  create_table "job_levels", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "appointment_id"
    t.string   "slug",           null: false
    t.string   "person_type",    null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "job_levels", ["slug"], name: "index_job_levels_on_slug", unique: true, using: :btree

  create_table "language_levels", force: :cascade do |t|
    t.string   "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "language_levels", ["level"], name: "index_language_levels_on_level", unique: true, using: :btree

  create_table "language_names", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "language_names", ["name"], name: "index_language_names_on_name", unique: true, using: :btree

  create_table "languages", force: :cascade do |t|
    t.integer  "profile_id",  null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.string   "level"
    t.integer  "table_order"
  end

  create_table "logs", force: :cascade do |t|
    t.integer  "administrator_id"
    t.string   "type"
    t.boolean  "successful",       default: true
    t.string   "action"
    t.text     "log"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.jsonb    "emails"
    t.integer  "log_id"
  end

  create_table "manage_emails", force: :cascade do |t|
    t.string   "type_data"
    t.string   "sender"
    t.jsonb    "fields_cc"
    t.jsonb    "fields_cco"
    t.string   "subject"
    t.text     "email_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.jsonb    "content_data",      default: {}
    t.text     "type_data"
    t.boolean  "permit",            default: false
    t.boolean  "ignore",            default: false
    t.datetime "change_state_at"
    t.integer  "administrator_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "no_publish_reason"
    t.string   "action"
    t.datetime "date_action"
    t.string   "action_abrev"
    t.boolean  "send_feedback",     default: false
    t.integer  "forwards",          default: 0
    t.text     "motive"
    t.jsonb    "changes_data"
    t.jsonb    "origin_data"
  end

  create_table "organisms", force: :cascade do |t|
    t.integer  "councillors_corporation_id"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "other_activities", force: :cascade do |t|
    t.integer  "activities_declaration_id", null: false
    t.text     "description"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "start_date"
    t.string   "end_date"
    t.integer  "table_order"
  end

  create_table "other_deposits", force: :cascade do |t|
    t.integer  "assets_declaration_id", null: false
    t.text     "description"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "year"
    t.integer  "mounth"
    t.string   "types"
    t.string   "value"
    t.string   "purchase_date"
    t.integer  "table_order"
  end

  create_table "other_personal_properties", force: :cascade do |t|
    t.integer  "assets_declaration_id", null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "year"
    t.integer  "mounth"
    t.string   "types"
    t.string   "purchase_date"
    t.integer  "table_order"
  end

  create_table "parties", force: :cascade do |t|
    t.string  "name"
    t.string  "logo"
    t.string  "long_name"
    t.integer "councillors_count", default: 0
  end

  create_table "people", force: :cascade do |t|
    t.integer  "personal_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "last_name"
    t.string   "document_nif"
    t.integer  "document_type_id"
    t.string   "sex"
    t.integer  "profile_id"
  end

  create_table "political_posts", force: :cascade do |t|
    t.integer  "profile_id",  null: false
    t.string   "entity"
    t.integer  "start_year"
    t.integer  "end_year"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "position"
    t.integer  "table_order"
  end

  create_table "private_activities", force: :cascade do |t|
    t.integer  "activities_declaration_id", null: false
    t.text     "description"
    t.string   "entity"
    t.string   "position"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "start_date"
    t.string   "end_date"
    t.string   "private_activity_type"
    t.integer  "table_order"
  end

  create_table "private_activity_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "private_jobs", force: :cascade do |t|
    t.integer  "profile_id",  null: false
    t.string   "entity"
    t.integer  "start_year"
    t.integer  "end_year"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "position"
    t.integer  "table_order"
  end

  create_table "profiles", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at",                            null: false
    t.string   "email"
    t.string   "twitter"
    t.string   "facebook"
    t.string   "calendar_url"
    t.string   "portrait_file_name"
    t.string   "portrait_content_type"
    t.integer  "portrait_file_size"
    t.datetime "portrait_updated_at"
    t.boolean  "cumplimented",          default: false
    t.string   "personal_email"
  end

  create_table "public_activities", force: :cascade do |t|
    t.integer  "activities_declaration_id", null: false
    t.string   "entity"
    t.string   "position"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "start_date"
    t.string   "end_date"
    t.integer  "table_order"
  end

  create_table "public_administrations", force: :cascade do |t|
    t.string   "name"
    t.string   "administration_code"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "public_administrations", ["administration_code"], name: "index_public_administrations_on_administration_code", unique: true, using: :btree

  create_table "public_jobs", force: :cascade do |t|
    t.integer  "profile_id",            null: false
    t.integer  "start_year"
    t.integer  "end_year"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "body_scale"
    t.string   "position"
    t.integer  "init_year"
    t.string   "consolidation_degree"
    t.string   "public_administration"
    t.integer  "table_order"
  end

  create_table "real_estate_properties", force: :cascade do |t|
    t.integer  "assets_declaration_id",    null: false
    t.string   "kind"
    t.string   "municipality"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "year"
    t.integer  "mounth"
    t.string   "straight_type"
    t.string   "adquisition_title"
    t.string   "participation_percentage"
    t.float    "cadastral_value"
    t.text     "observations"
    t.string   "purchase_date"
    t.integer  "table_order"
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "studies", force: :cascade do |t|
    t.integer  "profile_id",      null: false
    t.integer  "start_year"
    t.integer  "end_year"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "official_degree"
    t.string   "center"
    t.string   "education_level"
    t.integer  "table_order"
  end

  create_table "tax_data", force: :cascade do |t|
    t.integer  "assets_declaration_id",    null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.float    "irpf_big_import"
    t.text     "irpf_big_observation"
    t.float    "irpf_bi_import"
    t.text     "irpf_bi_observation"
    t.float    "irpf_deduc_import_a"
    t.text     "irpf_deduc_observation_a"
    t.float    "irpf_deduc_import_b"
    t.text     "irpf_deduc_observation_b"
    t.float    "isp_bi_import"
    t.text     "isp_bi_observation"
    t.float    "is_bi_import"
    t.text     "is_bi_observation"
    t.float    "import"
    t.string   "observation"
    t.string   "tax"
    t.string   "fiscal_data"
    t.integer  "table_order"
  end

  create_table "temporalities", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "termination_authorizations", force: :cascade do |t|
    t.datetime "authorization_date"
    t.text     "authorization_description"
    t.integer  "person_level_id"
    t.string   "person_level_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "termination_authorizations", ["person_level_type", "person_level_id"], name: "index_termination_authorizations_person_l", using: :btree

  create_table "text_contents", force: :cascade do |t|
    t.string   "section",                null: false
    t.string   "content",    limit: 250
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.integer  "assets_declaration_id", null: false
    t.string   "kind"
    t.string   "model"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "year"
    t.integer  "mounth"
    t.string   "purchase_date"
    t.integer  "table_order"
  end

  add_foreign_key "account_deposits", "assets_declarations"
  add_foreign_key "activities_declarations", "temporalities"
  add_foreign_key "assets_declarations", "temporalities"
  add_foreign_key "audits", "administrators"
  add_foreign_key "audits", "people"
  add_foreign_key "charges", "appointments"
  add_foreign_key "commisions", "appointments"
  add_foreign_key "corporations_electoral_lists", "corporations"
  add_foreign_key "corporations_electoral_lists", "electoral_lists"
  add_foreign_key "councillors_corporations", "appointments"
  add_foreign_key "councillors_corporations", "corporations"
  add_foreign_key "councillors_corporations", "electoral_lists"
  add_foreign_key "councillors_corporations", "job_levels"
  add_foreign_key "courses", "profiles"
  add_foreign_key "debts", "assets_declarations"
  add_foreign_key "dedications", "councillors_corporations"
  add_foreign_key "delete_logs", "administrators"
  add_foreign_key "duties", "temporalities"
  add_foreign_key "file_uploads", "administrators", column: "author_id"
  add_foreign_key "information", "information_types"
  add_foreign_key "information", "profiles"
  add_foreign_key "job_levels", "appointments"
  add_foreign_key "job_levels", "people"
  add_foreign_key "languages", "profiles"
  add_foreign_key "logs", "administrators"
  add_foreign_key "notifications", "administrators"
  add_foreign_key "organisms", "councillors_corporations"
  add_foreign_key "other_activities", "activities_declarations"
  add_foreign_key "other_deposits", "assets_declarations"
  add_foreign_key "other_personal_properties", "assets_declarations"
  add_foreign_key "people", "document_types"
  add_foreign_key "people", "profiles"
  add_foreign_key "political_posts", "profiles"
  add_foreign_key "private_activities", "activities_declarations"
  add_foreign_key "private_jobs", "profiles"
  add_foreign_key "public_activities", "activities_declarations"
  add_foreign_key "public_jobs", "profiles"
  add_foreign_key "real_estate_properties", "assets_declarations"
  add_foreign_key "studies", "profiles"
  add_foreign_key "tax_data", "assets_declarations"
  add_foreign_key "vehicles", "assets_declarations"
end
