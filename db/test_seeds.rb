require 'database_cleaner'

DatabaseCleaner.clean_with :truncation

# Administrators
Administrator.create!(email: 'admin@madrid.es', password: Rails.application.secrets.password)

puts "========================================================================="
puts "STATIC PROFILES"
puts "========================================================================="
puts "- START - STATIC PROFILES"
puts "..."
# Language names
[
  "Italiano", "Portugués", "Japonés", "Alemán", "Ruso", "Árabe", "Francés", 
  "Chino Mandarín", "Español", "Inglés", "Rumano", "Catalán", "Euskera", 
  "Gallego", "Hindi", "Coreano", "Valenciano", "Indonesio", "Chino"
].each {|name| LanguageName.create!(name: name)}

# Language levels
[
  "A - Usuario básico", "B - Usuario intermedio", "C - Usuario competente"
].each {|level| LanguageLevel.create!(level: level)}

#########################################################################
# Information Types
#########################################################################
# Otros comentarios sobre formación académica => studies_comments
# Otros comentarios sobre cursos => courses_comments
# Otros detalles sobre la carrera profesional => career_comments
# Otros comentarios sobre cargos políticos => political_posts_comments
# Publicación => publications
# Actividad docente => theacher_activity
# Distinciones honoríficas y otros méritos => special_mentions
# Otra información de interés => other
[
  "studies_comments", "courses_comments", "career_comments", "political_posts_comments",
  "publications", "theacher_activity", "special_mentions", "other"
].each {|name| InformationType.create!(name: name)}
puts "- END - STATIC PROFILES"
puts ""

puts "========================================================================="
puts "PUBLIC ADMINISTRATIONS"
puts "========================================================================="
puts "- START - PUBLIC ADMINISTRATIONS"
puts "..."
# Public administrations
[
  ["AC", "Adm. Comunitaria"], ["AG", "Adm. Gen Estado"], ["AL", "Adm. Local"],
  ["AR", "C.A. ARAGÓN"], ["AS", "C.A. P.ASTURIAS"], ["AY", "Ayto. Madrid"],
  ["CA", "Com. Autónoma"], ["CB", "C.A. CANTABRIA"], ["CC", "CIUDAD A. CEUTA"],
  ["CL", "C.A. CAS.-LEÓN"], ["CM", "C.A. C-LA MANCHA"], ["CV", "C. VALENCIANA"],
  ["EX", "C. A. EXTREMAD."], ["GC", "C.A. CATALUÑA"], ["IB", "C.A. I. BALEARES"],
  ["IC", "C.A. I. CANARIAS"], ["JA", "C.A. ANDALUCÍA"], ["MA", "C. DE MADRID"],
  ["ME", "CIUD. A. MELILLA"], ["MU", "C.A. MURCIA"], ["NA", "C.F. NAVARRA"],
  ["OA", "Org. Autónomo"], ["OI", "Org. Internac."], ["OT", "Otros"],
  ["PV", "C.A. PAÍS VASCO"], ["RI", "C.A. LA RIOJA"], ["XG", "C.A. GALICIA"]
].each {|pubad| PublicAdministration.create!(administration_code: pubad[0],name: pubad[1])}
puts "- END - PUBLIC ADMINISTRATIONS"
puts ""

puts "========================================================================="
puts "TEXT CONTENTS"
puts "========================================================================="
puts "- START - TEXT CONTENTS"
puts "..."
# Text contents
TextContent.create!(section: "actual_concillors", content: "Concejales y concejalas de la corporación 2019-2023, ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella")
TextContent.create!(section: "actual_concillors_not_working", content: "En esta página se irán incorporando los concejales y concejalas que cesen a lo largo del mandato")
TextContent.create!(section: "actual_concillors_no_data", content: "")
TextContent.create!(section: "directors", content: "Personal directivo en ejercicio")
TextContent.create!(section: "directors_not_working", content: "Personal directivo que ha cesado. Se muestran sus declaraciones de bienes y actividades durante los dos años siguientes a partir de la fecha de cese")
TextContent.create!(section: "directors_no_data", content: "")
TextContent.create!(section: "public_workers", content: "Personal funcionario del Ayuntamiento de Madrid de nivel 28 o superior que ha accedido al puesto mediante el procedimiento de libre designación")
TextContent.create!(section: "public_workers_no_data", content: "")
TextContent.create!(section: "spokespeople", content: "En esta página se incorporarán los vocales vecinos de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
TextContent.create!(section: "spokespeople_no_data", content: "")
TextContent.create!(section: "temporary_workers", content: "En esta página se incorporará el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
TextContent.create!(section: "temporary_workers_no_data", content: "En esta página se incorporarán el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
TextContent.create!(section: "old_corporation_2015-2019", content: "Concejales y concejalas que han formado parte de la Corporación 2015-2019 ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella. Se muestran sus declarac")
TextContent.create!(section: "old_corporation_2015-2019_no_data", content: "Concejales y concejalas que han formado parte de la Corporación 2015-2019 ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella.  ")
puts "- END - TEXT CONTENTS"
puts ""

puts "========================================================================="
puts "DOCUMENTS TYPES"
puts "========================================================================="
puts "- START - DOCUMENTS TYPES"
puts "..."
# Document types
["Tarjeta Residencia", "DNI", "PASAPORTE", "NIE"].each {|name| DocumentType.create!(name: name)}
puts "- END - DOCUMENTS TYPES"
puts ""

puts "========================================================================="
puts "CORPORATIONS"
puts "========================================================================="
puts "- START - CORPORATIONS"
puts "..."
# Corporations
Corporation.create!(start_year: 2015, end_year: 2019, name: "2015-2019", active: false, end_corporation_date: Time.zone.parse('2019-06-06'))
Corporation.create!(start_year: 2019, end_year: 2023, name: "2019-2023", end_corporation_date: Time.zone.parse('2023-06-06'))
puts "- END - CORPORATIONS"
puts ""

puts "========================================================================="
puts "ELECTORAL LISTS"
puts "========================================================================="
puts "- START - ELECTORAL LISTS"
puts "..."
# Electoral list
ElectoralList.create!(name: "Ahora Madrid", long_name: "GRUPO MUNICIPAL AHORA MADRID")
ElectoralList.create!(name: "Partido Popular", long_name: "GRUPO MUNICIPAL DEL PARTIDO POPULAR")
ElectoralList.create!(name: "VOX", long_name: "GRUPO MUNICIPAL VOX")
ElectoralList.create!(name: "Ciudadanos", long_name: "GRUPO MUNICIPAL CIUDADANOS - PARTIDO DE LA CIUDADANIA")
ElectoralList.create!(name: "Más Madrid", long_name: "GRUPO MUNICIPAL MÁS MADRID")
ElectoralList.create!(name: "PSOE", long_name: "GRUPO MUNICIPAL SOCIALISTA DE MADRID")
# Corporations electoral list
# Corporación 2019-2023
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2019-2023").id, 
  order: 0, electoral_list_id: ElectoralList.find_by(name: "Más Madrid").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2019-2023").id, 
  order: 1, electoral_list_id: ElectoralList.find_by(name: "Partido Popular").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2019-2023").id, 
  order: 2, electoral_list_id: ElectoralList.find_by(name: "Ciudadanos").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2019-2023").id, 
  order: 3, electoral_list_id: ElectoralList.find_by(name: "PSOE").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2019-2023").id, 
  order: 4, electoral_list_id: ElectoralList.find_by(name: "VOX").id)
# Corporación 2015-2019
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2015-2019").id, 
  order: 0, electoral_list_id: ElectoralList.find_by(name: "Partido Popular").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2015-2019").id, 
  order: 1, electoral_list_id: ElectoralList.find_by(name: "Ahora Madrid").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2015-2019").id, 
  order: 2, electoral_list_id: ElectoralList.find_by(name: "PSOE").id)
CorporationsElectoralList.create!(corporation_id: Corporation.find_by(name: "2015-2019").id, 
  order: 3, electoral_list_id: ElectoralList.find_by(name: "Ciudadanos").id)
puts "- END - ELECTORAL LISTS"
puts ""

puts "========================================================================="
puts "PRIVATE ACTIVITY TYPES"
puts "========================================================================="
puts "- START - PRIVATE ACTIVITY TYPES"
puts "..."
["Actividades mercantiles o industriales", "Actividades y ocupaciones profesionales", "Actividades por cuenta ajena"].each do |pa|
  PrivateActivityType.create!(name: pa)
end
puts "- END - PRIVATE ACTIVITY TYPES"
puts ""

puts "========================================================================="
puts "TEMPORALITIES"
puts "========================================================================="
puts "- START - TEMPORALITIES"
puts "..."
["Inicial", "Final", "Anual"].each do |temp|
  Temporality.create!(name: temp)
end
puts "- END - TEMPORALITIES"
puts ""

puts "========================================================================="
puts "DATA PREFERENCES"
puts "========================================================================="
puts "- START - DATA PREFERENCES"
puts "..."
DataPreference.create!(title: "Años publicados", type_data: 0, content_data: 2) 
DataPreference.create!(title: "Número declarciones anuales", type_data: 0, content_data: 5) 
puts "- END - DATA PREFERENCES"
puts ""

puts "========================================================================="
puts "MANAGE EMAILS"
puts "========================================================================="
puts "- START - MANAGE EMAILS"
puts "..."
data_json_cc= {"councillors" => "transparenciaydatos@madrid.es", "directors" => "transparenciaydatos@madrid.es", 
  "public_workers" => "transparenciaydatos@madrid.es", "temporary_workers" => "transparenciaydatos@madrid.es",
  "spokespeople" => "transparenciaydatos@madrid.es" }
data_json_cco = {"councillors" => "creaspodhe@madrid.es", "directors" => "creaspodhe@madrid.es", 
  "public_workers" => "creaspodhe@madrid.es", "temporary_workers" => "creaspodhe@madrid.es",
  "spokespeople" => "creaspodhe@madrid.es" }

ManageEmail.create!(type_data: "ProfilesUpload", sender: "transparenciaydatos@madrid.es",
  fields_cc: data_json_cc, 
  fields_cco: data_json_cco,
  subject: I18n.t("emails.subject.profiles_upload"), 
  email_body: I18n.t("emails.body.profiles_upload")) 

ManageEmail.create!(type_data: 'AssetsUpload', sender: "transparenciaydatos@madrid.es",
  fields_cc: data_json_cc, 
  fields_cco: data_json_cco,
  subject: I18n.t("emails.subject.assets_upload"), 
  email_body: I18n.t("emails.body.assets_upload")) 

ManageEmail.create!(type_data: 'ActivitiesUpload', sender: "transparenciaydatos@madrid.es",
  fields_cc: data_json_cc, 
  fields_cco: data_json_cco,
  subject: I18n.t("emails.subject.activities_upload"), 
  email_body: I18n.t("emails.body.activities_upload")) 
puts "- END - MANAGE EMAILS"
puts ""

# People
puts "========================================================================="
puts "PEOPLE"
puts "========================================================================="
# PRELOAD DATA
##############################################################################
# COUNCILLORS
##############################################################################
puts "- START - COUNCILLORS"
puts "..."
per_aux = Person.create!(personal_code: 171003, name: "Purificación", last_name: "Causapié Lopesino", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "Concejala", position_alt: "Portavoz del Grupo Político Municipal", unit: "GRUPO MUNICIPAL SOCIALISTA DE MADRID", start_date: "2015-06-13")
job_aux = JobLevel.create!(updated_at: Time.zone.now, updated_at: Time.zone.now, person_id: per_aux.id, person_type: "councillor", slug: "c-purificacion-causapie-lopesino")
CouncillorsCorporation.create!(appointment_id: app_aux.id, job_level_id: job_aux.id, corporation_id: Corporation.find_by(name: "2015-2019").id, electoral_list_id: 6, order_num: 44)

per_aux = Person.create!(personal_code: 9171654, name: "ISABEL", last_name: "ROSELL VOLART", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "CONCEJALA", unit: "GRUPO MUNICIPAL PARTIDO POPULAR", start_date: "2015-06-13")
job_aux = JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "councillor", slug: "c-isabel-rosell-volart")
CouncillorsCorporation.create!(appointment_id: app_aux.id, job_level_id: job_aux.id, corporation_id: Corporation.find_by(name: "2015-2019").id, electoral_list_id: 2, order_num: 9)

per_aux = Person.create!(personal_code: 171133, name: "JOSÉ LUIS", last_name: "MARTÍNEZ-ALMEIDA NAVASQÜÉS", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "CONCEJAL", unit: "GRUPO MUNICIPAL PARTIDO POPULAR", start_date: "2017-02-21")
job_aux = JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "councillor", slug: "c-jose-luis-martinez-almeida-navasques")
CouncillorsCorporation.create!(appointment_id: app_aux.id, job_level_id: job_aux.id, corporation_id: Corporation.find_by(name: "2019-2023").id, electoral_list_id: 2, order_num: 0)
app_aux = Appointment.create!(position: "Concejal", position_alt: "Portavoz del Grupo Político Municipal", unit: "GRUPO MUNICIPAL PARTIDO POPULAR", start_date: "2015-06-13")
CouncillorsCorporation.create!(appointment_id: app_aux.id, job_level_id: job_aux.id, corporation_id: Corporation.find_by(name: "2015-2019").id, electoral_list_id: 2, order_num: 3)

puts "- END - COUNCILLORS"
##############################################################################
# DIRECTORS
##############################################################################
puts "- START - DIRECTORS"
puts "..."
per_aux = Person.create!(personal_code: 1, name: "MERCEDES", last_name: "RUIZ GARIJO", sex: "", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "VOCAL DEL TRIBUNAL", unit: "VOCALIA 3ª TEAMM", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "director", slug: "d-mercedes-ruiz-garijo", appointment_id: app_aux.id)

per_aux = Person.create!(personal_code: 76399, name: "PEDRO", last_name: "GUITART GONZALEZ VALERIO", sex: "", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "COORDINADOR/A DEL DISTRITO", unit: "COORDINACION DEL DISTRITO DE TETUAN", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "director", slug: "d-pedro-guitart-gonzalez-valerio", appointment_id: app_aux.id)

per_aux = Person.create!(personal_code: 80166, name: "ALICIA", last_name: "MARTIN PEREZ", sex: "", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "DIRECTOR/A GENERAL", unit: "DIRECCION GENERAL DE DEPORTE", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "director", slug: "d-alicia-martin-perez", appointment_id: app_aux.id)


puts "- END - DIRECTORS"
##############################################################################
# TEMPORARY WORKERS
##############################################################################
puts "- START - TEMPORARY WORKERS"
puts "..."
per_aux = Person.create!(personal_code: 11440, name: "ROBERTO", last_name: "GOMEZ GONZALEZ", document_type_id: DocumentType.find_by(name: "DNI").id )  
app_aux = Appointment.create!(position: "ASESOR/A N28",unit: "UNIDAD DE PERSONAL EVENTUAL", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "temporary_worker", slug: "e-roberto-gomez-gonzalez", appointment_id: app_aux.id)

per_aux = Person.create!(personal_code: 178273, name: "OLGA", last_name: "MARTINEZ LOPEZ", document_type_id: DocumentType.find_by(name: "DNI").id ) 
app_aux = Appointment.create!(position: "ASESOR/A N26", unit: "CONCEJAL PRESIDENTE JMD VILLA DE VALLECAS", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "temporary_worker", slug: "e-olga-martinez-lopez", appointment_id: app_aux.id)


per_aux = Person.create!(personal_code: 173183, name: "NURIA", last_name: "DEL RIO PARACOLLS", document_type_id: DocumentType.find_by(name: "DNI").id )
app_aux = Appointment.create!(position: "ASESOR/A N28", start_date: "2017-02-21", unit: "GABINETE - AREA DE GOBIERNO DE ECONOMIA Y HACIENDA")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "temporary_worker", slug: "enuria-del-rio-paracolls", appointment_id: app_aux.id)

per_aux = Person.create!(personal_code: 177199, name: "ANA ISABEL", last_name: "ENCINAS DIAZ", document_type_id: DocumentType.find_by(name: "DNI").id ) 
app_aux = Appointment.create!(position: "ASESOR/A N26", start_date: "2017-02-21", unit: "CONCEJAL PRESIDENTE JMD SALAMANCA")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "temporary_worker", slug: "e-ana-isabel-encinas-diaz", appointment_id: app_aux.id)
puts "- END - TEMPORARY WORKERS"
##############################################################################
# PUBLIC WORKERS
##############################################################################
puts "- START - PUBLIC WORKERS"
puts "..."
per_aux = Person.create!(personal_code: 69243, name: "MARIA AINHOA", last_name: "ABAIGAR SANTOS", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "JEFE/A DEPARTAMENTO", unit: "DEPARTAMENTOJURIDICO DE LA CONTRATACION", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "public_worker", appointment_id: app_aux.id, slug: "f-maria-ainhoa-abaigar-santos")

per_aux = Person.create!(personal_code: 71633, name: "MARTA JESUS", last_name: "ABANADES AMO", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "SUBDIRECTOR/A GENERAL", unit: "SUBDIRECCION GENERAL DE COORDINACION DE LOS SERVICIOS", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "public_worker", appointment_id: app_aux.id, slug: "f-marta-jesus-abanades-amo")

per_aux = Person.create!(personal_code: 84821, name: "SEGUNDO", last_name: "SANCHEZ MORENO", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "JEFE/A SERVICIO", unit: "SERVICIO DE CIUDAD INTELIGENTE", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "public_worker", appointment_id: app_aux.id, slug: "f-segundo-sanchez-moreno")

per_aux = Person.create!(personal_code: 86356, name: "SARA EMMA", last_name: "ARANDA PLAZA", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "SUBDIRECTOR/A GENERAL", unit: "SUBDIRECCION GENERAL DE REGIMEN JURIDICO", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "public_worker", appointment_id: app_aux.id, slug: "f-sara-emma-aranda-plaza")

per_aux = Person.create!(personal_code: 118403, name: "MARTA", last_name: "HONDARZA UGEDO", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "JEFE/A DEPARTAMENTO", unit: "DEPARTAMENTO PROGRAMAC.ECONOMICA", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "public_worker", appointment_id: app_aux.id, slug: "f-marta-hondarza-ugedo")

puts "- END - PUBLIC WORKERS"
##############################################################################
# SPOKEPEOPLE
##############################################################################
puts "- START - SPOKEPEOPLE"
puts "..."
per_aux = Person.create!(personal_code:  33854864, name: "ROSA MARIA", last_name: "RIVEIRO GOMEZ", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "Vocal vecino - Portavoz Grupo Municipal Ahora Madrid", unit: "San Blas - Canillejas", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "spokesperson", appointment_id: app_aux.id, slug: "v-rosa-maria-riveiro-gomez")
per_aux = Person.create!(personal_code:  50809634, name: "INMACULADA", last_name: "FERNANDEZ RUIZ", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "Vocal vecino - Portavoz Grupo Municipal Socialista", unit: "Moratalaz", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "spokesperson", appointment_id: app_aux.id, slug: "v-inmaculada-fernandez-ruiz")
per_aux = Person.create!(personal_code:  1374683, name: "JOSE MARIA", last_name: "QUESADA VALVERDE", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "Vocal vecino - Portavoz adjunto Grupo Municipal del Partido Popular", unit: "Usera", end_date: "2019-02-08", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "spokesperson", appointment_id: app_aux.id, slug: "v-jose-maria-quesada-valverde")
per_aux = Person.create!(personal_code:  2282539, name: "MOISES", last_name: "LENDINEZ TEIXEIRA", document_type_id: DocumentType.find_by(name: "DNI").id)
app_aux = Appointment.create!(position: "Vocal vecino - Grupo Municipal del Partido Popular", unit: "Usera", start_date: "2017-02-21")
JobLevel.create!(updated_at: Time.zone.now, person_id: per_aux.id, person_type: "spokesperson", appointment_id: app_aux.id, slug: "v-moises-lendinez-teixeira")


puts "- END - SPOKEPEOPLE"
##############################################################################
# NOTIFICATIONS
##############################################################################
puts "- START - NOTIFICATIONS"
puts "..."

Notification.create!(id: 1,content_data: '{"cpId": 721, "cpActiva": 1, "cpNombre": "2020/2024", "cpObserv": "Corporación de pruebas", "cpAnioFin": 2024, "cpNumConcej": 6, "cpAnioInicio": 2020, "cpFechaFinal": null, "cpFechaElecciones": 1580023436000, "cpFechaPlenoConst": 1581751436000, "listasElectorales": [{"cpId": 721, "leId": 92, "leNombreLista": "PSOE"}, {"cpId": 721, "leId": 93, "leNombreLista": "PP"}, {"cpId": 721, "leId": 157, "leNombreLista": "AM (Ahora Madrid)"}]}', type_data: 'Corporation');
Notification.create!(id: 2,content_data:  '{"cpId": 681, "cpActiva": 0, "cpNombre": "2008/2012", "cpObserv": null, "cpAnioFin": 2012, "cpNumConcej": 57, "cpAnioInicio": 2008, "cpFechaFinal": 1575118052000, "cpFechaElecciones": 1212184800000, "cpFechaPlenoConst": 1213912800000, "listasElectorales": []}', type_data: 'Corporation');
Notification.create!(id: 3,content_data:  '{"cpId": 382, "cpActiva": 0, "cpNombre": "2019/2023", "cpObserv": "HOLA", "cpAnioFin": 2023, "cpNumConcej": 57, "cpAnioInicio": 2019, "cpFechaFinal": 1576164957000, "cpFechaElecciones": 1558821600000, "cpFechaPlenoConst": 1560549600000, "listasElectorales": []}', type_data: 'Corporation');
Notification.create!(id: 4,content_data:  '{"vocal": null, "dperId": 3506, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "MARIA AINHOA", "dperNumper": 69243, "funcionario": {"fldId": 1498, "dperId": 3506, "fldnId": 2558, "fldnCargo": "JEFE/A SERVICIO", "fldnUnidad": "IAM SERVICIO DE POBLACION Y TERRITORIO", "fldnFechaCese": null, "fldnFechaNomb": 1104793200000}, "dperApellido1": "JAVIER", "dperApellido2": "JAVIER", "dperDocumento": "01487872W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');


Notification.create!(id: 5,content_data:  '{"vocal": null, "dperId": 3506, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "JAVIER", "dperNumper": 80710, "funcionario": {"fldId": 1498, "dperId": 3506, "fldnId": 2558, "fldnCargo": "JEFE/A SERVICIO", "fldnUnidad": "IAM SERVICIO DE POBLACION Y TERRITORIO", "fldnFechaCese": null, "fldnFechaNomb": 1104793200000}, "dperApellido1": "JAVIER", "dperApellido2": "JAVIER", "dperDocumento": "01487872W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 6,content_data:  '{"vocal": null, "dperId": 3900, "concejal": null, "dperSexo": "M", "eventual": null, "directivo": null, "dperNombre": "MARIA", "dperNumper": 169352, "funcionario": {"fldId": 1892, "dperId": 3900, "fldnId": 2952, "fldnCargo": "JEFE/A DEPARTAMENTO", "fldnUnidad": "DEPARTAMENTO DE RECLAMACIONES I", "fldnFechaCese": null, "fldnFechaNomb": 1430776800000}, "dperApellido1": "MARIA", "dperApellido2": "MARIA", "dperDocumento": "50755712W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 7,content_data:  '{"vocal": null, "dperId": 1375, "concejal": null, "dperSexo": null, "eventual": null, "directivo": {"drId": 249, "drnId": 248, "dperId": 1375, "drnCargo": "COORDINADOR/A DEL DISTRITO", "drnUnidad": "CAR COORDINACION DEL DISTRITO", "drnFechaCese": null, "drnFechaNomb": 1516662000000}, "dperNombre": "JOSÉ CARLOS", "dperNumper": 71633, "funcionario": null, "dperApellido1": "JOSÉ CARLOS", "dperApellido2": "JOSÉ CARLOS", "dperDocumento": "50730272T", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 8,content_data:  '{"vocal": null, "dperId": 2703, "concejal": null, "dperSexo": "H", "eventual": {"evtId": 100, "dperId": 2703, "evtnId": 125, "evtnCargo": "VOCAL ASESOR/A", "evtnUnidad": "UNIDAD DE PERSONAL EVENTUAL", "evtnFechaCese": null, "evtnFechaNomb": 1562709600000}, "directivo": null, "dperNombre": "ANGEL IGNACIO", "dperNumper": 171421, "funcionario": null, "dperApellido1": "ANGEL IGNACIO", "dperApellido2": "ANGEL IGNACIO", "dperDocumento": "51424813X", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 9,content_data:  '{"vocal": null, "dperId": 2802, "concejal": null, "dperSexo": "M", "eventual": {"evtId": 199, "dperId": 2802, "evtnId": 224, "evtnCargo": "ASESOR/A N28", "evtnUnidad": "GRUPO MUNICIPAL PARTIDO POPULAR", "evtnFechaCese": null, "evtnFechaNomb": 1565560800000}, "directivo": null, "dperNombre": "MARIA DEL CARMEN", "dperNumper": 201910, "funcionario": null, "dperApellido1": "MARIA DEL CARMEN", "dperApellido2": "MARIA DEL CARMEN", "dperDocumento": "51376488P", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 10,content_data:  '{"vocal": null, "dperId": 1317, "concejal": null, "dperSexo": null, "eventual": null, "directivo": {"drId": 191, "drnId": 190, "dperId": 1317, "drnCargo": "COORDINADOR/A GENERAL", "drnUnidad": "PGD CG POLITICAS DE GENERO Y DIVERSIDAD", "drnFechaCese": null, "drnFechaNomb": 1494540000000}, "dperNombre": "ROSA MARÍA", "dperNumper": 9, "funcionario": null, "dperApellido1": "ROSA MARÍA", "dperApellido2": "ROSA MARÍA", "dperDocumento": "51701259L", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 11,content_data:  '{"vocal": {"vvId": 78, "vvnId": 78, "dperId": 1530, "vvnCargo": null, "vvnUnidad": "Junta Municipal Alcalá", "vvnFechaCese": null, "vvnFechaNomb": 1564005600000}, "dperId": 1530, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "Mai", "dperNumper": 8, "funcionario": null, "dperApellido1": "Mai", "dperApellido2": "Mai", "dperDocumento": "09035930N", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 12,content_data:  '{"vocal": {"vvId": 1, "vvnId": 1, "dperId": 361, "vvnCargo": null, "vvnUnidad": "Junta Municipal Rivas", "vvnFechaCese": 1575115765000, "vvnFechaNomb": 1564037158000}, "dperId": 361, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": {"drId": 10, "drnId": 10, "dperId": 361, "drnCargo": "Mandamás", "drnUnidad": "IAM", "drnFechaCese": null, "drnFechaNomb": 1567116000000}, "dperNombre": "OSCAR", "dperNumper": 5, "funcionario": null, "dperApellido1": "OSCAR", "dperApellido2": "OSCAR", "dperDocumento": "47463199P", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 13,content_data:  '{"vocal": null, "dperId": 381, "concejal": {"cjId": 242, "dperId": 381, "cjnAlcalde": 0, "cjnFechaNom": 1560722400000, "corporacion": {"cpId": 382, "cpActiva": 0, "cpNombre": "2019/2023", "cpObserv": "HOLA", "cpAnioFin": 2023, "cpNumConcej": 57, "cpAnioInicio": 2019, "cpFechaFinal": 1576164957000, "cpFechaElecciones": 1558821600000, "cpFechaPlenoConst": 1560549600000, "listasElectorales": []}, "cjnFechaCese": null}, "dperSexo": "M", "eventual": null, "directivo": {"drId": 1, "drnId": 1, "dperId": 381, "drnCargo": "Director/a General", "drnUnidad": "IAM", "drnFechaCese": null, "drnFechaNomb": 1553078357000}, "dperNombre": "Paloma", "dperNumper": 57146, "funcionario": null, "dperApellido1": "Paloma", "dperApellido2": "Paloma", "dperDocumento": "50421035K", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 14,content_data:  '{"vocal": null, "dperId": 1112, "concejal": {"cjId": 932, "dperId": 1112, "cjnAlcalde": 0, "cjnFechaNom": 1558354562000, "corporacion": {"cpId": 381, "cpActiva": 0, "cpNombre": "2015/2019", "cpObserv": null, "cpAnioFin": 2019, "cpNumConcej": 57, "cpAnioInicio": 2015, "cpFechaFinal": 1576163255000, "cpFechaElecciones": 1432418400000, "cpFechaPlenoConst": 1434146400000, "listasElectorales": []}, "cjnFechaCese": 1576163255000}, "dperSexo": null, "eventual": null, "directivo": null, "dperNombre": "María Begoña", "dperNumper": 10, "funcionario": null, "dperApellido1": "María Begoña", "dperApellido2": "María Begoña", "dperDocumento": "01385136F", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
Notification.create!(id: 15,content_data:  '{"deudas": [{"deuId": 2841, "decgId": 4221, "deuDesc": "Vivienda habitual", "deuClase": "Hopoteca", "deuImporte": 50000.0}], "decg_id": 4221, "vehiculos": [{"vehId": 3101, "decgId": 4221, "vehMes": "Abril", "vehAnyo": 1982, "vehClase": "Coche", "vehMarcaMod": "Renault 5"}], "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 6, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "depositosCuenta": [{"dpIpd": 3861, "decgId": 4221, "dpClase": "Depósito en cuenta corriente", "dpSaldo": 3000.0, "dpEntidad": "ING"}], "otrosBienesMuebles": [{"obmId": 2961, "decgId": 4221, "obmMes": "Agosto", "obmAnyo": 2019, "obmDesc": "Herencia de mi abuelo", "obmTipos": "Joyas"}], "otrosDepositosCuenta": [{"decgId": 4221, "optrId": 3461, "optrMes": "Febrero", "optrAnyo": 1998, "optrTipos": "Bonos", "optrValor": 1222.0, "optrTipoUnidad": "222", "optrDescripcion": "xxxx"}, {"decgId": 4221, "optrId": 3441, "optrMes": null, "optrAnyo": 2012, "optrTipos": "Deuda pública", "optrValor": 3000.0, "optrTipoUnidad": "€", "optrDescripcion": "Ahorrillos"}], "informacionTributaria": [{"decgId": 4221, "inftId": 3961, "inftIsBiObserv": null, "inftIsBiImporte": 0.0, "inftIspBiObserv": null, "inftIrpfBiObserv": null, "inftIspBiImporte": 0.0, "inftIrpfBiImporte": 4500.0, "inftIrpfBigObserv": null, "inftIrpfBigImporte": 35000.0, "inftIrpfDeducObservA": null, "inftIrpfDeducObservB": null, "inftIrpfDeducImporteA": 0.0, "inftIrpfDeducImporteB": 0.0}], "patrimonioInmobiliario": [{"mes": "Diciembre", "anyo": 2012, "clase": "Urbano", "pinId": 4021, "decgId": 4221, "municipio": "Madrid", "tipoDerecho": "Pleno dominio", "participacion": 50.0, "valorCatastral": 90000.0, "tituloAdquisicion": "Compraventa"}]}', type_data: 'AssetsDeclaration');
Notification.create!(id: 16,content_data:  '{"deudas": [], "decg_id": 4223, "vehiculos": [], "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 10, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "depositosCuenta": [], "otrosBienesMuebles": [], "otrosDepositosCuenta": [], "informacionTributaria": [], "patrimonioInmobiliario": []}', type_data: 'AssetsDeclaration');
Notification.create!(id: 17,content_data:  '{"decgId": 4221, "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 10, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "actividadesOtras": [], "actividadesPrivadas": [], "actividadesPublicas": []}', type_data: 'ActivitiesDeclaration');
Notification.create!(id: 18,content_data:  '{"decgId": 4223, "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 10, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "actividadesOtras": [{"acotId": 2541, "decgId": 4223, "fechaCese": null, "descripcion": "Youtuber", "fechaInicio": null}], "actividadesPrivadas": [{"id": 556061, "cargo": "Directivo/Consejero", "decgId": 4223, "entidad": "Iberdrola", "actividad": "Actividades por cuenta ajena", "fechaCese": null, "descripcion": "Consejero", "fechaInicio": 1578351600000}], "actividadesPublicas": [{"acpuId": 2581, "decgId": 4223, "acpuCargo": "Celador", "acpuEntidad": "IAM", "acpuFecCese": null, "acpuFecInicio": null}]}', type_data: 'ActivitiesDeclaration' );
Notification.create!(id: 19,content_data:  '{"dpfId": 1665, "dperId": 363, "idiomas": [{"dpfId": 1665, "idmId": 1450, "idiomaConf": {"idcId": 14, "idcIdioma": "Gallego"}, "idiomaNivel": {"nicId": 2, "nicNivel": "B - Usuario intermedio"}}, {"dpfId": 1665, "idmId": 1449, "idiomaConf": {"idcId": 8, "idcIdioma": "Chino Mandarín"}, "idiomaNivel": {"nicId": 1, "nicNivel": "A - Usuario básico"}}], "dperNumper": 5, "dpfTwitter": null, "dpfFacebook": null, "distinciones": [], "otrosEstudios": [{"oeId": 1185, "dpfId": 1665, "oeCurso": "SSMC", "oeCentro": "Itera", "oeAnyoFin": 2017, "oeAnyoIni": 2017}], "publicaciones": [{"dpfId": 1665, "pubId": 805, "pubAnioInicio": 2016, "pubPublicacion": "Tesis"}], "cargosPoliticos": [{"dpfId": 1665, "cpolId": 845, "cpolCargo": "Mandmás", "cpolAnyoFin": null, "cpolAnyoIni": 2017, "cpolInstitucion": "IAM"}], "dpfMailPersonal": "jaja@ja.ja", "otraInformacion": [{"dpfId": 1665, "oipId": 964, "oipInformacion": "Soy runner pero poco"}], "actividadesDocente": [], "carreraProfPrivado": [{"dpfId": 1665, "cpriId": 1145, "cpriCargo": "Analista", "cpriAnyoFin": null, "cpriAnyoIni": 2008, "cpriInstitucion": "Indra"}], "carreraProfPublico": [{"cpuId": 1485, "dpfId": 1665, "cpuPuesto": "Técnico", "cpuAnyoFin": 2007, "cpuAnyoIni": 2006, "cpuGradoConso": "16", "cpuAnyoIngreso": 2006, "cpuEscalaCuerpo": "AGE", "administracionesPublicas": {"aappId": 2, "aappCodigo": "12", "aappAdministracion": "Administración Comunitaria"}}], "formacionAcademica": [{"dpfId": 1665, "facId": 1305, "facAnyoFin": 2006, "facAnyoIni": 2000, "facCentroExped": "UAH", "facNivelEducativo": "Enseñanzas Universitarias", "facTitulacionOficial": "ITIS"}]}', type_data: 'Profile');

puts "- END - NOTIFICATIONS"
