class ChangeAssetsData < ActiveRecord::Migration
  def change
    [:data,:period].each {|column| remove_column :assets_declarations, column}
    remove_reference :assets_declarations, :person
    add_reference :assets_declarations, :temporality, foreign_key: true
    add_reference :assets_declarations, :person_level, polymorphic: true, index: {name: :index_asset_person_l}
    add_column :assets_declarations, :year_period, :integer

    [:vehicles, :real_estate_properties, :other_personal_properties, :other_deposits].each {|table| remove_column table, :purchase_date}
    [:vehicles, :real_estate_properties, :other_personal_properties, :other_deposits].each {|table| add_column table, :year, :integer, null: false}
    [:vehicles, :real_estate_properties, :other_personal_properties, :other_deposits].each {|table| add_column table, :mounth, :integer}

    #VEHICLES#
    add_column :vehicles, :car_registration, :string
    change_column_null :vehicles, :kind, false
    change_column_null :vehicles, :model, false
   
    #REAL ESTATE PROPERTIES#
    [:type, :share, :tax_value, :notes].each {|column| remove_column :real_estate_properties, column}
    [:straight_type, :adquisition_title, :spain_adquisition, :participation_percentage].each {|column| add_column :real_estate_properties, column, :string, null: false }
    add_column :real_estate_properties, :cadastral_value, :float, null: false
    add_column :real_estate_properties, :observations, :text
    change_column_null :real_estate_properties, :municipality, false

    #OTHER PERSONAL PROPERTIES#
    remove_column :other_personal_properties, :kind
    add_column :other_personal_properties, :types, :string, null: false
    add_column :other_personal_properties, :description, :text, null: false
    add_column :other_personal_properties, :value, :float, null: false
    add_column :other_personal_properties, :unit_type, :text

    #ACCOUNT DEPOSITS#
    remove_column :account_deposits, :banking_entity
    add_column :account_deposits, :entity, :string, null: false
    [:kind, :balance].each {|column| change_column_null :account_deposits, column, false}

    #DEBTS#
    [:amount, :comments].each {|column| remove_column :debts, column}
    add_column :debts, :descriptions, :text, null: false
    add_column :debts, :observations, :text
    add_column :debts, :import, :float, null: false
    change_column_null :debts, :kind, false

    #TAX DATA#
    [:tax, :fiscal_data, :amount, :comments].each {|column| remove_column :tax_data, column}
    add_column :tax_data, :irpf_big_import, :float, null: false
    add_column :tax_data, :irpf_big_observation, :text
    add_column :tax_data, :irpf_bi_import, :float, null: false
    add_column :tax_data, :irpf_bi_observation, :text
    add_column :tax_data, :irpf_deduc_import_a, :float
    add_column :tax_data, :irpf_deduc_observation_a, :text
    add_column :tax_data, :irpf_deduc_import_b, :float
    add_column :tax_data, :irpf_deduc_observation_b, :text
    add_column :tax_data, :isp_bi_import, :float
    add_column :tax_data, :isp_bi_observation, :text
    add_column :tax_data, :is_bi_import, :float
    add_column :tax_data, :is_bi_observation, :text

    #OTHER DEPOSITS# 
    [:kind, :amount].each {|column| remove_column :other_deposits, column}
    add_column :other_deposits, :types, :string, null: false

  end
end
