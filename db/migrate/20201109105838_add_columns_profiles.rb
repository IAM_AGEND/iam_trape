class AddColumnsProfiles < ActiveRecord::Migration
  def up
    add_column :profiles, :personal_email, :string
  end

  def down 
    remove_column :profiles, :personal_email
  end
end
