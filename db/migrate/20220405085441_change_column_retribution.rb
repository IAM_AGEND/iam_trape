class ChangeColumnRetribution < ActiveRecord::Migration
  def change
    remove_column :appointments, :retribution
    add_column :appointments, :retribution, :string
  end
end
