class AddEditableDeclaration < ActiveRecord::Migration
  def up
    add_column :assets_declarations, :editable, :boolean, default: false
    add_column :activities_declarations, :editable, :boolean, default: false
  end

  def down
    remove_column :assets_declarations, :editable
    remove_column :activities_declarations, :editable
  end
end
