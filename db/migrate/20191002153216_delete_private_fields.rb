class DeletePrivateFields < ActiveRecord::Migration
  def change
    remove_column :vehicles, :car_registration
    [:description, :spain_adquisition].each { |c| remove_column :real_estate_properties, c}
    remove_column :debts, :descriptions
    remove_column :other_personal_properties, :description

    change_column_null :other_deposits, :description, false
    change_column_null :other_deposits, :unit_type, false

    change_column_null :real_estate_properties, :kind, false
  end
end
