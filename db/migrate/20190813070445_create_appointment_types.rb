class CreateAppointmentTypes < ActiveRecord::Migration
  def change
    create_table :appointment_types do |t|
      t.string :name, index: {unique: true}
      t.timestamps null: false
    end
  end
end
