class AddPolimorphic < ActiveRecord::Migration
  def change
    remove_reference :activities_declarations, :person_level
    add_reference :activities_declarations, :person_level, polymorphic: true, index: {name: :index_activity_person_l}
  end
end
