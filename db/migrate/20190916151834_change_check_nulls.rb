class ChangeCheckNulls < ActiveRecord::Migration
  def change
    change_column_null :profiles, :created_at, true
  end
end
