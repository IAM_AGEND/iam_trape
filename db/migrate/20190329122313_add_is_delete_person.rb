class AddIsDeletePerson < ActiveRecord::Migration
  def change
    add_column :people, :is_delete, :boolean, :default => false
  end
end
