class AddColumnActionAbrev < ActiveRecord::Migration
  def up
    add_column :notifications, :action_abrev, :string
  end

  def down 
    remove_column :notifications, :action_abrev
  end
end
