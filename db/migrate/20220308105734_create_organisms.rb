class CreateOrganisms < ActiveRecord::Migration
  def change
    create_table :organisms do |t|
      t.references :councillors_corporation, foreign_key: true
      t.string :title
      t.timestamps
    end
  end
end
