class AddLogErrorNotifications < ActiveRecord::Migration
  def up
    add_column :notifications, :no_publish_reason, :string
  end

  def down 
    remove_column :notifications, :no_publish_reason
  end
end
