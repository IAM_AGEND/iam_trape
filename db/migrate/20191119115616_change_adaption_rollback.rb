class ChangeAdaptionRollback < ActiveRecord::Migration
  def up
    [:private_activities, :other_activities, :public_activities]. each do |t| 
      change_column t, :start_date, :string
      change_column t, :end_date, :string
    end


  end

  def down
    [:private_activities, :other_activities, :public_activities]. each do |t| 
      change_column t, :start_date, :date
      change_column t, :end_date, :date
    end
  end
end
