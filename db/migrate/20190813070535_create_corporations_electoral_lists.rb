class CreateCorporationsElectoralLists < ActiveRecord::Migration
  def change
    create_table :corporations_electoral_lists do |t|
      t.references :corporation, foreign_key: true
      t.references :electoral_list, foreign_key: true
      t.integer :order, default: 0
      t.timestamps null: false
    end
  end
end
