class CreateOtherPersonalProperties < ActiveRecord::Migration
  def change
    create_table :other_personal_properties do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.date :purchase_date

      t.timestamps null: false
    end
  end
end
