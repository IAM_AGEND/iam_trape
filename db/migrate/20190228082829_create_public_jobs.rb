class CreatePublicJobs < ActiveRecord::Migration
  def change
    create_table :public_jobs do |t|
      t.references :profile, foreign_key: true, null: false
      t.text :description
      t.string :entity
      t.integer :start_year
      t.integer :end_year
      
      t.timestamps null: false
    end
  end
end
