class AddColumsAppoinment < ActiveRecord::Migration
  def up
    add_column :appointments, :politic_group, :string
    add_column :appointments, :district, :string
    add_column :appointments, :corporation, :string
    add_column :appointments, :juntamd, :string
  end

  def down
    remove_column :appointments, :politic_group
    remove_column :appointments, :district
    remove_column :appointments, :corporation
    remove_column :appointments, :juntamd
  end
end
