class ChangeTypeColumns < ActiveRecord::Migration
  def change
    [:public_activities,:private_activities, :other_activities].each do |x|
      remove_column x, :start_date
      remove_column x, :end_date
      add_column x, :start_date, :date
      add_column x, :end_date, :date
    end
  end
end
