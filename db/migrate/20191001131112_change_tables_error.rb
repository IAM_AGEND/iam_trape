class ChangeTablesError < ActiveRecord::Migration
  def change
    [:value, :unit_type].each {|column| remove_column :other_personal_properties, column}

    add_column :other_deposits, :value, :float, null: false
    add_column :other_deposits, :unit_type, :text

  end
end
