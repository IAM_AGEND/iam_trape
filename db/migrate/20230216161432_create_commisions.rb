class CreateCommisions < ActiveRecord::Migration
  def change
    create_table :commisions do |t|
      t.text :commision
      t.references :appointment, foreign_key: true
      t.timestamps
    end
  end
end
