class CreateJobLevels < ActiveRecord::Migration
  def change
    create_table :job_levels do |t|
      t.references :person, foreign_key: true
      t.references :appointment, foreign_key: true
      t.references :profile, foreign_key: true
      t.string :slug, index: {unique: true}, null: false
      t.string :person_type, null: false
      t.timestamps null: false
    end
  end
end
