class CreateCorporations < ActiveRecord::Migration
  def change
    create_table :corporations do |t|
      t.string :name, null: false, index: {unique: true}
      t.date :election_date
      t.integer :start_year, null: false
      t.integer :end_year, null: false
      t.date :full_constitutional_date
      t.date :end_corporation_date
      t.integer :councillors_num, default: 0
      t.text :description
      t.boolean :active, default: true
      
      t.timestamps null: false
    end
  end
end
