class AddPrivateActivities < ActiveRecord::Migration
  def change
    [:private_activity_type].each {|c| remove_reference :private_activities, c}
    [:private_activity_type].each {|c| add_column :private_activities, c, :string}
    
  end
end
