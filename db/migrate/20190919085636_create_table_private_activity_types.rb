class CreateTablePrivateActivityTypes < ActiveRecord::Migration
  def change
    create_table :private_activity_types do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
