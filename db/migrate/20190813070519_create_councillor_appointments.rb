class CreateCouncillorAppointments < ActiveRecord::Migration
  def change
    create_table :councillor_appointments do |t|
      t.references :appointment_type, foreign_key: true
      t.string :name
      t.date :start_date
      t.date :end_date
      t.timestamps null: false
    end
  end
end
