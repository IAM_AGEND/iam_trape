class AddPartiesNew20192023 < ActiveRecord::Migration
  def change
    Party.create!(name: "VOX", logo: "logo_vox.png")
    Party.create!(name: "Más Madrid", logo: "logo_mas_madrid.png")
  end
end
