class AddColumnsLogs < ActiveRecord::Migration
  def up
    add_column :logs, :emails, :jsonb
    add_reference :logs, :log
  end

  def down 
    remove_column :logs, :emails
    remove_reference :logs, :log
  end
end
