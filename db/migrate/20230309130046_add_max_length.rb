class AddMaxLength < ActiveRecord::Migration
  def change
    change_column :charges, :charge, :text, limit: 300
    change_column :commisions, :commision, :text, limit: 300 
  end
end