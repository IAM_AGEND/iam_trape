class AddTaxData < ActiveRecord::Migration
  def change

    [:tax, :fiscal_data].each {|c| add_column :tax_data, c, :string}
  end
end
