class CreateCouncillorsCorporations < ActiveRecord::Migration
  def change
    create_table :councillors_corporations do |t|
      t.references :job_level, foreign_key: true
      t.references :corporation, foreign_key: true
      t.references :electoral_list, foreign_key: true
      t.references :councillor_appointment, foreign_key: true
      t.integer :order_num, default: 0
      
      t.timestamps null: false
    end
  end
end
