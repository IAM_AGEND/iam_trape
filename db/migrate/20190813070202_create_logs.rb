class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.references :administrator, foreign_key: true
      t.string :type
      t.boolean :successful, default: true
      t.string :action
      t.text :log
      t.timestamps null: false
    end
  end
end
