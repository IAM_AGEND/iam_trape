class CreatePrivateActivities < ActiveRecord::Migration
  def change
    create_table :private_activities do |t|
      t.references :activities_declaration, foreign_key: true, null: false
      t.string :kind
      t.text :description
      t.string :entity
      t.string :position
      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
