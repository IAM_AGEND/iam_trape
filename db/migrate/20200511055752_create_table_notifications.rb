class CreateTableNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.jsonb :content_data, default: {}
      t.text :type_data
      t.boolean :permit, :default => false
      t.boolean :ignore, :default => false
      t.datetime :change_state_at
      t.references :administrator, foreign_key: true
      t.timestamps null: false
    end
  end
end
