class CreateDataPreferences < ActiveRecord::Migration
  def change
    create_table :data_preferences do |t|
      t.string :title, null: false
      t.text :content_data
      t.integer :type_data, null: false, default: 0
      t.timestamps null: false
    end
  end
end
