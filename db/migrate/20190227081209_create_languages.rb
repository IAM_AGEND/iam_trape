class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.references :profile, foreign_key: true, null: false
      t.string :name
      t.string :level

      t.timestamps null: false
    end
  end
end
