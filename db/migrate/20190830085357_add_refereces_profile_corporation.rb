class AddReferecesProfileCorporation < ActiveRecord::Migration
  def up
    add_reference :councillors_corporations, :profile, foreign_key: true
  end

  def down
    remove_reference :councillors_corporations, :profile
  end
end
