class RemoveOldSequences < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS areas_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS objectives_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS departments_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS contracts_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS subventions_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS pages_id_seq CASCADE"
    ActiveRecord::Base.connection.execute "DROP SEQUENCE IF EXISTS pg_search_documents_id_seq CASCADE"
  end
end
