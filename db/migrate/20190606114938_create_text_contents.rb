class CreateTextContents < ActiveRecord::Migration
  def change
    create_table :text_contents do |t|
      t.string :section, null: false, unique: true
      t.text :content

      t.timestamps null: false
    end
  end
end
