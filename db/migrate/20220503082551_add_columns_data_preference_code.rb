class AddColumnsDataPreferenceCode < ActiveRecord::Migration
  def up
   add_column :data_preferences, :code, :string
  end

  def down
    remove_column :data_preferences, :code, :string
  end
end
