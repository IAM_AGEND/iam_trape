class CreateDedications < ActiveRecord::Migration
  def change
    create_table :dedications do |t|
      t.references :councillors_corporation, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.integer :percentage
      t.string :description
      t.timestamps
    end
  end
end
