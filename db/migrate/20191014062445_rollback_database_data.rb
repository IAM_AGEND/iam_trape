class RollbackDatabaseData < ActiveRecord::Migration
  def change
    #account_deposits
    [:kind, :balance, :entity].each {|c| change_column_null :account_deposits, c, true}
    
    #appointments
    [:position, :start_date].each {|c| change_column_null :appointments, c, true}

    #courses
    [:start_year, :end_year, :title, :center].each {|c| change_column_null :courses, c, true}
    
    #debts
    [:kind,:import].each {|c| change_column_null :debts, c, true}

    #languages
    [:name,:level].each {|c| add_column :languages, c, :string}
    [:language_name, :language_level].each {|c| remove_reference :languages, c}

    #other_deposits
    [:description, :types, :value, :year].each {|c| change_column_null :other_deposits, c, true}
    [:purchase_date].each{|c| add_column :other_deposits, c,:string}

    #other_personal_properties
    [:types,:year].each {|c| change_column_null :other_personal_properties, c, true}
    [:purchase_date].each{|c| add_column :other_personal_properties, c,:string}

    #political_posts
    [:entity,:start_year, :position].each {|c| change_column_null :political_posts, c, true}
    
    #private_jobs
    [:entity,:start_year, :position].each {|c| change_column_null :private_jobs, c, true}
    
    #public_jobs
    [:start_year, :body_scale, :position, :init_year, :consolidation_degree].each {|c| change_column_null :public_jobs, c, true}
    [:public_administration].each {|c| remove_reference :public_jobs, c}
    [:public_administration].each {|c| add_column :public_jobs, c, :string}
    
    #real_estate_properties
    [:kind, :municipality, :year, :straight_type, :adquisition_title, :participation_percentage, :cadastral_value].each {|c| change_column_null :real_estate_properties, c, true}
    [:purchase_date].each{|c| add_column :real_estate_properties, c,:string}
    
    #studies
    [:start_year, :end_year, :center, :education_level].each {|c| change_column_null :studies, c, true}
    
    #tax_data
    [:irpf_big_import, :irpf_bi_import].each {|c| change_column_null :tax_data, c, true}
    [:import].each{|c| add_column :tax_data, c,:float}
    [:observation].each{|c| add_column :tax_data, c,:string}

    #vehicles
    [:kind, :model,:year].each {|c| change_column_null :vehicles, c, true}
    [:purchase_date].each{|c| add_column :vehicles, c,:string}


  end
end
