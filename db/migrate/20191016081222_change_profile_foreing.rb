class ChangeProfileForeing < ActiveRecord::Migration
  def change
    [:councillors_corporations, :job_levels].each {|tabla| remove_reference tabla, :profile}
    add_reference :people, :profile, foreign_key: true

    rename_column :electoral_lists, :logo_name, :long_name
    add_index :electoral_lists, :long_name, unique: true
  end
end
