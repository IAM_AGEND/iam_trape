class CreateDeleteLogs < ActiveRecord::Migration
  def change
    create_table :delete_logs do |t|
      t.references :administrator, foreign_key: true, null: false
      t.string :action
      t.string :result
      
      t.timestamps null: false
    end
  end
end
