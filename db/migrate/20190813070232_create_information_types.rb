class CreateInformationTypes < ActiveRecord::Migration
  def change
    create_table :information_types do |t|
      t.string :name, index: {unique: true}
      t.timestamps null: false
    end
  end
end
