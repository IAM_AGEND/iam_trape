class AddOrderPeopleTables < ActiveRecord::Migration
  def change
    add_column :studies, :order, :integer
    add_column :courses, :order, :integer
    add_column :languages, :order, :integer
    add_column :public_jobs, :order, :integer
    add_column :private_jobs, :order, :integer
    add_column :political_posts, :order, :integer
    add_column :public_activities, :order, :integer
    add_column :private_activities, :order, :integer
    add_column :other_activities, :order, :integer
    add_column :real_estate_properties, :order, :integer
    add_column :account_deposits, :order, :integer
    add_column :other_deposits, :order, :integer
    add_column :vehicles, :order, :integer
    add_column :other_personal_properties, :order, :integer
    add_column :debts, :order, :integer
    add_column :tax_data, :order, :integer
  end
end
