class CreateDebts < ActiveRecord::Migration
  def change
    create_table :debts do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.float :amount
      t.text :comments
     
      t.timestamps null: false
    end
  end
end
