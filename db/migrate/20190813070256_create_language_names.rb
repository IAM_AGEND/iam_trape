class CreateLanguageNames < ActiveRecord::Migration
  def change
    create_table :language_names do |t|
      t.string :name, index: {unique: true}
      t.timestamps null: false
    end
  end
end
