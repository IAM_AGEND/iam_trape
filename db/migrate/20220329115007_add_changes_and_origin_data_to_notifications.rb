class AddChangesAndOriginDataToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :changes_data, :jsonb
    add_column :notifications, :origin_data, :jsonb
  end
end
