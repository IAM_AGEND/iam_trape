class AddContentSection < ActiveRecord::Migration
  def change
    change_column :text_contents, :content, :text, :limit => 200
    TextContent.create(section: "actual_concillors", content: "Concejales y concejalas de la corporación 2019-2023, ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella")
    TextContent.create(section: "actual_concillors_not_working", content: "En esta página se irán incorporando los concejales y concejalas que cesen a lo largo del mandato")
    TextContent.create(section: "actual_concillors_no_data", content: "")
    TextContent.create(section: "directors", content: "Personal directivo en ejercicio")
    TextContent.create(section: "directors_not_working", content: "Personal directivo que ha cesado. Se muestran sus declaraciones de bienes y actividades durante los dos años siguientes a partir de la fecha de cese")
    TextContent.create(section: "directors_no_data", content: "")
    TextContent.create(section: "public_workers", content: "Personal funcionario del Ayuntamiento de Madrid de nivel 28 o superior que ha accedido al puesto mediante el procedimiento de libre designación")
    TextContent.create(section: "public_workers_no_data", content: "")
    TextContent.create(section: "spokespeople", content: "En esta página se incorporarán los vocales vecinos de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create(section: "spokespeople_no_data", content: "En esta página se incorporarán los vocales vecinos de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create(section: "temporary_workers", content: "En esta página se incorporará el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create(section: "temporary_workers_no_data", content: "En esta página se incorporarán el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create(section: "old_corporation_2015-2019", content: "Concejales/as que han formado parte de la Corporación 2015-2019 ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella. Se muestran sus declaraciones de bienes y actividades durante dos años a partir de la fecha de cese")
    TextContent.create(section: "old_corporation_2015-2019_no_data", content: "")
  end
end
