class ChangeTableAppointment < ActiveRecord::Migration
  def change
    
    remove_column :appointments, :retribution_year
    remove_column :appointments, :retribution
    add_column :appointments, :retribution, :integer
    add_column :appointments, :retribution_year, :integer
    add_column :appointments, :transparency_link, :string
    add_column :appointments, :other_expenses, :string
    add_column :appointments, :gifts, :string
  end
end
