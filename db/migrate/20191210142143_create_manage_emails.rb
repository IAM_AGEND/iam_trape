class CreateManageEmails < ActiveRecord::Migration
  def change
    create_table :manage_emails do |t|
      t.string :type
      t.string :sender
      t.jsonb :fields_cc
      t.jsonb :fields_cco
      t.string :subject
      t.text :email_body
      t.timestamps null: false
    end
  end
end
