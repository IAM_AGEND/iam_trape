class AddConstraintTables < ActiveRecord::Migration
  def change
    add_foreign_key :people, :parties, column: :party_id
    add_foreign_key :activities_declarations, :people, column: :person_id
    add_foreign_key :assets_declarations, :people, column: :person_id
  end
end
