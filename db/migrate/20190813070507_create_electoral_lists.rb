class CreateElectoralLists < ActiveRecord::Migration
  def change
    create_table :electoral_lists do |t|
      t.string :name, index: {unique: true}
      t.string :logo_name
      t.timestamps null: false
    end
    add_attachment :electoral_lists, :logo
  end
end
