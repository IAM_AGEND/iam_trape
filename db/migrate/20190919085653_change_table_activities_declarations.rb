class ChangeTableActivitiesDeclarations < ActiveRecord::Migration
  def change
    [:data,:period].each {|column| remove_column :activities_declarations, column}
    remove_reference :activities_declarations, :person
    add_reference :activities_declarations, :temporality, foreign_key: true
    add_reference :activities_declarations, :person_level, polymorfhic: true
    add_column :activities_declarations, :year_period, :integer
  end
end
