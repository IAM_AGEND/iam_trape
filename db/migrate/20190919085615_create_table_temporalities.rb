class CreateTableTemporalities < ActiveRecord::Migration
  def change
    create_table :temporalities do |t|
      t.string :name
      t.text :description
      t.timestamps null: false
    end
  end
end
