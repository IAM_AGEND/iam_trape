class AddColumnSendNotification < ActiveRecord::Migration
  def up
    add_column :notifications, :send_feedback, :boolean, :default => false
  end

  def down
    remove_column :notifications, :send_feedback
  end
end
