class ReloadTaxDatas < ActiveRecord::Migration
  def change
    drop_table :tax_datas
    create_table :tax_datas do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :tax
      t.string :fiscal_data
      t.float :amount
      t.text :comments

      t.timestamps null: false
    end
  end
end
