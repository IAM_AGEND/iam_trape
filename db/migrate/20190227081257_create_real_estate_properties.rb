class CreateRealEstateProperties < ActiveRecord::Migration
  def change
    create_table :real_estate_properties do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.string :type
      t.text :description
      t.string :municipality
      t.integer :share
      t.date :purchase_date
      t.float :tax_value
      t.text :notes

      t.timestamps null: false
    end
  end
end
