class CreateOtherDeposits < ActiveRecord::Migration
  def change
    create_table :other_deposits do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.text :description
      t.float :amount
      t.date :purchase_date

      t.timestamps null: false
    end
  end
end
