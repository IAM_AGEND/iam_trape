class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.string :model
      t.date :purchase_date

      t.timestamps null: false
    end
  end
end
