class TriggerDestroyProfile < ActiveRecord::Migration
  def up
    execute %{
      DROP TRIGGER IF EXISTS delete_profiles ON profiles;
      DROP FUNCTION IF EXISTS profiles_references ();
      
      CREATE FUNCTION profiles_references () RETURNS OPAQUE AS '
      BEGIN
        UPDATE people SET profile_id = NULL WHERE profile_id=OLD.id;
          
        RETURN OLD;
      END;
      ' LANGUAGE 'plpgsql';

      CREATE TRIGGER delete_profiles BEFORE DELETE ON profiles FOR EACH ROW
        EXECUTE PROCEDURE profiles_references();
    }
  end

  def down
    execute %{
      DROP TRIGGER IF EXISTS delete_profiles ON profiles;
      DROP FUNCTION IF EXISTS profiles_references ();
    }
  end
end
