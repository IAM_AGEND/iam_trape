class RemoveFieldsFromJobLevelsAndCouncillorsCorporation < ActiveRecord::Migration
  def change
    remove_column :job_levels, :authorization_date
    remove_column :job_levels, :authorization_description
    remove_column :job_levels, :resolution_date
    remove_column :job_levels, :compatibility_description
    remove_column :job_levels, :public_private
    remove_column :councillors_corporations, :authorization_date
    remove_column :councillors_corporations, :authorization_description
    remove_column :councillors_corporations, :resolution_date
    remove_column :councillors_corporations, :compatibility_description
    remove_column :councillors_corporations, :public_private
  end
end
