class ChangeLimitText < ActiveRecord::Migration
  def change
    change_column :text_contents, :content, :string, :limit => 250
  end
end
