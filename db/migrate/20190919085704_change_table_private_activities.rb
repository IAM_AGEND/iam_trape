class ChangeTablePrivateActivities < ActiveRecord::Migration
  def change
    [:kind].each {|column| remove_column :private_activities, column}
    add_reference :private_activities, :private_activity_type, foreign_key: true
  end
end
