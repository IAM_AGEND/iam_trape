class ChangeOtherNullAppointment < ActiveRecord::Migration
  def change
    change_column_null :appointments, :unit, true
  end
end
