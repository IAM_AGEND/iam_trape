class AddColumnsAppointmentConcillors < ActiveRecord::Migration
  def up
    add_column :appointments, :url_form, :string
  end

  def down
    remove_column :appointments, :url_form
  end
end
