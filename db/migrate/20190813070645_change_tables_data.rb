class ChangeTablesData < ActiveRecord::Migration
  def up
    # Languages
    [:name,:level].each {|column| remove_column :languages, column}
    [:language_name,:language_level].each {|column| add_reference :languages, column, foreign_key: true}
    
    # Studies
    [:description,:entity].each {|column| remove_column :studies, column}
    add_column :studies, :official_degree, :string
    add_column :studies, :center, :string, null: false
    add_column :studies, :education_level, :string, null: false
    change_column_null :studies, :start_year, false
    change_column_null :studies, :end_year, false

    # Courses
    [:description,:entity].each {|column| remove_column :courses, column}
    add_column :courses, :title, :string, null: false
    add_column :courses, :center, :string, null: false
    change_column_null :courses, :start_year, false
    change_column_null :courses, :end_year, false

    # Political posts
    [:description].each {|column| remove_column :political_posts, column}
    add_column :political_posts, :position, :string, null: false
    change_column_null :political_posts, :start_year, false
    change_column_null :political_posts, :entity, false

    # Private jobs
    [:description].each {|column| remove_column :private_jobs, column}
    add_column :private_jobs, :position, :string, null: false
    change_column_null :private_jobs, :start_year, false
    change_column_null :private_jobs, :entity, false

    # Public jobs
    [:description,:entity].each {|column| remove_column :public_jobs, column}
    add_reference :public_jobs, :public_administration, foreign_key: true, null: false
    add_column :public_jobs, :body_scale, :string, null: false
    add_column :public_jobs, :position, :string, null: false
    add_column :public_jobs, :init_year, :integer, null: false
    change_column_null :public_jobs, :start_year, false
    add_column :public_jobs, :consolidation_degree, :string, null: false


    # Profile
    [:studies_comments,:courses_comments,:career_comments,:public_jobs_level,
    :public_jobs_body,:public_jobs_start_year,:political_posts_comment,
    :teaching_activity,:publications,:other].each {|column| remove_column :profiles, column}
    [:person].each {|refer| remove_reference :profiles, refer }
    add_column :profiles, :email, :string
    add_column :profiles, :twitter, :string
    add_column :profiles, :facebook, :string
    add_column :profiles, :calendar_url, :string
    add_attachment :profiles, :portrait


    # People
    [:email,:role,:functions,:profile,:twitter,:facebook,:diary,:councillor_code,
    :slug,:profiled_at,:unit, :area,:district, :job_level,:first_name,:last_name,
    :sorting_name,:previous_calendar_until, :previous_calendar_url, :admin_first_name, 
    :admin_last_name, :leaving_date, :councillor_order, :starting_date, :secondary_role, 
    :hidden_at, :hidden_reason, :unhidden_reason, :unhidden_at, :is_delete, :corporation
    ].each {|column| remove_column :people, column}
    [:party, :hidden_by, :unhidden_by].each {|refer| remove_reference :people, refer }
    remove_attachment :people, :portrait
    add_column :people, :name, :string
    add_column :people, :last_name, :string
    add_column :people, :last_name_alt, :string
    add_column :people, :document_nif, :string, index: {unique: true}
    add_reference :people, :document_type, foreign_key: true
    add_column :people, :sex, :string
  end

  def down
    # Languages
    [:name,:level].each {|column| add_column :languages, column, :string}
    [:language_name,:language_level].each {|column| remove_reference :languages, column}
    
    # Studies
    [:description,:entity].each {|column| add_column :studies, column, :string}
    remove_column :studies, :official_degree
    remove_column :studies, :center
    remove_column :studies, :education_level
    change_column_null :studies, :start_year, true
    change_column_null :studies, :end_year, true

    # Courses
    [:description,:entity].each {|column| add_column :courses, column, :string}
    remove_column :courses, :title
    remove_column :courses, :center
    change_column_null :courses, :start_year, true
    change_column_null :courses, :end_year, true

    # Political posts
    [:description].each {|column| add_column :political_posts, column, :string}
    remove_column :political_posts, :position
    change_column_null :political_posts, :start_year, true
    change_column_null :political_posts, :entity, true

    # Private jobs
    [:description].each {|column| add_column :private_jobs, column, :string}
    remove_column :private_jobs, :position
    change_column_null :private_jobs, :start_year, true
    change_column_null :private_jobs, :entity, true

    # Public jobs
    [:description,:entity].each {|column| add_column :public_jobs, column, :string}
    remove_reference :public_jobs, :public_administration
    remove_column :public_jobs, :body_scale
    remove_column :public_jobs, :position
    remove_column :public_jobs, :init_year
    change_column_null :public_jobs, :start_year, true
    remove_column :public_jobs, :consolidation_degree


    # Profile
    [:studies_comments,:courses_comments,:career_comments,:public_jobs_level,
    :public_jobs_body,:political_posts_comment, :teaching_activity,:publications,
    :other].each {|column| add_column :profiles, column, :text}
    [:person].each {|refer| add_reference :profiles, refer, foreign_key: true}
    add_column :profiles, :public_jobs_start_year, :integer
    remove_column :profiles, :email
    remove_column :profiles, :twitter
    remove_column :profiles, :facebook
    remove_column :profiles, :calendar_url
    remove_attachment :profiles, :portrait


    # People
    [:email,:role,:functions,:profile,:twitter,:facebook,:diary,:councillor_code,
    :slug,:unit, :area,:district, :job_level,:first_name,:last_name,
    :sorting_name,:previous_calendar_until, :previous_calendar_url, :admin_first_name, 
    :admin_last_name, :secondary_role, :hidden_reason, :unhidden_reason, :corporation
    ].each {|column| add_column :people, column, :string}
    [:party, :hidden_by, :unhidden_by].each {|refer| add_reference :people, refer, foreign_key: true }
    add_attachment :people, :portrait
    [:starting_date, :portrait_updated_at, :profiled_at, :unhidden_at, :hidden_at,:leaving_date].each {|col| add_column :people, col, :date}
    add_column :people,  :councillor_order, :integer
    add_column :people,  :is_delete,:boolean, :default => false
    remove_column :people, :name
    remove_column :people, :last_name
    remove_column :people, :last_name_alt
    remove_column :people, :document_nif
    remove_reference :people, :document_type
    remove_column :people, :sex
    change_column_null :people, :personal_code, true
  end
end
