class CreateCompatibilityActivities < ActiveRecord::Migration
  def change
    create_table :compatibility_activities do |t|
      t.datetime :resolution_date
      t.text :compatibility_description
      t.string :public_private
      t.references :person_level, polymorphic: true, index: {name: :index_compatibility_activities_person_l}
      t.timestamps
    end
  end
end
