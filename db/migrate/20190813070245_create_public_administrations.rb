class CreatePublicAdministrations < ActiveRecord::Migration
  def change
    create_table :public_administrations do |t|
      t.string :name 
      t.string :administration_code, index: {unique: true}
      t.timestamps null: false
    end
  end
end
