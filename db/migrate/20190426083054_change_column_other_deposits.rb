class ChangeColumnOtherDeposits < ActiveRecord::Migration
  def change
    change_column :other_deposits, :amount, :text
  end
end
