class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.text :charge
      t.references :appointment, foreign_key: true
      t.timestamps
    end
  end
end
