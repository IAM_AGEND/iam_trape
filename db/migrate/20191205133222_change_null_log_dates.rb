class ChangeNullLogDates < ActiveRecord::Migration
  def change
    change_column_null :logs, :created_at, true
    change_column_null :logs, :updated_at, true
  end
end
