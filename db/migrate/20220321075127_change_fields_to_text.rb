class ChangeFieldsToText < ActiveRecord::Migration
  def change 
    add_column :appointments, :possession_date, :datetime
    add_column :appointments, :description_possession, :string
    add_column :appointments, :description, :string
    add_column :appointments, :email, :string
    add_column :appointments, :url_possession, :string
    add_column :appointments, :functions, :string    
    add_column :appointments, :observations, :string

    add_column :job_levels, :resolution_date, :datetime
    add_column :job_levels, :compatibility_description, :string
    add_column :job_levels, :public_private, :string
    add_column :job_levels, :authorization_date, :datetime
    add_column :job_levels, :authorization_description, :string

    add_column :councillors_corporations, :resolution_date, :datetime
    add_column :councillors_corporations, :compatibility_description, :string
    add_column :councillors_corporations, :public_private, :string
    add_column :councillors_corporations, :authorization_date, :datetime
    add_column :councillors_corporations, :authorization_description, :string


    change_column :dedications, :description, :text
    change_column :appointments, :other_expenses, :text
    change_column :appointments, :gifts, :text
    change_column :appointments, :description_possession, :text
    change_column :appointments, :description, :text
    change_column :appointments, :observations, :text
    change_column :job_levels, :compatibility_description, :text
    change_column :job_levels, :authorization_description, :text
    change_column :councillors_corporations, :compatibility_description, :text
    change_column :councillors_corporations, :authorization_description, :text
  end
end
