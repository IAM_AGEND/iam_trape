class ChangeTablesPeople < ActiveRecord::Migration
  def up
    remove_column :people, :calendar_url
    remove_column :profiles, :special_mentions
  end

  def down
    add_column :people, :calendar_url, :string
    add_column :profiles, :special_mentions, :text
  end
end
