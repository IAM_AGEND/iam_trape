class DeleteColumnsOld < ActiveRecord::Migration
  def change
    remove_column :other_deposits, :unit_type
    change_column :other_deposits, :value, :string
  end
end
