class AddForwardColumnForNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :forwards, :integer, default: 0
  end
end
