class CreateAccountDeposits < ActiveRecord::Migration
  def change
    create_table :account_deposits do |t|
      t.references :assets_declaration, foreign_key: true, null: false
      t.string :kind
      t.string :banking_entity
      t.float :balance
     
      t.timestamps null: false
    end
  end
end
