class RemoveOldTables < ActiveRecord::Migration
  def up
    table_exists?(:pg_search_documents) ? drop_table(:pg_search_documents) : nil
    table_exists?(:pages) ? drop_table(:pages) : nil
    table_exists?(:subventions) ? drop_table(:subventions) : nil
    table_exists?(:contracts) ? drop_table(:contracts) : nil
    table_exists?(:objectives) ? drop_table(:objectives) : nil
    table_exists?(:departments) ? drop_table(:departments) : nil
    table_exists?(:areas) ? drop_table(:areas) : nil
  end
end
