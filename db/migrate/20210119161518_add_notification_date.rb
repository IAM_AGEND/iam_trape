class AddNotificationDate < ActiveRecord::Migration
  def up
    add_column :notifications, :action, :string
    add_column :notifications, :date_action, :datetime
  end

  def down 
    remove_column :notifications, :action
    remove_column :notifications, :date_action
  end
end
