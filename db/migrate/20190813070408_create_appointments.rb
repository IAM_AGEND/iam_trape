class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :area
      t.string :position, null: false
      t.string :unit, null: false
      t.date :start_date, null: false
      t.date :end_date     
      
      t.timestamps null: false
    end
  end
end
