class CreateInformation < ActiveRecord::Migration
  def change
    create_table :information do |t|
      t.references :profile, foreign_key: true
      t.references :information_type, foreign_key: true
      t.text :information
      t.date :information_at
      t.timestamps null: false
    end
  end
end
