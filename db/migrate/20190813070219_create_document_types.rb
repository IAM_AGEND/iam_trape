class CreateDocumentTypes < ActiveRecord::Migration
  def change
    create_table :document_types do |t|
      t.string :name, index: {unique: true}
      t.timestamps null: false
    end
  end
end
