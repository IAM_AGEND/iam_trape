class CreateLanguageLevels < ActiveRecord::Migration
  def change
    create_table :language_levels do |t|
      t.string :level, index: {unique: true}
      t.timestamps null: false
    end
  end
end
