class CreateTerminationAuthorizations < ActiveRecord::Migration
  def change
    create_table :termination_authorizations do |t|
      t.datetime :authorization_date
      t.text :authorization_description
      t.references :person_level, polymorphic: true, index: {name: :index_termination_authorizations_person_l}
      t.timestamps
    end
  end
end
