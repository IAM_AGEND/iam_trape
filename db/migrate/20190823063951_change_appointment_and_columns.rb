class ChangeAppointmentAndColumns < ActiveRecord::Migration
  def change
    remove_reference :councillors_corporations, :councillor_appointment
    add_reference :councillors_corporations, :appointment, foreign_key: true
    table_exists?(:councillor_appointments) ? drop_table(:councillor_appointments) : nil
    table_exists?(:appointment_types) ? drop_table(:appointment_types) : nil
    add_column :appointments, :position_alt, :string
    remove_column :people, :last_name_alt
  end
end
