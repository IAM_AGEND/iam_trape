class AddPermissionsAllTable < ActiveRecord::Migration
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE information_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE information_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE information_id_seq TO transparencia_read;

    ALTER TABLE information
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE information TO transparencia_owner;
GRANT SELECT ON TABLE information TO transparencia_read;

ALTER TABLE appointments_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE appointments_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE appointments_id_seq TO transparencia_read;

    ALTER TABLE appointments
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE appointments TO transparencia_owner;
GRANT SELECT ON TABLE appointments TO transparencia_read;


ALTER TABLE corporations_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE corporations_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE corporations_id_seq TO transparencia_read;

    ALTER TABLE corporations
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE corporations TO transparencia_owner;
GRANT SELECT ON TABLE corporations TO transparencia_read;


ALTER TABLE corporations_electoral_lists_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE corporations_electoral_lists_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE corporations_electoral_lists_id_seq TO transparencia_read;

    ALTER TABLE corporations_electoral_lists
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE corporations_electoral_lists TO transparencia_owner;
GRANT SELECT ON TABLE corporations_electoral_lists TO transparencia_read;

ALTER TABLE councillors_corporations_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE councillors_corporations_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE councillors_corporations_id_seq TO transparencia_read;

    ALTER TABLE councillors_corporations
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE councillors_corporations TO transparencia_owner;
GRANT SELECT ON TABLE councillors_corporations TO transparencia_read;

ALTER TABLE data_preferences_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE data_preferences_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE data_preferences_id_seq TO transparencia_read;

    ALTER TABLE data_preferences
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE data_preferences TO transparencia_owner;
GRANT SELECT ON TABLE data_preferences TO transparencia_read;

ALTER TABLE document_types_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE document_types_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE document_types_id_seq TO transparencia_read;

    ALTER TABLE document_types
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE document_types TO transparencia_owner;
GRANT SELECT ON TABLE document_types TO transparencia_read;

ALTER TABLE duties_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE duties_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE duties_id_seq TO transparencia_read;

    ALTER TABLE duties
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE duties TO transparencia_owner;
GRANT SELECT ON TABLE duties TO transparencia_read;
      }

    end
  end
end
