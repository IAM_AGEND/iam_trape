class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :person, foreign_key: true, null: false
      t.text :studies_comments
      t.text :courses_comments
      t.text :career_comments
      t.integer :public_jobs_level
      t.text :public_jobs_body
      t.integer :public_jobs_start_year
      t.text :political_posts_comment
      t.text :teaching_activity
      t.text :publications
      t.text :special_mentions
      t.text :other

      t.timestamps null: false
    end
  end
end
