class AddGrant < ActiveRecord::Migration
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE notifications_id_seq
  OWNER TO transparencia_owner;
GRANT ALL ON SEQUENCE notifications_id_seq TO transparencia_owner;
GRANT USAGE ON SEQUENCE notifications_id_seq TO transparencia_read;

    ALTER TABLE notifications
  OWNER TO transparencia_owner;
GRANT ALL ON TABLE notifications TO transparencia_owner;
GRANT SELECT ON TABLE notifications TO transparencia_read;

      }

    end

  end
end
