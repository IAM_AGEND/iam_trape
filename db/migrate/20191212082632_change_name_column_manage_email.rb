class ChangeNameColumnManageEmail < ActiveRecord::Migration
  def change

    rename_column :manage_emails, :type, :type_data
  end
end
