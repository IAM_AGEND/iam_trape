class ChangeCheckAuditsAdministrator < ActiveRecord::Migration
  def change
    change_column :audits, :administrator_id, :integer, null: true
  end
end
