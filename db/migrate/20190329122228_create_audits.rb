class CreateAudits < ActiveRecord::Migration
  def change
    create_table :audits do |t|
      t.references :person, foreign_key: true, null: false
      t.references :administrator, foreign_key: true, null: false
      t.string :action
      t.text :description
      t.timestamps null: false
    end
  end
end
