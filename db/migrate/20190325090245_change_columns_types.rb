class ChangeColumnsTypes < ActiveRecord::Migration
  def change
    change_column :public_activities, :start_date, :text
    change_column :public_activities, :end_date, :text
    change_column :private_activities, :start_date, :text
    change_column :private_activities, :end_date, :text
    change_column :other_activities, :start_date, :text
    change_column :other_activities, :end_date, :text
    change_column :real_estate_properties, :purchase_date, :text
    change_column :other_personal_properties, :purchase_date, :text
    change_column :other_deposits, :purchase_date, :text
    change_column :vehicles, :purchase_date, :text
  end
end
