class CreateDuties < ActiveRecord::Migration
  def change
    create_table :duties do |t|
      t.references :declaration, polymorphic: true, index: true
      t.references :person_level, polymorphic: true, index: true
      t.date :start_date
      t.date :limit_date
      t.date :end_date
      t.string :type_duty
      t.string :year
    end
  end
end
