class AddCumplimentedProfile < ActiveRecord::Migration
  def up
    add_column :profiles, :cumplimented, :boolean, default: false
  end

  def remove
    add_column :profiles, :cumplimented, :boolean
  end
end
