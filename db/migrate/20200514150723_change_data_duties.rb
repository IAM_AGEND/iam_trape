class ChangeDataDuties < ActiveRecord::Migration
  def change
    add_reference :duties, :temporality, foreign_key: true
  end
end
