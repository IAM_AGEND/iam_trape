class ChangeOrderTable < ActiveRecord::Migration
  def change
    rename_column :studies, :order, :table_order
    rename_column :courses, :order, :table_order
    rename_column :languages, :order, :table_order
    rename_column :public_jobs, :order, :table_order
    rename_column :private_jobs, :order, :table_order
    rename_column :political_posts, :order, :table_order
    rename_column :public_activities, :order, :table_order
    rename_column :private_activities, :order, :table_order
    rename_column :other_activities, :order, :table_order
    rename_column :real_estate_properties, :order, :table_order
    rename_column :account_deposits, :order, :table_order
    rename_column :other_deposits, :order, :table_order
    rename_column :vehicles, :order, :table_order
    rename_column :other_personal_properties, :order, :table_order
    rename_column :debts, :order, :table_order
    rename_column :tax_data, :order, :table_order
    rename_column :information, :order, :table_order
  end
end
