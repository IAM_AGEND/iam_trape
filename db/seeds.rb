# Default admin user (change password after first deploy to a server!)
if !Rails.env.test?
  if Administrator.count == 0
    Administrator.create!(email: 'admin@madrid.es', password: Rails.application.secrets.password)
  end

  if LanguageName.count == 0
    # Language names
    [
      "Italiano", "Portugués", "Japonés", "Alemán", "Ruso", "Árabe", "Francés", 
      "Chino Mandarín", "Español", "Inglés", "Rumano", "Catalán", "Euskera", 
      "Gallego", "Hindi", "Coreano", "Valenciano", "Indonesio", "Chino"
    ].each {|name| LanguageName.create!(name: name)}
  end

  if LanguageLevel.count == 0
    # Language levels
    [
      "A - Usuario básico", "B - Usuario intermedio", "C - Usuario competente"
    ].each {|level| LanguageLevel.create!(level: level)}
  end

  if InformationType.count == 0
    #########################################################################
    # Information Types
    #########################################################################
    # Otros comentarios sobre formación académica => studies_comments
    # Otros comentarios sobre cursos => courses_comments
    # Otros detalles sobre la carrera profesional => career_comments
    # Otros comentarios sobre cargos políticos => political_posts_comments
    # Publicación => publications
    # Actividad docente => theacher_activity
    # Distinciones honoríficas y otros méritos => special_mentions
    # Otra información de interés => other
    [
      "studies_comments", "courses_comments", "career_comments", "political_posts_comments",
      "publications", "theacher_activity", "special_mentions", "other"
    ].each {|name| InformationType.create!(name: name)}
  end

  if PublicAdministration.count == 0
    # Public administrations
    [
      ["AC", "Adm. Comunitaria"], ["AG", "Adm. Gen Estado"], ["AL", "Adm. Local"],
      ["AR", "C.A. ARAGÓN"], ["AS", "C.A. P.ASTURIAS"], ["AY", "Ayto. Madrid"],
      ["CA", "Com. Autónoma"], ["CB", "C.A. CANTABRIA"], ["CC", "CIUDAD A. CEUTA"],
      ["CL", "C.A. CAS.-LEÓN"], ["CM", "C.A. C-LA MANCHA"], ["CV", "C. VALENCIANA"],
      ["EX", "C. A. EXTREMAD."], ["GC", "C.A. CATALUÑA"], ["IB", "C.A. I. BALEARES"],
      ["IC", "C.A. I. CANARIAS"], ["JA", "C.A. ANDALUCÍA"], ["MA", "C. DE MADRID"],
      ["ME", "CIUD. A. MELILLA"], ["MU", "C.A. MURCIA"], ["NA", "C.F. NAVARRA"],
      ["OA", "Org. Autónomo"], ["OI", "Org. Internac."], ["OT", "Otros"],
      ["PV", "C.A. PAÍS VASCO"], ["RI", "C.A. LA RIOJA"], ["XG", "C.A. GALICIA"]
    ].each {|pubad| PublicAdministration.create!(administration_code: pubad[0],name: pubad[1])}
  end

  if TextContent.count == 0
    # Text contents
    TextContent.create!(section: "actual_concillors", content: "Concejales y concejalas de la corporación 2019-2023, ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella")
    TextContent.create!(section: "actual_concillors_not_working", content: "En esta página se irán incorporando los concejales y concejalas que cesen a lo largo del mandato")
    TextContent.create!(section: "actual_concillors_no_data", content: "")
    TextContent.create!(section: "directors", content: "Personal directivo en ejercicio")
    TextContent.create!(section: "directors_not_working", content: "Personal directivo que ha cesado. Se muestran sus declaraciones de bienes y actividades durante los dos años siguientes a partir de la fecha de cese")
    TextContent.create!(section: "directors_no_data", content: "")
    TextContent.create!(section: "public_workers", content: "Personal funcionario del Ayuntamiento de Madrid de nivel 28 o superior que ha accedido al puesto mediante el procedimiento de libre designación")
    TextContent.create!(section: "public_workers_no_data", content: "")
    TextContent.create!(section: "spokespeople", content: "En esta página se incorporarán los vocales vecinos de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create!(section: "spokespeople_no_data", content: "")
    TextContent.create!(section: "temporary_workers", content: "En esta página se incorporará el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create!(section: "temporary_workers_no_data", content: "En esta página se incorporarán el personal eventual de la Corporación 2019-2023 a medida que se efectúen los nombramientos")
    TextContent.create!(section: "old_corporation_2015-2019", content: "Concejales y concejalas que han formado parte de la Corporación 2015-2019 ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella. Se muestran sus declarac")
    TextContent.create!(section: "old_corporation_2015-2019_no_data", content: "Concejales y concejalas que han formado parte de la Corporación 2015-2019 ordenados según el número de concejales alcanzados por cada candidatura electoral y su orden en ella.  ")
  end

  if DocumentType.count == 0
    # Document types
    ["Tarjeta Residencia", "DNI", "PASAPORTE", "NIE"].each {|name| DocumentType.create!(name: name)}
  end

  if PrivateActivityType.count == 0
    ["Actividades mercantiles o industriales", "Actividades y ocupaciones profesionales", "Actividades por cuenta ajena"].each do |pa|
      PrivateActivityType.create!(name: pa)
    end
  end

  if Temporality.count == 0
    ["Inicial", "Final", "Anual"].each do |temp|
      Temporality.create!(name: temp)
    end
  end

 
  DataPreference.find_or_create_by(title: "Años publicados", type_data: 0, content_data: 2, code: "public_year")
  DataPreference.find_or_create_by(title: "Número declarciones anuales", type_data: 0, content_data: 5, code: "num_year_declaration")
  DataPreference.find_or_create_by(title: "Enlace Información sobre retribucciones", type_data: 1,code: "link_information_retribuction", content_data: "https://transparencia.madrid.es/portales/transparencia/es/Recursos-humanos/Retribuciones/Retribuciones-del-alcalde-concejales-cargos-directivos-y-personal-eventual/?vgnextfmt=default&vgnextoid=a9687bcf66b06310VgnVCM1000000b205a0aRCRD&vgnextchannel=0d59508929a56510VgnVCM1000008a4a900aRCRD")
  DataPreference.find_or_create_by(title: "Número reintentos notificaciones", type_data: 0, content_data: 5, code: "num_notification_reintent")
  DataPreference.find_or_create_by(title: "Número reintentos notificaciones manuales", type_data: 0, content_data: 5, code: "num_notification_reintent_manual")
  DataPreference.find_or_create_by(title: "Umbral notificaciones pendientes", type_data: 0, content_data: 0, code: "notification_pending_alert")
  DataPreference.find_or_create_by(title: "Número de envíos diarios", type_data: 1, content_data: 1, code: "num_send_retry_diary")
  DataPreference.find_or_create_by(title: "Destinatarios alertas", type_data: 1, content_data: "", code: "alert_senders")
  DataPreference.find_or_create_by(title: "Años de publicación de concejales", type_data: 0, content_data: "4", code: "councillors_publication_year")
  DataPreference.find_or_create_by(title: "Horas para la exportación de archivos", type_data: 0, content_data: "24", code: "hour_export_data")
  DataPreference.find_or_create_by(title: "Horas de ejecución tarea", type_data: 0, content_data: "24", code: "hour_exec_data")
  DataPreference.find_or_create_by(title: "Fecha de obligaciones", type_data: 3, content_data: "2020-06-12", code: "dutie_date")

  
  
  data_json_cc= {"councillors" => "transparenciaydatos@madrid.es", "directors" => "transparenciaydatos@madrid.es", 
    "public_workers" => "transparenciaydatos@madrid.es", "temporary_workers" => "transparenciaydatos@madrid.es",
    "spokespeople" => "transparenciaydatos@madrid.es" }
  data_json_cco = {"councillors" => "creaspodhe@madrid.es", "directors" => "creaspodhe@madrid.es", 
    "public_workers" => "creaspodhe@madrid.es", "temporary_workers" => "creaspodhe@madrid.es",
    "spokespeople" => "creaspodhe@madrid.es" }
  email = ManageEmail.find_by(type_data: "ProfilesUpload")
  if email.blank?
    ManageEmail.create!(type_data: "ProfilesUpload", sender: "transparenciaydatos@madrid.es",
      fields_cc: data_json_cc, 
      fields_cco: data_json_cco,
      subject: I18n.t("emails.subject.profiles_upload"), 
      email_body: I18n.t("emails.body.profiles_upload")) 
  end
  email = ManageEmail.find_by(type_data: "AssetsUpload")
  if email.blank?
    ManageEmail.create!(type_data: 'AssetsUpload', sender: "transparenciaydatos@madrid.es",
      fields_cc: data_json_cc, 
      fields_cco: data_json_cco,
      subject: I18n.t("emails.subject.assets_upload"), 
      email_body: I18n.t("emails.body.assets_upload")) 
  end
  email = ManageEmail.find_by(type_data: "ActivitiesUpload")
  if email.blank?
    ManageEmail.create!(type_data: 'ActivitiesUpload', sender: "transparenciaydatos@madrid.es",
      fields_cc: data_json_cc, 
      fields_cco: data_json_cco,
      subject: I18n.t("emails.subject.activities_upload"), 
      email_body: I18n.t("emails.body.activities_upload")) 
  end
 
end