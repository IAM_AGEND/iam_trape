class ProfileExporter
  #personal_code
  FIELDS = %w{
    profiled_at
    name
    last_name
    position
    unit
    twitter
    facebook
    first_study_official_degree
    first_study_center
    first_study_start_year
    first_study_end_year
    second_study_official_degree
    second_study_center
    second_study_start_year
    second_study_end_year
    third_study_official_degree
    third_study_center
    third_study_start_year
    third_study_end_year
    fourth_study_official_degree
    fourth_study_center
    fourth_study_start_year
    fourth_study_end_year
    studies_comments
    first_course_title
    first_course_center
    first_course_start_year
    first_course_end_year
    second_course_title
    second_course_center
    second_course_start_year
    second_course_end_year
    third_course_title
    third_course_center
    third_course_start_year
    third_course_end_year
    fourth_course_title
    fourth_course_center
    fourth_course_start_year
    fourth_course_end_year
    courses_comments
    language_english_level
    language_french_level
    language_german_level
    language_italian_level
    language_other_name
    language_other_level
    public_jobs_body
    public_jobs_start_year
    first_public_job_position
    first_public_job_public_administration
    first_public_job_start_year
    first_public_job_end_year
    second_public_job_position
    second_public_job_public_administration
    second_public_job_start_year
    second_public_job_end_year
    third_public_job_position
    third_public_job_public_administration
    third_public_job_start_year
    third_public_job_end_year
    fourth_public_job_position
    fourth_public_job_public_administration
    fourth_public_job_start_year
    fourth_public_job_end_year
    public_jobs_level
    first_private_job_position
    first_private_job_entity
    first_private_job_start_year
    first_private_job_end_year
    second_private_job_position
    second_private_job_entity
    second_private_job_start_year
    second_private_job_end_year
    third_private_job_position
    third_private_job_entity
    third_private_job_start_year
    third_private_job_end_year
    fourth_private_job_position
    fourth_private_job_entity
    fourth_private_job_start_year
    fourth_private_job_end_year
    career_comments
    first_political_post_position
    first_political_post_entity
    first_political_post_start_year
    first_political_post_end_year
    second_political_post_position
    second_political_post_entity
    second_political_post_start_year
    second_political_post_end_year
    third_political_post_position
    third_political_post_entity
    third_political_post_start_year
    third_political_post_end_year
    fourth_political_post_position
    fourth_political_post_entity
    fourth_political_post_start_year
    fourth_political_post_end_year
    political_posts_comments
    publications
    theacher_activity
    special_mentions
    other
    job_level_code
  }



  def headers
    FIELDS.map { |f| I18n.t("profile_exporter.#{f}") }
  end

  def person_to_row(job)
    FIELDS.map do |f|

      job.send(f)
    end
  end

  def windows_headers
    windows_array headers
  end

  def windows_person_row(job)
    windows_array person_to_row(job)
  end

  def save_csv(path)
    CSV.open(path, 'w', col_sep: ';', force_quotes: true, encoding: "ISO-8859-1") do |csv|
      csv << windows_headers
      porcentage = 0
      max_count = JobLevel.working.count + JobLevel.councillors_working(Corporation.active.first).count
      print "\r#{porcentage} / #{max_count}"

      JobLevel.working.find_each do |job|
        csv << windows_person_row(job)
        porcentage = porcentage + 1
        print "\r#{porcentage} / #{max_count}"
      end
      JobLevel.councillors_working(Corporation.active.first).find_each do |job|
        csv << windows_person_row(job)
        porcentage = porcentage + 1
        print "\r#{porcentage} / #{max_count}"
      end
    end
  end

  def save_xls(path)
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet
    sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    sheet.row(0).concat headers
    index = 1
    porcentage = 0
    max_count = JobLevel.working.count + JobLevel.councillors_working(Corporation.active.first).count
    print "\r#{porcentage} / #{max_count}"
    JobLevel.working.find_each do |job|
      sheet.row(index).concat person_to_row(job)
      index += 1
      porcentage = porcentage + 1
      print "\r#{porcentage} / #{max_count}"
    end
    JobLevel.councillors_working(Corporation.active.first).find_each do |job|
      sheet.row(index).concat person_to_row(job)
      index += 1
      porcentage = porcentage + 1
      print "\r#{porcentage} / #{max_count}"
    end

    book.write(path)
  end

  def save_json(path)
    data = []
    h = headers
    porcentage = 0
    max_count = JobLevel.working.count + JobLevel.councillors_working(Corporation.active.first).count
    print "\r#{porcentage} / #{max_count}"
    JobLevel.working.find_each do |job|
      data << h.zip(person_to_row(job)).to_h
      porcentage = porcentage + 1
      print "\r#{porcentage} / #{max_count}"
    end
    JobLevel.councillors_working(Corporation.active.first).find_each do |job|
      data << h.zip(person_to_row(job)).to_h
      porcentage = porcentage + 1
      print "\r#{porcentage} / #{max_count}"
    end
    File.open(path,"w") do |f|
      f.write(data.to_json)
    end
  end

  private
    def windows_array(values)
      values.map{|v| v.to_s.encode("ISO-8859-1", invalid: :replace, undef: :replace, replace: '')}
    end
end
