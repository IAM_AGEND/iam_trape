require 'profile_exporter'
require 'assets_exporter'
require 'activities_exporter'
require 'fileutils'

namespace :export do
  desc 'All'
  task all: [:profiles, :activities, :assets, :councillor_date, :corporations]
  
  desc "Exports profiles to public/export/profiles.csv & public/export/profiles.xls"
  task profiles: :environment do
    folder = Rails.root.join('public/system/people/export/profiles')
    FileUtils.rm_rf folder
    FileUtils.mkdir_p folder

    exporter = ProfileExporter.new
    puts "=========================================="
    puts "EXPORTANDO CSV"
    puts "=========================================="
    exporter.save_csv(folder.join('profiles.csv'))
    puts " FIN EXPORTACIÓN"
    puts "=========================================="
    puts "=========================================="
    puts "EXPORTANDO XLS"
    puts "=========================================="
    exporter.save_xls(folder.join('profiles.xls'))
    puts " FIN EXPORTACIÓN"
    puts "=========================================="
    puts "=========================================="
    puts "EXPORTANDO JSON"
    puts "=========================================="
    exporter.save_json(folder.join('profiles.json'))
    puts " FIN EXPORTACIÓN"
    puts "=========================================="
  end

  desc "Exportación de declaración de actividades por periodo
        public/export/ActividadesInicial_TODOS.xls,
        public/export/ActividadesFinal_TODOS.xls y
        public/export/ActividadesAnualConcejales<AÑO>.xls"
  task activities: :environment do
    folder = Rails.root.join('public/system/people/export/activities')
    FileUtils.rm_rf folder
    FileUtils.mkdir_p folder

    exporter = ActivitiesExporter.new
    puts "=========================================="
    puts "EXPORTANDO CSV"
    puts "=========================================="

    preference = DataPreference.find_by(:code => "councillors_publication_year")
    date_years = preference.blank? ? 4 : preference.type_data.to_i==0 ? preference.content_data.to_i : 4
    corporations = Corporation.where("end_corporation_date >= ?", Time.zone.now - date_years.years).order(name: :asc)

    puts "============================================"
    puts "DIRECTIVOS"
    puts "============================================"
    exporter.save_xls(folder.join('ActividadesInicial_Directivos.xls'),"Inicial") if ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Inicial'").count > 0
    exporter.save_xls(folder.join('ActividadesFinal_Directivos.xls'),"Final") if ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Final'").count > 0
    periods = {}
    ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Anual'").find_each do |dec|
      periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
    end
    porcentage = 0
    max_count = periods.count
    print "\r#{porcentage} / #{max_count}"

    periods.each do |k,v|
      exporter.save_xls(folder.join("ActividadesAnual_Directivos_#{k}.xls"),"Anual",k)
      porcentage=porcentage+1
      print "\r#{porcentage} / #{max_count}"
    end


    puts "============================================"
    puts "CONCEJALES"
    puts "============================================"
    corporations.each do |corp|

      exporter.save_xls(folder.join("ActividadesInicial_Concejales_#{corp.name}.xls"),"Inicial", 0, corp) if ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation'").where("temporalities.name = 'Inicial'").count > 0
      exporter.save_xls(folder.join("ActividadesFinal_Concejales_#{corp.name}.xls"),"Final", 0, corp) if ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation'").where("temporalities.name = 'Final'").count > 0
      periods = {}
      ActivitiesDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation'").where("temporalities.name = 'Anual'").find_each do |dec|
        periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
      end
      porcentage = 0
      max_count = periods.count
      print "\r#{porcentage} / #{max_count}"

      periods.each do |k,v|
        exporter.save_xls(folder.join("ActividadesAnual_Concejales_#{corp.name}_#{k}.xls"),"Anual",k, corp)
        porcentage=porcentage+1
        print "\r#{porcentage} / #{max_count}"
      end
    end
    puts " FIN EXPORTACIÓN"
    puts "=========================================="
  end

  desc "Exportación de declaración de bienes por periodo
        public/export/BienesInicial_TODOS.xls,
        public/export/BienesFinal_TODOS.xls y
        public/export/BienesAnualConcejales<AÑO>.xls"
  task assets: :environment do
    folder = Rails.root.join('public/system/people/export/assets')
    FileUtils.rm_rf folder
    FileUtils.mkdir_p folder

    exporter = AssetsExporter.new
    puts "=========================================="
    puts "EXPORTANDO CSV"
    puts "=========================================="

    preference = DataPreference.find_by(:code => "councillors_publication_year")
    date_years = preference.blank? ? 4 : preference.type_data.to_i==0 ? preference.content_data.to_i : 4
    corporations = Corporation.where("end_corporation_date >= ?", Time.zone.now - date_years.years).order(name: :asc)

    puts "============================================"
    puts "DIRECTIVOS"
    puts "============================================"
    exporter.save_xls(folder.join('BienesInicial_Directivos.xls'),"Inicial") if AssetsDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Inicial'").count > 0
    exporter.save_xls(folder.join('BienesFinal_Directivos.xls'),"Final") if AssetsDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Final'").count > 0
    periods = {}
    AssetsDeclaration.joins(:temporality).joins("INNER JOIN job_levels ON person_level_id = job_levels.id AND person_level_type = 'JobLevel'").where("temporalities.name = 'Anual'").find_each do |dec|
      periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
    end
    porcentage = 0
    max_count = periods.count
    print "\r#{porcentage} / #{max_count}"

    periods.each do |k,v|
      exporter.save_xls(folder.join("BienesAnual_Directivos_#{k}.xls"),"Anual",k)
      porcentage=porcentage+1
      print "\r#{porcentage} / #{max_count}"
    end

    puts "============================================"
    puts "CONCEJALES"
    puts "============================================"
    corporations.each do |corp|
      exporter.save_xls(folder.join("BienesInicial_Concejales_#{corp.name}.xls"),"Inicial", 0, corp) if AssetsDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id = #{corp.id}").where("temporalities.name = 'Inicial'").count > 0
      exporter.save_xls(folder.join("BienesFinal_Concejales_#{corp.name}.xls"),"Final", 0, corp) if AssetsDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id = #{corp.id}").where("temporalities.name = 'Final'").count > 0
      periods = {}

      AssetsDeclaration.joins(:temporality).joins("INNER JOIN councillors_corporations ON person_level_id = councillors_corporations.id AND person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id = #{corp.id}").where("temporalities.name = 'Anual'").find_each do |dec|
        periods.merge!({dec.try(:year_period)=>dec.try(:year_period)})
      end
      porcentage = 0
      max_count = periods.count
      print "\r#{porcentage} / #{max_count}"

      periods.each do |k,v|
        exporter.save_xls(folder.join("BienesAnual_Concejales_#{corp.name}_#{k}.xls"),"Anual",k, corp)
        porcentage=porcentage+1
        print "\r#{porcentage} / #{max_count}"
      end
    end
    puts " FIN EXPORTACIÓN"
    puts "=========================================="
  end

  desc "Años de publicación de concejales"
  task councillor_date: :environment do
    preference = DataPreference.find_by(:title => "Años de publicación de concejales")
    DataPreference.create!(title: "Años de publicación de concejales", type_data: 0, content_data: "4") if preference.blank?
    preference = DataPreference.find_by(:title => "Horas para la exportación de archivos")
    DataPreference.create!(title: "Horas para la exportación de archivos", type_data: 0, content_data: "24") if preference.blank?
  end

  desc "Actualizar corporaciones"
  task corporations: :environment do
   Corporation.all.each do |c|
      c.end_corporation_date = Time.zone.now.change(:year => c.end_year)
      c.full_constitutional_date = Time.zone.now.change(:year => c.start_year)
      c.election_date = Time.zone.now.change(:year => c.start_year)

      if c.save
        puts "Se ha actualizado la corporacion: #{c.name}"
      else
        puts "ERROR: La corporación: #{c.name} tiene los errores: #{c.errors.full_messages}"
      end
   end
  end
end
