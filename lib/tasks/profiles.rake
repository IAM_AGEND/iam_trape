namespace :profiles do
    desc 'Insert users for development environment'
    task insert: :environment do
        # Administrator.create!(email: 'ajmorenoh@indra.es', password: Rails.application.secrets.password)
        # Administrator.create!(email: 'galvezip@madrid.es', password: Rails.application.secrets.password)

        Administrator.create!(email: 'jimenezamm@madrid.es', password: Rails.application.secrets.password)
    end

    desc 'Insert users for development environment'
    task cumplimented: :environment do
        
        Person.all.each do |person|
            if person.try(:profile).blank?
                profile = Profile.new(cumplimented: false, updated_at: Time.zone.now) 
                if profile.save
                    if ActiveRecord::Base.connection.exec_query("UPDATE people SET profile_id = #{profile.id} WHERE id=#{person.id}")
                        puts "Se ha guardado correctamente: #{profile.try(:id)}"
                    else
                        puts "ERROR: No se ha guardado el perfil: #{person.id} #{person.errors.full_messages}"
                    end
                else
                    puts "ERROR: No se ha guardado el perfil: #{person.id} #{profile.errors.full_messages}"
                end
            else
                profile = person.profile
                if ActiveRecord::Base.connection.exec_query("UPDATE profiles SET cumplimented = #{person.profile.has_profile?} WHERE id=#{profile.id}")
                    puts "Se ha guardado correctamente: #{person.profile.id}"
                else
                    puts "ERROR: No se ha guardado el perfil: #{person.id} #{profile.errors.full_messages}"
                end
            end
        end
    end

  desc "Importacion perfil error página"
  task update_start_date: :environment do
    appointment = JobLevel.find_by(person_id: Person.find_by(personal_code: 134101).try(:id)).try(:appointment)
    puts "============================================="
    if appointment.blank?
        puts "No se ha encontrado la persona"
    else
        appointment.start_date = "23/07/2015"

        if appointment.save!
            puts "Fecha de alta modificada para la persona 134101"
        else
            puts "La fecha de alta no se ha modificado."
        end
    end
    puts "============================================="
  end

  desc "Update the crontab file"
    task update_crontab: :environment do
      `whenever -i cellar`
    end



    task delete: :environment do 
        Person.find_by(personal_code: '9031574').destroy
    end
end

