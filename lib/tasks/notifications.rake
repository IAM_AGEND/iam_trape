require 'notifications_importer'
require 'inper_ws'
include Rails.application.routes.url_helpers
namespace :notifications do
    # desc "Set envio"
    # task set_send: :environment do
    #     Notification.where(permit: true, send_feedback: false).each do |notification|
    #         notification.send_feedback = true
    #         if notification.no_publish_reason.blank? || !notification.no_publish_reason.include?("No se ha realizado el feedback")
    #             case notification.try(:type_data)
    #             when 'Profile'
    #                 notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / Se ha realizado el feedback"
    #             when 'AssetsDeclaration'
    #                 notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / Se ha realizado el feedback"
    #             when 'ActivitiesDeclaration'
    #                 notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / Se ha realizado el feedback"
    #             end
    #             if notification.save
    #                 puts "Se ha guardado correctamente la notificación: #{notification.id}"
    #             else
    #                 puts "ERROR: no se ha guardado la notificación: #{notification.id}"
    #             end
    #         end
    #     end
    # end

    desc "Envio periódico"
    task send_other: :environment do
        ws = INPERWS.new
        Notification.where(send_feedback: false).where("permit= true OR ignore=false").each do |notification|
            if notification.try(:no_publish_reason).to_s.include?("No se ha realizado el feedback")
                puts "======================================================="
                case notification.try(:type_data)
                when 'Profile'
                    begin
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / "
                            inper_response = ws.setWSPerfilesPublicado(notification.try(:content_data)['dpfId'], notification.change_state_at, 0,
                                                                        "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}")
                        else
                            inper_response = ws.setWSPerfilesPublicado(notification.try(:content_data)['dpfId'],notification.change_state_at,1,notification.no_publish_reason)
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / Razón: #{notification.no_publish_reason} / "
                        end
                        begin
                            respuesta= JSON.parse(inper_response)
                            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                                notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                                notification.motive = ""
                                notification.send_feedback = true
                            else
                                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                                notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                            end
                        rescue => e
                            notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                            notification.motive = e.message
                            puts "ERROR: #{e}"
                        end
                    rescue => e
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / No se ha realizado el feedback"
                        else
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
                        end
                        notification.motive = e.message
                        puts "ERROR: #{e}"
                    end
                when 'AssetsDeclaration'
                    begin
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / "
                            inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decg_id"],notification.change_state_at,0,
                                                                          "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}").to_s
                        else
                            inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)['decg_id'],notification.change_state_at,1,notification.no_publish_reason)
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} /  Razón: #{notification.no_publish_reason} / "
                        end
                        begin
                            respuesta= JSON.parse(inper_response)
                            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                                notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                                notification.motive = ""
                                notification.send_feedback = true
                            else
                                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                                notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                            end
                        rescue => e
                            notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                            notification.motive = e.message
                            puts "ERROR: #{e}"
                        end
                    rescue => e
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / No se ha realizado el feedback"
                        else
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
                        end
                        notification.motive = e.message
                        puts "ERROR: #{e}"
                    end
                when 'ActivitiesDeclaration'
                    begin
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / "
                            inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decgId"],notification.change_state_at,0,
                                                                          "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}").to_s
                        else
                            inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)['decgId'],notification.change_state_at,1,notification.no_publish_reason)
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} /  Razón: #{notification.no_publish_reason} / "
                        end
                        begin
                            respuesta= JSON.parse(inper_response)
                            if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                                notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                                notification.motive = ""
                                notification.send_feedback = true
                            else
                                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                                notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                            end
                        rescue => e
                            notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                            notification.motive = e.message
                            puts "ERROR: #{e}"
                        end
                    rescue => e
                        if notification.permit
                            notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / No se ha realizado el feedback"
                        else
                            notification.no_publish_reason = "Se han rechazado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / Razón: #{notification.no_publish_reason} / No se ha realizado el feedback"
                        end
                        notification.motive = e.message
                        puts "ERROR: #{e}"
                    end
                end
                if !notification.send_feedback
                    notification.forwards = notification.forwards + 1
                end

                begin
                    umbral = ::DatePreference.find_by(code: "notification_pending_alert").try(:content_data).to_i
                rescue
                    umbral=nil
                end

                if !umbral.blank? && umbral <= notification.forwards.to_i
                  begin
                    Mailer.notification_alerts(notification).deliver_now
                  rescue => e
                    puts "ERROR: #{e}"
                  end
                end

                if notification.save
                    puts "Se ha guardado correctamente la notificación: #{notification.id}"
                else
                    puts "ERROR: no se ha guardado la notificación: #{notification.id}"
                end
                puts "======================================================="
            end
        end
    end

    desc "Envío de alertas por umbral alcanzado de notificaciones pendientes"
    task pending_alerts: :environment do
        umbral = DataPreference.find_by(code: "num_send_retry_diary").try(:content_data)
        begin
            Mailer.pending_alerts('Perfiles').deliver_now if !umbral.blank? && umbral.to_i <= Notification.pending.with_type_data('Profile').count
        rescue => e
            puts "ERROR: #{e}"
            begin
            logger.error("ERROR: #{e}")
            rescue
            end
        end
        begin
        Mailer.pending_alerts('Declaraciones de bienes').deliver_now if !umbral.blank? && umbral.to_i <= Notification.pending.with_type_data('AssetsDeclaration').count
        rescue => e
            puts "ERROR: #{e}"
            begin
                logger.error("ERROR: #{e}")
                rescue
                end
        end
        begin
            Mailer.pending_alerts('Declaraciones de actividades').deliver_now if !umbral.blank? && umbral.to_i <= Notification.pending.with_type_data('ActivitiesDeclaration').count
        rescue => e
            puts "ERROR: #{e}"
            begin
                logger.error("ERROR: #{e}")
                rescue
                end
        end
        begin
            Mailer.pending_alerts('Personas').deliver_now if !umbral.blank? && umbral.to_i <= Notification.pending.with_type_data('Person').count
        rescue => e
            puts "ERROR: #{e}"
            begin
                logger.error("ERROR: #{e}")
                rescue
                end
        end
    end

    desc "Importación de notificaciones"
    task change_permit: :environment do
        begin
            id = nil
            notification = Notification.new
            # notification.content_data = '{"estId": 4, "decgId": 6361, "dperId": 7164, "tdecId": 4, "cpNombre": null, "decgYear": 2020, "dperSexo": "M", "tdNombre": "Anual", "gperAbrev": "R", "decgDcmtId": "0902c31980cae685", "dperNombre": "GEMA T.", "dperNumper": 201338, "decgFecAlta": 1610636571000, "dperApellido1": "PEREZ", "dperApellido2": "RAMON", "dperDocumento": "18029050V", "dperCorreoCorp": "perezrgem@madrid.es", "decgFecRegistro": 1610636571000, "decgNumRegistro": "RA202100001", "actividadesOtras": [], "decgNumExpediente": "18029050V_001", "actividadesPrivadas": [], "actividadesPublicas": []}'
            # notification.type_data = "ActivitiesDeclaration"
            notification.permit = false
            notification.ignore = false

            if notification.save
                puts "Se ha guardado correctamente la notificación: #{notification.id}"
            else
                puts "ERROR: no se ha guardado la notificación: #{notification.id}"
            end
        rescue => e
            puts "ERROR: #{e}"
        end
    end

    desc "Importación de notificaciones"
    task import: :environment do
        NotificationsImporter.new().import
    end

    desc "Tramitar al histórico las notificaciones que no aportan datos nuevos"
    task ignore_unless_new_data: :environment do
        NotificationsImporter.new().ignore_unless_new_data
    end

    desc "Importación de notificaciones"
    task delete_dup: :environment do
        [11,12,10,198,151,200,149,147,176,14,148,182,152,199,150,201].each do |id|
            notification = Notification.find_by(id: id)
            if notification.destroy
                puts "Se ha borrado correctamente la notificación con id: #{id}"
            else
                puts "ERROR: no se ha borrado la notificación con id: #{id}"
            end
        end
    end

    task resend: :environment do
        notification = Notification.find_by(id: 3)
        puts INPERWS.new().setWSPerfilesPublicado(notification.try(:content_data)["dpfId"])
    end

    desc "Importación de datos personales"
    task update_person: :environment do
        NotificationsImporter.new().update_level_job
    end

    desc "Importación de preferencias de fecha de obligaciones"
    task date: :environment do
        preference = DataPreference.find_by(:title => "Horas de ejecución tarea")
        DataPreference.create!(title: "Horas de ejecución tarea", type_data: 0, content_data: "24") if preference.blank?
    end

    task pruebas: :environment do
        Notification.create!(content_data: '{"cpId": 721, "cpActiva": 1, "cpNombre": "2020/2024", "cpObserv": "Corporación de pruebas", "cpAnioFin": 2024, "cpNumConcej": 6, "cpAnioInicio": 2020, "cpFechaFinal": null, "cpFechaElecciones": 1580023436000, "cpFechaPlenoConst": 1581751436000, "listasElectorales": [{"cpId": 721, "leId": 92, "leNombreLista": "PSOE"}, {"cpId": 721, "leId": 93, "leNombreLista": "PP"}, {"cpId": 721, "leId": 157, "leNombreLista": "AM (Ahora Madrid)"}]}', type_data: 'Corporation');
        Notification.create!(content_data:  '{"cpId": 681, "cpActiva": 0, "cpNombre": "2008/2012", "cpObserv": null, "cpAnioFin": 2012, "cpNumConcej": 57, "cpAnioInicio": 2008, "cpFechaFinal": 1575118052000, "cpFechaElecciones": 1212184800000, "cpFechaPlenoConst": 1213912800000, "listasElectorales": []}', type_data: 'Corporation');
        Notification.create!(content_data:  '{"cpId": 382, "cpActiva": 0, "cpNombre": "2019/2023", "cpObserv": "HOLA", "cpAnioFin": 2023, "cpNumConcej": 57, "cpAnioInicio": 2019, "cpFechaFinal": 1576164957000, "cpFechaElecciones": 1558821600000, "cpFechaPlenoConst": 1560549600000, "listasElectorales": []}', type_data: 'Corporation');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 3506, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "MARIA AINHOA", "dperNumper": 111168, "funcionario": {"fldId": 1498, "dperId": 3506, "fldnId": 2558, "fldnCargo": "JEFE/A SERVICIO", "fldnUnidad": "IAM SERVICIO DE POBLACION Y TERRITORIO", "fldnFechaCese": null, "fldnFechaNomb": 1104793200000}, "dperApellido1": "JAVIER", "dperApellido2": "JAVIER", "dperDocumento": "01487872W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');


        Notification.create!(content_data:  '{"vocal": null, "dperId": 3506, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "JAVIER", "dperNumper": 80710, "funcionario": {"fldId": 1498, "dperId": 3506, "fldnId": 2558, "fldnCargo": "JEFE/A SERVICIO", "fldnUnidad": "IAM SERVICIO DE POBLACION Y TERRITORIO", "fldnFechaCese": null, "fldnFechaNomb": 1104793200000}, "dperApellido1": "JAVIER", "dperApellido2": "JAVIER", "dperDocumento": "01487872W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 3900, "concejal": null, "dperSexo": "M", "eventual": null, "directivo": null, "dperNombre": "MARIA", "dperNumper": 169352, "funcionario": {"fldId": 1892, "dperId": 3900, "fldnId": 2952, "fldnCargo": "JEFE/A DEPARTAMENTO", "fldnUnidad": "DEPARTAMENTO DE RECLAMACIONES I", "fldnFechaCese": null, "fldnFechaNomb": 1430776800000}, "dperApellido1": "MARIA", "dperApellido2": "MARIA", "dperDocumento": "50755712W", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 1375, "concejal": null, "dperSexo": null, "eventual": null, "directivo": {"drId": 249, "drnId": 248, "dperId": 1375, "drnCargo": "COORDINADOR/A DEL DISTRITO", "drnUnidad": "CAR COORDINACION DEL DISTRITO", "drnFechaCese": null, "drnFechaNomb": 1516662000000}, "dperNombre": "JOSÉ CARLOS", "dperNumper": 71633, "funcionario": null, "dperApellido1": "JOSÉ CARLOS", "dperApellido2": "JOSÉ CARLOS", "dperDocumento": "50730272T", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 2703, "concejal": null, "dperSexo": "H", "eventual": {"evtId": 100, "dperId": 2703, "evtnId": 125, "evtnCargo": "VOCAL ASESOR/A", "evtnUnidad": "UNIDAD DE PERSONAL EVENTUAL", "evtnFechaCese": null, "evtnFechaNomb": 1562709600000}, "directivo": null, "dperNombre": "ANGEL IGNACIO", "dperNumper": 171421, "funcionario": null, "dperApellido1": "ANGEL IGNACIO", "dperApellido2": "ANGEL IGNACIO", "dperDocumento": "51424813X", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 2802, "concejal": null, "dperSexo": "M", "eventual": {"evtId": 199, "dperId": 2802, "evtnId": 224, "evtnCargo": "ASESOR/A N28", "evtnUnidad": "GRUPO MUNICIPAL PARTIDO POPULAR", "evtnFechaCese": null, "evtnFechaNomb": 1565560800000}, "directivo": null, "dperNombre": "MARIA DEL CARMEN", "dperNumper": 201910, "funcionario": null, "dperApellido1": "MARIA DEL CARMEN", "dperApellido2": "MARIA DEL CARMEN", "dperDocumento": "51376488P", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 1317, "concejal": null, "dperSexo": null, "eventual": null, "directivo": {"drId": 191, "drnId": 190, "dperId": 1317, "drnCargo": "COORDINADOR/A GENERAL", "drnUnidad": "PGD CG POLITICAS DE GENERO Y DIVERSIDAD", "drnFechaCese": null, "drnFechaNomb": 1494540000000}, "dperNombre": "ROSA MARÍA", "dperNumper": 9, "funcionario": null, "dperApellido1": "ROSA MARÍA", "dperApellido2": "ROSA MARÍA", "dperDocumento": "51701259L", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": {"vvId": 7428, "vvnId": 7248, "dperId": 125430, "vvnCargo": "Portavoz", "vvnUnidad": "Junta Municipal Alcalá", "vvnFechaCese": null, "vvnFechaNomb": 1564005600000, "vvnJuntaMd": "pepe", "disNombre":"Arganzuela","gpNombre": "AM (Ahora Madrid)", "cpNombre": "2020/2024"}, "dperId": 1530, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": null, "dperNombre": "Mai", "dperNumper": 4444, "funcionario": null, "dperApellido1": "Mai", "dperApellido2": "Mai", "dperDocumento": "09035930N", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person', action_abrev: 'V');
        Notification.create!(content_data:  '{"vocal": {"vvId": 1, "vvnId": 1, "dperId": 361, "vvnCargo": null, "vvnUnidad": "Junta Municipal Rivas", "vvnFechaCese": 1575115765000, "vvnFechaNomb": 1564037158000}, "dperId": 361, "concejal": null, "dperSexo": "H", "eventual": null, "directivo": {"drId": 10, "drnId": 10, "dperId": 361, "drnCargo": "Mandamás", "drnUnidad": "IAM", "drnFechaCese": null, "drnFechaNomb": 1567116000000}, "dperNombre": "OSCAR", "dperNumper": 5, "funcionario": null, "dperApellido1": "OSCAR", "dperApellido2": "OSCAR", "dperDocumento": "47463199P", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 381, "concejal": {"cjId": 242, "dperId": 381, "cjnAlcalde": 0, "cjnFechaNom": 1560722400000, "corporacion": {"cpId": 382, "cpActiva": 0, "cpNombre": "2019/2023", "cpObserv": "HOLA", "cpAnioFin": 2023, "cpNumConcej": 57, "cpAnioInicio": 2019, "cpFechaFinal": 1576164957000, "cpFechaElecciones": 1558821600000, "cpFechaPlenoConst": 1560549600000, "listasElectorales": []}, "cjnFechaCese": null}, "dperSexo": "M", "eventual": null, "directivo": {"drId": 1, "drnId": 1, "dperId": 381, "drnCargo": "Director/a General", "drnUnidad": "IAM", "drnFechaCese": null, "drnFechaNomb": 1553078357000}, "dperNombre": "Paloma", "dperNumper": 57146, "funcionario": null, "dperApellido1": "Paloma", "dperApellido2": "Paloma", "dperDocumento": "50421035K", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"vocal": null, "dperId": 1112, "concejal": {"cjId": 932, "dperId": 1112, "cjnAlcalde": 0, "cjnFechaNom": 1558354562000, "corporacion": {"cpId": 381, "cpActiva": 0, "cpNombre": "2015/2019", "cpObserv": null, "cpAnioFin": 2019, "cpNumConcej": 57, "cpAnioInicio": 2015, "cpFechaFinal": 1576163255000, "cpFechaElecciones": 1432418400000, "cpFechaPlenoConst": 1434146400000, "listasElectorales": []}, "cjnFechaCese": 1576163255000}, "dperSexo": null, "eventual": null, "directivo": null, "dperNombre": "María Begoña", "dperNumper": 10, "funcionario": null, "dperApellido1": "María Begoña", "dperApellido2": "María Begoña", "dperDocumento": "01385136F", "tipoDocumento": {"tdocId": 1, "tdocNombre": "DNI"}}', type_data: 'Person');
        Notification.create!(content_data:  '{"decg_id": 4223,"tdecId": 4,"dperId": 7164,"decgFecAlta": 1610636571000,"estId": 4,"tdNombre": "Anual","decgYear": 2020,"decgNumExpediente": "18029050V_001","decgNumRegistro": "RA202100001","decgFecRegistro": 1610636571000 ,"decgDcmtId": "0902c31980cae685" ,"gperAbrev": "R" ,"cpNombre": null ,"dperNombre": "GEMA T." ,"dperApellido1": "PEREZ","dperApellido2": "RAMON","dperDocumento": "18029050V" ,"dperSexo": null ,"dperCorreoCorp": "perezrgem@madrid.es" ,"dperNumper": 201338,"deudas": [{"deuId": 2841, "decgId": 4221, "deuDesc": "Vivienda habitual", "deuClase": "Hopoteca", "deuImporte": 50000.0, "deuOrden": 1}], "vehiculos": [{"vehId": 3101, "decgId": 4221, "vehMes": "Abril", "vehAnyo": 1982, "vehClase": "Coche", "vehMarcaMod": "Renault 5", "vehOrden": 1}],"depositosCuenta": [{"decgId": 4221,"dpIpd": 3861,"dpClase": "Depósito en cuenta corriente","dpEntidad": "ING","dpSaldo": 3000.0,"dpOrden": 1}], "otrosBienesMuebles": [{"obmId": 2961, "decgId": 4221, "obmMes": "Agosto", "obmAnyo": 2019, "obmDesc": "Herencia de mi abuelo", "obmTipos": "Joyas","obmOrden": 1}], "otrosDepositosCuenta": [{"decgId": 4221, "optrId": 3461, "optrMes": "Febrero", "optrAnyo": 1998, "optrTipos": "Bonos", "optrValor": 1222.0, "optrTipoUnidad": "222", "optrDescripcion": "xxxx","optrOrden": 1}, {"decgId": 4221, "optrId": 3441, "optrMes": null, "optrAnyo": 2012, "optrTipos": "Deuda pública", "optrValor": 3000.0, "optrTipoUnidad": "€", "optrDescripcion": "Ahorrillos","optrOrden": 2}], "informacionTributaria": [{"decgId": 4221, "inftId": 3961, "inftIsBiObserv": null, "inftIsBiImporte": 0.0, "inftIspBiObserv": null, "inftIrpfBiObserv": null,"inftIspBiImporte": 0.0, "inftIrpfBiImporte": 4500.0, "inftIrpfBigObserv": null, "inftIrpfBigImporte": 35000.0, "inftIrpfDeducObservA": null, "inftIrpfDeducObservB": null, "inftIrpfDeducImporteA": 0.0, "inftIrpfDeducImporteB": 0.0}], "patrimonioInmobiliario": [{"decgId": 4221,"pinId": 4021,"clase": "Urbano","tipoDerecho": "Pleno dominio","tituloAdquisicion": "Compraventa","municipio": "Madrid","participacion": 50.0,"mes": "Diciembre","anyo": 2012,"valorCatastral": 90000.0,"observaciones": null,"orden": 1 }]}', type_data: 'AssetsDeclaration');
        Notification.create!(content_data:  '{"deudas": [], "decg_id": 4223, "vehiculos": [], "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 10, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "depositosCuenta": [], "otrosBienesMuebles": [], "otrosDepositosCuenta": [], "informacionTributaria": [], "patrimonioInmobiliario": []}', type_data: 'AssetsDeclaration');
        Notification.create!(content_data:  '{"estId": 4, "decgId": 6361, "dperId": 7164, "tdecId": 4, "cpNombre": null, "decgYear": 2020, "dperSexo": "M", "tdNombre": "Anual", "gperAbrev": "R", "decgDcmtId": "0902c31980cae685", "dperNombre": "GEMA T.", "dperNumper": 201338, "decgFecAlta": 1610636571000, "dperApellido1": "PEREZ", "dperApellido2": "RAMON", "dperDocumento": "18029050V", "dperCorreoCorp": "perezrgem@madrid.es", "decgFecRegistro": 1610636571000, "decgNumRegistro": "RA202100001","decgNumExpediente": "18029050V_001","actividadesOtras": [{"decgId": 3424, "acotId": 32442, "descripcion": "dssa", "fechaInicio": 1564005600000, "fechaCese": null, "orden": 1}], "actividadesPrivadas": [{"decgId": 4223,"id": 556061,"actividad": "Actividades por cuenta ajena","descripcion": "dasda","entidad": "adsa","cargo": "dsads","fechaInicio": 1564005600000,"fechaCese":  null,"orden": 1}], "actividadesPublicas": [{"decgId": 24324, "acpuId": 367665, "acpuEntidad": "fdsf", "acpuCargo": "fdsfds", "acpuFecInicio": 1564005600000, "acpuFecCese": null, "orden": 1}]}', type_data: 'ActivitiesDeclaration');
        Notification.create!(content_data:  '{"decgId": 4223, "datosPersonales": {"dperId": 363, "nombre": "Javier", "apellido1": "Javier", "apellido2": "Javier", "fechaAlta": 1585141625000, "dperNumper": 111168, "corporacion": {"cpId": 721, "cpNombre": "2020/2024", "cpAnioFin": 2024, "cpAnioInicio": 2020}, "temporalidad": {"tdId": 1, "tdDesc": "Inicial", "tdNombre": "Inicial"}, "fechaRegistro": 1585141656000, "grupoPersonal": {"gperId": 1, "gperDesc": "Cargos Electos", "gperAbrev": "C"}}, "actividadesOtras": [{"acotId": 2541, "decgId": 4223, "fechaCese": null, "descripcion": "Youtuber", "fechaInicio": null}], "actividadesPrivadas": [{"id": 556061, "cargo": "Directivo/Consejero", "decgId": 4223, "entidad": "Iberdrola", "actividad": "Actividades por cuenta ajena", "fechaCese": null, "descripcion": "Consejero", "fechaInicio": 1578351600000}], "actividadesPublicas": [{"acpuId": 2581, "decgId": 4223, "acpuCargo": "Celador", "acpuEntidad": "IAM", "acpuFecCese": null, "acpuFecInicio": null}]}', type_data: 'ActivitiesDeclaration' );
        Notification.create!(content_data:  '{"dpfId": 1665,"dperId": 363,"dperNombre": "Matite","dperApellido1": "RAMON" ,"dperApellido2": "RAMON" ,"dperDocumento": "18029050V","dperSexo": "H" ,"dperNumper": 111168,"dperCorreoCorp": "perezrgem@madrid.es","dpfFecRegistro": 1585141625000,"dpfFecAlta": 8985141625000,"dpfTwitter": null,"dpfFacebook": null,"dpfMailCorporativo": "jaja@ja.ja","dpfMailPersonal": "jaja@ja.ja","gperAbrev": "C","ultimoDirectivo": [],"ultimoEventual": [],"ultimoFuncionario": [],"ultimoVocal": [],"ultimoConcejal": [],"cargosPoliticos": [{"dpfId": 1665, "cpolId": 845, "cpolCargo": "Mandmás", "cpolAnyoFin": null, "cpolAnyoIni": 2017, "cpolInstitucion": "IAM","cpolOrden": 1}], "carreraProfPrivado": [{"dpfId": 1665, "cpriId": 1145, "cpriCargo": "Analista", "cpriAnyoFin": null, "cpriAnyoIni": 2008, "cpriInstitucion": "Indra","cpriOrden": 1}], "carreraProfPublico": [{"cpuId": 1485, "dpfId": 1665, "cpuPuesto": "Técnico", "cpuAnyoFin": 2007, "cpuAnyoIni": 2006, "cpuGradoConso": "16", "cpuAnyoIngreso": 2006, "cpuEscalaCuerpo": "AGE","cpuOrden": 1, "administracionesPublicas": {"aappId": 2, "aappCodigo": "12", "aappAdministracion": "Administración Comunitaria"}}], "idiomas": [{"dpfId": 1665, "idmId": 1450,"idmOrden":1, "idiomaConf": {"idcId": 14, "idcIdioma": "Gallego"}, "idiomaNivel": {"nicId": 2, "nicNivel": "B - Usuario intermedio"}}, {"dpfId": 1665, "idmId": 1449, "idiomaConf": {"idcId": 8, "idcIdioma": "Chino Mandarín"}, "idiomaNivel": {"nicId": 1, "nicNivel": "A - Usuario básico"}}], "distinciones": [{"dhmId": 342,"dpfId": 5532,"dhmDistinciones": "DSF","dhmOrden": 1}], "otrosEstudios": [{"oeId": 1185, "dpfId": 1665, "oeCurso": "SSMC", "oeCentro": "Itera", "oeAnyoFin": 2017, "oeAnyoIni": 2017,"oeOrden": 1}], "publicaciones": [{"dpfId": 1665, "pubId": 805, "pubAnioInicio": 2016, "pubPublicacion": "Tesis","pubOrden": 1}], "otraInformacion": [{"dpfId": 1665, "oipId": 964, "oipInformacion": "Soy runner pero poco"}], "actividadesDocente": [{"actdId": 4325,"dpfId": 3432,"actdActividad": "FDA","actdOrden": 1}], "formacionAcademica": [{"dpfId": 1665, "facId": 1305, "facAnyoFin": 2006, "facAnyoIni": 2000, "facCentroExped": "UAH", "facNivelEducativo": "Enseñanzas Universitarias", "facTitulacionOficial": "ITIS","facOrden": 1}]}', type_data: 'Profile');
    end

    desc "Notificaciones para la bateria de pruebas"
    task delete_test: :environment do
        Notification.where(type_data: 'Person').each {|p| p.destroy}
    end

    task delete_assets_test: :environment do
        Notification.where(type_data: 'AssetsDeclarations').each {|p| p.destroy}
    end

    task delete_activities_test: :environment do
        Notification.where(type_data: 'ActivitiesDeclarations').each {|p| p.destroy}
    end

    desc "Notificaciones de declaraciones de actividades para la bateria de pruebas"
    task activities_declarations: :environment do
        declarations = "{ \"listaDeclaraciones\":
            [
                {\"estId\": 5, \"decgId\": 444444, \"dperId\": 444, \"tdecId\": 4, \"cpNombre\": \"2030/2034\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"decgDcmtId\": \"0902c25b800fc17a\", \"dperNombre\": \"ANTONIO JAVIER\", \"dperNumper\": null, \"decgFecAlta\": 1602067513000, \"dperApellido1\": \"MORENO\", \"dperApellido2\": \"HERNÁNDEZ\", \"dperDocumento\": \"71093040V\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1600984800000, \"decgNumRegistro\": \"CA202000015\", \"actividadesOtras\": [], \"decgNumExpediente\": \"2030_2034_71093040V\", \"actividadesPrivadas\": [], \"actividadesPublicas\": [{\"acpuId\": 2883, \"decgId\": 444444, \"acpuCargo\": \"CESAR\", \"acpuEntidad\": \"CESAR\", \"acpuFecCese\": 1577574000000, \"acpuFecInicio\": 1575759600000}]},
                {\"estId\": 5, \"decgId\": 444445, \"dperId\": 444, \"tdecId\": 4, \"cpNombre\": \"2030/2034\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"decgDcmtId\": \"0902c25b800fc17a\", \"dperNombre\": \"ANTONIO JAVIER\", \"dperNumper\": null, \"decgFecAlta\": 1602067513000, \"dperApellido1\": \"MORENO\", \"dperApellido2\": \"HERNÁNDEZ\", \"dperDocumento\": \"71093040V\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1600984800000, \"decgNumRegistro\": \"CA202000015\", \"actividadesOtras\": [], \"decgNumExpediente\": \"2030_2034_71093040V\", \"actividadesPrivadas\": [], \"actividadesPublicas\": [{\"acpuId\": 2883, \"decgId\": 444445, \"acpuCargo\": \"CESAR\", \"acpuEntidad\": \"CESAR\", \"acpuFecCese\": 1577574000000, \"acpuFecInicio\": 1575759600000}]},
                {\"estId\": 5, \"decgId\": 444446, \"dperId\": 444, \"tdecId\": 4, \"cpNombre\": \"2030/2034\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"decgDcmtId\": \"0902c25b800fc17a\", \"dperNombre\": \"ANTONIO JAVIER\", \"dperNumper\": null, \"decgFecAlta\": 1602067513000, \"dperApellido1\": \"MORENO\", \"dperApellido2\": \"HERNÁNDEZ\", \"dperDocumento\": \"71093040V\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1600984800000, \"decgNumRegistro\": \"CA202000015\", \"actividadesOtras\": [], \"decgNumExpediente\": \"2030_2034_71093040V\", \"actividadesPrivadas\": [], \"actividadesPublicas\": [{\"acpuId\": 2883, \"decgId\": 444446, \"acpuCargo\": \"CESAR\", \"acpuEntidad\": \"CESAR\", \"acpuFecCese\": 1577574000000, \"acpuFecInicio\": 1575759600000}]},
                {\"estId\": 6, \"decgId\": 5555, \"dperId\": 55555, \"tdecId\": 4, \"cpNombre\": null, \"decgYear\": 2018, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"decgDcmtId\": null, \"dperNombre\": \"JOSÉ AMADOR\", \"dperNumper\": 12, \"decgFecAlta\": 1523484000000, \"dperApellido1\": \"FERNÁNDEZ\", \"dperApellido2\": \"VIEJO\", \"dperDocumento\": \"09383067X\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"actividadesOtras\": [], \"decgNumExpediente\": null, \"actividadesPrivadas\": [], \"actividadesPublicas\": []},
                {\"estId\": 6, \"decgId\": 5556, \"dperId\": 55555, \"tdecId\": 4, \"cpNombre\": null, \"decgYear\": 2018, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"decgDcmtId\": null, \"dperNombre\": \"JOSÉ AMADOR\", \"dperNumper\": 11, \"decgFecAlta\": 1523484000000, \"dperApellido1\": \"FERNÁNDEZ\", \"dperApellido2\": \"VIEJO\", \"dperDocumento\": \"09383067X\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"actividadesOtras\": [], \"decgNumExpediente\": null, \"actividadesPrivadas\": [], \"actividadesPublicas\": []},
                {\"estId\": 6, \"decgId\": 5557, \"dperId\": 55555, \"tdecId\": 4, \"cpNombre\": null, \"decgYear\": 2018, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"decgDcmtId\": null, \"dperNombre\": \"JOSÉ AMADOR\", \"dperNumper\": 10, \"decgFecAlta\": 1523484000000, \"dperApellido1\": \"FERNÁNDEZ\", \"dperApellido2\": \"VIEJO\", \"dperDocumento\": \"09383067X\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"actividadesOtras\": [], \"decgNumExpediente\": null, \"actividadesPrivadas\": [], \"actividadesPublicas\": []},
                {\"estId\": 6, \"decgId\": 6666, \"dperId\": 666666, \"tdecId\": 4, \"cpNombre\": null, \"decgYear\": 2018, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"decgDcmtId\": null, \"dperNombre\": \"JOSÉ AMADOR\", \"dperNumper\": 9, \"decgFecAlta\": 1523484000000, \"dperApellido1\": \"FERNÁNDEZ\", \"dperApellido2\": \"VIEJO\", \"dperDocumento\": \"09383067X\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"actividadesOtras\": [], \"decgNumExpediente\": null, \"actividadesPrivadas\": [], \"actividadesPublicas\": []}
            ]
        }"
        NotificationsImporter.new().import_test_declarations(JSON.parse(declarations.to_s).with_indifferent_access, 'ActivitiesDeclaration')
    end

    desc "Notificaciones de declaraciones de bienes para la bateria de pruebas"
    task assets_declarations: :environment do
        declarations = "{ \"listaDeclaraciones\":
            [

                {\"estId\": 4, \"deudas\": [{\"deuId\": 2286, \"decgId\": 3333, \"deuDesc\": \"deuda\", \"deuClase\": \"HIPOTECA\", \"deuImporte\": 56000.0}], \"dperId\": 33333, \"tdecId\": 1, \"decg_id\": 3333, \"cpNombre\": \"2019/2023\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"vehiculos\": [], \"decgDcmtId\": \"0902c25b800dabef\", \"dperNombre\": \"OSCAR\", \"dperNumper\": 5, \"decgFecAlta\": 1573197159000, \"dperApellido1\": \"MANGAS\", \"dperApellido2\": \"FERNANDEZ\", \"dperDocumento\": \"47463199P\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1571758802000, \"decgNumRegistro\": \"CB201900018\", \"depositosCuenta\": [{\"dpIpd\": 3266, \"decgId\": 3333, \"dpClase\": \"Depósito en cuenta corriente\", \"dpSaldo\": 20122.0, \"dpEntidad\": \"ING\"}], \"decgNumExpediente\": \"2019_2023_09031574A\", \"otrosBienesMuebles\": [{\"obmId\": 2286, \"decgId\": 3506, \"obmMes\": \"Enero\", \"obmAnyo\": 2015, \"obmDesc\": \"piel piel\", \"obmTipos\": \"Pieles\"}], \"otrosDepositosCuenta\": [{\"decgId\": 3506, \"optrId\": 2826, \"optrMes\": \"Enero\", \"optrAnyo\": 2018, \"optrTipos\": \"Deuda pública\", \"optrValor\": 20212.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": \"asdasda\"}], \"informacionTributaria\": [{\"decgId\": 3506, \"inftId\": 3386, \"inftIsBiObserv\": null, \"inftIsBiImporte\": 0.0, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": 0.0, \"inftIrpfBiImporte\": 45645.0, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 45645645.0, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 0.0, \"inftIrpfDeducImporteB\": 0.0}], \"patrimonioInmobiliario\": [{\"mes\": \"Noviembre\", \"anyo\": 2012, \"clase\": \"Urbano\", \"pinId\": 3402, \"decgId\": 3506, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 100.0, \"valorCatastral\": 546489.0, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 4, \"deudas\": [{\"deuId\": 2286, \"decgId\": 3334, \"deuDesc\": \"deuda\", \"deuClase\": \"HIPOTECA\", \"deuImporte\": 56000.0}], \"dperId\": 33333, \"tdecId\": 1, \"decg_id\": 3334, \"cpNombre\": \"2019/2023\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"vehiculos\": [], \"decgDcmtId\": \"0902c25b800dabef\", \"dperNombre\": \"OSCAR\", \"dperNumper\": 6, \"decgFecAlta\": 1573197159000, \"dperApellido1\": \"MANGAS\", \"dperApellido2\": \"FERNANDEZ\", \"dperDocumento\": \"47463199P\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1571758802000, \"decgNumRegistro\": \"CB201900018\", \"depositosCuenta\": [{\"dpIpd\": 3266, \"decgId\": 3334, \"dpClase\": \"Depósito en cuenta corriente\", \"dpSaldo\": 20122.0, \"dpEntidad\": \"ING\"}], \"decgNumExpediente\": \"2019_2023_09031574A\", \"otrosBienesMuebles\": [{\"obmId\": 2286, \"decgId\": 3506, \"obmMes\": \"Enero\", \"obmAnyo\": 2015, \"obmDesc\": \"piel piel\", \"obmTipos\": \"Pieles\"}], \"otrosDepositosCuenta\": [{\"decgId\": 3506, \"optrId\": 2826, \"optrMes\": \"Enero\", \"optrAnyo\": 2018, \"optrTipos\": \"Deuda pública\", \"optrValor\": 20212.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": \"asdasda\"}], \"informacionTributaria\": [{\"decgId\": 3506, \"inftId\": 3386, \"inftIsBiObserv\": null, \"inftIsBiImporte\": 0.0, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": 0.0, \"inftIrpfBiImporte\": 45645.0, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 45645645.0, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 0.0, \"inftIrpfDeducImporteB\": 0.0}], \"patrimonioInmobiliario\": [{\"mes\": \"Noviembre\", \"anyo\": 2012, \"clase\": \"Urbano\", \"pinId\": 3402, \"decgId\": 3506, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 100.0, \"valorCatastral\": 546489.0, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 4, \"deudas\": [{\"deuId\": 2286, \"decgId\": 3335, \"deuDesc\": \"deuda\", \"deuClase\": \"HIPOTECA\", \"deuImporte\": 56000.0}], \"dperId\": 33333, \"tdecId\": 1, \"decg_id\": 3335, \"cpNombre\": \"2019/2023\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"vehiculos\": [], \"decgDcmtId\": \"0902c25b800dabef\", \"dperNombre\": \"OSCAR\", \"dperNumper\": 7, \"decgFecAlta\": 1573197159000, \"dperApellido1\": \"MANGAS\", \"dperApellido2\": \"FERNANDEZ\", \"dperDocumento\": \"47463199P\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1571758802000, \"decgNumRegistro\": \"CB201900018\", \"depositosCuenta\": [{\"dpIpd\": 3266, \"decgId\": 3335, \"dpClase\": \"Depósito en cuenta corriente\", \"dpSaldo\": 20122.0, \"dpEntidad\": \"ING\"}], \"decgNumExpediente\": \"2019_2023_09031574A\", \"otrosBienesMuebles\": [{\"obmId\": 2286, \"decgId\": 3506, \"obmMes\": \"Enero\", \"obmAnyo\": 2015, \"obmDesc\": \"piel piel\", \"obmTipos\": \"Pieles\"}], \"otrosDepositosCuenta\": [{\"decgId\": 3506, \"optrId\": 2826, \"optrMes\": \"Enero\", \"optrAnyo\": 2018, \"optrTipos\": \"Deuda pública\", \"optrValor\": 20212.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": \"asdasda\"}], \"informacionTributaria\": [{\"decgId\": 3506, \"inftId\": 3386, \"inftIsBiObserv\": null, \"inftIsBiImporte\": 0.0, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": 0.0, \"inftIrpfBiImporte\": 45645.0, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 45645645.0, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 0.0, \"inftIrpfDeducImporteB\": 0.0}], \"patrimonioInmobiliario\": [{\"mes\": \"Noviembre\", \"anyo\": 2012, \"clase\": \"Urbano\", \"pinId\": 3402, \"decgId\": 3506, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 100.0, \"valorCatastral\": 546489.0, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 4, \"deudas\": [{\"deuId\": 2286, \"decgId\": 4444, \"deuDesc\": \"deuda\", \"deuClase\": \"HIPOTECA\", \"deuImporte\": 56000.0}], \"dperId\": 44444, \"tdecId\": 1, \"decg_id\": 3336, \"cpNombre\": \"2019/2023\", \"decgYear\": null, \"dperSexo\": \"H\", \"tdNombre\": \"Inicial\", \"gperAbrev\": \"C\", \"vehiculos\": [], \"decgDcmtId\": \"0902c25b800dabef\", \"dperNombre\": \"OSCAR\", \"dperNumper\": 8, \"decgFecAlta\": 1573197159000, \"dperApellido1\": \"MANGAS\", \"dperApellido2\": \"FERNANDEZ\", \"dperDocumento\": \"47463199P\", \"dperCorreoCorp\": null, \"decgFecRegistro\": 1571758802000, \"decgNumRegistro\": \"CB201900018\", \"depositosCuenta\": [{\"dpIpd\": 3266, \"decgId\": 4444, \"dpClase\": \"Depósito en cuenta corriente\", \"dpSaldo\": 20122.0, \"dpEntidad\": \"ING\"}], \"decgNumExpediente\": \"2019_2023_09031574A\", \"otrosBienesMuebles\": [{\"obmId\": 2286, \"decgId\": 3506, \"obmMes\": \"Enero\", \"obmAnyo\": 2015, \"obmDesc\": \"piel piel\", \"obmTipos\": \"Pieles\"}], \"otrosDepositosCuenta\": [{\"decgId\": 3506, \"optrId\": 2826, \"optrMes\": \"Enero\", \"optrAnyo\": 2018, \"optrTipos\": \"Deuda pública\", \"optrValor\": 20212.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": \"asdasda\"}], \"informacionTributaria\": [{\"decgId\": 3506, \"inftId\": 3386, \"inftIsBiObserv\": null, \"inftIsBiImporte\": 0.0, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": 0.0, \"inftIrpfBiImporte\": 45645.0, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 45645645.0, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 0.0, \"inftIrpfDeducImporteB\": 0.0}], \"patrimonioInmobiliario\": [{\"mes\": \"Noviembre\", \"anyo\": 2012, \"clase\": \"Urbano\", \"pinId\": 3402, \"decgId\": 3506, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 100.0, \"valorCatastral\": 546489.0, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 6, \"deudas\": [{\"deuId\": 1161, \"decgId\": 7777, \"deuDesc\": null, \"deuClase\": \"Préstamo hipotecario vivienda habitual\", \"deuImporte\": 195738.13}, {\"deuId\": 1162, \"decgId\": 7777, \"deuDesc\": null, \"deuClase\": \"Préstamo efectivo\", \"deuImporte\": 5872.74}], \"dperId\": 77777, \"tdecId\": 1, \"decg_id\": 2373, \"cpNombre\": null, \"decgYear\": 2017, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"vehiculos\": [{\"vehId\": 1325, \"decgId\": 7777, \"vehMes\": \"04\", \"vehAnyo\": 2004, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW 316\"}, {\"vehId\": 1326, \"decgId\": 7777, \"vehMes\": null, \"vehAnyo\": null, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW X1\"}], \"decgDcmtId\": null, \"dperNombre\": \"JESÚS\", \"dperNumper\": 1, \"decgFecAlta\": 1505426400000, \"dperApellido1\": \"ARRIBAS\", \"dperApellido2\": \"DÍAZ\", \"dperDocumento\": \"20263796Z\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"depositosCuenta\": [{\"dpIpd\": 2089, \"decgId\": 7777, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2090, \"decgId\": 7777, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2091, \"decgId\": 7777, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 3000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2092, \"decgId\": 7777, \"dpClase\": \"Plan de ahorro\", \"dpSaldo\": 11000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2093, \"decgId\": 7777, \"dpClase\": \"Fondo de inversión\", \"dpSaldo\": 14000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}], \"decgNumExpediente\": null, \"otrosBienesMuebles\": [], \"otrosDepositosCuenta\": [{\"decgId\": 7777, \"optrId\": 1490, \"optrMes\": null, \"optrAnyo\": null, \"optrTipos\": \"Seguros de vida, planes de pensiones, rentas temporales y vitalicias\", \"optrValor\": 10000.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": null}], \"informacionTributaria\": [{\"decgId\": 7777, \"inftId\": 2236, \"inftIsBiObserv\": null, \"inftIsBiImporte\": null, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": null, \"inftIrpfBiImporte\": 20.62, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 86841.16, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 743.52, \"inftIrpfDeducImporteB\": 54.0}], \"patrimonioInmobiliario\": [{\"mes\": \"09\", \"anyo\": 2004, \"clase\": \"Urbano\", \"pinId\": 1951, \"decgId\": 7777, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 135478.87, \"tituloAdquisicion\": \"Compraventa\"}, {\"mes\": \"03\", \"anyo\": 2017, \"clase\": \"Urbano\", \"pinId\": 1952, \"decgId\": 7777, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 502871.88, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 6, \"deudas\": [{\"deuId\": 1161, \"decgId\": 7778, \"deuDesc\": null, \"deuClase\": \"Préstamo hipotecario vivienda habitual\", \"deuImporte\": 195738.13}, {\"deuId\": 1162, \"decgId\": 7778, \"deuDesc\": null, \"deuClase\": \"Préstamo efectivo\", \"deuImporte\": 5872.74}], \"dperId\": 77777, \"tdecId\": 1, \"decg_id\": 2374, \"cpNombre\": null, \"decgYear\": 2017, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"vehiculos\": [{\"vehId\": 1325, \"decgId\": 7778, \"vehMes\": \"04\", \"vehAnyo\": 2004, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW 316\"}, {\"vehId\": 1326, \"decgId\": 7778, \"vehMes\": null, \"vehAnyo\": null, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW X1\"}], \"decgDcmtId\": null, \"dperNombre\": \"JESÚS\", \"dperNumper\": 2, \"decgFecAlta\": 1608550439000, \"dperApellido1\": \"ARRIBAS\", \"dperApellido2\": \"DÍAZ\", \"dperDocumento\": \"20263796Z\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"depositosCuenta\": [{\"dpIpd\": 2089, \"decgId\": 7778, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2090, \"decgId\": 7778, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2091, \"decgId\": 7778, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 3000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2092, \"decgId\": 7778, \"dpClase\": \"Plan de ahorro\", \"dpSaldo\": 11000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2093, \"decgId\": 7778, \"dpClase\": \"Fondo de inversión\", \"dpSaldo\": 14000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}], \"decgNumExpediente\": null, \"otrosBienesMuebles\": [], \"otrosDepositosCuenta\": [{\"decgId\": 7778, \"optrId\": 1490, \"optrMes\": null, \"optrAnyo\": null, \"optrTipos\": \"Seguros de vida, planes de pensiones, rentas temporales y vitalicias\", \"optrValor\": 10000.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": null}], \"informacionTributaria\": [{\"decgId\": 7778, \"inftId\": 2236, \"inftIsBiObserv\": null, \"inftIsBiImporte\": null, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": null, \"inftIrpfBiImporte\": 20.62, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 86841.16, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 743.52, \"inftIrpfDeducImporteB\": 54.0}], \"patrimonioInmobiliario\": [{\"mes\": \"09\", \"anyo\": 2004, \"clase\": \"Urbano\", \"pinId\": 1951, \"decgId\": 7778, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 135478.87, \"tituloAdquisicion\": \"Compraventa\"}, {\"mes\": \"03\", \"anyo\": 2017, \"clase\": \"Urbano\", \"pinId\": 1952, \"decgId\": 7778, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 502871.88, \"tituloAdquisicion\": \"Compraventa\"}]},
                {\"estId\": 6, \"deudas\": [{\"deuId\": 1161, \"decgId\": 7779, \"deuDesc\": null, \"deuClase\": \"Préstamo hipotecario vivienda habitual\", \"deuImporte\": 195738.13}, {\"deuId\": 1162, \"decgId\": 7779, \"deuDesc\": null, \"deuClase\": \"Préstamo efectivo\", \"deuImporte\": 5872.74}], \"dperId\": 77797, \"tdecId\": 1, \"decg_id\": 2373, \"cpNombre\": null, \"decgYear\": 2017, \"dperSexo\": null, \"tdNombre\": \"Anual\", \"gperAbrev\": \"R\", \"vehiculos\": [{\"vehId\": 1325, \"decgId\": 7779, \"vehMes\": \"04\", \"vehAnyo\": 2004, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW 316\"}, {\"vehId\": 1326, \"decgId\": 7779, \"vehMes\": null, \"vehAnyo\": null, \"vehClase\": \"Automóvil\", \"vehMarcaMod\": \"BMW X1\"}], \"decgDcmtId\": null, \"dperNombre\": \"JESÚS\", \"dperNumper\": 3, \"decgFecAlta\": 1505426400000, \"dperApellido1\": \"ARRIBAS\", \"dperApellido2\": \"DÍAZ\", \"dperDocumento\": \"20263796Z\", \"dperCorreoCorp\": null, \"decgFecRegistro\": null, \"decgNumRegistro\": null, \"depositosCuenta\": [{\"dpIpd\": 2089, \"decgId\": 7779, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2090, \"decgId\": 7779, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 6000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2091, \"decgId\": 7779, \"dpClase\": \"Cuenta bancaria\", \"dpSaldo\": 3000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2092, \"decgId\": 7779, \"dpClase\": \"Plan de ahorro\", \"dpSaldo\": 11000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}, {\"dpIpd\": 2093, \"decgId\": 7779, \"dpClase\": \"Fondo de inversión\", \"dpSaldo\": 14000.0, \"dpEntidad\": \"Bilbao Bizkaia Kutxa\"}], \"decgNumExpediente\": null, \"otrosBienesMuebles\": [], \"otrosDepositosCuenta\": [{\"decgId\": 7779, \"optrId\": 1490, \"optrMes\": null, \"optrAnyo\": null, \"optrTipos\": \"Seguros de vida, planes de pensiones, rentas temporales y vitalicias\", \"optrValor\": 10000.0, \"optrTipoUnidad\": \"€\", \"optrDescripcion\": null}], \"informacionTributaria\": [{\"decgId\": 7779, \"inftId\": 2236, \"inftIsBiObserv\": null, \"inftIsBiImporte\": null, \"inftIspBiObserv\": null, \"inftIrpfBiObserv\": null, \"inftIspBiImporte\": null, \"inftIrpfBiImporte\": 20.62, \"inftIrpfBigObserv\": null, \"inftIrpfBigImporte\": 86841.16, \"inftIrpfDeducObservA\": null, \"inftIrpfDeducObservB\": null, \"inftIrpfDeducImporteA\": 743.52, \"inftIrpfDeducImporteB\": 54.0}], \"patrimonioInmobiliario\": [{\"mes\": \"09\", \"anyo\": 2004, \"clase\": \"Urbano\", \"pinId\": 1951, \"decgId\": 7779, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 135478.87, \"tituloAdquisicion\": \"Compraventa\"}, {\"mes\": \"03\", \"anyo\": 2017, \"clase\": \"Urbano\", \"pinId\": 1952, \"decgId\": 7779, \"municipio\": \"Madrid\", \"tipoDerecho\": \"Pleno dominio\", \"participacion\": 50.0, \"valorCatastral\": 502871.88, \"tituloAdquisicion\": \"Compraventa\"}]}
            ]
        }"
        NotificationsImporter.new().import_test_declarations(JSON.parse(declarations.to_s).with_indifferent_access, 'AssetsDeclaration')
    end

    desc "Pruebas"
    task p2: :environment do
      personas = "{ \"listaPersonas\": [{\"gper\": \"R\",
      \"vocal\": {\"vvId\": 602,
      \"vvnId\": 3581,
      \"dperId\": 26251,
      \"vvnArea\": null,
      \"cpNombre\": \"2019/2023\",
      \"gpNombre\": \"PP (Partido Popular)\",
      \"vvnCargo\": \"Vocal vecino\",
      \"disNombre\": \"Puente de Vallecas\",
      \"vvnUnidad\": \"Junta de distrito Puente de Vallecas\",
      \"vvnJuntaMd\": \"Puente de Vallecas\",
      \"vvnFechaCese\": null,
      \"vvnFechaNomb\": 1606777200000,
      \"vvnRetribucion\": 582.23,
      \"vvnFechaPosesion\": null,
      \"vvnObservaciones\": \"mensuales\",
      \"vvnAnnoRetribucion\": 2023,
      \"vvnDescripcionPosesion\": null},
      \"dperId\": 26251,
      \"concejal\": null,
      \"dperSexo\": \"M\",
      \"eventual\": null,
      \"directivo\": null,
      \"dperNombre\": \"NURIA\",
      \"dperNumper\": null,
      \"funcionario\": null,
      \"dperApellido1\": \"GARCIA \",
      \"dperApellido2\": \"MORENO\",
      \"dperDocumento\": \"51924840A\",
      \"tipoDocumento\": {\"tdocId\": 1,
      \"tdocNombre\": \"DNI\"},
      \"dperCorreoCorp\": \"nuriagar01@yahoo.es\"}]}"
     NotificationsImporter.new().import_test(JSON.parse(personas.to_s), 'Person')
    end

    desc "Pruebas"
    task p1: :environment do
      personas = "{ \"listaPersonas\": [
        {
            \"dperId\": 1453,
            \"dperNumper\": 111165,
            \"dperNombre\": \"RAQUEL\",
            \"dperApellido1\": \"MUROS\",
            \"dperApellido2\": \"DELGADO\",
            \"dperDocumento\": \"11843719F\",
            \"tipoDocumento\": {
                \"tdocId\": 1,
                \"tdocNombre\": \"DNI\"
            },
            \"dperSexo\": \"M\",
            \"dperCorreoCorp\": \"murosdr@madrid.es\",
            \"gper\": \"R\",
            \"concejal\": null,
            \"directivo\": null,
            \"eventual\": null,
            \"funcionario\": null,
            \"vocal\": null
        }
    ]}"
      NotificationsImporter.new().import_test(JSON.parse(personas.to_s), 'Person')
    end

    desc "Notificaciones para la bateria de pruebas"
    task test: :environment do
        personas = "{ \"listaPersonas\":
        [
            {
                \"dperId\": 1115,
                \"dperNumper\": 171177,
                \"dperNombre\": \"Jose\",
                \"dperApellido1\": \"Aniorte\",
                \"dperApellido2\": \"Rueda\",
                \"dperDocumento\": \"10293829G\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 1115,
                    \"cjId\": 935,
                    \"cjnId\": 1161,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": \"\",
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1554242400000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 381,
                \"dperNumper\": 57146,
                \"dperNombre\": \"Paloma\",
                \"dperApellido1\": \"Hernández\",
                \"dperApellido2\": \"Navarro\",
                \"dperDocumento\": \"50421035K\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"hernandezp@madrid.es\",
                \"gper\": \"R\",
                \"concejal\": \"\",
                \"directivo\": {
                    \"dperId\": 381,
                    \"drId\": 217,
                    \"drnId\": 216,
                    \"drnFechaNomb\": 1529964000000,
                    \"drnCargo\": \"DIRECTOR/A GENERAL\",
                    \"drnUnidad\": \"GC ASESORIA JURIDICA\",
                    \"drnFechaCese\": \"\",
                    \"drnArea\": \"\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2938,
                \"dperNumper\": 171358,
                \"dperNombre\": \"Jose Javier\",
                \"dperApellido1\": \"Barbero\",
                \"dperApellido2\": \"Gutierrez\",
                \"dperDocumento\": \"78346272S\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"F\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": {
                    \"dperId\": 2938,
                    \"fldId\": 930,
                    \"fldnId\": 1990,
                    \"fldnFechaNomb\": 809906400000,
                    \"fldnCargo\": \"JEFE/A DEPARTAMENTO\",
                    \"fldnUnidad\": \"DEPARTAMENTO GRANDES DEUDORES\",
                    \"fldnFechaCese\": \"\",
                    \"fldnArea\": \"AGENCIA TRIBUTARIA MADRID\"
                },
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2798,
                \"dperNumper\": 65556,
                \"dperNombre\": \"PEDRO ESTEBAN\",
                \"dperApellido1\": \"BARRERO\",
                \"dperApellido2\": \"CUADRADO\",
                \"dperDocumento\": \"74855199C\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"H\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"E\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": {
                    \"dperId\": 2798,
                    \"evtId\": 195,
                    \"evtnId\": 220,
                    \"evtnFechaNomb\": 1565215200000,
                    \"evtnCargo\": \"ASESOR/A N28\",
                    \"evtnUnidad\": \"UNIDAD DE PERSONAL EVENTUAL\",
                    \"evtnFechaCese\": \"\",
                    \"evtnArea\": \"ALCALDIA\"
                },
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1561,
                \"dperNumper\": 171004,
                \"dperNombre\": \"Sergio\",
                \"dperApellido1\": \"Brabezo\",
                \"dperApellido2\": \"Carballo\",
                \"dperDocumento\": \"89213573T\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"H\",
                \"dperCorreoCorp\": \"antonio.gonzalez@madrid.com\",
                \"gper\": \"C\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": {
                    \"dperId\": 1561,
                    \"vvId\": 123,
                    \"vvnId\": 123,
                    \"vvnFechaNomb\": 1565992800000,
                    \"vvnCargo\": \"Coordinador\",
                    \"vvnUnidad\": \"Distrito Arganzuela\",
                    \"vvnFechaCese\": 1567116000000,
                    \"vvnArea\": \"\"
                }
            },
            {
                \"dperId\": 1315,
                \"dperNumper\": 171053,
                \"dperNombre\": \"Manuela\",
                \"dperApellido1\": \"Carmena\",
                \"dperApellido2\": \"Castrillo\",
                \"dperDocumento\": \"05468049Y\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 1315,
                    \"cjId\": 935,
                    \"cjnId\": 1161,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": \"\",
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1554242400000,
                    \"cjnFechaCese\": 1576163255000,
                    \"cjnArea\": \"\"
                },
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 381,
                \"dperNumper\": 57146,
                \"dperNombre\": \"Paloma\",
                \"dperApellido1\": \"Hernández\",
                \"dperApellido2\": \"Navarro\",
                \"dperDocumento\": \"50421035K\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"hernandezp2@madrid.es\",
                \"gper\": \"R\",
                \"concejal\": {
                    \"dperId\": 381,
                    \"cjId\": 242,
                    \"cjnId\": 593,
                    \"corporacion\": {
                        \"cpId\": 382,
                        \"cpNombre\": \"2019/2023\",
                        \"cpAnioInicio\": 2019,
                        \"cpAnioFin\": 2023,
                        \"cpFechaElecciones\": 1558821600000,
                        \"cpFechaFinal\": 1576164957000,
                        \"cpFechaPlenoConst\": 1560549600000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"HOLA\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": 2,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1560722400000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": {
                    \"dperId\": 381,
                    \"drId\": 217,
                    \"drnId\": 216,
                    \"drnFechaNomb\": 1529964000000,
                    \"drnCargo\": \"DIRECTOR/A GENERAL\",
                    \"drnUnidad\": \"GC ASESORIA JURIDICA\",
                    \"drnFechaCese\": 1529964000000,
                    \"drnArea\": \"\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 383,
                \"dperNumper\": 9066,
                \"dperNombre\": \"Beatriz Maria\",
                \"dperApellido1\": \"Elorriaga\",
                \"dperApellido2\": \"Pisarik\",
                \"dperDocumento\": \"50421035K\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"hernandezp@madrid.es\",
                \"gper\": \"R\",
                \"concejal\": {
                    \"dperId\": 381,
                    \"cjId\": 242,
                    \"cjnId\": 593,
                    \"corporacion\": {
                        \"cpId\": 382,
                        \"cpNombre\": \"2019/2023\",
                        \"cpAnioInicio\": 2019,
                        \"cpAnioFin\": 2023,
                        \"cpFechaElecciones\": 1558821600000,
                        \"cpFechaFinal\": 1576164957000,
                        \"cpFechaPlenoConst\": 1560549600000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"HOLA\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": 2,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1560722400000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": {
                    \"dperId\": 381,
                    \"drId\": 1,
                    \"drnId\": 1,
                    \"drnFechaNomb\": 1553036400000,
                    \"drnCargo\": \"Director/a General\",
                    \"drnUnidad\": \"IAM\",
                    \"drnFechaCese\": \"\",
                    \"drnArea\": \"INFORMATICA DEL AYUNTAMIENTO DE MADRID\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1343,
                \"dperNumper\": \"20679\",
                \"dperNombre\": \"Paloma\",
                \"dperApellido1\": \"García\",
                \"dperApellido2\": \"Romero\",
                \"dperDocumento\": \"50413978W\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": \"\",
                \"directivo\": {
                    \"dperId\": 1343,
                    \"drId\": 217,
                    \"drnId\": 216,
                    \"drnFechaNomb\": 1529964000000,
                    \"drnCargo\": \"DIRECTOR/A GENERAL\",
                    \"drnUnidad\": \"GC ASESORIA JURIDICA\",
                    \"drnFechaCese\": \"\",
                    \"drnArea\": \"\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1344,
                \"dperNumper\": \"20679\",
                \"dperNombre\": \"Paloma\",
                \"dperApellido1\": \"García\",
                \"dperApellido2\": \"Romero\",
                \"dperDocumento\": \"50413978W\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 381,
                    \"cjId\": 242,
                    \"cjnId\": 593,
                    \"corporacion\": {
                        \"cpId\": 382,
                        \"cpNombre\": \"2019/2023\",
                        \"cpAnioInicio\": 2019,
                        \"cpAnioFin\": 2023,
                        \"cpFechaElecciones\": 1558821600000,
                        \"cpFechaFinal\": 1576164957000,
                        \"cpFechaPlenoConst\": 1560549600000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"HOLA\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": 2,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1560722400000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": {
                    \"dperId\": 1343,
                    \"drId\": 217,
                    \"drnId\": 216,
                    \"drnFechaNomb\": 1529964000000,
                    \"drnCargo\": \"DIRECTOR/A GENERAL\",
                    \"drnUnidad\": \"GC ASESORIA JURIDICA\",
                    \"drnFechaCese\": 1529964000000,
                    \"drnArea\": \"\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2939,
                \"dperNumper\": 5697,
                \"dperNombre\": \"Iñigo\",
                \"dperApellido1\": \"Henriquez de Luna\",
                \"dperApellido2\": \"Losada\",
                \"dperDocumento\": \"03427361S\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"F\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": {
                    \"dperId\": 2939,
                    \"fldId\": 930,
                    \"fldnId\": 1990,
                    \"fldnFechaNomb\": 809906400000,
                    \"fldnCargo\": \"JEFE/A DEPARTAMENTO\",
                    \"fldnUnidad\": \"DEPARTAMENTO GRANDES DEUDORES\",
                    \"fldnFechaCese\": \"\",
                    \"fldnArea\": \"AGENCIA TRIBUTARIA MADRID\"
                },
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2674,
                \"dperNumper\": 171140,
                \"dperNombre\": \"ESTHER\",
                \"dperApellido1\": \"GOMEZ\",
                \"dperApellido2\": \"MORANTE\",
                \"dperDocumento\": \"03427360S\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"F\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": {
                    \"dperId\": 2674,
                    \"evtId\": 71,
                    \"evtnId\": 96,
                    \"evtnFechaNomb\": 1565128800000,
                    \"evtnCargo\": \"ASESOR/A N26\",
                    \"evtnUnidad\": \"CONCEJAL PRESIDENTE JMD CENTRO\",
                    \"evtnFechaCese\": \"\",
                    \"evtnArea\": \"DISTRITO DE CENTRO\"
                },
                \"funcionario\": {
                    \"dperId\": 2938,
                    \"fldId\": 930,
                    \"fldnId\": 1990,
                    \"fldnFechaNomb\": 809906400000,
                    \"fldnCargo\": \"JEFE/A DEPARTAMENTO\",
                    \"fldnUnidad\": \"DEPARTAMENTO GRANDES DEUDORES\",
                    \"fldnFechaCese\": 809906400000,
                    \"fldnArea\": \"AGENCIA TRIBUTARIA MADRID\"
                },
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1116,
                \"dperNumper\": 171154,
                \"dperNombre\": \"MARTA MARIA\",
                \"dperApellido1\": \"HIGUERAS\",
                \"dperApellido2\": \"GARROBO\",
                \"dperDocumento\": \"05468043Y\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 1116,
                    \"cjId\": 935,
                    \"cjnId\": 1161,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ahora Madrid\",
                    \"numOrden\": \"\",
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1554242400000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 3104,
                \"dperNumper\": 171152,
                \"dperNombre\": \"BOSCO\",
                \"dperApellido1\": \"LABRADO\",
                \"dperApellido2\": \"PRIETO\",
                \"dperDocumento\": \"00693810S\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"F\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": {
                    \"dperId\": 3104,
                    \"fldId\": 1096,
                    \"fldnId\": 2156,
                    \"fldnFechaNomb\": 676677600000,
                    \"fldnCargo\": \"JEFE/A DEPARTAMENTO\",
                    \"fldnUnidad\": \"DEPARTAMENTO DE REGULARIZACION JURIDICA DEL INVENTARIO\",
                    \"fldnFechaCese\": \"\",
                    \"fldnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"vocal\": \"\"
            },
            {
                \"dperId\": 3105,
                \"dperNumper\": 63921,
                \"dperNombre\": \"MARIA BEGOÑA\",
                \"dperApellido1\": \"LARRAINZAR\",
                \"dperApellido2\": \"ZABALLA\",
                \"dperDocumento\": \"00693811S\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"F\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": {
                    \"dperId\": 3105,
                    \"fldId\": 1096,
                    \"fldnId\": 2156,
                    \"fldnFechaNomb\": 676677600000,
                    \"fldnCargo\": \"JEFE/A DEPARTAMENTO\",
                    \"fldnUnidad\": \"DEPARTAMENTO DE REGULARIZACION JURIDICA DEL INVENTARIO\",
                    \"fldnFechaCese\": 676677600000,
                    \"fldnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1088,
                \"dperNumper\": 171356,
                \"dperNombre\": \"Rita\",
                \"dperApellido1\": \"Maestre\",
                \"dperApellido2\": \"Fernández\",
                \"dperDocumento\": \"05301912K\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 1088,
                    \"cjId\": 908,
                    \"cjnId\": 1164,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ciudadanos\",
                    \"numOrden\": 8,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1547679600000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1089,
                \"dperNumper\": 171357,
                \"dperNombre\": \"Sara\",
                \"dperApellido1\": \"Fernandez\",
                \"dperApellido2\": \"Maestre\",
                \"dperDocumento\": \"05301913K\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"C\",
                \"concejal\": {
                    \"dperId\": 1089,
                    \"cjId\": 908,
                    \"cjnId\": 1164,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ciudadanos\",
                    \"numOrden\": 8,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1547679600000,
                    \"cjnFechaCese\": 1576163255000,
                    \"cjnArea\": \"\"
                },
                \"directivo\": \"\",
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1312,
                \"dperNumper\": 5586,
                \"dperNombre\": \"FERNANDO\",
                \"dperApellido1\": \"MARTÍNEZ\",
                \"dperApellido2\": \"VIDAL\",
                \"dperDocumento\": \"51367050T\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"R\",
                \"concejal\": \"\",
                \"directivo\": {
                    \"dperId\": 1312,
                    \"drId\": 186,
                    \"drnId\": 398,
                    \"drnFechaNomb\": 1561672800000,
                    \"drnCargo\": \"PENDIENTE DE ADSCRIPCION\",
                    \"drnUnidad\": \"AGO SGT OBRAS Y EQUIPAMIENTOS\",
                    \"drnFechaCese\": \"\",
                    \"drnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 1313,
                \"dperNumper\": 171135,
                \"dperNombre\": \"CELIA\",
                \"dperApellido1\": \"MAYER\",
                \"dperApellido2\": \"DUQUE\",
                \"dperDocumento\": \"51367051T\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"R\",
                \"concejal\": \"\",
                \"directivo\": {
                    \"dperId\": 1313,
                    \"drId\": 186,
                    \"drnId\": 398,
                    \"drnFechaNomb\": 1561672800000,
                    \"drnCargo\": \"PENDIENTE DE ADSCRIPCION\",
                    \"drnUnidad\": \"AGO SGT OBRAS Y EQUIPAMIENTOS\",
                    \"drnFechaCese\": 1561672800000,
                    \"drnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2682,
                \"dperNumper\": 171131,
                \"dperNombre\": \"MARIA CARLOTA\",
                \"dperApellido1\": \"MERCHÁN\",
                \"dperApellido2\": \"MESÓN\",
                \"dperDocumento\": \"02253335W\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"E\",
                \"concejal\": \"\",
                \"directivo\": \"\",
                \"eventual\": {
                    \"dperId\": 2682,
                    \"evtId\": 79,
                    \"evtnId\": 104,
                    \"evtnFechaNomb\": 1565560800000,
                    \"evtnCargo\": \"ASESOR/A N26\",
                    \"evtnUnidad\": \"CONCEJAL PRESIDENTE JMD VILLAVERDE\",
                    \"evtnFechaCese\": \"\",
                    \"evtnArea\": \"DISTRITO DE VILLAVERDE\"
                },
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2683,
                \"dperNumper\": 171005,
                \"dperNombre\": \"SOFIA\",
                \"dperApellido1\": \"MIRANDA\",
                \"dperApellido2\": \"ESTEBAN\",
                \"dperDocumento\": \"02253336W\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"E\",
                \"concejal\": \"\",
                \"directivo\": {
                    \"dperId\": 2683,
                    \"drId\": 186,
                    \"drnId\": 398,
                    \"drnFechaNomb\": 1561672800000,
                    \"drnCargo\": \"PENDIENTE DE ADSCRIPCION\",
                    \"drnUnidad\": \"AGO SGT OBRAS Y EQUIPAMIENTOS\",
                    \"drnFechaCese\": \"\",
                    \"drnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"eventual\": {
                    \"dperId\": 2683,
                    \"evtId\": 79,
                    \"evtnId\": 104,
                    \"evtnFechaNomb\": 1565560800000,
                    \"evtnCargo\": \"ASESOR/A N26\",
                    \"evtnUnidad\": \"CONCEJAL PRESIDENTE JMD VILLAVERDE\",
                    \"evtnFechaCese\": 1565560800000,
                    \"evtnArea\": \"DISTRITO DE VILLAVERDE\"
                },
                \"funcionario\": \"\",
                \"vocal\": \"\"
            },
            {
                \"dperId\": 2684,
                \"dperNumper\": 171655,
                \"dperNombre\": \"MARIA\",
                \"dperApellido1\": \"PEREZ\",
                \"dperApellido2\": \"PEREZ\",
                \"dperDocumento\": \"02253337W\",
                \"tipoDocumento\": {
                    \"tdocId\": 1,
                    \"tdocNombre\": \"DNI\"
                },
                \"dperSexo\": \"M\",
                \"dperCorreoCorp\": \"\",
                \"gper\": \"E\",
                \"concejal\": {
                    \"dperId\": 2684,
                    \"cjId\": 908,
                    \"cjnId\": 1164,
                    \"corporacion\": {
                        \"cpId\": 381,
                        \"cpNombre\": \"2015/2019\",
                        \"cpAnioInicio\": 2015,
                        \"cpAnioFin\": 2019,
                        \"cpFechaElecciones\": 1432418400000,
                        \"cpFechaFinal\": 1576163255000,
                        \"cpFechaPlenoConst\": 1434146400000,
                        \"cpNumConcej\": 57,
                        \"cpObserv\": \"\",
                        \"cpActiva\": 0,
                        \"listasElectorales\": []
                    },
                    \"afiliacion\": \"Ciudadanos\",
                    \"numOrden\": 8,
                    \"numLista\": 1,
                    \"cjnAlcalde\": 0,
                    \"cjnFechaNom\": 1547679600000,
                    \"cjnFechaCese\": \"\",
                    \"cjnArea\": \"\"
                },
                \"directivo\": {
                    \"dperId\": 2684,
                    \"drId\": 186,
                    \"drnId\": 398,
                    \"drnFechaNomb\": 1561672800000,
                    \"drnCargo\": \"PENDIENTE DE ADSCRIPCION\",
                    \"drnUnidad\": \"AGO SGT OBRAS Y EQUIPAMIENTOS\",
                    \"drnFechaCese\": 1561672800000,
                    \"drnArea\": \"AREA DE GOBIERNO DE OBRAS Y EQUIPAMIENTOS\"
                },
                \"eventual\": \"\",
                \"funcionario\": \"\",
                \"vocal\": \"\"
            }
        ]}"

        NotificationsImporter.new().import_test(JSON.parse(personas.to_s).with_indifferent_access, 'Person')
    end

    desc "Corregir notificación 4854"
    task fix_4854: :environment do

        @notification = Notification.find(4854)
        @notification.permit = true
        @notification.administrator = Administrator.find(15)
        @notification.change_state_at = Time.zone.now
        @notification.send_feedback = false
        asset_new = AssetsDeclaration.transformData(@notification)
        duty = Duty.new(declaration: asset_new, start_date: asset_new.declaration_date, end_date: asset_new.declaration_date,
            limit_date: asset_new.declaration_date, person_level: asset_new.person_level, type_duty: asset_new.model_name.to_s, year: asset_new.year_period,
            temporality: asset_new.temporality)
        duty.save
        Audit.create!(administrator: Administrator.find(15),
            person: asset_new.try(:person_level).try(:person),
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: asset_new.try(:person_level).try(:person).try(:backwards_name)))
        if @notification.save
            ws = INPERWS.new
            person_aux_types = {
                'C' => 'councillors',
                'R' => 'directors',
                'F' => 'public_workers',
                'E' => 'temporary_workers',
                'V' => 'spokespeople' }
            type = @notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : person_aux_types[@notification.try(:content_data).try{|x| x['gperAbrev']}]
            person_identificator = @notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? @notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : @notification.try(:content_data).try{|x| x['dperNumper']}.to_s
            aux_person = Person.find_by(personal_code: person_identificator)

            begin
                @notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / "
                inper_response = ws.setWSDeclaracionPublicado(@notification.try(:content_data)["decg_id"],@notification.change_state_at,0,
                                                              "Se han publicado los datos en TRAPE con fecha #{@notification.change_state_at}").to_s
                begin
                  respuesta= JSON.parse(inper_response)
                  if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                    @notification.no_publish_reason = @notification.no_publish_reason + "Se ha realizado el feedback"
                    @notification.motive = ""
                    @notification.send_feedback = true
                  else
                    @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
                    @notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                  end
                rescue => e
                  @notification.no_publish_reason = @notification.no_publish_reason + "No se ha realizado el feedback"
                  @notification.motive = e.message
                end
            rescue => e
                @notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{@notification.try(:content_data)['decg_id']} / No se ha realizado el feedback"
                @notification.motive = e.message
                @notification.save
                begin
                  logger.error("ERROR: #{e}")
                  rescue
                  end
            end
            begin
                if !Rails.env.production?
                    mailers_test.each do |mail|
                        Mailer.assets_upload(type,mail).deliver_now
                    end
                else
                    if !aux_person.profile.email.blank?
                        Mailer.assets_upload(type,aux_person.profile.email).deliver_now
                    elsif !aux_person.profile.personal_email.blank?
                        Mailer.assets_upload(type,aux_person.profile.personal_email).deliver_now
                    end
                end
            rescue => e
                begin
                    logger.error("ERROR: #{e}")
                    rescue
                    end
            end
        end

    end
end
