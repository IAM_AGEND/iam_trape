require 'importers/initial/initial_importer'
require 'importers/people/person_importer'
require 'importers/profiles/profiles_importer'
require 'importers/activities/activities_importer'
require 'importers/assets/assets_importer'
require 'importers/administrator/administrator_importer'
require 'importers/emails_importer'
require 'database_cleaner'
require 'inper_ws'

namespace :import do
  desc "Limpieza de bases de datos"
  task clear: :environment do
    DatabaseCleaner.clean_with(:truncation, :except => %w[administrators])
  end

  desc "Imports everything"
  task all: ['import:statics', 'import:people', 'import:profiles', 'import:activities_declarations','import:assets_declarations','import:administrator','import:preferences']

  desc "Importación de todos los elementos estáticos"
  task statics: :environment do 
    Importers::TemporalitiesImporter.new('./import-data/initial/temporalities.csv').import!
    Importers::ElectoralListsImporter.new('./import-data/initial/electoral_lists.csv').import!
    Importers::DocumentTypesImporter.new('./import-data/initial/document_types.csv').import!
    Importers::CorporationsImporter.new('./import-data/initial/corporations.csv').import!
    Importers::PrivateActivityTypesImporter.new('./import-data/initial/private_activity_types.csv').import!
    Importers::TextContentsImporter.new('./import-data/initial/text_contents.csv').import!
    Importers::LanguageNamesImporter.new('./import-data/initial/language_names.csv').import!
    Importers::LanguageLevelsImporter.new('./import-data/initial/language_levels.csv').import!
    Importers::InformationTypesImporter.new('./import-data/initial/information_types.csv').import!
    Importers::CorporationsElectoralListsImporter.new('./import-data/initial/corporations_electoral_lists.csv').import!
    Importers::PublicAdministrationsImporter.new('./import-data/initial/public_administrations.csv').import!
  end

  desc "Importación de personas"
  task people: :environment do
    Importers::PersonImporter.new('./import-data/people/people.csv').import!
    Importers::CouncillorsImporter.new('./import-data/people/councillors.csv').import!
    Importers::DirectorsImporter.new('./import-data/people/directors.csv').import!
    Importers::TemporaryWorkersImporter.new('./import-data/people/temporary_workers.csv').import!
    Importers::PublicWorkersImporter.new('./import-data/people/public_workers.csv').import!
    Importers::SpokepeopleImporter.new('./import-data/people/spokepeople.csv').import!
  end

  desc "Importación de perfiles"
  task profiles: :environment do
    Importers::ProfilesImporter.new('./import-data/profiles/profiles.csv').import!
    Importers::PoliticalPostsImporter.new('./import-data/profiles/political_posts.csv').import!
    Importers::PrivateJobsImporter.new('./import-data/profiles/private_jobs.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/studies_comments.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/courses_comments.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/career_comments.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/political_posts_comments.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/teaching_activities.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/publications.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/special_mentions.csv').import!
    Importers::InformationsImporter.new('./import-data/profiles/other.csv').import!
    Importers::StudiesImporter.new('./import-data/profiles/studies.csv').import!
    Importers::CoursesImporter.new('./import-data/profiles/courses.csv').import!
    Importers::PublicJobsImporter.new('./import-data/profiles/public_jobs.csv').import!
    Importers::LanguagesImporter.new('./import-data/profiles/languages.csv').import!
  end

  desc "Importación de declaraciones de actividades"
  task activities_declarations: :environment do
    Importers::ActivitiesDeclarationsImporter.new('./import-data/activities/activities_declarations.csv').import!
    Importers::PublicActivitiesImporter.new('./import-data/activities/public_activities.csv').import!
    Importers::PrivateActivitiesImporter.new('./import-data/activities/private_activities.csv').import!
    Importers::OtherActivitiesImporter.new('./import-data/activities/other_activities.csv').import!
  end

  desc "Importación de declaraciones de bienes"
  task assets_declarations: :environment do
    Importers::AssetsDeclarationsImporter.new('./import-data/assets/assets_declarations.csv').import!
    Importers::VehiclesImporter.new('./import-data/assets/vehicles.csv').import!
    Importers::RealEstatePropertiesImporter.new('./import-data/assets/real_estate_properties.csv').import!
    Importers::AccountDepositsImporter.new('./import-data/assets/account_deposits.csv').import!
    Importers::OtherDepositsImporter.new('./import-data/assets/other_deposits.csv').import!
    Importers::DebtsImporter.new('./import-data/assets/debts.csv').import!
    Importers::TaxDataImporter.new('./import-data/assets/tax_data.csv').import!
    Importers::OtherPersonalPropertiesImporter.new('./import-data/assets/other_personal_properties.csv').import!
  end

  desc "Importación de partes de administración"
  task administrator: :environment do
    Importers::AuditsImporter.new('./import-data/administrator/audits.csv').import!
    Importers::DeleteLogsPostsImporter.new('./import-data/administrator/delete_logs.csv').import!
    Importers::FileUploadsImporter.new('./import-data/administrator/file_uploads.csv').import!
  end

  desc "Importación de preferencias genéricas"
  task preferences: :environment do
    DataPreference.find_or_create_by(title: "Años publicados", type_data: 0, content_data: 2, code: "public_year")
    DataPreference.find_or_create_by(title: "Número declarciones anuales", type_data: 0, content_data: 5, code: "num_year_declaration")
    DataPreference.find_or_create_by(title: "Enlace Información sobre retribuciones", type_data: 1,code: "link_information_retribuction", content_data: "https://transparencia.madrid.es/portales/transparencia/es/Recursos-humanos/Retribuciones/Retribuciones-del-alcalde-concejales-cargos-directivos-y-personal-eventual/?vgnextfmt=default&vgnextoid=a9687bcf66b06310VgnVCM1000000b205a0aRCRD&vgnextchannel=0d59508929a56510VgnVCM1000008a4a900aRCRD")
    DataPreference.find_or_create_by(title: "Número de días reintentos notificaciones automáticos", type_data: 0, content_data: 5, code: "num_notification_reintent")
    DataPreference.find_or_create_by(title: "Número reintentos notificaciones manuales", type_data: 0, content_data: 5, code: "num_notification_reintent_manual")
    DataPreference.find_or_create_by(title: "Umbral notificaciones pendientes", type_data: 0, content_data: 0, code: "notification_pending_alert")
    DataPreference.find_or_create_by(title: "Número de reintentos automáticos notificaciones al día", type_data: 0, content_data: 1, code: "num_send_retry_diary")
    DataPreference.find_or_create_by(title: "Destinatarios alertas", type_data: 1, content_data: "", code: "alert_senders")
    DataPreference.find_or_create_by(title: "Años de publicación de concejales", type_data: 0, content_data: "4", code: "councillors_publication_year")
    DataPreference.find_or_create_by(title: "Horas para la exportación de archivos", type_data: 0, content_data: "24", code: "hour_export_data")
    DataPreference.find_or_create_by(title: "Horas de ejecución tarea", type_data: 0, content_data: "24", code: "hour_exec_data")
    DataPreference.find_or_create_by(title: "Fecha de obligaciones", type_data: 3, content_data: "2020-06-12", code: "dutie_date")
    DataPreference.find_or_create_by(title: "Denominación del 'Enlace Información sobre retribuciones'", type_data: 1,code: "name_link_information_retribuction", content_data: I18n.t("people.retribuccion"))
    DataPreference.find_or_create_by(title: "Denominación del 'Enlace de contacto de concejal'", type_data: 1,code: "name_link_councillor_contact", content_data: I18n.t("people.councillor_contact"))

  end

  desc "Importación de preferencias emails"
  task emails: :environment do
    data_json_cc= {"councillors" => "transparenciaydatos@madrid.es", "directors" => "transparenciaydatos@madrid.es", 
      "public_workers" => "transparenciaydatos@madrid.es", "temporary_workers" => "transparenciaydatos@madrid.es",
      "spokespeople" => "transparenciaydatos@madrid.es" }
    data_json_cco = {"councillors" => "creaspodhe@madrid.es", "directors" => "creaspodhe@madrid.es", 
      "public_workers" => "creaspodhe@madrid.es", "temporary_workers" => "creaspodhe@madrid.es",
      "spokespeople" => "creaspodhe@madrid.es" }

    email = ManageEmail.find_by(:type_data => "ProfilesUpload")
    if email.blank?
      ManageEmail.create!(type_data: "ProfilesUpload", sender: "transparenciaydatos@madrid.es",
        fields_cc: data_json_cc, 
        fields_cco: data_json_cco,
        subject: I18n.t("emails.subject.profiles_upload"), 
        email_body: I18n.t("emails.body.profiles_upload")) 
    end
    email = ManageEmail.find_by(:type_data => 'AssetsUpload')
    if email.blank?
      ManageEmail.create!(type_data: 'AssetsUpload', sender: "transparenciaydatos@madrid.es",
        fields_cc: data_json_cc, 
        fields_cco: data_json_cco,
        subject: I18n.t("emails.subject.assets_upload"), 
        email_body: I18n.t("emails.body.assets_upload")) 
    end
    email = ManageEmail.find_by(:type_data => 'ActivitiesUpload')
    if email.blank?
      ManageEmail.create!(type_data: 'ActivitiesUpload', sender: "transparenciaydatos@madrid.es",
        fields_cc: data_json_cc, 
        fields_cco: data_json_cco,
        subject: I18n.t("emails.subject.activities_upload"), 
        email_body: I18n.t("emails.body.activities_upload")) 
    end
  end

  desc "Importación de personas"
  task update_emails: :environment do
    Importers::EmailsImporter.new('./import-data/emails.csv').import!
  end

  desc "Importación de administrador"
  task admin: :environment do
    begin
      Administrator.create!(email: 'admin@madrid.es', password: Rails.application.secrets.password)
    rescue
      puts "Ya está creado"
    end
  end
end
