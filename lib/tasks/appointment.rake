namespace :appointment do
    desc 'Insert users for development environment'
    task change: :environment do
        appointments = Appointment.where(area: "Sin Área de Gobierno")
        contador = appointments.count
        puts "="*30
        puts "Areas a cambiar: #{appointments.count}"
        i = 0
        appointments.each do |ap|
            ap.area = ""
            if ap.save
                puts "- Área cambiada con id: #{ap.id}"
                i = i + 1
            end
        end
        puts "="*30
        puts "Cambiadas #{i}/#{contador}"
        puts "="*30
    end
end