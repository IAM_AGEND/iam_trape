require 'duties_importer'

namespace :duties do

    desc "Importación de obligaciones"
    task import: :environment do
        DutiesImporter.new().import
    end

    task remove_duplicated: :environment do
        Duty.all.order(end_date: :desc).each do |duty1|
            duties = Duty.where(declaration: duty1.declaration, person_level: duty1.person_level, temporality: duty1.temporality, year: duty1.year,type_duty: duty1.type_duty).where("id != #{self.id}")
            duties.each do |duty2|
                puts "=========================Repetido============================"
                puts duty1.id
                puts duty2.attributes
                puts "====================================================="
                if duty2.destroy
                    puts "Se ha eliminado correctamente la obligación duplicada"
                else
                    puts "ERROR: No se ha podido eliminar la obligación duplicada"
                end
            end

            duties = Duty.where(declaration: nil, person_level: duty1.person_level, temporality: duty1.temporality, year: duty1.year,type_duty: duty1.type_duty).where("id != #{duty1.id}")
            duties.each do |duty2|
                puts "=========================Repetido============================"
                puts duty1.id
                puts duty2.attributes
                puts "====================================================="
                if duty2.destroy
                    puts "Se ha eliminado correctamente la obligación duplicada"
                else
                    puts "ERROR: No se ha podido eliminar la obligación duplicada"
                end
            end
        end
    end

    desc "Importación de preferencias de fecha de obligaciones"
    task duties_date: :environment do
        preference = DataPreference.find_by(:title => "Fecha de obligaciones")
        DataPreference.create!(title: "Fecha de obligaciones", type_data: 3, content_data: "2020-06-12") if preference.blank?
    end

    desc "Elimina las declaraciones anuales que coinciden con el año de una inicial o que se crean después de una final"
    task remove_anual_error: :environment do
      Duty.where(declaration: nil,temporality: Temporality.find_by(name: 'Anual')).each do |d|
        duty_exist = Duty.where(person_level: d.person_level, temporality: Temporality.find_by(name: 'Inicial'),type_duty: d.type_duty).where("cast(start_date as varchar) like (?)","%#{d.year}%").first

        if !duty_exist.blank?
          p "INICIAL"
          p "*"*50
          p d.attributes
          p "="*50
          p duty_exist.attributes
          if d.destroy
            p "Se ha eliminado correctamente #{d.id}"
          else
            p "No se ha podido eliminar #{d.id}"
          end
        end

        duty_exist = Duty.where(person_level: d.person_level, temporality: Temporality.find_by(name: 'Final'),type_duty: d.type_duty).where("cast(start_date as varchar) like (?)","%#{d.year}%").first

        if !duty_exist.blank? && duty_exist.start_date <= d.start_date
          p "FINAL"
          p "*"*50
          p d.attributes
          p "="*50
          p duty_exist.attributes
          if d.destroy
            p "Se ha eliminado correctamente #{d.id}"
          else
            p "No se ha podido eliminar #{d.id}"
          end
        end

      end

    end

    desc "Elimina las declaraciones que coinciden y están vacías"
    task remove_error: :environment do
      Duty.where(declaration: nil).each do |d|
        duty_exist = Duty.where(person_level: d.person_level,year: d.year, temporality: d.temporality,type_duty: d.type_duty).where("id != ?",d.id).first

        if !duty_exist.blank?
          p "Duplicada vacia"
          p "*"*50
          p d.attributes
          p "="*50
          p duty_exist.attributes
          if d.destroy
            p "Se ha eliminado correctamente #{d.id}"
          else
            p "No se ha podido eliminar #{d.id}"
          end
        end
      end
    end

    desc "Correción año 2106 por 2016"
    task change_year: :environment do
        AssetsDeclaration.where(year_period: 2106).each do |asset|
            asset.year_period = 2016
            if asset.save
                puts "Periodo modificado para la declaración #{asset.id}"
            else
                puts asset.errors.full_messages
            end
        end

        ActivitiesDeclaration.where(year_period: 2106).each do |activity|
            activity.year_period = 2016
            if activity.save
                puts "Periodo modificado para la declaración #{activity.id}"
            else
                puts activity.errors.full_messages
            end
        end
    end

    desc "Eliminación de todos los duties"
    task delete_all: :environment do
        Duty.all.each do |duty|
            duty.delete
        end
    end

end
