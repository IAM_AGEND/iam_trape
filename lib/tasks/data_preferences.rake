namespace :data_preferences do

  desc "Modifica el texto del title de la tabla data_preferences"
  task change_titles: :environment do
    data_reference_1 = DataPreference.find_by(title: "Número reintentos notificaciones")
    data_reference_1.update(title: "Número de reintentos manuales")
    data_reference_2 = DataPreference.find_by(title: "Umbral notificaciones pendientes")
    data_reference_2.update(title: "Número de días de reintentos automáticos")
    data_reference_3 = DataPreference.find_by(title: "Número de envíos diarios")
    data_reference_3.update(title: "Numero de reintentos automáticos en el día")
  end

  desc "Elimina las preferencias duplicadas"
  task remove_duplicates: :environment do
    DataPreference.find_by(title: "Años de publicación de concejales", code: nil).try(:delete)
    DataPreference.find_by(title: "Años publicados", code: nil).try(:delete)
    DataPreference.find_by(title: "Destinatarios alertas", code: nil).try(:delete)
    DataPreference.find_by(title: "Enlace Información sobre retribuciones", code: nil).try(:delete)
    DataPreference.find_by(title: "Fecha de obligaciones", code: nil).try(:delete)
    DataPreference.find_by(title: "Horas de ejecución tarea", code: nil).try(:delete)
    DataPreference.find_by(title: "Horas para la exportación de archivos", code: nil).try(:delete)
    DataPreference.find_by(title: "Número declarciones anuales", code: nil).try(:delete)
    DataPreference.find_by(title: "Número reintentos notificaciones", code: nil).try(:delete)
    DataPreference.find_by(title: "Umbral notificaciones pendientes", code: nil).try(:delete)
    DataPreference.find_by(title: "Umbral notificaciones pendientes", code: nil).try(:delete)
  end

  desc "Cambia de titulos V2"
  task change_titles_v2: :environment do
    DataPreference.find_by(title: "Número reintentos notificaciones manuales").update(title: "Número de reintentos manuales")
    DataPreference.find_by(title: "Número de reintentos automáticos notificaciones al día").update(title: "Número de reintentos automáticos en el día")
    DataPreference.find_by(title: "Número de días reintentos notificaciones automáticos").update(title: "Número de días de reintentos automáticos")
  end

  desc "Texto para enlace de posesión"
  task url_possession_text: :environment do
    DataPreference.create(title: "Texto de enlace de posesión", content_data: "Enlace a toma de posesión", type_data: 1, code: "url_possession_text")
  end 

  desc "Actualizar texto de enlace información sobre retribuciones"
  task transparency_link_update: :environment do 
    DataPreference.find_by(code: "name_link_information_retribuction").update(content_data: "Información sobre retribuciones", title: "Denominación del 'Enlace Información sobre retribuciones'")
    DataPreference.find_by(code: "link_information_retribuction").update(title: "Enlace Información sobre retribuciones")
  end
  
end
