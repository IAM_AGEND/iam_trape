class DutiesImporter
  def import
      JobLevel.all.each do |job|
          if job.councillor?
              job.councillors_corporations.each {|c| getDeclarations(c) }
          elsif job.director?
              getDeclarations(job)
          end
      end
  end

  private

  def getDeclarations(person_level)
      [:assets_declarations, :activities_declarations].each do |declaration|
          person_level.try(declaration).each do |asset|
              exist = Duty.find_by(declaration_id: asset.id, declaration_type: asset.model_name.to_s)

              duty_exist = Duty.find_by(declaration_id: nil, declaration_type: nil, type_duty: asset.model_name.to_s, temporality: asset.temporality, year: asset.year_period, person_level: person_level)
              if exist.blank? && duty_exist.blank?
                  puts "No existe la obligacion de #{asset.model_name} #{asset.id}"
                  duty = Duty.new(declaration: asset, start_date: asset.declaration_date, end_date: asset.declaration_date,
                      limit_date: asset.declaration_date, person_level: person_level, type_duty: asset.model_name.to_s, year: asset.year_period,
                      temporality: asset.temporality)
                  if duty.save
                      exist = Duty.find_by( declaration_type: asset.model_name.to_s == "AssetsDeclaration" ? "ActivitiesDeclaration" : "AssetsDeclaration", temporality: asset.temporality, year: asset.year_period, person_level: person_level)
                      duty_exist = Duty.find_by(declaration_id: nil, declaration_type: nil, type_duty: asset.model_name.to_s == "AssetsDeclaration" ? "ActivitiesDeclaration" : "AssetsDeclaration", temporality: asset.temporality, year: asset.year_period, person_level: person_level)

                      if duty_exist.blank? && exist.blank?
                          duty = Duty.new(declaration: nil, start_date: asset.declaration_date, end_date: asset.declaration_date,
                              limit_date: asset.declaration_date, person_level: person_level,
                              type_duty: asset.model_name.to_s == "AssetsDeclaration" ? "ActivitiesDeclaration" : "AssetsDeclaration", year: asset.year_period,
                              temporality: asset.temporality)
                          if duty.save
                              puts "--|||| Se ha guardado correctamente la obligación y su par"
                          else
                              puts "-- Se ha guardado correctamente la obligación"
                          end
                      end
                  else
                      puts "-- ERROR: #{duty.errors.full_messages}"
                  end
              elsif !duty_exist.blank? && exist.blank?
                  duty_exist.declaration = asset
                  if duty_exist.save
                      puts "-- Se ha actualizado correctamente la obligación"
                  else
                      puts "-- ERROR: #{duty_exist.errors.full_messages}"
                  end
              else
                  puts "Ya existe la obligacion de #{asset.model_name} #{asset.id}"
              end
          end
      end

      start_year = person_level.try(:appointment).try(:start_date).try(:year)
      end_year = person_level.try(:appointment).try(:end_date).try(:year)

      if !start_year.blank? && !end_year.blank?
          getDeclarationsPending(person_level.try(:appointment).try(:start_date), person_level.try(:appointment).try(:end_date), start_year, end_year, person_level)
      elsif !start_year.blank? && end_year.blank?
          end_year = Time.zone.now.year
          getDeclarationsPending(person_level.try(:appointment).try(:start_date), person_level.try(:appointment).try(:end_date), start_year, end_year, person_level)
      elsif start_year.blank? && !end_year.blank?
          start_year = 2017
          getDeclarationsPending(person_level.try(:appointment).try(:start_date), person_level.try(:appointment).try(:end_date), end_year, end_year, person_level)
      else
          start_year = 2017
          end_year = Time.zone.now.year
          getDeclarationsPending(person_level.try(:appointment).try(:start_date), person_level.try(:appointment).try(:end_date), start_year, end_year, person_level)
      end
  end

  def getDeclarationsPending(start_date, end_date, init_year, finish_year, person_level)
      start_year = start_date.try(:year)
      end_year = end_date.try(:year)
      date_preference = DataPreference.find_by(:code => "dutie_date").try(:content_data).try{|x| x.to_date}
      date_preference = date_preference.blank? ? Time.zone.now : date_preference


      (init_year..finish_year).each do |year|
          if year == start_year
              ['AssetsDeclaration','ActivitiesDeclaration'].each do |type|
                  exist = Duty.find_by(type_duty: type, temporality: Temporality.find_by(name: 'Inicial'), person_level: person_level)
                  if exist.blank?
                      puts "No existe la obligacion de #{type}"
                      duty = Duty.new(declaration_id: nil, declaration_type: nil, start_date: start_date, end_date: nil,
                          limit_date: start_date, person_level: person_level, type_duty: type, year: nil,
                          temporality: Temporality.find_by(name: 'Inicial'))
                      if duty.save
                          puts "-- Se ha guardado correctamente la obligación"
                      else
                          puts "-- ERROR: #{duty.errors.full_messages}"
                      end
                  else
                      puts "Ya existe la obligacion de #{type} #{exist.try(:declaration).try(:id)}"
                  end
              end
          elsif year == end_year
              [ 'AssetsDeclaration','ActivitiesDeclaration'].each do |type|
                  exist = Duty.find_by(type_duty: type, temporality: Temporality.find_by(name: 'Final'), person_level: person_level)
                  if exist.blank?
                      puts "No existe la obligacion de #{type}"
                      duty = Duty.new(declaration_id: nil, declaration_type: nil, start_date: end_date, end_date: nil,
                          limit_date: end_date, person_level: person_level, type_duty: type, year: nil,
                          temporality: Temporality.find_by(name: 'Final'))
                      if duty.save
                          puts "-- Se ha guardado correctamente la obligación"
                      else
                          puts "-- ERROR: #{duty.errors.full_messages}"
                      end
                  else
                      puts "Ya existe la obligacion de #{type} #{exist.try(:declaration).try(:id)}"
                  end
              end
          else
              ['AssetsDeclaration','ActivitiesDeclaration'].each do |type|
                  if !date_preference.blank? && date_preference.change(:year => year) <= Time.zone.now
                      exist = Duty.find_by(type_duty: type, temporality: Temporality.find_by(name: 'Anual'), year: year, person_level: person_level)
                      exist = Duty.where(type_duty: type, temporality: Temporality.find_by(name: 'Inicial'), person_level: person_level).where("cast(start_date as varchar) like (?)","%#{year}%").first if exist.blank?
                      exist = Duty.where(type_duty: type, temporality: Temporality.find_by(name: 'Final'), person_level: person_level).where("cast(start_date as varchar) like (?)","%#{year}%").first if exist.blank?
                      if exist.blank?
                          puts "No existe la obligacion de #{type}"
                          duty = Duty.new(declaration_id: nil, declaration_type: nil, start_date: date_preference.change(:year => year), end_date: nil,
                              limit_date: date_preference.change(:year => year), person_level: person_level, type_duty: type, year: year,
                              temporality: Temporality.find_by(name: 'Anual'))
                          if duty.save
                              puts "-- Se ha guardado correctamente la obligación"
                          else
                              puts "-- ERROR: #{duty.errors.full_messages}"
                          end
                      else
                          puts "Ya existe la obligacion de #{type} #{exist.try(:declaration).try(:id)}"
                      end
                  end
              end
          end
      end
  end
end
