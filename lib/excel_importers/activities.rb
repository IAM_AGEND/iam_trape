require 'excel_importers/base'
require 'logger'

module ExcelImporters
  class Activities < Base
    def initialize(path_to_file, period, corporation, job, logger = ExcelImporters::Base::NullLogger.new)
      @path_to_file = path_to_file
      @period = period
      @logger = logger
      @corporation = corporation
      @file_format = :xls
      @job = job
      @emails={}
    end

    def import
      declarations = Declarations.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '1.DatosPersonales')
      pub = Public.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '3.1.PuestosDeTrabajo')
      priv = Private.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '3.2.ActividadesPrivadas')
      other = Other.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '3.3.OtrasActividades')

      ActiveRecord::Base.transaction do
        declarations.import! && other.import! && pub.import! && priv.import! 
      end
      true
    rescue => err
      @logger.error(err.message)
      false
    end

    class ActivitiesBaseImporter < ::ExcelImporters::Base
      def initialize(path_to_file, period, corporation, job, emails, logger: nil, sheet_name: nil)
        super(path_to_file, logger: logger, sheet_name: sheet_name)
        @period = period
        @corporation = corporation
        @job = job
        @emails = emails
      end

      def get_declaration(person, dup = false)
        temporality = Temporality.find_by(name: @period)
        
        if temporality.blank?
          tempo= Temporality.find_by(name: "Anual")
          declarations =  person.activities_declarations.in_temporality(tempo, @period).order(created_at: :desc)
          if declarations.blank?
            person.activities_declarations.find_or_initialize_by(temporality: tempo,year_period: @period)
          else
            declaration = declarations.first
            if dup && !declaration.blank?
              declaration = declaration.dup
              declaration.id = nil
              declaration.editable = true
            end
            declaration
          end
        else
          declarations =  person.activities_declarations.in_temporality(tempo, @period).order(created_at: :desc)
          if declarations.blank?
            person.activities_declarations.find_or_initialize_by(temporality: temporality)
          else
            declaration = declarations.first
            if dup && !declaration.blank?
              declaration = declaration.dup
              declaration.id = nil
              declaration.editable = true
            end
            declaration
          end
        end
      end

      def import_row!(row, row_index)
        identifier = row.fetch(:identificador)
        if identifier.present?
          person = CouncillorsCorporation.joins(:job_level => [:person]).find_by("job_levels.person_type = ? AND people.personal_code = ? AND councillors_corporations.corporation_id = ?", @job,identifier, @corporation.blank? ? nil :  @corporation)
          person = JobLevel.joins(:person).find_by("job_levels.person_type = ? AND people.personal_code=?", @job,identifier) if person.blank?
          if person.present?
            import_person_row!(person, row)
          else
            logger.error("¡¡PROBLEMA DE DATOS!! No se encuentra la persona con identificador #{identifier} y tipo de persona #{I18n.t("people.titles.#{@job}")} en la fila #{row_index}")
          end
        else
          logger.error("¡¡PROBLEMA DE DATOS!! No se encuentra el identificador en la fila #{row_index}")
        end
      end
    end

    ###################################################
    # INSERT DECLARATIONS
    ###################################################
    class Declarations < ActivitiesBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person, true)

        declaration.public_activities.each { |data| data.destroy }
        declaration.private_activities.each { |data| data.destroy }
        declaration.other_activities.each { |data| data.destroy }
        declaration.declaration_date = row[:fecha_de_declaracion]

        if declaration.save
          if !declaration.try(:person_level).try(:person).try(:profile).try(:email).blank? 
            @emails.merge!({@emails.try(:count) => [declaration.try(:person_level).try(:person).try(:profile).try(:email),@job+"s",declaration.try(:person_level).try(:person).try(:profile).try(:personal_code)]})
          end
          logger.info(I18n.t('excel_importers.activities.declarations.imported', person: person.backwards_name, date: declaration.declaration_date))
        end
      end
    end

    ###################################################
    # INSERT PUBLIC ACTIVITIES
    ###################################################
    class Public < ActivitiesBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        public_activity = PublicActivity.new(
          entity: row[:entidad],
          position: row[:cargo_o_categoria], 
          start_date: parse_at(row[:fecha_inicio]).try{|t| t.strftime("%d/%m/%Y")},
          end_date: parse_at(row[:fecha_cese]).try{|t| t.strftime("%d/%m/%Y")},
          activities_declaration: declaration
        )

        if public_activity.valid?
          declaration.public_activities << public_activity
          if declaration.save!
            logger.info(I18n.t('excel_importers.activities.public.imported',
              period: declaration.try(:period_name), 
              person: person.try(:backwards_name),
              entity: public_activity.try(:entity), 
              position: public_activity.try(:position),
              start_date: public_activity.try(:start_date), 
              end_date: public_activity.try(:end_date)
            ))
          
          end
        end
      end
    end

    ###################################################
    # INSERT PRIVATE ACTIVITIES
    ###################################################
    class Private < ActivitiesBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        private_activity = PrivateActivity.new(
          private_activity_type: row[:actividad],
          description: row[:descripcion],
          entity: row[:entidad_colegio_profesional],
          position: row[:cargo_o_categoria],
          start_date: parse_at(row[:fecha_inicio]).try{|t| t.strftime("%d/%m/%Y")},
          end_date: parse_at(row[:fecha_cese]).try{|t| t.strftime("%d/%m/%Y")},
          activities_declaration: declaration
        )

        if private_activity.valid?
          declaration.private_activities << private_activity
          if declaration.save!
            logger.info(I18n.t('excel_importers.activities.private.imported',
              period: declaration.period_name,
              person: person.backwards_name,
              kind: private_activity.private_activity_type,
              entity: private_activity.entity,
              position: private_activity.position,
              start_date: private_activity.start_date,
              end_date: private_activity.end_date))
          end
        end
      end
    end

    ###################################################
    # INSERT OTHER ACTIVITIES
    ###################################################
    class Other < ActivitiesBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        other_activity = OtherActivity.new(
          description: row[:descripcion],
          start_date: parse_at(row[:fecha_inicio]).try{|t| t.strftime("%d/%m/%Y")},
          end_date: parse_at(row[:fecha_cese]).try{|t| t.strftime("%d/%m/%Y")},
          activities_declaration: declaration
        )

        if other_activity.valid?
          declaration.other_activities << other_activity
          if declaration.save!
            logger.info(I18n.t('excel_importers.activities.other.imported',
              period: declaration.period_name,
              person: person.backwards_name,
              start_date: other_activity.start_date,
              end_date: other_activity.end_date))
          end
        end
      end
    end
  end
end
