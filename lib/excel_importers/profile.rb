require 'excel_importers/base'

module ExcelImporters
  class ProfileImport < Base
    JOB_LEVELS = {
      'C' => 'councillor',
      'D' => 'director',
      'E' => 'temporary_worker',
      'F' => 'public_worker',
      'V' => 'spokesperson',
      'L' => 'labour'
    }.freeze

    JOB_LEVELS_P = {
      'C' => 'councillors',
      'D' => 'directors',
      'E' => 'temporary_workers',
      'F' => 'public_workers',
      'V' => 'spokespeople',
      'L' => 'labour'
    }.freeze

    LANGUAGES_LEVELS = { 
      'bajo' => "A - Usuario básico",
      'medio' => "B - Usuario intermedio",
      'alto' => "C - Usuario competente"
    }.freeze
    
    def initialize(path_to_file, header_field: nil, corporation: nil, logger: NullLogger.new, author: nil)
      super
      @header_field = header_field
      @corporation = corporation
      @emails={}
    end

    def import
      successful = super

      unless successful
        @imported = 0
        @updated = 0
      end

      logger.info ''
      logger.info I18n.t('excel_importers.profile.summary')
      logger.info I18n.t('excel_importers.profile.imported', count: @imported)
      logger.info I18n.t('excel_importers.profile.updated', count: @updated)
      logger.info I18n.t('excel_importers.profile.skipped', count: @skipped)

      successful
    end

    def import!
      @imported = 0
      @updated = 0
      @skipped = 0
      super
    end

    def import_row!(row, _row_index)
      exist =  Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", "%#{row[:nombre]}%", "%#{row[:apellidos]}%")

      if !row[:n_personal].blank?
        person = Person.find_by(personal_code: row[:n_personal])
        person = exist if person.blank?
      else
        person = exist
      end

      person = Person.new  if person.blank?
     
      updated_at_aux = person.try(:profile).try(:updated_at)
      person.personal_code = row[:n_personal]
      person.name = row[:nombre].blank? ? person.first_name : row[:nombre].mb_chars.upcase
      person.last_name = row[:apellidos].blank? ? person.last_name : row[:apellidos].mb_chars.upcase
      person.document_type = DocumentType.find_by(name: "DNI")
      person.profile = parse_profile(person.profile,row)

      parse_job_level(person, updated_at_aux, row)
    end

    private

      def get_index(row, field) 
        key_ant = nil
        row.each do |key, value|
          if key.to_s == field.to_s
            return key_ant.to_s
          else
            key_ant = key
          end
        end
        return nil
      end

      def parse_profiled_at(profiled_at)
        parse_at(profiled_at)
      end


      ###################################################
      # INSERT PROFILES
      ###################################################
      def parse_profile(profile, row)
        profile = Profile.new if profile.blank?
        profile.email = row[:correo_electronico] unless row[:correo_electronico].blank?
        profile.twitter = row[:cuenta_de_twitter]
        profile.facebook = row[:cuenta_de_facebook]
        destroy_collections(profile.informations)
        parse_studies(profile, row)
        parse_courses(profile, row)
        parse_languages(profile, row)
        parse_career(profile, row)
        parse_political_posts(profile, row)
       
        generate_comments(profile, row[:publicaciones], "publications")
        generate_comments(profile, row[:actividad], "theacher_activity")
        generate_comments(profile, row[:distinciones], "special_mentions")
        generate_comments(profile, row[:otra_informacion], "other")
        
        profile
      end

      ###################################################
      # INSERT JOB LEVELS
      ###################################################
      def parse_job_level(person,updated_at_aux, row)
        job_level_aux = JOB_LEVELS[row[:tipo_de_vinculacion] || row[:codigo_cargo]]
        unless job_level_aux.blank?
          job_level = person.id.blank? ? nil : JobLevel.find_by(person_id: person.id, person_type: job_level_aux) 

          job_level = JobLevel.new if job_level.blank?
          job_level.person_type = job_level_aux
          job_level.person = person
          job_level.updated_at = Time.zone.now
          
          if job_level_aux.to_s == "councillor"
            councillor_corporation = parse_corporation(job_level, row)
          else
            appointment = parse_appointment(job_level,row)
            job_level.appointment = appointment if appointment.valid?
          end
        else
          job_level=nil
        end
        person_profile_at = updated_at_aux
        person.job_levels << job_level if !job_level.blank? && job_level.valid?
        
  
        aux_fecha = row[:fecha].to_s
        if !aux_fecha.blank? && !aux_fecha.split(" ").blank? && !aux_fecha.split(" ")[0].split("/").blank? && aux_fecha.split(" ")[0].split("/")[2].length.to_i <= 2
          aux_fecha.insert(6, "20") 
        end
        profiled_at = parse_profiled_at(aux_fecha).change(:offset => Time.zone.now.strftime("%:z").to_s)
  
        if !job_level.blank? && (updated_at_aux.blank? || updated_at_aux <= profiled_at)
         
          if updated_at_aux.blank?
            importacion=true
          else
            importacion=false
          end
            
          person.profile.updated_at = profiled_at
          person.profile.updated_at = person.profile.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min)

          if person.save   
            person.profile.cumplimented = person.profile.has_profile?
            person.save
            if importacion
              @imported = @imported + 1
              logger.info I18n.t('excel_importers.profile.importing', person: person.full_name)
              Audit.create!(administrator: @author, 
                person: person, 
                action: I18n.t('audits.create'), 
                description: I18n.t('audits.create_description', full_name: person.backwards_name))
            else
              @updated = @updated + 1
              logger.info I18n.t('excel_importers.profile.updating',
                person: person.full_name,
                reference: row[:referencia],
                person_profiled_at:  person_profile_at,
                file_profiled_at: profiled_at)
              Audit.create!(administrator: @author, 
                person: person, 
                action: I18n.t('audits.update'), 
                description: I18n.t('audits.update_description', full_name: person.backwards_name))
            end
            @emails.merge!({@emails.count => [person.profile.email, JOB_LEVELS_P[row[:tipo_de_vinculacion] || row[:codigo_cargo]], person.personal_code]}) unless person.try(:profile).try(:email).blank?
          else
            @skipped = @skipped + 1
            logger.info person.errors.full_messages
            logger.info I18n.t('excel_importers.profile.skipping',
              person: person.full_name,
              reference: row[:referencia],
              person_profiled_at: person_profile_at,
              file_profiled_at: profiled_at)
          end
        else
          @skipped = @skipped + 1
          if job_level.blank?
            logger.error I18n.t('excel_importers.profile.no_job_level',
              person: person.full_name,
              reference: row[:referencia],
              file_profiled_at: profiled_at) 
          else
            logger.info I18n.t('excel_importers.profile.skipping',
              person: person.full_name,
              reference: row[:referencia],
              person_profiled_at: person_profile_at,
              file_profiled_at: profiled_at)
          end
        end
      end

      ###################################################
      # INSERT APPOINTMENT
      ###################################################
      def parse_appointment(job_level,row)
        if job_level.appointment.blank?
          appointment = Appointment.new
        else
          appointment = job_level.appointment
        end
       
        appointment.unit = row[:unidad]
        appointment.position = row[:cargo].blank? ? row[:cargo] : row[:cargo].mb_chars.upcase
        appointment.start_date = appointment.start_date
        appointment.end_date = nil

        appointment
      end

      ###################################################
      # INSERT CORPORATIONS
      ###################################################
      def parse_corporation(job_level,row)
        corporation_assing = job_level.id.blank? ? nil : CouncillorsCorporation.find_by(job_level_id: job_level.id, corporation_id: @corporation)
        corporation_assing = CouncillorsCorporation.new if corporation_assing.blank?
        corporation_assing.job_level = job_level
        corporation_assing.corporation = @corporation.blank? ? nil : Corporation.find(@corporation)
        corporation_assing.order_num = corporation_assing.order_num.blank? ? 0 : corporation_assing.order_num

        electoral_list =ElectoralList.where("TRANSLATE(UPPER(long_name), 'AEIOU', 'ÁÉÍÓÚ') = TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')", row[:unidad]).first
        corporation_assing.electoral_list = row[:unidad].blank? || electoral_list.blank? ? corporation_assing.electoral_list :  electoral_list
        appointment = parse_appointment(corporation_assing,row)
        corporation_assing.appointment = appointment if appointment.valid?
        job_level.councillors_corporations << corporation_assing if corporation_assing.valid?

        corporation_assing
      end


      ###################################################
      # INSERT STUDIES
      ###################################################
      def parse_studies(profile, row)
        destroy_collections(profile.studies)
        (1..4).each do |index|
          col = get_index(row, "#{index}_titulacion_oficial").to_i
          study=Study.new
          study.profile = profile
          study.official_degree = row[col]
          study.center = row[col+1]
          study.start_year = row[col+2]
          study.end_year = row[col+3]

          if study.valid?
            profile.studies << study
          else
            study.profile = nil
          end
        end
        if !get_index(row, "texto11").blank? && get_index(row,"4_titulacion_oficial").to_i + 4 == get_index(row,"texto11").to_i
          studies_comment_col = get_index(row, "4_titulacion_oficial").to_i + 5
        else
          studies_comment_col = get_index(row, "4_titulacion_oficial").to_i + 4
        end
        generate_comments(profile, row[studies_comment_col], "studies_comments")
      end

      ###################################################
      # INSERT COURSES
      ###################################################
      def parse_courses(profile, row)
        destroy_collections(profile.courses)
        (1..4).each do |index|
          col = get_index(row,"#{index}_nombre_del_curso").to_i        
          course = Course.new
          course.profile = profile
          course.title = row[col]
          course.center = row[col+1]
          course.start_year = row[col+2]
          course.end_year = row[col+3]
          if course.valid?
            profile.courses << course 
          else
            course.profile= nil
          end
        end
        if !get_index(row, "texto12").blank? && get_index(row,"4_nombre_del_curso").to_i+4 == get_index(row,"texto12").to_i
          courses_comment_col = get_index(row,"4_nombre_del_curso").to_i + 5
        else
          courses_comment_col = get_index(row,"4_nombre_del_curso").to_i + 4
        end
        generate_comments(profile, row[courses_comment_col], "courses_comments")
      end

      ###################################################
      # INSERT LANGUAGES
      ###################################################
      def parse_languages(profile, row)
        destroy_collections(profile.languages)
        {
          :ingles => "Inglés", :frances => "Francés", :aleman => "Alemán",
          :italiano => "Italiano", :nivel_otro_idioma => row[:otro_idioma]
        }.each do |key, value|
          if row[key].present?
            language=Language.new
            language.profile = profile
            language.name =  value
            language.level =  LANGUAGES_LEVELS[row[key]] || row[key]
            if language.valid?
              profile.languages << language 
            else
              language.profile = nil
            end
          end
        end
      end

      ###################################################
      # INSERT CAREER
      ###################################################
      def parse_career(profile, row)
        parse_private_jobs(profile, row)
        parse_public_jobs(profile, row)
        if !get_index(row,"texto13").blank? && get_index(row,"4_cargo_actividad").to_i + 4 == get_index(row,"texto13").to_i
          career_comment_col = get_index(row,"4_cargo_actividad").to_i + 5
        else
          career_comment_col = get_index(row,"4_cargo_actividad").to_i + 4
        end
        generate_comments(profile, row[career_comment_col], "career_comments")
      end

      ###################################################
      # INSERT PRIVATE JOBS
      ###################################################
      def parse_private_jobs(profile, row)
        destroy_collections(profile.private_jobs)
        (1..4).each do |index|
          col = get_index(row,"#{index}_cargo_actividad").to_i
          private_job=PrivateJob.new
          private_job.profile = profile
          private_job.position = row[col]
          private_job.entity = row[col+1]
          private_job.start_year = row[col+2]
          private_job.end_year = row[col+3]
          if private_job.valid?
            profile.private_jobs << private_job 
          else
            private_job.profile = nil
          end
        end
      end

      ###################################################
      # INSERT PUBLIC JOBS
      ###################################################
      def parse_public_jobs(profile, row)
        destroy_collections(profile.public_jobs)
        (1..4).each do |index|
          col = get_index(row,"#{index}_puesto_desempenado").to_i
          public_job=PublicJob.new
          public_job.profile = profile
          public_job.position = row[col]
          public_job.public_administration =  row[col+1]
          public_job.start_year = row[col+2]
          public_job.end_year = row[col+3]
          public_job.consolidation_degree = row[:grado_consolidado]
          public_job.body_scale = row[:cuerpo_o_escala_actual_de_la_administracion] || row[:cuerpo_o_escala_de_la_administracion]
          public_job.init_year = row[:ano_de_ingreso]
          if public_job.valid?
            profile.public_jobs << public_job 
          else
            public_job.profile = nil
          end
        end
      end

      ###################################################
      # INSERT POLITICAL POSTS
      ###################################################
      def parse_political_posts(profile, row)
        destroy_collections(profile.political_posts)
        (1..4).each do |index|
          col = get_index(row,"#{index}_cargo").to_i
          political_post=PoliticalPost.new
          political_post.profile = profile
          political_post.position = row[col]
          political_post.entity = row[col+1]
          political_post.start_year = row[col+2]
          political_post.end_year = row[col+3]
          if political_post.valid?
            profile.political_posts << political_post 
          else
            political_post.profile = nil
          end
        end
        if !get_index(row,"texto14").blank? && get_index(row,"4_cargo").to_i + 4 == get_index(row,"texto14").to_i
          political_posts_comment_col = get_index(row,"4_cargo").to_i + 5
        else
          political_posts_comment_col = get_index(row,"4_cargo").to_i + 4
        end
        generate_comments(profile, row[political_posts_comment_col], "political_posts_comments")
      end

      def destroy_collections(collection)
        collection.each do |col|
          col.destroy
        end
      end

      def generate_comments(profile, data, type)
        if type.to_s != "publications"
          comment = Information.find_by(profile: profile, information_type: InformationType.find_by(name: type))
        end
        comment= Information.new(profile: profile, information_type: InformationType.find_by(name: type)) if type.to_s != "publications" || comment.blank?
        comment.information = data
       

        if type.to_s != "publications" || type.to_s == "publications" && !comment.information.blank?
          profile.informations << comment
        else
          comment.profile = nil
        end
      end
  end
end
