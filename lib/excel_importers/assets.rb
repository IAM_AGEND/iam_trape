require 'excel_importers/base'
require 'logger'

module ExcelImporters
  class Assets < Base
    def initialize(path_to_file, period,corporation, job, logger = ExcelImporters::Base::NullLogger.new)
      @path_to_file = path_to_file
      @period = period
      @logger = logger
      @corporation = corporation
      @job = job
      @file_format = :xls
      @emails={}
    end

    def import
      declarations = Declarations.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '1.DatosPersonales')
      real_estate_properties = RealEstateProperties.new(@path_to_file, @period, @corporation,
        @job,@emails, logger: @logger, sheet_name: '3.PatrimonioInmobiliario')
      account_deposits = AccountDeposits.new(@path_to_file, @period, @corporation, @job, @emails,
        logger: @logger, sheet_name: '4.DepositosEnCuenta')
      other_deposits = OtherDeposits.new(@path_to_file, @period, @corporation, @job,@emails,
        logger: @logger, sheet_name: '5.OtroPatrimonioMobiliario')
      vehicles = Vehicles.new(@path_to_file, @period, @corporation, @job,@emails,
        logger: @logger, sheet_name: '6.Vehículos')
      other_personal_properties = OtherPersonalProperties.new(@path_to_file, @period,
        @corporation, @job,@emails, logger: @logger, sheet_name: '7.OtrosBienesMuebles')
      debts = Debts.new(@path_to_file, @period, @corporation, @job,@emails,
        logger: @logger, sheet_name: '8.Deudas')
      tax_data = TaxData.new(@path_to_file, @period, @corporation, @job,@emails,
        logger: @logger, sheet_name: '9.InformacionTributaria')

      ActiveRecord::Base.transaction do
        aux_1 = declarations.import! && real_estate_properties.import! && account_deposits.import! 
        aux_2 = other_deposits.import! && vehicles.import! && other_personal_properties.import! && debts.import! 
        aux_3 = tax_data.import!
        
        aux_1 && aux_2 && aux_3
      end
      true
    rescue => err
      @logger.error(err.message)
      false
    end

    class AssetsBaseImporter < ::ExcelImporters::Base
      def initialize(path_to_file, period, corporation, job,emails, logger: nil, sheet_name: nil)
        super(path_to_file, logger: logger, sheet_name: sheet_name)
        @period = period
        @corporation = corporation
        @job = job
        @emails= emails
      end
      
      def get_declaration(person, dup = false)
        temporality = Temporality.find_by(name: @period)
        
        if temporality.blank?
          tempo= Temporality.find_by(name: "Anual")
          declarations =  person.assets_declarations.in_temporality(tempo, @period).order(created_at: :desc)
          if declarations.blank?
            person.assets_declarations.find_or_initialize_by(temporality: tempo,year_period: @period)
          else
            declaration = declarations.first
            if dup && !declaration.blank?
              declaration = declaration.dup
              declaration.id = nil
              declaration.editable = true
            end
            declaration
          end
        else
          declarations =  person.assets_declarations.in_temporality(tempo, @period).order(created_at: :desc)
          if declarations.blank?
            person.assets_declarations.find_or_initialize_by(temporality: temporality)
          else
            declaration = declarations.first
            if dup && !declaration.blank?
              declaration = declaration.dup
              declaration.id = nil
              declaration.editable = true
            end
            declaration
          end
        end
      end

      def import_row!(row, row_index)
        identifier = row.fetch(:identificador)
        if identifier.present?
          person = CouncillorsCorporation.joins(:job_level => [:person]).find_by("job_levels.person_type = ? AND people.personal_code = ? AND councillors_corporations.corporation_id = ?", @job,identifier, @corporation.blank? ? nil :  @corporation)
          person = JobLevel.joins(:person).find_by("job_levels.person_type = ? AND people.personal_code=?", @job,identifier) if person.blank?
         
          if person.present?
            import_person_row!(person, row)
          else
            logger.error("¡¡PROBLEMA DE DATOS!! No se encuentra la persona con identificador #{identifier} y tipo de persona #{I18n.t("people.titles.#{@job}")} en la fila #{row_index}")
          end
        else
          logger.error("¡¡PROBLEMA DE DATOS!! No se encuentra el identificador en la fila #{row_index}")
        end
      end
    end

    ###################################################
    # INSERT DECLARATIONS
    ###################################################
    class Declarations < AssetsBaseImporter
      def import_person_row!(person, row)         
        declaration = get_declaration(person, true)       

        declaration.real_estate_properties.each { |data|  data.destroy }
        declaration.account_deposits.each { |data| data.destroy }
        declaration.other_deposits.each { |data| data.destroy }
        declaration.vehicles.each { |data| data.destroy }
        declaration.other_personal_properties.each { |data| data.destroy }
        declaration.debts.each { |data| data.destroy }
        declaration.tax_data.each { |data| data.destroy }
        declaration.declaration_date = row[:fecha_de_declaracion]

        if declaration.save
          if !declaration.try(:person_level).try(:person).try(:profile).try(:email).blank? 
            @emails.merge!({@emails.try(:count) => [declaration.try(:person_level).try(:person).try(:profile).try(:email),@job+"s",declaration.try(:person_level).try(:person).try(:profile).try(:personal_code)]})
          end
          logger.info(I18n.t('excel_importers.assets.declarations.imported', person: person.backwards_name, date: declaration.declaration_date))
        end
      end
    end

    ###################################################
    # INSERT REAL ESTATE PROPERTIES
    ###################################################
    class RealEstateProperties < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        real_estate_property = RealEstateProperty.new(
          assets_declaration: declaration,
          kind:  row[:clase],
          straight_type: row[:tipo_de_derecho],
          adquisition_title: row[:titulo_de_adquisicion],
          municipality: row[:municipio],
          participation_percentage: row[:participacion],
          purchase_date: row[:fecha_de_adquisicion],
          cadastral_value: get_value_amount(row[:valor_catastral]),
          observations: row[:observaciones]
        )
        if real_estate_property.valid?
          declaration.real_estate_properties << real_estate_property
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.real_estate_properties.imported',
              period: @period,
              person: person.backwards_name,
              kind: real_estate_property.kind,
              type: real_estate_property.straight_type,
              municipality: real_estate_property.municipality,
              share: real_estate_property.participation_percentage,
              purchase_date: real_estate_property.purchase_date,
              tax_value: real_estate_property.cadastral_value,
              notes: real_estate_property.observations))
          end
        end
      end
    end

    ###################################################
    # INSERT ACCOUNT DEPOSITS
    ###################################################
    class AccountDeposits < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        account_deposit = AccountDeposit.new(
          assets_declaration: declaration,
          kind: row[:clase],
          entity: row[:entidad_de_deposito],
          balance: get_value_amount(row[:saldo_medio_anual_o_valor_euros])
        )        

        if account_deposit.valid?
          declaration.account_deposits << account_deposit
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.account_deposits.imported',
            period: @period,
            person: person.backwards_name,
            kind: account_deposit.kind,
            banking_entity: account_deposit.entity,
            balance: account_deposit.balance))
          end
        end
      end
    end

    ###################################################
    # INSERT OTHER DEPOSITS
    ###################################################
    class OtherDeposits < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        other_deposit = OtherDeposit.new(
          assets_declaration: declaration,
          types: row[:clase],
          description: row[:descripcion],
          purchase_date: row[:fecha_de_adquisicion] || row[:fecha_de_adquisicion_o_suscripcion],
          value: row[:numero_cuantia_o_valor_en_euros]
        )
       
        if other_deposit.valid?
          declaration.other_deposits << other_deposit
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.other_deposits.imported',
              period: @period,
              person: person.backwards_name,
              kind: other_deposit.types,
              description: other_deposit.description,
              amount: other_deposit.value,
              purchase_date: other_deposit.purchase_date))
          end
        end
      end
    end

    ###################################################
    # INSERT VEHICLES
    ###################################################
    class Vehicles < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        vehicle = Vehicle.new(
          assets_declaration: declaration,
          kind: row[:clase],
          model: row[:marca_y_modelo],
          purchase_date: row[:fecha_de_adquisicion] || row[:fecha_de_adquisicion_o_suscripcion]
        )

        if vehicle.valid?
          declaration.vehicles << vehicle
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.vehicles.imported',
              period: @period,
              person: person.backwards_name,
              kind: vehicle.kind,
              model: vehicle.model,
              purchase_date: vehicle.purchase_date))
          end
        end
      end
    end

    ###################################################
    # INSERT OTHER PERSONAL PROPERTIES
    ###################################################
    class OtherPersonalProperties < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        other_personal_property = OtherPersonalProperty.new(
          assets_declaration: declaration,
          types: row[:clase],
          purchase_date: row[:fecha_de_adquisicion] || row[:fecha_de_adquisicion_o_suscripcion]
        )
        if other_personal_property.valid?
          declaration.other_personal_properties << other_personal_property
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.other_personal_properties.imported',
              period: @period,
              person: person.backwards_name,
              kind: other_personal_property.types,
              purchase_date: other_personal_property.purchase_date))
          end
        end
      end
    end

    ###################################################
    # INSERT DEBTS
    ###################################################
    class Debts < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        debt = Debt.new(
          assets_declaration: declaration,
          kind: row[:clase],
          import: get_value_amount(row[:importe_actual_en_euros]),
          observations: row[:observaciones]
        )
        
        if debt.valid?
          declaration.debts << debt
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.debts.imported',
              period: @period,
              person: person.backwards_name,
              kind: debt.kind,
              amount: debt.import,
              comments: debt.observations))
          end
        end
      end
    end

    ###################################################
    # INSERT TAX DATA
    ###################################################
    class TaxData < AssetsBaseImporter
      def import_person_row!(person, row)
        declaration = get_declaration(person)
        tax_datum = TaxDatum.new(assets_declaration: declaration,
          tax: row [:impuesto],
          fiscal_data:row[:dato_fiscal],
          import: get_value_amount(row[:importe]),
          observation: row[:observaciones])
          
        if tax_datum.valid?
          declaration.tax_data << tax_datum
          if declaration.save
            logger.info(I18n.t('excel_importers.assets.tax_data.imported',
              period: @period,
              person: person.backwards_name,
              tax: tax_datum.tax,
              fiscal_data: tax_datum.fiscal_data,
              amount: tax_datum.import,
              comments: tax_datum.observation))
          end
        end
      end
    end
  end
end
