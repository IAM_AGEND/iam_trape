class NotificationsImporter
    require 'inper_ws'
    require 'json-compare'

    def import(system = true)
        ws = INPERWS.new
        profilesData(ws)
        assetsData(ws)
        activitiesData(ws)
        peopleData(ws)
    end

    def ignore_unless_new_data
      Notification.pending.where(type_data: "Person").each do |notification|
        begin
          result = compare_by_data(notification.content_data, "Person")
          if result.blank?
            notification.update(no_publish_reason: "Historificado automático - Rechazado automático: no aporta datos nuevos", ignore: true, change_state_at: Time.zone.now)
          end
        rescue => e
          Rails.logger.error("Error en ignore_unless_new_data: #{e}")
        end
      end
    end

    def import_test(list = [], type)
      list["listaPersonas"].each {|c| createNotification(c, type,'dperId') }
    rescue
    end

    def import_test_declarations(list = [], type)
      list["listaDeclaraciones"].each {|c| createNotification(c, type,'dperId') }
    rescue
    end

    ###########################################################################################
    ## METODO GUARDAR DATOS PERFILES
    ###########################################################################################
    def saveProfileData(notification, automatic = true, administrator = nil)
      profile_new = Profile.transformData(notification, true)
      personal_code = profile_new.try(:people).try{|x| x[0]}.try(:personal_code)
      return false if !validateBlank(notification, personal_code)
      exist = profile_new.id.blank? ? nil : Profile.find(profile_new.id)



      if exist.blank?
        if automatic
          return false if !validateBlank(notification,nil, "No se ha creado todavía la persona")
        else
          person = Person.find_by("people.personal_code = ?", personal_code)
          return false if !validateBlank(notification,person)
          profile_new.updated_at ||= Time.zone.now

          if profile_new.save
            Audit.create!(administrator: administrator,
              person: profile_new.people[0],
              action: I18n.t('audits.create'),
              description: I18n.t('audits.create_description', full_name: profile_new.people[0].try(:backwards_name)))
            return true
          else
            profile_new.errors.each do |attribute, error|
              notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
            end
            notification.save
            return false
          end
        end
      elsif exist.updated_at > profile_new.updated_at
        return false if !validateBlank(notification,nil, "La fecha de actualización es menor a la ya publicada")
      else
        profile_new.updated_at ||= Time.zone.now

        if profile_new.save
          Audit.create!(administrator: administrator,
            person: profile_new.people[0],
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: profile_new.people[0].try(:backwards_name)))
          return true
        else
          if profile_new.errors.blank?
            notification.no_publish_reason = "No se han podido actualizar los datos de la persona"
          else
            profile_new.errors.each do |attribute, error|
              notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
            end
          end
          notification.save
          return false
        end
      end
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación")
    rescue => e
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación: #{e}")
    end

    ###########################################################################################
    ## METODO GUARDAR DATOS DECLARACIÓN DE BIENES
    ###########################################################################################
    def saveAssetsDeclarationData(notification, automatic = true, administrator = nil)
      asset_new = AssetsDeclaration.transformData(notification)
      if asset_new.person_level_type == "CouncillorsCorporation"
        personal_code =  asset_new.person_level.job_level.person.personal_code
        name = asset_new.person_level.job_level.person.name
        last_name = asset_new.person_level.job_level.person.last_name
        exist = AssetsDeclaration.joins("INNER JOIN councillors_corporations ON councillors_corporations.id = assets_declarations.person_level_id AND assets_declarations.person_level_type = 'CouncillorsCorporation'
          INNER JOIN job_levels ON job_levels.id = councillors_corporations.job_level_id INNER JOIN people ON people.id = job_levels.person_id")
          .find_by("temporality_id = #{asset_new.temporality.id} #{"AND year_period=#{asset_new.year_period}" unless asset_new.year_period.blank?} AND (#{"people.personal_code = '#{personal_code}' OR" unless personal_code.blank?} people.name = '#{name}' AND people.last_name = '#{last_name}')
          AND councillors_corporations.corporation_id = #{Corporation.find_by(name: notification.try(:content_data).try{|x| x['cpNombre']}.gsub('/','-')).id}")

        if personal_code.blank? && (name.blank? || last_name.blank?)
          return false if !validateBlank(notification,nil)
        end
      else
        personal_code =  asset_new.person_level.person.personal_code
        name = asset_new.person_level.person.name
        last_name = asset_new.person_level.person.last_name
        exist = AssetsDeclaration.joins("INNER JOIN job_levels ON job_levels.id = assets_declarations.person_level_id AND assets_declarations.person_level_type = 'JobLevel'
          INNER JOIN people ON people.id = job_levels.person_id")
          .find_by("temporality_id = #{asset_new.temporality.id} #{"AND year_period=#{asset_new.year_period}" unless asset_new.year_period.blank?} AND (#{"people.personal_code = '#{personal_code}' OR" unless personal_code.blank?} people.name = '#{name}' AND people.last_name = '#{last_name}')")

        if personal_code.blank? && (name.blank? || last_name.blank?)
          return false if !validateBlank(notification,nil)
        end
      end


      if exist.blank?
        level = nil
        person = Person.find_by("people.personal_code = #{personal_code}")
        return false if !validateBlank(notification,person)
        if asset_new.person_level_type == "CouncillorsCorporation" && !person.councillors.blank?
          level = person.councillors[0].councillors_corporations.find_by(corporation: Corporation.find_by(name: asset_new.person_level.corporation.name))
        elsif !person.blank? && !person.directors.blank?
          level = person.job_levels.find_by(person_type: asset_new.person_level.person_type)
        end
        return false if !validateBlank(notification,level, "No existe el tipo de persona")
        level.updated_at = Time.zone.now
        asset_new.person_level = level
        if asset_new.save
          duty = Duty.new(declaration: asset_new, start_date: asset_new.declaration_date, end_date: asset_new.declaration_date,
            limit_date: asset_new.declaration_date, person_level: asset_new.person_level, type_duty: asset_new.model_name.to_s, year: asset_new.year_period,
            temporality: asset_new.temporality)
          duty.save
          Audit.create!(administrator: administrator,
            person: asset_new.try(:person_level).try(:person),
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: asset_new.try(:person_level).try(:person).try(:backwards_name)))
          return true
        else
          asset_new.errors.each do |attribute, error|
            notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
          end
          notification.save
          return false
        end
      elsif exist.declaration_date.strftime('%Y-%m-%d') > asset_new.declaration_date.strftime('%Y-%m-%d')
        return false if !validateBlank(notification,nil, "La fecha de declaración es menor a la publicada")
      else
        asset_new.editable = true

        if asset_new.save
          Audit.create!(administrator: administrator,
            person: asset_new.try(:person_level).try(:person),
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: asset_new.try(:person_level).try(:person).try(:backwards_name)))
          return true
        else
          asset_new.errors.each do |attribute, error|
            notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
          end
          notification.save
          return false
        end
      end
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación")
    rescue => e
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación: #{e}")
    end

    ###########################################################################################
    ## METODO GUARDAR DATOS DECLARACIÓN DE ACTIVIDADES
    ###########################################################################################
    def saveActivitiesDeclarationData(notification, automatic = true, administrator = nil)
      activity_new = ActivitiesDeclaration.transformData(notification)
      if activity_new.person_level_type == "CouncillorsCorporation"
        personal_code =  activity_new.person_level.job_level.person.personal_code
        name = activity_new.person_level.job_level.person.name
        last_name = activity_new.person_level.job_level.person.last_name
        exist = ActivitiesDeclaration.joins("INNER JOIN councillors_corporations ON councillors_corporations.id = activities_declarations.person_level_id AND activities_declarations.person_level_type = 'CouncillorsCorporation'
          INNER JOIN job_levels ON job_levels.id = councillors_corporations.job_level_id INNER JOIN people ON people.id = job_levels.person_id")
          .find_by("temporality_id = #{activity_new.temporality.id} #{"AND year_period=#{activity_new.year_period}" unless activity_new.year_period.blank?} AND (#{"people.personal_code = '#{personal_code}' OR" unless personal_code.blank?} people.name = '#{name}' AND people.last_name = '#{last_name}')
          AND councillors_corporations.corporation_id = #{Corporation.find_by(name: notification.try(:content_data).try{|x| x['cpNombre']}.gsub('/','-')).id}")


        if personal_code.blank? && (name.blank? || last_name.blank?)
          return false if !validateBlank(notification,nil)
        end
      else
        personal_code =  activity_new.person_level.person.personal_code
        name = activity_new.person_level.person.name
        last_name = activity_new.person_level.person.last_name
        exist = ActivitiesDeclaration.joins("INNER JOIN job_levels ON job_levels.id = activities_declarations.person_level_id AND activities_declarations.person_level_type = 'JobLevel'
          INNER JOIN people ON people.id = job_levels.person_id")
          .find_by("temporality_id = #{activity_new.temporality.id} #{"AND year_period=#{activity_new.year_period}" unless activity_new.year_period.blank?} AND (#{"people.personal_code = '#{personal_code}' OR" unless personal_code.blank?} people.name = '#{name}' AND people.last_name = '#{last_name}')")

        if personal_code.blank? && (name.blank? || last_name.blank?)
          return false if !validateBlank(notification,nil)
        end
      end

      if exist.blank?
        level = nil
        person = Person.find_by("people.personal_code = #{personal_code}")
        return false if !validateBlank(notification,person)
        if activity_new.person_level_type == "CouncillorsCorporation" && !person.councillors.blank?
          level = person.councillors[0].councillors_corporations.find_by(corporation: Corporation.find_by(name: activity_new.person_level.corporation.name))
        elsif !person.directors.blank?
          level = person.job_levels.find_by(person_type: activity_new.person_level.person_type)
        end
        return false if !validateBlank(notification,level, "No existe el tipo de persona")
        level.updated_at = Time.zone.now
        activity_new.person_level = level
        if activity_new.save
          duty = Duty.new(declaration: activity_new, start_date: activity_new.declaration_date, end_date: activity_new.declaration_date,
            limit_date: activity_new.declaration_date, person_level: activity_new.person_level, type_duty: activity_new.model_name.to_s, year: activity_new.year_period,
            temporality: activity_new.temporality)
          duty.save
          Audit.create!(administrator: administrator,
            person: activity_new.try(:person_level).try(:person),
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: activity_new.try(:person_level).try(:person).try(:backwards_name)))
          return true
        else
          activity_new.errors.each do |attribute, error|
            notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
          end
          notification.save
          return false
        end
      elsif exist.declaration_date.strftime('%Y-%m-%d') > activity_new.declaration_date.strftime('%Y-%m-%d')
        return false if !validateBlank(notification,nil, "La fecha de declaración es menor a la publicada")
      else
        activity_new.editable = true

        if activity_new.save
          Audit.create!(administrator: administrator,
            person: activity_new.try(:person_level).try(:person),
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: activity_new.try(:person_level).try(:person).try(:backwards_name)))
          return true
        else
          activity_new.errors.each do |attribute, error|
            notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
          end
          notification.save
          return false
        end
      end
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación")
    rescue => e
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación: #{e}")
    end


    ###########################################################################################
    ## METODO GUARDAR DATOS PERSONAS
    ###########################################################################################
    def savePersonData(notification, automatic = true, administrator = nil)
      person = Person.transformData(notification, true)
      return false if !validateBlank(notification,person)

      if person.profile.blank?
        profile = Profile.new(cumplimented: false, email: notification.try(:content_data).try{|x| x['dperCorreoCorp']}, updated_at: Time.zone.now)
        comment= Information.new(profile: profile, information_type: InformationType.find_by(name: "other"))
        comment.information = "El contenido del perfil y trayectoria profesional se publicará una vez que sea facilitado por el empleado público."
        profile.informations << comment

        person.profile = profile
      end

      create_aux = person.id.blank?

      if person.save
        if create_aux
          Audit.create!(administrator: administrator,
            person: person,
            action: I18n.t('audits.create'),
            description: I18n.t('audits.create_description', full_name: person.try(:backwards_name)))
        else
          Audit.create!(administrator: administrator,
            person: person,
            action: I18n.t('audits.update'),
            description: I18n.t('audits.update_description', full_name: person.try(:backwards_name)))
        end
        return true
      else
        person.errors.each do |attribute, error|
          notification.no_publish_reason = "#{notification.no_publish_reason} / #{get_min_errors(attribute, "person")} #{error}"
        end
        notification.save
        return false
      end
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación")
    rescue => e
      return false if !validateBlank(notification,nil, "ERROR producido al tratar la notificación: #{e}")
    end

    private

    def profilesData(ws)
        profiles = JSON.parse(ws.getWSPerfiles)
        profiles.each {|c| createNotification(c, 'Profile','dpfId') }
    rescue => e
        puts "ERROR: perfil #{e}"
    end

    def assetsData(ws)
        assets = JSON.parse(ws.getWSDeclaracionesbienes)
        assets.each { |c|  createNotification(c, 'AssetsDeclaration', 'decg_id') }
    rescue => e
        puts "ERROR: bienes #{e}"
    end

    def activitiesData(ws)
        activities = JSON.parse(ws.getWSDeclaracionesactividades)
        activities.each {|c| createNotification(c, 'ActivitiesDeclaration', 'decgId')}
    rescue => e
        puts "ERROR: actividades #{e}"
    end

    def peopleData(ws, start_pag = 0, end_pag=100)
      personaspag = JSON.parse(ws.getWSPersonasPag(start_pag, end_pag))
      #personaspag = JSON.parse(ws.getWSPersonas)


      if !personaspag['listaPersonas'].blank?
        personaspag['listaPersonas'].each {|c| createNotification(c, 'Person','dperId') }

        peopleData(ws, personaspag['finPaginacion'].to_i, personaspag['finPaginacion'].to_i + 100)
      end
    rescue => e
        puts "ERROR: persona #{e}"
    end

    def notification_data(data)
      notification = Notification.new(content_data: data.to_json,type_data: 'Person')
      data_export = {title: "", date: "", person_type: ""}
      contenido = notification.try(:content_data)

      return data_export if contenido.blank?
      person_type = contenido.try{|x| x['gper']}

      ##############################################################
      # Obtener a la persona
      ##############################################################
      person = Person.find_by(personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : contenido.try{|x| x['dperNumper']})
      if person.blank?
        person = Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
          "%#{contenido.try{|x| x['dperNombre']}}%", "%#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}%")
      end

      appointment_fin = get_last_appointment(contenido)

      if person.blank?
        # INSERCCIÓN
        puts "INSERCIÓN: #{appointment_fin[:appointment].try(:start_date)} - #{appointment_fin[:appointment].try(:end_date)}"
        return {title: "#{I18n.t("notification_type.#{!appointment_fin[:appointment].try(:end_date).blank? ? "end" : "new"}")}",
          person_type: appointment_fin[:person_type],
          date: !appointment_fin[:appointment].try(:end_date).blank? ? appointment_fin[:appointment].try(:end_date) : appointment_fin[:appointment].try(:start_date)}
      else
        # MODIFICACIÓN
        # BAJA
        puts "MODIDICACIÓN/BAJA: #{appointment_fin[:appointment].try(:start_date)} - #{appointment_fin[:appointment].try(:end_date)}"
        if person.job_levels.count == 0
          return {title: "#{I18n.t("notification_type.#{!appointment_fin[:appointment].try(:end_date).blank? ? "end" : "edit"}")}",
            person_type: appointment_fin[:person_type],
            date: !appointment_fin[:appointment].try(:end_date).blank? ? appointment_fin[:appointment].try(:end_date) : appointment_fin[:appointment].try(:start_date)}
        else
          case appointment_fin[:person_type]
          when "C"
            aux = contenido.try{|x| x['concejal']}
            if !aux.blank?
              corporation = Corporation.find_by(name: aux['corporacion'].try{|x| x['cpNombre'].gsub('/','-')})
              result = validation_dif_appointment(appointment_fin[:appointment],  person.councillors[0].get_appointment(corporation))
              return get_result_response(result, appointment_fin, person, contenido, "C")
            end
          when "R"
            aux = contenido.try{|x| x['directivo']}
            if !aux.blank?
              result = validation_dif_appointment(appointment_fin[:appointment],  person.directors[0].try(:appointment))
              return get_result_response(result, appointment_fin, person, contenido,"R")
            end
          when "F"
            aux = contenido.try{|x| x['funcionario']}
            if !aux.blank?
              result = validation_dif_appointment(appointment_fin[:appointment],  person.public_workers[0].try(:appointment))
              return get_result_response(result, appointment_fin, person, contenido)
            end
          when "E"
            aux = contenido.try{|x| x['eventual']}
            if !aux.blank?
              result = validation_dif_appointment(appointment_fin[:appointment],  person.temporary_workers[0].try(:appointment))
              return get_result_response(result, appointment_fin, person, contenido)
            end
          when "V"
            aux = contenido.try{|x| x['vocal']}
            if !aux.blank?
              result = validation_dif_appointment(appointment_fin[:appointment],  person.spokespeople[0].try(:appointment))
              return get_result_response(result, appointment_fin, person, contenido)
            end
          end
        end
      end

      return {title: "", date: "", person_type: ""}
    rescue => e
      puts "ERROR: No se puede cargar la peticion -> #{e}"
      {title: "", date: "", person_type: ""}
    end

    def get_last_appointment(contenido)
      person_type = contenido.try{|x| x['gper']}

      if !contenido.try{|x| x['concejal']}.blank?
        aux = contenido.try{|x| x['concejal']}
        corporation = Corporation.find_by(name: aux['corporacion'].try{|x| x['cpNombre'].gsub('/','-')})

        appointment_concejal = Appointment.new(position: aux['cjnAlcalde'] ? 'Alcalde' : 'Concejal',
          start_date: aux['cjnFechaNom'].blank? ? '' : Time.at(aux['cjnFechaNom']/1000).strftime("%d/%m/%Y"),
          end_date: aux['cjnFechaCese'].blank? ? '' : Time.at(aux['cjnFechaCese']/1000).strftime("%d/%m/%Y"),
          area: aux['cjnArea'])
      else
        appointment_concejal= nil
        corporation = nil
      end

      if !contenido.try{|x| x["directivo"]}.blank?
        aux = contenido.try{|x| x["directivo"]}

        appointment_directivo = Appointment.new(position: aux["drnCargo"], unit: aux["drnUnidad"],
          start_date: aux["drnFechaNomb"].blank? ? '' : Time.at(aux["drnFechaNomb"]/1000).strftime("%d/%m/%Y"),
          end_date: aux["drnFechaCese"].blank? ? '' : Time.at(aux["drnFechaCese"]/1000).strftime("%d/%m/%Y"),
          area: aux["drnArea"])
      else
        appointment_directivo = nil
      end

      if !contenido.try{|x| x["eventual"]}.blank?
        aux = contenido.try{|x| x["eventual"]}

        appointment_eventual = Appointment.new(position: aux["evtnCargo"], unit: aux["evtnUnidad"],
          start_date: aux["evtnFechaNomb"].blank? ? '' : Time.at(aux["evtnFechaNomb"]/1000).strftime("%d/%m/%Y"),
          end_date: aux["evtnFechaCese"].blank? ? '' : Time.at(aux["evtnFechaCese"]/1000).strftime("%d/%m/%Y"),
          area: aux["evtnArea"])
      else
        appointment_eventual = nil
      end

      if !contenido.try{|x| x["funcionario"]}.blank?
        aux = contenido.try{|x| x["funcionario"]}

        appointment_funcionario = Appointment.new(position: aux["fldnCargo"], unit: aux["fldnUnidad"],
          start_date: aux["fldnFechaNomb"].blank? ? '' : Time.at(aux["fldnFechaNomb"]/1000).strftime("%d/%m/%Y"),
          end_date: aux["fldnFechaCese"].blank? ? '' : Time.at(aux["fldnFechaCese"]/1000).strftime("%d/%m/%Y"),
          area: aux["fldnArea"])
      else
        appointment_funcionario = nil
      end

      if !contenido.try{|x| x["vocal"]}.blank?
        aux = contenido.try{|x| x["vocal"]}

        appointment_vocal = Appointment.new(position: aux["vvnCargo"], unit: aux["vvnUnidad"],
          start_date: aux["vvnFechaNomb"].blank? ? '' : Time.at(aux["vvnFechaNomb"]/1000).strftime("%d/%m/%Y"),
          end_date: aux["vvnFechaCese"].blank? ? '' : Time.at(aux["vvnFechaCese"]/1000).strftime("%d/%m/%Y"),
          area: aux["vvnArea"])
      else
        appointment_vocal = nil
      end

      case person_type
      when "C"
        if appointment_concejal.blank? && !appointment_vocal.blank?
          return {person_type: "V", appointment: appointment_vocal}
        elsif appointment_concejal.blank? && !appointment_directivo.blank?
          return {person_type: "R", appointment: appointment_directivo}
        elsif appointment_concejal.blank? && !appointment_eventual.blank?
          return {person_type: "E", appointment: appointment_eventual}
        elsif appointment_concejal.blank? && !appointment_funcionario.blank?
          return {person_type: "F", appointment: appointment_funcionario}
        else
          return {person_type: "C", appointment: appointment_concejal}
        end
      when "R"
        if appointment_directivo.blank? && !appointment_vocal.blank?
          return {person_type: "V", appointment: appointment_vocal}
        elsif appointment_directivo.blank? && !appointment_concejal.blank?
          return {person_type: "C", appointment: appointment_concejal}
        elsif appointment_directivo.blank? && !appointment_eventual.blank?
          return {person_type: "E", appointment: appointment_eventual}
        elsif appointment_directivo.blank? && !appointment_funcionario.blank?
          return {person_type: "F", appointment: appointment_funcionario}
        else
          return {person_type: "R", appointment: appointment_directivo}
        end
      when "F"
        if appointment_funcionario.blank? && !appointment_vocal.blank?
          return {person_type: "V", appointment: appointment_vocal}
        elsif appointment_funcionario.blank? && !appointment_directivo.blank?
          return {person_type: "R", appointment: appointment_directivo}
        elsif appointment_funcionario.blank? && !appointment_eventual.blank?
          return {person_type: "E", appointment: appointment_eventual}
        elsif appointment_funcionario.blank? && !appointment_concejal.blank?
          return {person_type: "C", appointment: appointment_concejal}
        else
          return {person_type: "F", appointment: appointment_funcionario}
        end
      when "E"
        if appointment_eventual.blank? && !appointment_vocal.blank?
          return {person_type: "V", appointment: appointment_vocal}
        elsif appointment_eventual.blank? && !appointment_directivo.blank?
          return {person_type: "R", appointment: appointment_directivo}
        elsif appointment_eventual.blank? && !appointment_concejal.blank?
          return {person_type: "C", appointment: appointment_concejal}
        elsif appointment_eventual.blank? && !appointment_funcionario.blank?
          return {person_type: "F", appointment: appointment_funcionario}
        else
          return {person_type: "E", appointment: appointment_eventual}
        end
      when "V"
        if appointment_vocal.blank? && !appointment_concejal.blank?
          return {person_type: "C", appointment: appointment_concejal}
        elsif appointment_vocal.blank? && !appointment_directivo.blank?
          return {person_type: "R", appointment: appointment_directivo}
        elsif appointment_vocal.blank? && !appointment_eventual.blank?
          return {person_type: "E", appointment: appointment_eventual}
        elsif appointment_vocal.blank? && !appointment_funcionario.blank?
          return {person_type: "F", appointment: appointment_funcionario}
        else
          return {person_type: "V", appointment: appointment_vocal}
        end
      end
      return {person_type: nil, appointment: nil}
    end

    def get_result_response(result, appointment_fin, person, contenido, tipo = nil)
      if result == 1
        return {title: I18n.t("notification_type.end"), date: appointment_fin[:appointment].try(:end_date), person_type: appointment_fin[:person_type]}
      elsif result == 2
        return {title: I18n.t("notification_type.edit"), date:  appointment_fin[:appointment].try(:start_date), person_type: appointment_fin[:person_type]}
      else
        result = validation_dif_person(person, contenido)
        result2= 0
        if !tipo.blank?
          result2 = validation_dif_compatibilidad(person, contenido, tipo)
        end
        if result == 1 || result2 == 1
          return {title: I18n.t("notification_type.edit"), date:  appointment_fin[:appointment].try(:start_date), person_type: appointment_fin[:person_type]}
        end
      end
      {title: "", date: "", person_type: ""}
    end

    def validation_dif_person(exist, contenido)
      person = Person.new(
        name: contenido.try{|x| x['dperNombre']},
        last_name: "#{contenido.try{|x| x['dperApellido1']}} #{contenido.try{|x| x['dperApellido2']}}",
        document_nif: contenido.try{|x| x['dperDocumento']},
        document_type: DocumentType.find_by(name: contenido.try{|x| x['tipoDocumento']['tdocNombre']}),
        sex: contenido.try{|x| x['dperSexo']},
        personal_code: contenido.try{|x| x['dperNumper']}.blank? ? contenido.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '') : contenido.try{|x| x['dperNumper']}.to_s )
      [:name, :last_name, :document_nif, :personal_code].each{|p| return 1 if (person.try(p).to_s != exist.try(p).to_s)}
      0
    end

    def validation_dif_compatibilidad(exist, contenido, tipo)
      if tipo=="C"
        begin
          exist = exist.councillors.first.councillors_corporations.last
          return 1 if  exist.compatibility_activities.count < contenido['concejal']['actividadesCompatibilidad'].count || exist.compatibility_activities.count > contenido['concejal']['actividadesCompatibilidad'].count
          [0..exist.compatibility_activities.count].each do |index|
            [[:resolution_date,"accFechaResolucion"],[:compatibility_description,"accDescripcion"],[:public_private,"accPrivadaPublica"]].each do |t|
              if (t[1] == "accFechaResolucion")
                fecha_content = contenido['concejal']['actividadesCompatibilidad'].sort_by { |elem| elem["accFechaResolucion"] }[index][t[1]].blank? ? '' : Time.at(contenido['concejal']['actividadesCompatibilidad'].sort_by { |elem| elem["accFechaResolucion"] }[index][t[1]].to_i/1000)
                return 1 if exist.compatibility_activities.order(:resolution_date)[index].try(t[0]).to_s != fecha_content.to_s
              else
                return 1 if exist.compatibility_activities.order(:resolution_date)[index].try(t[0]).to_s !=contenido['concejal']['actividadesCompatibilidad'].sort_by { |elem| elem["accFechaResolucion"] }[index][t[1]].to_s
              end
            end
          end

          return 1 if  exist.termination_authorizations.count < contenido['concejal']['autorizacionesCese'].count || exist.termination_authorizations.count > contenido['concejal']['autorizacionesCese'].count
          [0..exist.termination_authorizations.count].each do |index|
            [[:authorization_date,"atcFecha"],[:authorization_description,"atcDescripcion"]].each do |t|
              if (t[1] == "atcFecha")
                fecha_content = contenido['concejal']['autorizacionesCese'].sort_by { |elem| elem["atcFecha"] }[index][t[1]].blank? ? '' : Time.at(contenido['concejal']['autorizacionesCese'].sort_by { |elem| elem["atcFecha"] }[index][t[1]].to_i/1000)
                return 1 if exist.termination_authorizations.order(:authorization_date)[index].try(t[0]).to_s != fecha_content.to_s
              else
                return 1 if exist.termination_authorizations.order(:authorization_date)[index].try(t[0]).to_s !=contenido['concejal']['autorizacionesCese'].sort_by { |elem| elem["atcFecha"] }[index][t[1]].to_s
              end
            end
          end
        rescue
          return 1
        end
      else
        begin
          exist = exist.directors.first
          return 1 if  exist.compatibility_activities.count < contenido['directivo']['actividadesCompatibilidad'].count || exist.compatibility_activities.count > contenido['directivo']['actividadesCompatibilidad'].count
          [0..exist.compatibility_activities.count].each do |index|
            [[:resolution_date,"acrFechaResolucion"],[:compatibility_description,"acrDescripcion"],[:public_private,"acrPrivadaPublica"]].each do |t|
              if (t[1] == "acrFechaResolucion")
                fecha_content = contenido['directivo']['actividadesCompatibilidad'].sort_by { |elem| elem["acrFechaResolucion"] }[index][t[1]].blank? ? '' : Time.at(contenido['directivo']['actividadesCompatibilidad'].sort_by { |elem| elem["acrFechaResolucion"] }[index][t[1]].to_i/1000)
                return 1 if exist.compatibility_activities.order(:resolution_date)[index].try(t[0]).to_s != fecha_content.to_s
              else
                return 1 if exist.compatibility_activities.order(:resolution_date)[index].try(t[0]).to_s !=contenido['directivo']['actividadesCompatibilidad'].sort_by { |elem| elem["acrFechaResolucion"] }[index][t[1]].to_s
              end
            end
          end

          return 1 if  exist.termination_authorizations.count < contenido['directivo']['autorizacionesCese'].count || exist.termination_authorizations.count > contenido['directivo']['autorizacionesCese'].count
          [0..exist.termination_authorizations.count].each do |index|
            [[:authorization_date,"atrFecha"],[:authorization_description,"atrDescripcion"]].each do |t|
              if (t[1] == "atrFecha")
                fecha_content = contenido['directivo']['autorizacionesCese'].sort_by { |elem| elem["atrFecha"] }[index][t[1]].blank? ? '' : Time.at(contenido['directivo']['autorizacionesCese'].sort_by { |elem| elem["atrFecha"] }[index][t[1]].to_i/1000)
                return 1 if exist.termination_authorizations.order(:authorization_date)[index].try(t[0]).to_s != fecha_content.to_s
              else
                return 1 if exist.termination_authorizations.order(:authorization_date)[index].try(t[0]).to_s !=contenido['directivo']['autorizacionesCese'].sort_by { |elem| elem["atrFecha"] }[index][t[1]].to_s
              end
            end
          end
        rescue
          return 1
        end
      end
      0
    end

    def validation_dif_appointment(appointment, new_appointment = nil)
      nombramiento = new_appointment.blank? ? Appointment.find_by(id: appointment.id) : new_appointment
      mod = false

      if nombramiento.try(:end_date).blank? && !appointment.try(:end_date).blank?
        1
      elsif nombramiento.blank? && !appointment.blank?
        !appointment.try(:end_date).blank? ? 1 : 2
      else
        [:position, :unit, :area, :start_date, :end_date, :position_alt, :politic_group,:district,:corporation,:juntamd,:retribution_year,:transparency_link,:other_expenses,:gifts,:possession_date,:description_possession,:description,:email,:url_possession,:functions,:observations,:retribution,:url_form].each do |field|
          if nombramiento.try(field) != appointment.try(field)
            mod = true
            break
          end
        end
        mod ? 2 : 0
      end
    end

    def createNotification(data, type, compare)
      return if type.blank? || data["#{compare}"].blank?
      if type=='Person'
        exist = Notification.where("type_data = ? and permit=false and ignore=false", type).where("content_data ->> '#{compare.to_s.mb_chars}' = '#{data["#{compare}"].to_s.mb_chars}'").order(id: :desc).first
      else
        exist = Notification.where("type_data = ?", type).where("content_data ->> '#{compare.to_s.mb_chars}' = '#{data["#{compare}"].to_s.mb_chars}'").order(id: :desc).first
      end
      notification_data = {title: "", date: "", person_type: ""}
      notification_data = notification_data(data) if type == 'Person'
      appointment = false
      unit = false
      area = false
      action_abrev = data["gper"]

      # case action_abrev
      # when "F"
        if !data["funcionario"].blank?
          appointment = true if !data["funcionario"]["fldnCargo"].blank?
          unit = true if !data["funcionario"]["fldnUnidad"].blank?
          area = true if !data["funcionario"]["fldnArea"].blank?
        end
      # when "C"
        if !data["concejal"].blank?
          appointment = true
          unit = true
          area = true
        end
      # when "R"
        if !data["directivo"].blank?
          appointment = true if !data["directivo"]["drnCargo"].blank?
          unit = true if !data["directivo"]["drnUnidad"].blank?
          area = true if !data["directivo"]["drnArea"].blank?
        end
      # when "E"
        if !data["eventual"].blank?
          appointment = true if !data["eventual"]["evtnCargo"].blank?
          unit = true if !data["eventual"]["evtnUnidad"].blank?
          area = true if !data["eventual"]["evtnArea"].blank?
        end
      # when "V"
        if !data["vocal"].blank?
          appointment = true if !data["vocal"]["vvnCargo"].blank?
          unit = true if !data["vocal"]["gpNombre"].blank?
          area = true if !data["vocal"]["vvnUnidad"].blank?
        end
      # end

      if type != 'Person' || type == 'Person' && !notification_data[:title].blank? && !action_abrev.blank? && !data["dperNombre"].blank? && !data["dperApellido1"].blank? && !data["dperCorreoCorp"].blank?  && appointment && unit && area #&& ((action_abrev != "V" && !data["dperNumper"].blank?) || action_abrev == "V")
        if exist.blank?
          if type=='Person'
            result2 = compare_by_data(data, type)
            if !result2.blank?
              notification = Notification.new(content_data: data.to_json, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date])
              if notification.save
                puts "Se ha guardado correctamente la notificación #{data}"
                # permit(notification, true) if type != 'Person'
              else
                puts "Error al guardar la notificación"
              end
            else
              puts "RECHAZADO POR DUPLICADO"
              notification = Notification.new(content_data: data.to_json,no_publish_reason: "Historificado automático - Rechazado automático: no aporta datos nuevos",ignore: true, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date], change_state_at: Time.zone.now)
              notification.save
            end
          else
            notification = Notification.new(content_data: data.to_json, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date])
            if notification.save
              puts "Se ha guardado correctamente la notificación #{data}"
              # permit(notification, true) if type != 'Person'
            else
              puts "Error al guardar la notificación"
            end
          end
        else
          result = compare_by_type(exist.content_data,data, type)
          result2 = compare_by_data(data, type)
          if type != 'Person' && !result.blank? || type == 'Person' && !result.blank? && !result2.blank?
            if exist.historic?
              num_count = Notification.where("type_data = ? AND permit=false AND ignore=false", type).where("content_data ->> '#{compare}' = '#{data["#{compare}"]}'").count
              if num_count <= 0 || type=='Person'
                notification = Notification.new(content_data: data.to_json, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date])

                if notification.save
                   puts "Histórico: Se ha guardado correctamente la notificación #{data}"

                    # permit(notification, true) if type != 'Person'
                else
                    puts "Error al guardar la notificación"
                end
              else
                puts "Ya existe la notificación"
              end
            elsif type!='Person'
              if exist.update_attributes(content_data: data.to_json, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date])
                  puts "Se ha actualizado correctamente la notificación #{data}"
                  # permit(notification, true) if type != 'Person'
              else
                  puts "Error al actualizar la notificación"
              end
            elsif type == 'Person'
              notification = Notification.new(content_data: data.to_json, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date])
              if notification.save
                puts "Histórico: Se ha guardado correctamente la notificación #{data}"
                # permit(notification, true) if type != 'Person'
              else
                  puts "Error al guardar la notificación"
              end
            end
          else
            puts "RECHAZADO POR DUPLICADO"
            notification = Notification.new(content_data: data.to_json,no_publish_reason: "Historificado automático - Rechazado automático: no aporta datos nuevos",ignore: true, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date], change_state_at: Time.zone.now)
            notification.save
          end
        end
      else
        razon = "Historificado automático - Rechazado automático: No se va a añadir una notificación de persona en blanco"
        if notification_data[:title].blank?
          razon = "Historificado automático - Rechazado automático: Se ha rechazado por los filtros de datos de diferenciación"
        elsif action_abrev.blank?
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene la abreviatura del tipo de perfil"
        elsif data["dperNombre"].blank?
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene el nombre de la persona relleno"
        elsif data["dperApellido1"].blank?
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene el primer apellido de la persona relleno"
        elsif data["dperCorreoCorp"].blank?
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene el correo corporativo de la persona relleno"
        elsif !appointment
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene el cargo relleno"
        elsif !unit
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene la unidad relleno"
        elsif !area
          razon = "Historificado automático - Rechazado automático: Se ha rechazado porque no tiene el área rellena"
        end

        notification = Notification.new(content_data: data.to_json,no_publish_reason: razon,ignore: true, type_data: type, action_abrev: notification_data[:person_type], action: notification_data[:title], date_action: notification_data[:date], change_state_at: Time.zone.now)
        notification.save
        puts "No se va a añadir una notificación de persona en blanco."
        puts "#{data}"
      end
    end

    def permit(notification, automatic = false)
      current_administrator ||=nil
      begin
        permitido = false
        case notification.try(:type_data)
        when 'Profile'
          permitido = saveProfileData(notification, automatic, current_administrator)
        when 'AssetsDeclaration'
          permitido = saveAssetsDeclarationData(notification, automatic, current_administrator)
        when 'ActivitiesDeclaration'
          permitido = saveActivitiesDeclarationData(notification, automatic, current_administrator)
        end

        notification.permit = true
        notification.administrator = current_administrator
        notification.change_state_at = Time.zone.now
        notification.send_feedback = false


        if permitido && notification.save
          ws = INPERWS.new
          person_aux_types = {
              'C' => 'councillors',
              'R' => 'directors',
              'F' => 'public_workers',
              'E' => 'temporary_workers',
              'V' => 'spokespeople' }
          type = notification.try(:content_data).try{|x| x['gperAbrev']}.blank? ? '' : person_aux_types[notification.try(:content_data).try{|x| x['gperAbrev']}]
          person_identificator = notification.try(:content_data).try{|x| x['dperNumper']}.blank? ? notification.try(:content_data).try{|x| x['dperDocumento']}.to_s : notification.try(:content_data).try{|x| x['dperNumper']}.to_s
          aux_person = Person.find_by(personal_code: person_identificator)

          case notification.try(:type_data)
          when 'Profile'
            begin
              notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / "
              inper_response = ws.setWSPerfilesPublicado(notification.try(:content_data)['dpfId'], notification.change_state_at, 0,
                                                        "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}")
              begin
                respuesta= JSON.parse(inper_response)
                if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                  notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                  notification.motive = ""
                  notification.send_feedback = true
                else
                  notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                  notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                end
              rescue => e
                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                notification.motive = e.message
              end
            rescue => e
              notification.no_publish_reason = "Se han publicado los datos, Notificación perfil con id: #{notification.try(:content_data)['dpfId']} / No se ha realizado el feedback"
              notification.motive = e.message
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
            begin
              if !aux_person.profile.email.blank?
                Mailer.profile_upload(type,aux_person.profile.email).deliver_now
              elsif !aux_person.profile.personal_email.blank?
                Mailer.profile_upload(type,aux_person.profile.personal_email).deliver_now
              end
            rescue => e
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
          when 'AssetsDeclaration'
            begin
              notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / "
              inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decg_id"],notification.change_state_at,0,
                                                            "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}").to_s
              begin
                respuesta= JSON.parse(inper_response)
                if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                  notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                  notification.motive = ""
                  notification.send_feedback = true
                else
                  notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                  notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                end
              rescue => e
                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                notification.motive = e.message
              end
            rescue => e
              notification.no_publish_reason = "Se han publicado los datos, Notificación bienes con id: #{notification.try(:content_data)['decg_id']} / No se ha realizado el feedback"
              notification.motive = e.message
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
            begin
              if !aux_person.profile.email.blank?
                Mailer.assets_upload(type,aux_person.profile.email).deliver_now
              elsif !aux_person.profile.personal_email.blank?
                Mailer.assets_upload(type,aux_person.profile.personal_email).deliver_now
              end
            rescue => e
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
          when 'ActivitiesDeclaration'
            begin
              notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / "
              inper_response = ws.setWSDeclaracionPublicado(notification.try(:content_data)["decgId"],notification.change_state_at,0,
                                                            "Se han publicado los datos en TRAPE con fecha #{notification.change_state_at}")
              begin
                respuesta= JSON.parse(inper_response)
                if !respuesta['respuesta'].blank? && respuesta['respuesta'].to_s == 'OK'
                  notification.no_publish_reason = notification.no_publish_reason + "Se ha realizado el feedback"
                  notification.motive = ""
                  notification.send_feedback = true
                else
                  notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                  notification.motive = "#{respuesta['codigo']}: #{respuesta['mensaje']}"
                end
              rescue => e
                notification.no_publish_reason = notification.no_publish_reason + "No se ha realizado el feedback"
                notification.motive = e.message
              end
            rescue => e
              notification.no_publish_reason = "Se han publicado los datos, Notificación actividades con id: #{notification.try(:content_data)['decgId']} / No se ha realizado el feedback"
              notification.motive = e.message
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
            begin
              if !aux_person.profile.email.blank?
                Mailer.activities_upload(type,aux_person.profile.email).deliver_now
              elsif !aux_person.profile.personal_email.blank?
                Mailer.activities_upload(type,aux_person.profile.personal_email).deliver_now
              end
            rescue => e
              begin
                logger.error("ERROR: #{e}")
                rescue
                end
            end
          end

          notification.save
        end
      rescue
        case notification.try(:type_data)
        when 'Profile'
          puts "ERROR: No se ha aceptado la petición de perfil #{notification.id}"
        when 'AssetsDeclaration'
          puts "ERROR: No se ha aceptado la petición de declaración de bienes #{notification.id}"
        when 'ActivitiesDeclaration'
          puts "ERROR: No se ha aceptado la petición de declaración de actividades #{notification.id}"
        end
      end
    end

    def get_min_errors(error_name,model)
      return "" if error_name.blank?
      aux= error_name.to_s.split('.')

      return I18n.t("#{model}.#{error_name}")  if aux.count <= 1
      I18n.t("#{aux[aux.count-2]}.#{aux[aux.count-1]}")
    end

    def validateBlank(notification, person, message = "No existe la persona")
      if person.blank?
        notification.no_publish_reason = message
        notification.save
        return false
      end
      true
    end

    def compare_by_type(exist,data, type)
      if type=="Person"
        puts "===================================================="
        puts "Comparacion de datos persona"
        puts "===================================================="
        ["dperId","dperNumper","dperNombre","dperApellido1","dperApellido2","dperDocumento","dperSexo","dperCorreoCorp","gper"].each do |f1|
          return data if exist[f1].to_s.mb_chars != data[f1].to_s.mb_chars
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación tipos documento"
        puts "===================================================="
        begin
          return data if exist["tipoDocumento"]["tdocNombre"].to_s.mb_chars != data["tipoDocumento"]["tdocNombre"].to_s.mb_chars
        rescue
          return data
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación Vocal vecino"
        puts "===================================================="
        if !exist["vocal"].blank? && !data["vocal"].blank?
          ["vvnFechaNomb","vvnCargo","vvnUnidad","vvnFechaCese","vvnArea","vvnJuntaMd","disNombre","gpNombre","cpNombre","vvnAnnoRetribucion","vvnRetribucion",
            "vvnObservaciones","vvnFechaPosesion","vvnDescripcionPosesion"].each do |f2|
              return data if exist["vocal"][f2].to_s.mb_chars != data["vocal"][f2].to_s.mb_chars
          end
        elsif exist["vocal"].blank? && !data["vocal"].blank?# || !exist["vocal"].blank? && data["vocal"].blank?
          return data
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación Funcionario"
        puts "===================================================="
        if !exist["funcionario"].blank? && !data["funcionario"].blank?
          ["fldnFechaNomb","fldnCargo","fldnUnidad","fldnFechaCese","fldnArea"].each do |f2|
              return data if exist["funcionario"][f2].to_s.mb_chars != data["funcionario"][f2].to_s.mb_chars
          end
        elsif exist["funcioario"].blank? && !data["funcionario"].blank?# || !exist["funcionario"].blank? && data["funcionario"].blank?
          return data
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación Eventual"
        puts "===================================================="
        if !exist["eventual"].blank? && !data["eventual"].blank?
          ["evtnFechaNomb","evtnCargo","evtnUnidad","evtnFechaCese","evtnArea","evtnFechaPosesion","evtnDescripcionPosesion","evtnAnnoRetribucion","evtnRetribucion","evtnObservaciones"].each do |f2|
              return data if exist["eventual"][f2].to_s.mb_chars != data["eventual"][f2].to_s.mb_chars
          end
        elsif exist["eventual"].blank? && !data["eventual"].blank?# || !exist["eventual"].blank? && data["eventual"].blank?
          return data
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación Directivo"
        puts "===================================================="
        if !exist["directivo"].blank? && !data["directivo"].blank?
          ["drnFechaNomb","drnCargo","drnUnidad","drnFechaCese","drnArea","drnFechaPosesion","drnDescripcionPosesion","drnEmail","drnFunciones","drnRetribucion","drnAnnoRetribucion","drnObservaciones","drnDescripcion"].each do |f2|
              return data if exist["directivo"][f2].to_s.mb_chars != data["directivo"][f2].to_s.mb_chars
          end
          return data if exist["directivo"]["actividadesCompatibilidad"].count != data["directivo"]["actividadesCompatibilidad"].count
          (0..exist["directivo"]["actividadesCompatibilidad"].count-1).each do |index|
            ["acrFechaResolucion","acrPrivadaPublica","acrDescripcion"].each do |d1|
              return data if exist["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1].to_s.mb_chars != data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1].to_s.mb_chars
            end
          end

          return data if exist["directivo"]["autorizacionesCese"].count != data["directivo"]["autorizacionesCese"].count
          (0..exist["directivo"]["autorizacionesCese"].count-1).each do |index|
            ["atrFecha","atrDescripcion"].each do |d1|
              return data if exist["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1].to_s.mb_chars != data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1].to_s.mb_chars
            end
          end
        elsif exist["directivo"].blank? && !data["directivo"].blank?# || !exist["directivo"].blank? && data["directivo"].blank?
          return data
        end
        puts "OK"
        puts "===================================================="
        puts "Comparación Concejal"
        puts "===================================================="
        if !exist["concejal"].blank? && !data["concejal"].blank?
          ["afiliacion","numOrden","numLista","cjnFechaNom","cjnAlcalde","cjnFechaCese","cjnArea","cjnFechaPosesion","cjnUrlPosesion","cjnUrlFormulario","cjnFunciones",
            "cjnAnnoRetribucion","cjnRetribucion","cjnObservaciones"].each do |f2|
              return data if exist["concejal"][f2].to_s.mb_chars != data["concejal"][f2].to_s.mb_chars
          end

          begin
            return data if exist["concejal"]["corporacion"]["cpNombre"].to_s.mb_chars != data["concejal"]["corporacion"]["cpNombre"].to_s.mb_chars
          rescue
            return data
          end


          return data if exist["concejal"]["actividadesCompatibilidad"].count != data["concejal"]["actividadesCompatibilidad"].count
          (0..exist["concejal"]["actividadesCompatibilidad"].count-1).each do |index|
            ["accFechaResolucion","accPrivadaPublica","accDescripcion"].each do |d1|
              return data if exist["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1].to_s.mb_chars != data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1].to_s.mb_chars
            end
          end

          return data if exist["concejal"]["autorizacionesCese"].count != data["concejal"]["autorizacionesCese"].count
          (0..exist["concejal"]["autorizacionesCese"].count-1).each do |index|
            ["atcFecha","atcDescripcion"].each do |d1|
              return data if exist["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1].to_s.mb_chars != data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1].to_s.mb_chars
            end
          end
        elsif exist["concejal"].blank? && !data["concejal"].blank? #|| !exist["concejal"].blank? && data["concejal"].blank?
          return data
        end
        puts "OK"


        return nil
      else
        result = JsonCompare.get_diff(exist.content_data.to_json, data.to_json)
      end
    end

    def compare_by_data(data, type)
      return data if type != "Person"
      person = Person.find_by(personal_code: data.try{|x| x['dperNumper']}.blank? ? data.try{|x| x['dperDocumento']}.to_s.gsub(/[A-Za-z]*/, '').to_i : data.try{|x| x['dperNumper']})
      if person.blank?
        person = Person.find_by("TRANSLATE(UPPER(name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?),'AEIOU', 'ÁÉÍÓÚ') AND TRANSLATE(UPPER(last_name), 'AEIOU', 'ÁÉÍÓÚ') LIKE TRANSLATE(UPPER(?), 'AEIOU', 'ÁÉÍÓÚ')",
          "%#{data.try{|x| x['dperNombre']}}%", "%#{data.try{|x| x['dperApellido1']}} #{data.try{|x| x['dperApellido2']}}%")
      end
      return data if person.blank?
      puts "===================================================="
      puts "Comparación datos reales"
      puts "===================================================="

      puts "===================================================="
      puts "Comparacion de datos persona"
      puts "===================================================="
      [["dperNumper",:personal_code],["dperNombre",:name],["dperApellido1",:last_name],["dperDocumento",:document_nif],["dperSexo",:sex],["dperCorreoCorp",:email]].each do |f1|
        if f1[0] == "dperApellido1"
          return data if  person.try(f1[1]).to_s.mb_chars.strip != "#{data["dperApellido1"]} #{data["dperApellido2"]}".to_s.mb_chars.strip
        elsif f1[0] == "dperCorreoCorp"
            return data if person.profile.try(f1[1]).to_s.mb_chars.strip != data[f1[0]].to_s.mb_chars.strip
        elsif f1[0] == "dperNumper"
          return data if  !data[f1[0]].to_s.mb_chars.strip.blank? && person.try(f1[1]).to_s.mb_chars.strip != data[f1[0]].to_s.mb_chars.strip || data[f1[0]].to_s.mb_chars.strip.blank? && person.try(f1[1]).to_s.mb_chars.strip != data["dperDocumento"].to_s.gsub(/[A-Za-z]*/, '').mb_chars.strip
        else
          return data if  person.try(f1[1]).to_s.mb_chars.strip != data[f1[0]].to_s.mb_chars.strip
        end
      end
      puts "OK"
      puts "===================================================="
      puts "Comparación tipos documento"
      puts "===================================================="
      begin
        return data if person.try(:document_type).try(:name).to_s.mb_chars.strip != data["tipoDocumento"]["tdocNombre"].to_s.mb_chars.strip
      rescue
        return data
      end
      puts "OK"
      puts "===================================================="
      puts "Comparación Vocal vecino"
      puts "===================================================="
      if !person.spokespeople.first.blank? && !data["vocal"].blank?
        data_person = person.spokespeople.first.try(:appointment)
        [["vvnFechaNomb",:start_date],["vvnCargo",:position],["vvnUnidad",:area],["vvnFechaCese",:end_date],["gpNombre",:unit],["vvnAnnoRetribucion",:retribution_year],["vvnRetribucion",:retribution],
          ["vvnObservaciones",:observations],["vvnFechaPosesion",:possession_date],["vvnDescripcionPosesion",:description_possession]].each do |f2|
            if f2[0] == "vvnFechaNomb" || f2[0] == "vvnFechaCese" || f2[0] == "vvnFechaPosesion"
              return data if data_person.try(f2[1]).blank? && !data["vocal"][f2[0]].blank? || !data_person.try(f2[1]).blank? && data["vocal"][f2[0]].blank? || !data_person.try(f2[1]).blank? && !data["vocal"][f2[0]].blank? && data_person.try(f2[1]).strftime("%d/%m/%Y") !=  Time.at(data["vocal"][f2[0]].to_i/1000).strftime("%d/%m/%Y")
            else
              return data if  data_person.try(f2[1]).to_s.mb_chars != data["vocal"][f2[0]].to_s.mb_chars
            end
        end
      elsif person.spokespeople.first.blank? && !data["vocal"].blank? #|| !person.spokespeople.first.blank? && data["vocal"].blank?
        return data
      end
      puts "OK"
      puts "===================================================="
      puts "Comparación Funcionario"
      puts "===================================================="
      if !person.public_workers.first.blank? && !data["funcionario"].blank?
        data_person = person.public_workers.first.try(:appointment)
        [["fldnFechaNomb",:start_date],["fldnCargo",:position],["fldnUnidad",:unit],["fldnFechaCese",:end_date],["fldnArea",:area]].each do |f2|
          if f2[0] == "fldnFechaNomb" || f2[0] == "fldnFechaCese"
            return data if data_person.try(f2[1]).blank? && !data["funcionario"][f2[0]].blank? || !data_person.try(f2[1]).blank? && data["funcionario"][f2[0]].blank? || !data_person.try(f2[1]).blank? && !data["funcionario"][f2[0]].blank? && data_person.try(f2[1]).strftime("%d/%m/%Y") !=  Time.at(data["funcionario"][f2[0]].to_i/1000).strftime("%d/%m/%Y")
          else
            return data if  data_person.try(f2[1]).to_s.mb_chars != data["funcionario"][f2[0]].to_s.mb_chars
          end
        end
      elsif person.public_workers.first.blank? && !data["funcionario"].blank? #|| !person.public_workers.first.blank? && data["funcionario"].blank?
        return data
      end
      puts "OK"


      puts "===================================================="
      puts "Comparación Eventual"
      puts "===================================================="
      if !person.temporary_workers.first.blank? && !data["eventual"].blank?
        data_person = person.temporary_workers.first.try(:appointment)
        [["evtnFechaNomb", :start_date],["evtnCargo",:position],["evtnUnidad",:unit],["evtnFechaCese",:end_date],["evtnArea",:area],["evtnFechaPosesion",:possession_date],
          ["evtnDescripcionPosesion",:description_possession],["evtnAnnoRetribucion",:retribution_year],["evtnRetribucion",:retribution],["evtnObservaciones",:observations]].each do |f2|
          if f2[0] == "evtnFechaNomb" || f2[0] == "evtnFechaCese" || f2[0] == "evtnFechaPosesion"
            return data if data_person.try(f2[1]).blank? && !data["eventual"][f2[0]].blank? || !data_person.try(f2[1]).blank? && data["eventual"][f2[0]].blank? || !data_person.try(f2[1]).blank? && !data["eventual"][f2[0]].blank? && data_person.try(f2[1]).strftime("%d/%m/%Y") !=  Time.at(data["eventual"][f2[0]].to_i/1000).strftime("%d/%m/%Y")
          else
            return data if  data_person.try(f2[1]).to_s.mb_chars != data["eventual"][f2[0]].to_s.mb_chars
          end
        end
      elsif person.temporary_workers.first.blank? && !data["eventual"].blank?# || !person.temporary_workers.first.blank? && data["eventual"].blank?
        return data
      end
      puts "OK"
      puts "===================================================="
      puts "Comparación Directivo"
      puts "===================================================="
      if !person.directors.first.blank? && !data["directivo"].blank?
        data_person = person.directors.first.try(:appointment)
        [["drnFechaNomb",:start_date],["drnCargo",:position],["drnUnidad",:unit],["drnFechaCese",:end_date],["drnArea",:area],["drnFechaPosesion",:possession_date],
          ["drnDescripcionPosesion",:description_possession],["drnEmail",:email],["drnFunciones",:functions],["drnRetribucion",:retribution],["drnAnnoRetribucion",:retribution_year],["drnObservaciones",:observations],["drnDescripcion",:description]].each do |f2|
          if f2[0] == "drnFechaNomb" || f2[0] == "drnFechaCese" || f2[0] == "drnFechaPosesion"
            return data if data_person.try(f2[1]).blank? && !data["directivo"][f2[0]].blank? || !data_person.try(f2[1]).blank? && data["directivo"][f2[0]].blank? || !data_person.try(f2[1]).blank? && !data["directivo"][f2[0]].blank? && data_person.try(f2[1]).strftime("%d/%m/%Y") !=  Time.at(data["directivo"][f2[0]].to_i/1000).strftime("%d/%m/%Y")
          else
            return data if  data_person.try(f2[1]).to_s.mb_chars != data["directivo"][f2[0]].to_s.mb_chars
          end
        end

        data_compatibility = person.directors.first.try(:compatibility_activities)
        return data if data_compatibility.count != data["directivo"]["actividadesCompatibilidad"].count
        (0..data_compatibility.count-1).each do |index|
          [["acrFechaResolucion",:resolution_date],["acrPrivadaPublica",:public_private],["acrDescripcion",:compatibility_description]].each do |d1|
            if d1[0] == "acrFechaResolucion"
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && !data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].blank? || !data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].blank? || !data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && !data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].blank? && data_compatibility.order(created_at: :asc)[index].try(d1[1]).strftime("%d/%m/%Y") !=  Time.at(data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].to_i/1000).strftime("%d/%m/%Y")
            elsif d1[0] == "acrPrivadaPublica"
              public_private_enum = data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].to_s
              public_private = public_private_enum.blank? ? "" : public_private_enum == "1" ? "Pública" : public_private_enum == "2" ? "Privada" : public_private_enum
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != public_private.to_s.mb_chars
            else
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != data["directivo"]["actividadesCompatibilidad"].sort_by{|x| x["acrId"]}[index][d1[0]].to_s.mb_chars
            end
          end
        end

        data_auth = person.directors.first.try(:termination_authorizations)
        return data if data_auth.count != data["directivo"]["autorizacionesCese"].count
        (0..data_auth.count-1).each do |index|
          [["atrFecha",:authorization_date],["atrDescripcion",:authorization_description]].each do |d1|
            if d1[0] == "atrFecha"
              return data if data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && !data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1[0]].blank? || !data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1[0]].blank? || !data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && !data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1[0]].blank? && data_auth.order(created_at: :asc)[index].try(d1[1]).strftime("%d/%m/%Y") !=  Time.at(data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1[0]].to_i/1000).strftime("%d/%m/%Y")
            else
              return data if data_auth.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != data["directivo"]["autorizacionesCese"].sort_by{|x| x["atrId"]}[index][d1[0]].to_s.mb_chars
            end
          end
        end
      elsif person.directors.first.blank? && !data["directivo"].blank?# || !person.directors.first.blank? && data["directivo"].blank?
        return data
      end
      puts "OK"
      puts "===================================================="
      puts "Comparación Concejal"
      puts "===================================================="
      if !data["concejal"].blank? && !person.councillors.first.blank? && !person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).blank?
        data_person = person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).first.try(:appointment)
        [["afiliacion",:name],["numLista", :order_num],["cjnFechaNom",:start_date],["cjnAlcalde",:position],["cjnFechaCese",:end_date],["cjnArea",:area],["cjnFechaPosesion",:possession_date],["cjnUrlPosesion",:url_possession],["cjnUrlFormulario",:url_form],["cjnFunciones",:functions],
          ["cjnAnnoRetribucion",:retribution_year],["cjnRetribucion",:retribution],["cjnObservaciones",:observations]].each do |f2|
            if f2[0] == "cjnFechaNom" || f2[0] == "cjnFechaCese" || f2[0] == "cjnFechaPosesion"
              return data if data_person.try(f2[1]).blank? && !data["concejal"][f2[0]].blank? || !data_person.try(f2[1]).blank? && data["concejal"][f2[0]].blank? || !data_person.try(f2[1]).blank? && !data["concejal"][f2[0]].blank? && data_person.try(f2[1]).strftime("%d/%m/%Y") !=  Time.at(data["concejal"][f2[0]].to_i/1000).strftime("%d/%m/%Y")
            elsif  f2[0] == "numLista"
              return data if person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).first.try(f2[1]).to_s.mb_chars != data["concejal"][f2[0]].to_s.mb_chars
            elsif  f2[0] == "cjnAlcalde"
              return data if  data_person.try(f2[1]).to_s.mb_chars != (data["concejal"][f2[0]].to_i==1 ? 'Alcalde' : 'Concejal').to_s.mb_chars
            elsif  f2[0] == "afiliacion"
              return data if person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).first.try(:electoral_list).try(f2[1]).to_s.mb_chars != data["concejal"][f2[0]].to_s.mb_chars
            else
              return data if  data_person.try(f2[1]).to_s.mb_chars != data["concejal"][f2[0]].to_s.mb_chars
            end
        end

        data_compatibility = person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).first.try(:compatibility_activities)
        return data if data_compatibility.count != data["concejal"]["actividadesCompatibilidad"].count
        (0..data_compatibility.count-1).each do |index|
          [["accFechaResolucion",:resolution_date],["accPrivadaPublica",:public_private],["accDescripcion",:compatibility_description]].each do |d1|
            if d1[0] == "accFechaResolucion"
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && !data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].blank? || !data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].blank? || !data_compatibility.order(created_at: :asc)[index].try(d1[1]).blank? && !data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].blank? && data_compatibility.order(created_at: :asc)[index].try(d1[1]).strftime("%d/%m/%Y") !=  Time.at(data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].to_i/1000).strftime("%d/%m/%Y")
            elsif d1[0] == "accPrivadaPublica"
              public_private_enum = data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].to_s
              public_private = public_private_enum.blank? ? "" : public_private_enum == "1" ? "Pública" : public_private_enum == "2" ? "Privada" : public_private_enum
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != public_private.to_s.mb_chars
            else
              return data if data_compatibility.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != data["concejal"]["actividadesCompatibilidad"].sort_by{|x| x["accId"]}[index][d1[0]].to_s.mb_chars
            end
          end
        end

        data_auth = person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).first.try(:termination_authorizations)
        return data if data_auth.count != data["concejal"]["autorizacionesCese"].count
        (0..data_auth.count-1).each do |index|
          [["atcFecha",:authorization_date],["atcDescripcion",:authorization_description]].each do |d1|
            if d1[0] == "atcFecha"
              return data if data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && !data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1[0]].blank? || !data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1[0]].blank? || !data_auth.order(created_at: :asc)[index].try(d1[1]).blank? && !data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1[0]].blank? && data_auth.order(created_at: :asc)[index].try(d1[1]).strftime("%d/%m/%Y") !=  Time.at(data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1[0]].to_i/1000).strftime("%d/%m/%Y")
            else
              return data if data_auth.order(created_at: :asc)[index].try(d1[1]).to_s.mb_chars != data["concejal"]["autorizacionesCese"].sort_by{|x| x["atcId"]}[index][d1[0]].to_s.mb_chars
            end
          end
        end
      elsif !data["concejal"].blank? && (person.councillors.first.blank? || person.councillors.first.councillors_corporations.where("corporation_id in (?)",  Corporation.where(name: data['concejal']['corporacion']['cpNombre'].gsub('/','-')).select(:id)).blank?)# || !person.councillors.first.blank? && data["concejal"].blank?
        return data
      end
      puts "OK"



      return nil
    end

end
