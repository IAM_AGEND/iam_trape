class INPERWS
    require "net/http"
    require "uri"

    def getWSCorporaciones
        getHTTP("wscorporaciones")
    end

    def getWSPerfiles
        getHTTP("wsperfiles")
    end

    def getWSPerfilesNumper(num)
        getHTTP("wsperfiles/numper/#{num}")
    end

    def getWSPersonas
        getHTTP("wspersonas")
    end
     
    def getWSPersonasNumper(num)
        getHTTP("wspersonas/numper/#{num}")
    end

    def getWSPersonasPag(start_pag, end_pag)
        getHTTP("wspersonas/#{start_pag}/#{end_pag}")
    end

    def getWSDeclaracionesactividades
        getHTTP("wsdeclaracionesactividades")
    end

    def getWSDeclaracionesactividadesNumper(num)
        getHTTP("wsdeclaracionesactividades/numper/#{num}")
    end

    def getWSDeclaracionesbienes
        getHTTP("wsdeclaracionesbienes")
    end

    def getWSDeclaracionesbienesNumper(num)
        getHTTP("wsdeclaracionesbienes/numper/#{num}")
    end

    def setWSDeclaracionPublicado(id, date_published, rejected, reason)
      url = "wsdeclaraciones/publicado"
      body_params = get_body(id,  date_published,  rejected,  reason)
      setHTTP(url, body_params )
    end

    def setWSPerfilesPublicado(id, date_published, rejected, reason)
      url = 'wsperfiles/publicado'
      body_params = get_body( id, date_published,  rejected, reason)
      setHTTP(url, body_params)
    end

    private 

    def get_body(id, date_published, rejected, reason)
      {
        'decgId': id,
        'fecPublicado': date_published,
        'rechazado': rejected,
        'motivo': reason
      }.to_json
    end

    def getHeaders
        {
            'Content-type' => 'application/json; charset=UTF-8',
            'app' => Rails.application.secrets.ws_trape_app,
            'pw' => Rails.application.secrets.ws_trape_pw
        }
    end

    def getHTTP(url)        
        url = URI("#{Rails.application.secrets.ws_trape_url}#{url}")

        Net::HTTP.start(url.hostname, url.port, :use_ssl => url.scheme == 'https') do |http|
            request = Net::HTTP::Post.new(url, getHeaders)

            response = http.request(request)
            return response.body
        end
    rescue => e
        puts "ERROR(#{url}): #{e.message}"
        {error: e}        
    end

    def setHTTP(url, body_params)
        url = URI("#{Rails.application.secrets.ws_trape_url}#{url}")
        Net::HTTP.start(url.hostname, url.port, :use_ssl => url.scheme == 'https') do |http|
            request = Net::HTTP::Put.new(url, getHeaders)
            request.body=body_params
            response = http.request(request)
            return response.body
        end
    rescue => e
        puts "ERROR(#{url}): #{e.message}"
        {error: e}
    end

end