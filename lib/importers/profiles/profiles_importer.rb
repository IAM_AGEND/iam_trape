require 'importers/base_importer'

module Importers
  class ProfilesImporter < BaseImporter
    def import!
      each_row do |row|
        person = Person.find_by(personal_code: row[:codigo_personal])
        
        profile = person.try(:profile)
        profile = Profile.new if profile.blank?

        profile.email = row[:correo_electronico]
        profile.twitter = row[:twitter]
        profile.facebook = row[:facebook]
        profile.calendar_url = row[:url_calendario]
        begin
          profile.portrait = File.new(Rails.root.join("app/assets", 'images/new_people/', row[:foto_perfil]).to_s, 'r')
        rescue
          profile.portrait = nil
        end
       
        profiled_at= parse_at(row[:fecha_actualizacion])
        profile.updated_at = profiled_at
        profile.updated_at = profile.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) unless profiled_at.blank?
        
        profile.political_posts.each {|x| x.destroy} if profile.political_posts.count > 0
        profile.private_jobs.each {|x| x.destroy} if profile.private_jobs.count > 0
        profile.public_jobs.each {|x| x.destroy} if profile.public_jobs.count > 0
        profile.informations.each {|x| x.destroy} if profile.informations.count > 0
        profile.studies.each {|x| x.destroy} if profile.studies.count > 0
        profile.courses.each {|x| x.destroy} if profile.courses.count > 0
        profile.languages.each {|x| x.destroy} if profile.languages.count > 0
        begin
          if profile.save
            person.profile = profile
            if person.save
              puts "Perfil importado: #{person.backwards_name}"
            else
              puts "No se ha importado el perfil: (#{person.backwards_name}) #{get_full_messages_error(person)}"
            end
          else            
            puts "No se ha importado el perfil: (#{person.backwards_name}) #{get_full_messages_error(profile)}"
          end
        rescue
          puts "ERROR: No se ha importado el perfil: #{row[:codigo_personal]}"
        end
      end
    end
  end

  class PoliticalPostsImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          political_posts = PoliticalPost.new(profile: profile, position: row[:puesto], entity: row[:entidad],
            start_year: row[:ano_inicio], end_year: row[:ano_fin])

          if political_posts.save
            puts "Cargo político importado: #{political_posts.position}/#{political_posts.entity}"
          else            
            puts "No se ha importado el cargo político: (#{political_posts.position}/#{political_posts.entity}) #{get_full_messages_error(political_posts)}"
          end
        end
      end
    end
  end

  class PrivateJobsImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          private_job = PrivateJob.new(profile: profile, position: row[:puesto], entity: row[:entidad],
            start_year: row[:ano_inicio], end_year: row[:ano_fin])
        
          if private_job.save
            puts "Ámbito privado importado: #{private_job.position}/#{private_job.entity}"
          else            
            puts "No se ha importado el ámbito privado: (#{private_job.position}/#{private_job.entity}) #{get_full_messages_error(private_job)}"
          end
        end
      end
    end
  end

  class PublicJobsImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          public_job = PublicJob.new(profile: profile, public_administration: row[:administracion_publica], body_scale: row[:escala_cuerpo], 
            init_year: row[:ano_inicial], position: row[:puesto], consolidation_degree: row[:grado_de_consolidacion],
            start_year: row[:ano_inicio], end_year: row[:ano_fin])
        
          if public_job.save
            puts "Ámbito público importado: #{public_job.position}/#{public_job.public_administration}"
          else            
            puts "No se ha importado el ámbito público: (#{public_job.position}/#{public_job.public_administration}) #{get_full_messages_error(public_job)}"
          end
        end
      end
    end
  end

  class InformationsImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          information = Information.new(profile: profile, information: row[:informacion], information_type: InformationType.find_by(name: row[:tipo_informacion]), 
            information_at: row[:fecha_informacion])
        
          if information.save
            puts "Comentario importado: #{information.information}"
          else            
            puts "No se ha importado el comentario: (#{information.information}) #{get_full_messages_error(information)}"
          end
        end
      end
    end
  end

  class StudiesImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          study = Study.new(profile: profile, official_degree: row[:titulacion_oficial], center: row[:centro], 
            start_year: row[:ano_inicio], end_year: row[:ano_fin], education_level: row[:nivel_educativo])
        
          if study.save
            puts "Formación académica importada: #{study.official_degree}"
          else
            puts "No se ha importado la formación académica: (#{study.official_degree}) #{get_full_messages_error(study)}"
          end
        end
      end
    end
  end

  class CoursesImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          course = Course.new(profile: profile, title: row[:titulo], center: row[:centro], 
            start_year: row[:ano_inicio], end_year: row[:ano_fin])
        
          if course.save
            puts "Otro estudio importado: #{course.title}"
          else
            puts "No se ha importado otro estudio: (#{course.title}) #{get_full_messages_error(course)}"
          end
        end
      end
    end
  end

  class LanguagesImporter < BaseImporter
    def import!
      each_row do |row|
        profile = get_profile(row)
        unless profile.blank?
          language = Language.new(profile: profile, name: row[:nombre], level: row[:nivel])
        
          if language.save
            puts "Idioma importado: #{language.name}"
          else
            puts "No se ha importado el idioma: (#{language.name}) #{get_full_messages_error(language)}"
          end
        end
      end
    end
  end
end
