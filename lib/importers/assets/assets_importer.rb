require 'importers/base_importer'

module Importers
  class AssetsDeclarationsImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin

            
            assets_declaration = AssetsDeclaration.find_or_initialize_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
            assets_declaration.declaration_date = parse_at_dec(row[:fecha_declaracion])

            assets_declaration.vehicles.each {|pa| pa.destroy} if assets_declaration.vehicles.count > 0
            assets_declaration.real_estate_properties.each {|pa| pa.destroy} if assets_declaration.real_estate_properties.count > 0
            assets_declaration.account_deposits.each {|pa| pa.destroy} if assets_declaration.account_deposits.count > 0
            assets_declaration.other_deposits.each {|pa| pa.destroy} if assets_declaration.other_deposits.count > 0
            assets_declaration.debts.each {|pa| pa.destroy} if assets_declaration.debts.count > 0
            assets_declaration.tax_data.each {|pa| pa.destroy} if assets_declaration.tax_data.count > 0
            assets_declaration.other_personal_properties.each {|pa| pa.destroy} if assets_declaration.other_personal_properties.count > 0

            if assets_declaration.save
              puts "Declaración de bienes importada: (#{row[:codigo_personal]}) #{temporality.name}/#{assets_declaration.year_period}"
            else
              puts "No se ha podido importar la declaración de bienes: (#{row[:codigo_personal]}) #{get_full_messages_error(assets_declaration)}"
            end
          rescue => err
            puts "ERROR: No se ha podido importar la declaración de bienes: (#{row[:codigo_personal]}) #{err.message}"
          end
        end
      end
    end
  end

  class VehiclesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            vehicle = Vehicle.new(assets_declaration: assets_declaration, kind: row[:clase], model: row[:modelo] )
            vehicle.purchase_date = row[:fecha]
            # vehicle.mounth = get_split_mounth_date(row[:fecha])
            # vehicle.year = get_split_year_date(row[:fecha])
          
            if vehicle.save
              puts "Vehículo importado: (#{row[:codigo_personal]}) #{vehicle.kind}/#{vehicle.model}"
            else
              puts "No se ha importado el vehículo: (#{row[:codigo_personal]}) #{get_full_messages_error(vehicle)}"
            end
          rescue => err
            puts "ERROR: No se ha importado el vehículo: (#{row[:codigo_personal]}) #{row[:clase]}/#{row[:modelo]} #{err.message}"
          end
        end
      end
    end
  end

  class RealEstatePropertiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            real_estate_property = RealEstateProperty.new(assets_declaration: assets_declaration, kind: row[:clase], straight_type: row[:tipo] )
            real_estate_property.adquisition_title = row[:descripcion]
            real_estate_property.municipality = row[:municipio]
            real_estate_property.participation_percentage = row[:participacion]
            real_estate_property.cadastral_value = get_value_amount(row[:valor_catastral])
            real_estate_property.observations = row[:notas]
            real_estate_property.purchase_date = row[:fecha]
            # real_estate_property.mounth = get_split_mounth_date(row[:fecha])
            # real_estate_property.year = get_split_year_date(row[:fecha])
          
            if real_estate_property.save
              puts "Patrimonio inmobiliario importado: (#{row[:codigo_personal]}) #{real_estate_property.kind}/#{real_estate_property.straight_type}"
            else
              puts "No se ha importado el patrimonio inmobiliario: (#{row[:codigo_personal]}) #{get_full_messages_error(real_estate_property)}"
            end
          rescue => err
            puts "ERROR: No se ha importado el patrimonio inmobiliario: (#{row[:codigo_personal]}) #{row[:clase]}/#{row[:tipo]} #{err.message}"
          end
        end
      end
    end
  end

  class OtherDepositsImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            other_deposit = OtherDeposit.new(assets_declaration: assets_declaration, types: row[:clase] )
            other_deposit.description = row[:descripcion]
            other_deposit.value = row[:valor]
            other_deposit.purchase_date = row[:fecha]
            # other_deposit.mounth = get_split_mounth_date(row[:fecha])
            # other_deposit.year = get_split_year_date(row[:fecha])
          
            if other_deposit.save
              puts "Otros depositos importado: (#{row[:codigo_personal]}) #{other_deposit.types}"
            else
              puts "No se ha importado otros depositos: (#{row[:codigo_personal]}) #{get_full_messages_error(other_deposit)}"
            end
          rescue => err
            puts "ERROR: No se ha importado otros depositos: (#{row[:codigo_personal]}) #{row[:clase]} #{err.message}"
          end
        end
      end
    end
  end

  class AccountDepositsImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            account_deposit = AccountDeposit.new(assets_declaration: assets_declaration, kind: row[:clase], entity: row[:entidad_bancaria] )
            account_deposit.balance = get_value_amount(row[:balance])
          
            if account_deposit.save
              puts "Deposito de cuenta importado: (#{row[:codigo_personal]}) #{account_deposit.kind}/#{account_deposit.entity}"
            else
              puts "No se ha importado deposito de cuenta: (#{row[:codigo_personal]}) #{get_full_messages_error(account_deposit)}"
            end
          rescue => err
            puts "ERROR: No se ha importado deposito de cuenta: (#{row[:codigo_personal]}) #{row[:clase]}/#{row[:entidad_bancaria]} #{err.message}"
          end
        end
      end
    end
  end

  class DebtsImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            debt = Debt.new(assets_declaration: assets_declaration, kind: row[:clase])
            debt.import = get_value_amount(row[:valor])
            debt.observations = row[:comentarios]
          
            if debt.save
              puts "Deuda importada: (#{row[:codigo_personal]}) #{debt.kind}"
            else
              puts "No se ha importado deuda: (#{row[:codigo_personal]}) #{get_full_messages_error(debt)}"
            end
          rescue => err
            puts "ERROR: No se ha importado deuda: (#{row[:codigo_personal]}) #{row[:clase]} #{err.message}"
          end
        end
      end
    end
  end

  
  class TaxDataImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            tax_datum = TaxDatum.new(assets_declaration: assets_declaration, tax: row[:impuesto], fiscal_data: row[:datos_fiscales])
            tax_datum.import = get_value_amount(row[:valor])
            tax_datum.observation = row[:comentarios]
          
            if tax_datum.save
              puts "Impuesto importado: (#{row[:codigo_personal]}) #{row[:impuesto]}/#{row[:datos_fiscales]}"
            else
              puts "No se ha importado impuesto: (#{row[:codigo_personal]}) #{get_full_messages_error(tax_datum)}"
            end
          rescue => err
            puts "ERROR: No se ha importado impuesto: (#{row[:codigo_personal]}) #{row[:impuesto]}/#{row[:datos_fiscales]} #{err.message}"
          end
        end
      end
    end
  end

  class OtherPersonalPropertiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            assets_declaration = AssetsDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            other_personal_property = OtherPersonalProperty.new(assets_declaration: assets_declaration, types: row[:clase])
            other_personal_property.purchase_date = row[:fecha]
            # other_personal_property.mounth = get_split_mounth_date(row[:fecha])
            # other_personal_property.year = get_split_year_date(row[:fecha])
          
            if other_personal_property.save
              puts "Otra propiedad personal importada: (#{row[:codigo_personal]}) #{other_personal_property.types}"
            else
              puts "No se ha importado otra propiedad personal: (#{row[:codigo_personal]}) #{get_full_messages_error(other_personal_property)}"
            end
          rescue => err
            puts "ERROR: No se ha importado otra propiedad personal: (#{row[:codigo_personal]}) #{other_personal_property.types} #{err.message}"
          end
        end
      end
    end
  end
end