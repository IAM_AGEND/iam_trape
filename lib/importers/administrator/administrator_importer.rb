require 'importers/base_importer'

module Importers
  class AuditsImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])
          administrador = Administrator.find_by(email: row[:administrador])

          audit = Audit.new(person: person, administrator: administrador,action: row[:acciones],description: row[:descripcion])
                
          if audit.save
            puts "Auditoría importada importado: #{person.backwards_name}/#{row[:administrador]}"
          else            
            puts "No se ha importado la auditoría: (#{person.backwards_name}/#{row[:administrador]}) #{get_full_messages_error(audit)}"
          end
        rescue => err
          puts "ERROR: No se ha importado la auditoría (#{row[:codigo_personal]}/#{row[:administrador]}) #{err.message}"
        end
      end
    end
  end

  class DeleteLogsPostsImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          administrador = Administrator.find_by(email: row[:administrador])
          delete_data = DeleteData.new
          created_at= parse_at(row[:fecha])
          created_at=Time.zone.now if created_at.blank?
          delete_data.assign_attributes(created_at: created_at,log: "#{row[:acciones]}<br><br>#{row[:log]}", successful: row[:realizado], administrator: administrador)
          delete_data.created_at =  delete_data.created_at.change(:hour => created_at.hour, :min => created_at.min) 
          
          if delete_data.save
            puts "Log de borrado de datos importado: #{row[:administrador]}"
          else            
            puts "No se ha importado el Log de borrado de datos: (#{row[:administrador]}) #{get_full_messages_error(political_posts)}"
          end
        rescue => err
          puts "ERROR: No se ha importado el Log de borrado de datos: (#{row[:administrador]}) #{err.message}"
        end
      end
    end
  end

  class FileUploadsImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          administrador = Administrator.find_by(email: row[:administrador])
          created_at= parse_at(row[:fecha])
          created_at=Time.zone.now if created_at.blank?
          
          if row[:tipo].to_s=="ProfileUpload"
            upload=ProfileUpload.new
            log = I18n.t('admin.profile_uploads.logs', 
              log: row[:log], 
              created_at: created_at, 
              period: row[:periodo],
              author_email: administrador.try(:email), 
              original_filename: row[:nombre_archivo],
              file_format: row[:formato_archivo],
              translated_successful: I18n.t("shared.#{row[:realizado].to_s=='t'}"))
          elsif row[:tipo].to_s=="AssetsUpload"
            upload=AssetsUpload.new
            log = I18n.t('admin.assets_uploads.logs', 
              log:  row[:log], 
              created_at: created_at, 
              period: row[:periodo],
              author_email: administrador.try(:email), 
              original_filename: row[:nombre_archivo],
              file_format: row[:formato_archivo],
              translated_successful: I18n.t("shared.#{row[:realizado].to_s=='t'}"))
          else
            upload=ActivitiesUpload.new
            log = I18n.t('admin.activities_uploads.logs', 
              log:  row[:log], 
              created_at: created_at, 
              period: row[:periodo],
              author_email: administrador.try(:email), 
              original_filename: row[:nombre_archivo],
              file_format: row[:formato_archivo],
              translated_successful: I18n.t("shared.#{row[:realizado].to_s=='t'}"))
          end

          upload.assign_attributes(
              log: log,
              successful: row[:realizado].to_s=='t',
              administrator: administrador,
              created_at: created_at
            )
            upload.created_at =  upload.created_at.change(:hour => created_at.hour, :min => created_at.min) 
            
          if upload.save
            puts "Fichero de importación se ha importado: #{row[:administrador]}/#{row[:tipo]}"
          else            
            puts "No se ha importado los datos del log del fichero: (#{row[:administrador]}/#{row[:tipo]}) #{get_full_messages_error(private_job)}"
          end
        rescue => err
          puts "ERROR: No se ha importado los datos del log del fichero: (#{row[:administrador]}/#{row[:tipo]}) #{err.message}"
        end
      end
    end
  end
end
