require 'importers/base_importer'

module Importers
  class EmailsImporter < BaseImporter
    def import!
      each_row do |row|
        person = Person.find_by(personal_code: row[:numper])
        if person.blank?
            puts "No existe la persona con código personal #{row[:numper]}"
        elsif person.profile.blank?
            puts "No existe el perfil con la persona con código personal #{row[:numper]}"
        else
            person.profile.email = row[:email].blank? ? person.profile.email : row[:email]

            begin
                
                    
                if person.profile.update_attributes(email: row[:email].blank? ? person.profile.email : row[:email])
                    puts "Perfil modificado: #{person.backwards_name} (#{person.profile.email}) - #{row[:email]}"
                else
                    puts "No se ha importado el perfil: (#{person.backwards_name}/#{person.profile.email} - #{row[:email]}) #{get_full_messages_error(person)}"
                end
                
            rescue
                puts "ERROR: No se ha importado el perfil: #{row[:numper]}"
            end
        end
      end
    end
  end
end