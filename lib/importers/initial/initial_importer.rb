require 'importers/base_importer'

module Importers
  class TemporalitiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_or_initialize_by(name: row[:nombre])
        temporality.description = row[:descripcion]
       
        if temporality.save
          puts "Temporalidad importada: #{temporality.name}"
        else
          puts "No se ha importado la temporalidad: (#{temporality.name}) (#{get_full_messages_error(temporality)})"
        end
      end
    end
  end

  class ElectoralListsImporter < BaseImporter
    def import!
      each_row do |row|
        electoral_list = ElectoralList.find_or_initialize_by(name: row[:nombre])
        electoral_list.long_name = row[:nombre_logo]
        begin
          electoral_list.logo = File.new(Rails.root.join("app/assets", 'images', row[:logo]).to_s, 'r')
        rescue
          electoral_list.logo=nil
        end
       
        if electoral_list.save
          puts "Lista electoral importada: #{electoral_list.name}"
        else
          puts "No se ha importado la lista electoral: (#{electoral_list.name}) (#{get_full_messages_error(electoral_list)})"
        end
      end
    end
  end

  class DocumentTypesImporter < BaseImporter
    def import!
      each_row do |row|
        document_type = DocumentType.find_or_initialize_by(name: row[:nombre])
       
        if document_type.save
          puts "Tipo de documento importado: #{document_type.name}"
        else
          puts "No se ha importado el tipo de documento: (#{document_type.name}) (#{get_full_messages_error(document_type)})"
        end
      end
    end
  end

  class CorporationsImporter < BaseImporter
    def import!
      each_row do |row|
        corporation = Corporation.find_or_initialize_by(name: row[:nombre])
        corporation.election_date = parse_at(row[:fecha_elecciones])
        corporation.start_year = row[:ano_inicial]
        corporation.end_year = row[:ano_final]
        corporation.full_constitutional_date = parse_at(row[:fecha_pleno_constitucional])
        corporation.end_corporation_date = parse_at(row[:fecha_fin_corporacion])
        corporation.councillors_num = row[:numero_concejales]
        corporation.description = row[:descripcion]
        corporation.active = (row[:activo].to_s == "Sí" || row[:activo].to_s == "true") ? true : false

        if corporation.save
          puts "Corporación importada: #{corporation.name}"
        else
          puts "No se ha importado la corporación: (#{corporation.name}) (#{get_full_messages_error(corporation)})"
        end
      end
    end
  end

  class CorporationsElectoralListsImporter < BaseImporter
    def import!
      each_row do |row|
        corporation_electoral_list = CorporationsElectoralList.find_or_initialize_by(corporation: Corporation.find_by(name: row[:nombre_corporacion]), electoral_list: ElectoralList.find_by(name: row[:nombre_lista]))
        corporation_electoral_list.order = row[:orden]

        if corporation_electoral_list.save
          puts "Asignación de corporación y lista electoral importada: #{row[:nombre_corporacion]} <--> #{row[:nombre_lista]}"
        else
          puts "No se ha realizado la importación de la lista en la corporación: (#{get_full_messages_error(corporation_electoral_list)})"
        end
      end
    end
  end

  class LanguageNamesImporter < BaseImporter
    def import!
      each_row do |row|
        language_name = LanguageName.find_or_initialize_by(name: row[:nombre])
       
        if language_name.save
          puts "Nombre del idioma importado: #{language_name.name}"
        else
          puts "No se ha importado el lenguaje: (#{language_name.name}) (#{get_full_messages_error(language_name)})"
        end
      end
    end
  end

  class LanguageLevelsImporter < BaseImporter
    def import!
      each_row do |row|
        language_level = LanguageLevel.find_or_initialize_by(level: row[:nivel])
       
        if language_level.save
          puts "Nivel del lenguaje importado: #{language_level.level}"
        else
          puts "No se ha importado el nivel del lenguaje: (#{language_level.level}) (#{get_full_messages_error(language_level)})"
        end
      end
    end
  end

  class InformationTypesImporter < BaseImporter
    def import!
      each_row do |row|
        information_type = InformationType.find_or_initialize_by(name: row[:nombre])
       
        if information_type.save
          puts "Tipo de información importado: #{information_type.name}"
        else
          puts "No se ha importado el tipo de información: (#{information_type.name}) (#{get_full_messages_error(information_type)})"
        end
      end
    end
  end

  class PublicAdministrationsImporter < BaseImporter
    def import!
      each_row do |row|
        public_administration = PublicAdministration.find_or_initialize_by(name: row[:nombre])
        public_administration.administration_code = row[:codigo]
       
        if public_administration.save
          puts "Administración pública importada: #{public_administration.name}"
        else
          puts "No se ha importado la administración pública: (#{public_administration.name}) (#{get_full_messages_error(public_administration)})"
        end
      end

    end
  end

  class PrivateActivityTypesImporter < BaseImporter
    def import!
      each_row do |row|
        private_activity_type = PrivateActivityType.find_or_initialize_by(name: row[:nombre])
       
        if private_activity_type.save
          puts "Tipo de actividad privada importado: #{private_activity_type.name}"
        else
          puts "No se ha importado la administración pública: (#{public_administration.name}) (#{get_full_messages_error(public_administration)})"
        end
      end
    end
  end

  class TextContentsImporter < BaseImporter
    def import!
      each_row do |row|
        text_content = TextContent.find_or_initialize_by(section: row[:seccion])
        text_content.content = row[:contenido]
       
        if text_content.save
          puts "Contenido sección importado: #{text_content.section}"
        else
          puts "No se ha importado el contenido de sección: (#{text_content.section}) (#{get_full_messages_error(text_content)})"
        end
      end
    end
  end
end
