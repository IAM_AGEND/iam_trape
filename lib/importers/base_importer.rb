require 'csv_converters'

module Importers
  class BaseImporter

    OPTIONS = {
      headers: true,
      header_converters: [:transliterate, :symbol],
      converters: [:all, :blank_to_nil]
    }

    def initialize(path_to_file)
      @path_to_file = path_to_file
    end

    def each_row(options = {}, &block)
      CSV.foreach(@path_to_file, OPTIONS.merge(options)) do |row|
        block.call(row)
      end
    end

    def import!
      each_row{ |row| puts row.inspect }
    end

    # def parse_declaration_date(str)
    #   day, month, year = str.split(/[-\/]/)
    #   day, month, year = day.to_i, month.to_i, year.to_i
    #   year += 2000 if year < 1000
    #   begin
    #     Date.new(year, month, day)
    #   rescue ArgumentError # dates with day and month flipped
    #     Date.new(year, day, month)
    #   end
    # end

    # def parse_spanish_date(str)
    #   return nil if str.blank?
    #   day, month, year = str.to_s.split('/')
    #   return nil unless day.present? && month.present? && year.present?

    #   day, month, year = day.to_i, month.to_i, year.to_i
    #   year += 2000 if year < 1000
    #   begin
    #     Date.new(year, month, day)
    #   rescue ArgumentError # dates with day and month flipped
    #     Date.new(year, day, month)
    #   end
    # end

    def transliterate(str)
      ActiveSupport::Inflector.transliterate(str)
    end

    def parse_text(str)
      return '' if str.blank?
      str.mb_chars.capitalize
    end

    def get_value_amount(amount)
      amount.to_s.gsub(/[ €.]/,'').gsub(/[,]/,'.')
    end

    def parse_at(date_at)
      return date_at if date_at.respond_to?(:year)
      begin
        date_at = Time.zone.parse(date_at).change(:offset => Time.zone.now.strftime("%:z").to_s) 
      rescue
        nil
      end
    end

    def parse_at_dec(date_at)
      return date_at if date_at.respond_to?(:year)
      begin
        date_at = Time.parse(date_at) 
      rescue
        nil
      end
    end

    # def get_collection_mounths
    #   mounths = {}
    #   (1..12).each {|m| mounths.merge!({I18n.l(DateTime.parse(Date::MONTHNAMES[m]), format: "%B").capitalize => m})}
    #   mounths
    # end

    # def get_split_mounth_date(date)
      
    #   if date.try{|f| f.split('/').count > 2 }
    #     aux_date = parse_at(date)
    #     return aux_date.month.to_i if !aux_date.blank?
    #   end

    #   aux = date.try {|f| get_collection_mounths[f.split('/')[0]]}
    #   aux.blank? ? "" : aux
    # end

    # def get_split_year_date(date)
    #   if date.try{|f| f.split('/').count > 2 }
    #     aux_date = parse_at(date)
    #     return aux_date.year if !aux_date.blank?
    #   end


    #   aux = date.try {|f| f.split('/')[1]}
    #   aux.blank? ? date : aux
    # end

    def get_job_level(row)
      if row[:tipo_persona].to_s != "councillor"
        job_level = JobLevel.joins(:person).find_by("people.personal_code =? AND job_levels.person_type = ?", row[:codigo_personal], row[:tipo_persona])
      else
        job_level = CouncillorsCorporation.joins(:corporation, :job_level => [:person]).find_by("corporations.name =? AND people.personal_code =? AND job_levels.person_type = ?",row[:corporacion], row[:codigo_personal], row[:tipo_persona])
      end

      job_level
    end

    def get_profile(row)
      person = Person.find_by(personal_code: row[:codigo_personal])
      person.try(:profile)
    end

    def get_full_messages_error(record=nil)
      return "" if record.blank?
      record.errors.full_messages
    end
  end
end
