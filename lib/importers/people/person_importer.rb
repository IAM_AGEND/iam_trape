require 'importers/base_importer'

module Importers
  class PersonImporter < BaseImporter
    def import!
      each_row do |row|
        person = Person.find_by(personal_code: row[:codigo_personal])
        person = Person.find_or_initialize_by(name: row[:nombre], last_name: row[:apellidos]) if person.blank?

        person.name = row[:nombre]
        person.last_name = row[:apellidos]
        person.personal_code = row[:codigo_personal]

        if person.save
          puts "Persona importada: #{person.backwards_name}"
        else
          puts "No se ha importado la persona: (#{person.backwards_name}) #{get_full_messages_error(person)}"
        end
      end
    end
  end

  class CouncillorsImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])

          job_level = JobLevel.find_or_initialize_by(person: person, person_type: "councillor")

          profiled_at= parse_at(row[:fecha_actualizacion])
          job_level.updated_at = profiled_at
          unless profiled_at.blank?
            job_level.updated_at = job_level.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) 
          else
            job_level.updated_at = Time.zone.now
          end


          councillors_corporation = CouncillorsCorporation.find_or_initialize_by(job_level: job_level,
            corporation: Corporation.find_by(name: row[:corporacion]), electoral_list: ElectoralList.find_by(name: row[:lista_electoral]))
          councillors_corporation.order_num = row[:orden]

          appointment = councillors_corporation.appointment.blank? ? Appointment.new : councillors_corporation.appointment
        
          appointment.area= row[:area]
          appointment.position= row[:puesto]
          appointment.position_alt= row[:puesto_alternativo]
          appointment.unit= row[:unidad]
          appointment.start_date= parse_at_dec(row[:fecha_inicio_puesto])
          appointment.end_date= parse_at_dec(row[:fecha_cese_puesto])

          councillors_corporation.appointment = appointment
        
          if appointment.save
            puts "Puesto importado: #{appointment.position}"
          else
            puts "No se ha importado el puesto: (#{appointment.position}) #{get_full_messages_error(appointment)}"
          end
          if job_level.save
            puts "Concejal importado: #{job_level.backwards_name}"
          else
            puts "No se ha importado el concejal: (#{job_level.backwards_name}) #{get_full_messages_error(job_level)}"
          end
          if councillors_corporation.save
            puts "Asociación a corporación importada: #{councillors_corporation.corporation.name}"
          else
            puts "No se ha importado la asociación a corporación: (#{councillors_corporation.corporation.name}) #{get_full_messages_error(councillors_corporation)}"
          end
        rescue => err
          puts "ERROR: No se ha podido importar el concejal: #{err.message}"
        end
      end
    end
  end

  class DirectorsImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])

          job_level = JobLevel.find_or_initialize_by(person: person, person_type: "director")
          profiled_at= parse_at(row[:fecha_actualizacion])
          job_level.updated_at = profiled_at
          unless profiled_at.blank?
            job_level.updated_at = job_level.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) 
          else
            job_level.updated_at = Time.zone.now
          end

          appointment = job_level.appointment.blank? ? Appointment.new : job_level.appointment
        
          appointment.area= row[:area]
          appointment.position= row[:puesto]
          appointment.position_alt= row[:puesto_alternativo]
          appointment.unit= row[:unidad]
          appointment.start_date= parse_at_dec(row[:fecha_inicio_puesto])
          appointment.end_date= parse_at_dec(row[:fecha_cese_puesto])

          job_level.appointment = appointment
        
          if appointment.save
            puts "Puesto importado: #{appointment.position}"
          else
            puts "No se ha importado el puesto: (#{appointment.position}) #{get_full_messages_error(appointment)}"
          end
          if job_level.save
            puts "Directivo importado: #{job_level.backwards_name}"
          else
            puts "No se ha importado el directivo: (#{job_level.backwards_name}) #{get_full_messages_error(job_level)}"
          end
        rescue => err
          puts "ERROR: No se ha podido importar el directivo: #{err.message}"
        end
      end
    end
  end

  class TemporaryWorkersImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])

          job_level = JobLevel.find_or_initialize_by(person: person, person_type: "temporary_worker")
          profiled_at= parse_at(row[:fecha_actualizacion])
          job_level.updated_at = profiled_at
          unless profiled_at.blank?
            job_level.updated_at = job_level.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) 
          else
            job_level.updated_at = Time.zone.now
          end

          appointment = job_level.appointment.blank? ? Appointment.new : job_level.appointment
        
          appointment.area= row[:area]
          appointment.position= row[:puesto]
          appointment.position_alt= row[:puesto_alternativo]
          appointment.unit= row[:unidad]
          appointment.start_date= parse_at_dec(row[:fecha_inicio_puesto])
          appointment.end_date= parse_at_dec(row[:fecha_cese_puesto])

          job_level.appointment = appointment
        
          if appointment.save
            puts "Puesto importado: #{appointment.position}"
          else
            puts "No se ha importado el puesto: (#{appointment.position}) #{get_full_messages_error(appointment)}"
          end
          if job_level.save
            puts "Personal eventual importado: #{job_level.backwards_name}"
          else
            puts "No se ha importado el personal eventual: (#{job_level.backwards_name}) #{get_full_messages_error(job_level)}"
          end
        rescue => err
          puts "ERROR: No se ha podido importar el personal eventual: #{err.message}"
        end
      end
    end
  end

  class PublicWorkersImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])

          job_level = JobLevel.find_or_initialize_by(person: person, person_type: "public_worker")
          profiled_at= parse_at(row[:fecha_actualizacion])
          job_level.updated_at = profiled_at
          unless profiled_at.blank?
            job_level.updated_at = job_level.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) 
          else
            job_level.updated_at = Time.zone.now
          end

          appointment = job_level.appointment.blank? ? Appointment.new : job_level.appointment
        
          appointment.area= row[:area]
          appointment.position= row[:puesto]
          appointment.position_alt= row[:puesto_alternativo]
          appointment.unit= row[:unidad]
          appointment.start_date= parse_at_dec(row[:fecha_inicio_puesto])
          appointment.end_date= parse_at_dec(row[:fecha_cese_puesto])

          job_level.appointment = appointment
        
          if appointment.save
            puts "Puesto importado: #{appointment.position}"
          else
            puts "No se ha importado el puesto: (#{appointment.position}) #{get_full_messages_error(appointment)}"
          end
          if job_level.save
            puts "Personal funcionario importado: #{job_level.backwards_name}"
          else
            puts "No se ha importado el funcionario: (#{job_level.backwards_name}) #{get_full_messages_error(job_level)}"
          end
        rescue => err
          puts "ERROR: No se ha podido importar el personal funcionario: #{err.message}"
        end
      end
    end
  end

  class SpokepeopleImporter < BaseImporter
    def import!
      each_row do |row|
        begin
          person = Person.find_by(personal_code: row[:codigo_personal])

          job_level = JobLevel.find_or_initialize_by(person: person, person_type: "spokesperson")
          profiled_at= parse_at(row[:fecha_actualizacion])
          job_level.updated_at = profiled_at
          unless profiled_at.blank?
            job_level.updated_at = job_level.updated_at.change(:hour => profiled_at.hour, :min => profiled_at.min) 
          else
            job_level.updated_at = Time.zone.now
          end
          
          appointment = job_level.appointment.blank? ? Appointment.new : job_level.appointment
        
          appointment.area= row[:area]
          appointment.position= row[:puesto]
          appointment.position_alt= row[:puesto_alternativo]
          appointment.unit= row[:unidad]
          appointment.start_date= parse_at_dec(row[:fecha_inicio_puesto])
          appointment.end_date= parse_at_dec(row[:fecha_cese_puesto])

          job_level.appointment = appointment
        
          if appointment.save
            puts "Puesto importado: #{appointment.position}"
          else
            puts "No se ha importado el puesto: (#{appointment.position}) #{get_full_messages_error(appointment)}"
          end
          if job_level.save
            puts "Vocal vecino importado: #{job_level.backwards_name}"
          else
            puts "No se ha importado el vocal vecino: (#{job_level.backwards_name}) #{get_full_messages_error(job_level)}"
          end
        rescue => err
          puts "ERROR: No se ha podido importar el vocal vecino: #{err.message}"
        end
      end
    end
  end
end