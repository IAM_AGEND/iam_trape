require 'importers/base_importer'

module Importers
  class ActivitiesDeclarationsImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            activities_declaration = ActivitiesDeclaration.find_or_initialize_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            activities_declaration.declaration_date = parse_at_dec(row[:fecha_declaracion])

            activities_declaration.public_activities.each {|pa| pa.destroy} if activities_declaration.public_activities.count > 0
            activities_declaration.private_activities.each {|pa| pa.destroy} if activities_declaration.private_activities.count > 0
            activities_declaration.other_activities.each {|pa| pa.destroy} if activities_declaration.other_activities.count > 0

            if activities_declaration.save
              puts "Declaración de actividad importada: (#{row[:codigo_personal]}) #{temporality.name}/#{activities_declaration.year_period}"
            else
              puts "No se ha podido importar la declaración de actividad: (#{row[:codigo_personal]}) #{get_full_messages_error(activities_declaration)}"
            end
          rescue => err
            puts "ERROR: No se ha podido importar la declaración de actividad: (#{row[:codigo_personal]}) #{err.message}"
          end
        end
      end
    end
  end

  class PublicActivitiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            activities_declaration = ActivitiesDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)
          
            public_activity = PublicActivity.new(activities_declaration: activities_declaration, entity: row[:entidad], position: row[:cargo] )
            public_activity.start_date= row[:fecha_de_inicio]
            public_activity.end_date = row[:fecha_de_fin]
          
            if public_activity.save
              puts "Actividad pública importada: (#{row[:codigo_personal]}) #{public_activity.entity}/#{public_activity.position}"
            else
              puts "No se ha podido importar la actividad pública: (#{row[:codigo_personal]}) #{get_full_messages_error(public_activity)}"
            end
          rescue => err
            puts "ERROR: No se ha podido importar la actividad pública: (#{row[:codigo_personal]}) #{err.message}"
          end
        end
      end
    end
  end

  class PrivateActivitiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            activities_declaration = ActivitiesDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)

            private_activity = PrivateActivity.new(activities_declaration: activities_declaration, entity: row[:entity],position: row[:cargo] )
            private_activity.private_activity_type = row[:clase]
            private_activity.description = row[:descripcion]
            private_activity.start_date= row[:fecha_de_inicio]
            private_activity.end_date = row[:fecha_de_fin]
          
            if private_activity.save
              puts "Actividad privada importada: (#{row[:codigo_personal]}) #{private_activity.entity}/#{private_activity.position}"
            else
              puts "No se ha podido importar la actividad privada: (#{row[:codigo_personal]}) #{get_full_messages_error(private_activity)}"
            end
          rescue => err
            puts "ERROR: No se ha podido importar la actividad privada: (#{row[:codigo_personal]}) #{err.message}"
          end
        end
      end
    end
  end

  class OtherActivitiesImporter < BaseImporter
    def import!
      each_row do |row|
        temporality = Temporality.find_by(name: row[:temporalidad])
        job_level = get_job_level(row)
        unless job_level.blank?
          begin
            activities_declaration = ActivitiesDeclaration.find_by(temporality: temporality, year_period: row[:ano_periodo], person_level: job_level)

            other_activity = OtherActivity.new(activities_declaration: activities_declaration, description: row[:descripcion])
            other_activity.start_date= row[:fecha_de_inicio]
            other_activity.end_date = row[:fecha_de_fin]
          
            if other_activity.save
              puts "Otra actividad importada: (#{row[:codigo_personal]}) #{other_activity.description}"
            else
              puts "No se ha podido importar otra actividad: (#{row[:codigo_personal]}) #{get_full_messages_error(other_activity)}"
            end
          rescue => err
            puts "ERROR: No se ha podido importar otra actividad: (#{row[:codigo_personal]}) #{err.message}"
          end
        end
      end
    end
  end
end