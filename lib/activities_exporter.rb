class ActivitiesExporter
  @corporation = nil

  #person_identificator
  FIELDS_PERSONAL_DATA = %w{
    type
    period_name
    declaration_date_format
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
  }

  #person_identificator
  FIELDS_PUBLIC_ACTIVITIES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    entity
    position
    start_date
    end_date
  }

  #person_identificator
  FIELDS_PRIVATE_ACTIVITIES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    private_activity_type
    description
    entity
    position
    start_date
    end_date
  }

  #person_identificator
  FIELDS_OTHER_ACTIVITIES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    description
    start_date
    end_date
  }

  FIELDS_DIRECTIVE = %w{
    person_corporation
  }

  FIELDS= {
    1 => FIELDS_PERSONAL_DATA,
    2 => FIELDS_PUBLIC_ACTIVITIES,
    3 => FIELDS_PRIVATE_ACTIVITIES,
    4 => FIELDS_OTHER_ACTIVITIES
  }

  def headers(fields,sheet)
    if @corporation.blank?
      aux = fields - FIELDS_DIRECTIVE
    else
      aux = fields
    end

    aux.map { |f| I18n.t("activities_exporter.#{sheet}.#{f}")  }
  end

  def activity_to_row(activity,fields)
    if @corporation.blank?
      aux = fields - FIELDS_DIRECTIVE
    else
      aux = fields
    end


    aux.map {|f| f.to_s == "period_name" ? "#{"Modificación - " if activity.try(:editable)}#{activity.send(f)}" : activity.send(f) }
  end

  def set_data(sheet,i,activity)
    case i
    when 1
      sheet.row(@index).concat activity_to_row(activity,FIELDS[1])
      @index += 1
    when 2
      activity.try(:public_activities).find_each do |puba|
        sheet.row(@index).concat activity_to_row(puba,FIELDS[2])
        @index += 1
      end
    when 3
      activity.try(:private_activities).find_each do |priva|
        sheet.row(@index).concat activity_to_row(priva,FIELDS[3])
        @index += 1
      end
    when 4
      activity.try(:other_activities).find_each do |otha|
        sheet.row(@index).concat activity_to_row(otha,FIELDS[4])
        @index += 1
      end
    else
    end
  end

  def save_xls(path, period, year=0, corporation = nil)
    book = Spreadsheet::Workbook.new
    (1..4).each do |i|
      @corporation = corporation
      sheet=book.create_worksheet :name => I18n.t("activities_exporter.sheet#{i}")
      sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
      sheet.row(0).concat headers(FIELDS[i],"sheet#{i}_attr")
      @index = 1



      if !corporation.blank?
        join = "INNER JOIN councillors_corporations ON activities_declarations.person_level_id = councillors_corporations.id AND activities_declarations.person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id =  #{corporation.id} INNER JOIN job_levels ON job_levels.id = councillors_corporations.job_level_id"
      else
        join = "INNER JOIN job_levels ON activities_declarations.person_level_id = job_levels.id AND activities_declarations.person_level_type = 'JobLevel'"
      end

      ActivitiesDeclaration.joins(:temporality).joins(join).joins("INNER JOIN people ON people.id = job_levels.person_id")
        .where("temporalities.name = ? AND (activities_declarations.year_period= ? OR activities_declarations.year_period is null) ", period, year).order("people.personal_code asc")
        .each do |activity|
        if activity.should_display_declarations?
          set_data(sheet,i,activity)
        end
      end
    end
    book.write(path)
  end
end
