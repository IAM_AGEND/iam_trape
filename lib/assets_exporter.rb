class AssetsExporter
  @corporation = nil

  #person_identificator
  FIELDS_PERSONAL_DATA = %w{
    type
    period_name
    declaration_date_format
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
  }

  #person_identificator
  FIELDS_REAL_ESTATE_PROPIERTIES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    kind
    straight_type
    adquisition_title
    municipality
    participation_percentage
    purchase_date
    get_cadastral_value
    observations
  }

  #person_identificator
  FIELDS_ACCOUNT_DEPOSITS = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    kind
    entity
    get_balance
  }

  #person_identificator
  FIELDS_OTHER_DEPOSITS = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    types
    description
    purchase_date
    get_value
  }

  #person_identificator
  FIELDS_VEHICLES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    kind
    model
    purchase_date
  }

  #person_identificator
  FIELDS_OTHER_PERSONAL_PROPERTIES = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    types
    purchase_date
  }

  #person_identificator
  FIELDS_DEBTS = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    kind
    get_import
    observations
  }

  #person_identificator
  FIELDS_TAX_DATAS = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    tax
    fiscal_data
    get_amount
    observation
  }

  #person_identificator
  TAX_DATA_IRPF_BIG = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    get_irpf
    get_irpf_big
    get_irpf_big_import
    irpf_big_observation
  }

  #person_identificator
  TAX_DATA_IRPF_BI = %w{
  type
  period_name
  declaration_date
  person_first_last_name
  person_second_last_name
  person_fist_name
  person_corporation    
  appointment_start_date
  appointment_end_date
  get_irpf
  get_irpf_bi
  get_irpf_bi_import
  irpf_bi_observation
  }
   
  #person_identificator
  TAX_DATA_IRPF_DEDUC_A = %w{
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    type
    appointment_start_date
    appointment_end_date
    get_irpf
    get_irpf_deduc_a
    get_irpf_deduc_import_a
    irpf_deduc_observation_a
  }

  #person_identificator
  TAX_DATA_IRPF_DEDUC_B = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    get_irpf
    get_irpf_deduc_b
    get_irpf_deduc_import_b
    irpf_deduc_observation_b
  }

  #person_identificator
  TAX_DATA_ISP_BI = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    get_isp
    get_isp_bi
    get_isp_bi_import
    isp_bi_observation
  }
  
  #person_identificator
  TAX_DATA_IS_BI = %w{
    type
    period_name
    declaration_date
    person_first_last_name
    person_second_last_name
    person_fist_name
    person_corporation
    appointment_start_date
    appointment_end_date
    get_is
    get_is_bi
    get_is_bi_import
    is_bi_observation
  }

  FIELDS_DIRECTIVE = %w{
    person_corporation
  }


  FIELDS= {
    1 => FIELDS_PERSONAL_DATA,
    2 => FIELDS_REAL_ESTATE_PROPIERTIES,
    3 => FIELDS_ACCOUNT_DEPOSITS,
    4 => FIELDS_OTHER_DEPOSITS,
    5 => FIELDS_VEHICLES,
    6 => FIELDS_OTHER_PERSONAL_PROPERTIES,
    7 => FIELDS_DEBTS,
    8 => FIELDS_TAX_DATAS,
    9 => TAX_DATA_IRPF_BIG,
    10 => TAX_DATA_IRPF_BI,
    11 => TAX_DATA_IRPF_DEDUC_A,
    12 => TAX_DATA_IRPF_DEDUC_B,
    13 => TAX_DATA_ISP_BI,
    14 => TAX_DATA_IS_BI
  }

  def headers(fields,sheet)
    if @corporation.blank?
      aux = fields - FIELDS_DIRECTIVE
    else
      aux = fields
    end

    aux.map { |f| I18n.t("assets_exporter.#{sheet}.#{f}") }
  end

  def asset_to_row(asset,fields)
    if @corporation.blank?
      aux = fields - FIELDS_DIRECTIVE
    else
      aux = fields
    end
    aux.map {|f| f.to_s == "period_name" ? "#{"Modificación - " if asset.try(:editable)}#{asset.send(f)}" : asset.send(f) }
  end

  def set_data(sheet,i,asset)
    case i
    when 1
      sheet.row(@index).concat asset_to_row(asset,FIELDS[1])
      @index += 1
    when 2
      asset.try(:real_estate_properties).find_each do |rep|
        sheet.row(@index).concat asset_to_row(rep,FIELDS[2])
        @index += 1
      end
    when 3
      asset.try(:account_deposits).find_each do |ad|
        sheet.row(@index).concat asset_to_row(ad,FIELDS[3])
        @index += 1
      end
    when 4
      asset.try(:other_deposits).find_each do |op|
        sheet.row(@index).concat asset_to_row(op,FIELDS[4])
        @index += 1
      end
    when 5
      asset.try(:vehicles).find_each do |v|
        sheet.row(@index).concat asset_to_row(v,FIELDS[5])
        @index += 1
      end
    when 6
      asset.try(:other_personal_properties).find_each do |opp|
        sheet.row(@index).concat asset_to_row(opp,FIELDS[6])
        @index += 1
      end
    when 7
      asset.try(:debts).find_each do |d|
        sheet.row(@index).concat asset_to_row(d,FIELDS[7])
        @index += 1
      end
    when 8
      asset.try(:tax_data).find_each do |td|
        # if !td.try(:irpf_big_observation).blank? || !td.try(:irpf_big_import).blank?
          sheet.row(@index).concat asset_to_row(td,FIELDS[8])
          @index += 1
        # end
        # if !td.try(:irpf_bi_observation).blank? || !td.try(:irpf_bi_import).blank?
        #   sheet.row(@index).concat asset_to_row(td,FIELDS[10])
        #   @index += 1
        # end
        # if !td.try(:irpf_deduc_observation_a).blank? || !td.try(:irpf_deduc_import_a).blank?
        #   sheet.row(@index).concat asset_to_row(td,FIELDS[11])
        #   @index += 1
        # end
        # if !td.try(:irpf_deduc_observation_b).blank? || !td.try(:irpf_deduc_import_b).blank?
        #   sheet.row(@index).concat asset_to_row(td,FIELDS[12])
        #   @index += 1
        # end
        # if !td.try(:isp_bi_observation).blank? || !td.try(:isp_bi_import).blank?
        #   sheet.row(@index).concat asset_to_row(td,FIELDS[13])
        #   @index += 1
        # end
        # if !td.try(:is_bi_observation).blank? || !td.try(:is_bi_import).blank?
        #   sheet.row(@index).concat asset_to_row(td,FIELDS[14])
        #   @index += 1
        # end
      end
    else
    end
  end

  def save_xls(path, period, year=0, corporation = nil)
    book = Spreadsheet::Workbook.new
    (1..8).each do |i|
      @corporation = corporation
      sheet=book.create_worksheet :name => I18n.t("assets_exporter.sheet#{i}")
      sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
      sheet.row(0).concat headers(FIELDS[i],"sheet#{i}_attr")
      @index = 1


      if !corporation.blank?
        join = "INNER JOIN councillors_corporations ON assets_declarations.person_level_id = councillors_corporations.id AND assets_declarations.person_level_type = 'CouncillorsCorporation' AND councillors_corporations.corporation_id =  #{corporation.id} INNER JOIN job_levels ON job_levels.id = councillors_corporations.job_level_id"
      else
        join = "INNER JOIN job_levels ON assets_declarations.person_level_id = job_levels.id AND assets_declarations.person_level_type = 'JobLevel'"
      end

      AssetsDeclaration.joins(:temporality).joins(join).joins("INNER JOIN people ON people.id = job_levels.person_id")
        .where("temporalities.name = ? AND (assets_declarations.year_period= ? OR assets_declarations.year_period is null) ", period, year).order("people.personal_code asc")
        .each do |asset|
          if asset.should_display_declarations?
            set_data(sheet,i,asset)
          end
      end
    end
    book.write(path)
  end
end
