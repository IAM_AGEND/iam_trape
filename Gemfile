source 'http://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'
# Use PostgreSQL
gem 'pg', '~> 0.18'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.6'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 4.3.1'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
gem 'foundation-rails', '5.5.1' # 5.5.3 has a js problem: https://github.com/zurb/foundation-sites/issues/8416
gem 'foundation_rails_helper'
gem 'trix'
gem 'ancestry'
gem 'devise', '~> 4.6.0'
gem "devise-async"#, "~> 1.0.0"
gem "devise_security_extension"#, git: "https://github.com/phatworx/devise_security_extension.git" #, "~> 0.10"
gem 'kaminari'
gem 'unicorn', '~> 5.0.1'
gem 'friendly_id', '~> 5.1.0'
gem 'paperclip', '~> 5.2.1'
gem 'rinku', require: 'rails_rinku'
gem 'roo', '~> 2.7'
gem 'roo-xls', '~> 1.1'
gem 'spreadsheet'
gem 'whenever', require: false
gem 'simple_form'
gem 'nested_form'
gem 'jquery-ui-rails', '~> 5.0.5'
gem 'foundation-datepicker-rails'
gem 'will_paginate', '~> 3.1.0'
gem 'will_paginate-foundation'
gem 'premailer-rails'
gem 'json-compare'
gem "font-awesome-rails"

# For rake db:danger:truncate
gem 'database_cleaner'
gem 'activerecord-session_store'

gem 'tinymce-rails', '~> 4.2.8'
gem 'tinymce-rails-langs', '~> 4.20140129'
gem 'activerecord-diff'
gem 'rest-client'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'rspec-rails', '~> 3.0'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'fuubar'
  gem 'launchy'
  gem 'quiet_assets'
  gem 'i18n-tasks'

  gem 'capistrano', '3.4.1',           require: false
  gem "capistrano-bundler", '1.1.4',   require: false
  gem "capistrano-rails", '1.1.6',     require: false
  gem "capistrano-rvm",                require: false
  gem "capistrano-rake",               require: false
end

group :development do
  gem 'letter_opener_web'
  gem 'web-console', '~> 3.0'
end

group :test do
  gem 'poltergeist'
  gem 'coveralls', require: false
  gem 'simplecov', require: false
end
