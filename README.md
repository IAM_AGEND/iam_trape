# Transparencia

Bienvenidos al repositorio de la nueva página de transparencia de Madrid.

## Configuración para desarrollo y tests

Prerequisitos: git, Ruby 2.2.3, la gema bundler y PostgreSQL (9.4 o superior).

```
git clone https://bitbucket.org/IAM_AGEND/iam_trape.git
cd iam_trape
bundle install
cp config/secrets.yml.example config/secrets.yml
cp config/database.yml.example config/database.yml
bin/rake db:setup
RAILS_ENV=test bin/rake db:setup
```

Para ejecutar la aplicación en local:

```
bin/rails s
```

Para ejecutar los tests:

```
bin/rspec
```

## Configuración a tener en cuenta

Hay que tener cuidado con las versiones superiores de Ruby ya que pueden originar un error.

Esto se debe a la versión de las gemas que se utilizan y que no son compatibles con versiones superiores.



