lock '3.4.1'

def deploysecret(key)
  @deploy_secrets_yml ||= YAML.load_file('config/deploy-secrets.yml')[fetch(:stage).to_s]
  @deploy_secrets_yml.fetch(key.to_s, 'undefined')
end

set :rails_env, fetch(:stage)
set :rvm_ruby_version, '2.2.3'
set :rvm_type, :user

set :application, 'transparencia'
set :full_app_name, fetch(:application)

set :server_name, deploysecret(:server_name)
#set :repo_url, 'git@github.com:AyuntamientoMadrid/transparencia.git'
# If ssh access is restricted, probably you need to use https access
set :repo_url, 'http://bitbucket.org/IAM_AGEND/iam_trape.git'

set :scm, :git
set :revision, `git rev-parse --short #{fetch(:branch)}`.strip

set :log_level, :info
set :pty, true
set :use_sudo, false

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{log tmp public/system public/assets}
set :symlinks, %w{log tmp public/system public/assets}

set :keep_releases, 5

set :local_user, ENV['USER']

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

# Config files should be copied by deploy:setup_config
set(:config_files, %w(
  log_rotation
  database.yml
  secrets.yml
  unicorn.rb
  sidekiq.yml
))

namespace :deploy do
  after :finishing, 'deploy:cleanup', 'deploy:generate_exported'
  after :published, 'mapeo_nas'
  after 'deploy:publishing', 'deploy:restart'

  task :generate_exported do
    on roles(:all) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "export:activities"
          execute :rake, "export:assets"
          execute :rake, "export:profiles"
        end
      end
    end
  end
end

task :mapeo_nas do
  on roles(:app) do
    execute "ln -ds /aytomad/app/transparencia/transparencia/current/public/system/people/export /aytomad/app/transparencia/transparencia/current/public/export"
    # Solo para instalación inicial en entornos
    # execute "ln -ds /aytomad/app/volun/volun_backend/shared/private /aytomad/app/volun/volun_backend/shared/public"
  end
end


