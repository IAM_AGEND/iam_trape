Rails.application.routes.draw do
  devise_for :administrators, {
    skip: [:registrations]
  }
  #resources :home, only: [:index]
  if Rails.application.secrets.try(:maintenance).blank? || !Rails.application.secrets.maintenance
    root 'people#councillors'
  else
    root 'static_pages#maintance'
  end
  
  resources :people, only: [:index, :show] do
    member do
      post :contact
      put :hide
      put :unhide
    end
    collection do
      get :councillors
      get :councillors_not_work
      get :councillors_other
      get :directors
      get :directors_not_work
      get :temporary_workers
      get :public_workers
      get :labours
      get :spokespeople
    end
  end

  concern :mailer_log do
    get :show_mailer, on: :member
    post :send_mailer, on: :member
  end
  
  namespace :admin do
    resources :data_exports, only: [:index] 
    resources :notifications, only: [:show] do
      get :profiles, on: :collection
      get :assets, on: :collection
      get :activities, on: :collection
      get :person, on: :collection
      get :profiles_historic, on: :collection
      get :assets_historic, on: :collection
      get :activities_historic, on: :collection
      get :person_historic, on: :collection
      get :permit, on: :member
      get :reject, on: :member
      get :publicable, on: :member
      get :update_data, on: :collection
      get :insert_reject, on: :member
      get :forward, on: :member
    end
    resources :preferences, only: [:index]
    resources :manage_emails
    resources :data_preferences do
      post :update_all, on: :collection
    end
    root to: "dashboard#index"
    resources :text_contents
    resources :corporations
    resources :electoral_lists
    resources :delete_data, only: [:show, :index] do
      post :delete_information, on: :collection
    end
    resources :profile_uploads, concerns: :mailer_log, except: [:edit, :update, :destroy]
    resources :assets_uploads, concerns: :mailer_log, except: [:edit, :update, :destroy] do
      post :export,on: :collection
    end
    resources :activities_uploads, concerns: :mailer_log, except: [:edit, :update, :destroy] do
      post :export,on: :collection
    end
    resources :people, except: :show do
      get :show_hide, on: :member
      get :show_unhide, on: :member
      get :portrait_delete, on: :member
      get :audits, on: :member
      put :hide
      put :unhide
      get :download, on: :collection
    end
    resources :statistics, only: [:index] do
      get :profiles, on: :collection
      get :councillors, on: :collection
      get :directors, on: :collection
      get :update_data, on: :collection
    end
  end

  # static pages
  #get "/welcome", to: "home#welcome"
  #get "/maintance", to: "static_pages#maintance"
  get "/accessibility", to: "static_pages#accessibility"
  get "/conditions", to: "static_pages#conditions"
  get "/privacy", to: "static_pages#privacy"

  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  get '*path' => redirect('/')
  get "*missing" => redirect('/') 
end
