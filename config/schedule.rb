# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require "./"+ File.dirname(__FILE__) + "/environment.rb"
set :output, File.join(Whenever.path, "log", "cron.log")

date = DataPreference.find_by(:code => "hour_exec_data")
value= date.try(:content_data).blank? ? 24 : date.content_data.to_i

date_export = DataPreference.find_by(:code => "hour_export_data")
value_export= date_export.try(:content_data).blank? ? 24 : date_export.content_data.to_i

every value.hours, at: '01:00', roles: [:db] do
  rake 'notifications:import'
  rake 'duties:import'
  rake 'duties:remove_duplicated'
  rake 'profiles:cumplimented'
end

every 1.days, at: '00:00', roles: [:app] do
  rake 'notifications:ignore_unless_new_data'
end

every 1.days, at: '01:00', roles: [:app] do
  rake 'export:activities'
  rake 'export:assets'
  rake 'export:profiles'
end

forwards = DataPreference.find_by(:code => "num_send_retry_diary")
value = forwards.try(:content_data).blank? ? 24 : (24/forwards.content_data.to_i)

every value.hours, at: '09:00', roles: [:db] do
  rake 'notifications:send_other'
  rake 'notifications:pending_alerts'
end
